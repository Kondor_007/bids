<?php

namespace common\assets;

use yii\web\AssetBundle;

class CallsAsset extends AssetBundle
{
    public $basePath = '@common/JS/calls';
    public $baseUrl = '@common/JS/calls';

    public $css =
        [

        ];

    public $js =
        [
            '../modalShow.js',
            'calls.js',
        ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
?>