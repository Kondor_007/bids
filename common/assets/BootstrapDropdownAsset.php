<?php 

namespace common\assets;

use yii\web\AssetBundle;

class BootstrapDropdownAsset extends AssetBundle
{
	public $basePath = '@common/JS/bootstrap/bootstrap_dropdowns_enhancement-master';
	public $baseUrl = '@common/JS/bootstrap/bootstrap_dropdowns_enhancement-master';

	public $css = [
		'dist/css/dropdowns-enhancement.min.css',
	];

	public $js = 
	[
		'dist/js/dropdowns-enhancement.js',
	];
	
}

?>