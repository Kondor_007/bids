<?php 

namespace common\assets;

use yii\web\AssetBundle;

class RequestAsset extends AssetBundle
{
	public $basePath = '@common/JS/request';
	public $baseUrl = '@common/JS/request';

	public $css = 
	[

	];

	public $js = 
	[
		'../num_correction.js',
		'../modalShow.js',
		'request_create.js',

		//'list_modal_show.js',
		'addWidget.js',
	];

	public $depends = [
			'yii\web\JqueryAsset',
    ];
}
?>