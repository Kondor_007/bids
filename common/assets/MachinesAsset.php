<?php

namespace common\assets;

use yii\web\AssetBundle;

class MachinesAsset extends AssetBundle
{
    public $basePath = '@common/JS/machines';
    public $baseUrl = '@common/JS/machines';

    public $css =
        [

        ];

    public $js =
        [
            '../modalShow.js',
            'machines.js',
        ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
?>