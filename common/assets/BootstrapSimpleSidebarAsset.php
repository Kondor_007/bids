<?php 

namespace common\assets;

use yii\web\AssetBundle;

class BootstrapSimpleSidebarAsset extends AssetBundle
{
	public $basePath = '@common/JS/bootstrap/bootstrap-simple-sidebar';
	public $baseUrl = '@common/JS/bootstrap/bootstrap-simple-sidebar';

	public $css = [
		'css/simple-sidebar.css',
	];

	public $js = [
		
	];	
}

?>
