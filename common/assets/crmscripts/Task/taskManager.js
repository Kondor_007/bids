$(document).ready( function()
{
	attachHandlers();
	function attachHandlers()
	{
		$('a[class="startTaskButton"]').unbind('click');
		$('a[class="finishTaskButton"]').unbind('click');
		$('a[class="putToInspect"]').unbind('click');
		$('a[class="pauseTaskButton"]').unbind('click');
		$('a[class="stopTaskButton"]').unbind('click');
		$('.editTaskButton').unbind( 'click' );
		$( '#view-options' ).unbind( 'change' );

		$('a[class="startTaskButton"]').bind('click', startTaskHandler );
		$('a[class="finishTaskButton"]').bind( 'click', finishTaskHandler );
		$('a[class="putToInspect"]').bind( 'click', inspectTaskHandler );
		$('a[class="pauseTaskButton"]').bind( 'click', pauseTaskHandler );
		$('a[class="stopTaskButton"]').bind( 'click', stopTaskHandler );
		$('.editTaskButton').bind( 'click', editTaskHandler );
		$( '#view-options' ).bind( 'change', taskViewController );
	}

	/*
	* Функция делает ajax запрос 
	* к контроллеру добавляющему в таблицу состояний 
	* запись состояния "Задача выполняется" для задачи, айди которой 
	* предается в аттрибуте name тэга породившего событие.
	* 
	*/
	function startTaskHandler(e)
	{
		e.preventDefault();
		var recordId = $(this).attr('name'); // аттрибут name содержит id задачи

		if( isNumber( recordId ) == false )
		{
			alert( 'Ошибка. Запрос составлен некорректно. Невозможно определить id записи.' );
			return;
		}

		$.ajax(
		{
			url: 'index.php?r=task-status/start',
			dataType: 'html',
			type: 'POST',
			data: '&id='+recordId,
			// Если запрос выполнен успешно
			success: function( responseData, error )
			{	
				$( '#task-actions-' + recordId  ).html( responseData );
				attachHandlers();
			},
			error: function( errText, textStatus ) 
			{
				console.log(errText.responseText);
				alert('Ошибка выполнения AJAX запроса.');
			}
		});
	}
	//==========================================================================


	/*
	* Функция делает ajax запрос 
	* к контроллеру добавляющему в таблицу состояний 
	* запись состояния "Ожидает контроль постановщика" для задачи, айди которой 
	* предается в аттрибуте name тэга породившего событие.
	* 
	*/
	function inspectTaskHandler(e)
	{
		e.preventDefault();
		var recordId = $(this).attr('name'); // аттрибут name содержит id задачи

		if( isNumber( recordId ) == false )
		{
			alert( 'Ошибка. Запрос составлен некорректно. Невозможно определить id записи.' );
			return;
		}

		$.ajax(
		{
			url: 'index.php?r=task-status/inspect',
			dataType: 'text',
			type: 'POST',
			data: '&id='+recordId,
			success: function( responseData, error )
			{	
				$( '#task-actions-' + recordId  ).html( responseData );
				attachHandlers();
			},
			error: function( errText, textStatus ) 
			{
				alert('Ошибка выполнения AJAX запроса.');
			}

		});
	}
	//==============================================================================

	/*
	* Функция делает ajax запрос 
	* к контроллеру добавляющему в таблицу состояний 
	* запись состояния "Завершена" для задачи, айди которой 
	* предается в аттрибуте name тэга породившего событие.
	* 
	*/
	function finishTaskHandler(e)
	{
		e.preventDefault();
		var recordId = $(this).attr('name'); // аттрибут name содержит id задачи

		if( isNumber( recordId ) == false )
		{
			alert( 'Ошибка. Запрос составлен некорректно. Невозможно определить id записи.' );
			return;
		}

		$.ajax(
		{
			url: 'index.php?r=task-status/finish',
			dataType: 'text',
			type: 'POST',
			data: '&id='+recordId,
			success: function( responseData, error )
			{	
				$( '#task-actions-' + recordId  ).html( responseData );
				attachHandlers();
			},
			error: function( errText, textStatus ) 
			{
				console.log(errText.responseText);
				alert('Ошибка выполнения AJAX запроса.');
			}

		});
	}
	//==============================================================================
	
	/*
	* Функция делает ajax запрос 
	* к контроллеру добавляющему в таблицу состояний 
	* запись состояния "Приостановлена" для задачи, айди которой 
	* предается в аттрибуте name тэга породившего событие.
	* 
	*/
	function pauseTaskHandler(e)
	{
		e.preventDefault();
		var recordId = $(this).attr('name'); // аттрибут name содержит id задачи

		if( isNumber( recordId ) == false )
		{
			alert( 'Ошибка. Запрос составлен некорректно. Невозможно определить id записи.' );
			return;
		}

		$.ajax(
		{
			url: 'index.php?r=task-status/pause',
			dataType: 'text',
			type: 'POST',
			data: '&id='+recordId,
			success: function( responseData, error )
			{	
				$( '#task-actions-' + recordId  ).html( responseData );
				attachHandlers();
			},
			error: function( errText, textStatus ) 
			{
				console.log(errText.responseText);
				alert('Ошибка выполнения AJAX запроса.');
			}
		});
	}
	//==============================================================================

	/*
	* Функция делает ajax запрос 
	* к контроллеру добавляющему в таблицу состояний 
	* запись состояния "Приостановлена" для задачи, айди которой 
	* предается в аттрибуте name тэга породившего событие.
	* 
	*/
	function stopTaskHandler(e)
	{
		e.preventDefault();
		var recordId = $(this).attr('name'); // аттрибут name содержит id задачи

		if( isNumber( recordId ) == false )
		{
			alert( 'Ошибка. Запрос составлен некорректно. Невозможно определить id записи.' );
			return;
		}

		$.ajax({
			url: 'index.php?r=task-status/stop',
			dataType: 'html',
			type: 'POST',
			data: '&id='+recordId,
			success: function( responseData, textStatus ) {		
				$( '#task-actions-' + recordId  ).html( responseData );
				attachHandlers();
			},
			error: function( errText, textStatus ) {
				console.log(errText.responseText);
				alert('Ошибка выполнения AJAX запроса.');
			}
		});
	}
	//==============================================================================
	
	
	function taskViewController( e )
	{
		var value = $( this ).val();

		$.ajax({
			url: 'index.php?r=task/view-tasks',
			dataType: 'html',
			type: 'POST',
			data: '&view-option='+value,
			success: function( responseData, textStatus )
			{
				$('#task-view-all').replaceWith( responseData );
				attachHandlers();
			},
			error: function( error )
			{
				console.log( error );
			}
		});
	}

	function editTaskHandler(e)
	{
		var key = this.name;
		$.ajax(
		{
			url: 'index.php?r=task/get-task-info',
			type: 'POST',
			data: '&id=' + key,
			success: function ( responseData, textStatus )
			{
				var response = JSON.parse( responseData );
				console.log( response );
				fillEditForm( response );
			},
			error: function ( responseData, textStatus )
			{
				alert('Ошибка AJAX запроса.');
			},
		});
	}

	
	function fillEditForm( data )
	{
		$('#task-name').attr( 'value', data['name'] );
		$('#description').attr('text', data['desciption']);
		$('#priority-'+data['priority']).prop('checked', true);
		$('#responsible').val( data['responsible'] );

		if( data['parent_id'] !== 0 )
		{
			$('#choose-parent-task').attr( 'checked', true );
			$('#row-parent-task').removeAttr('hidden');
			$('#task-list').val( data['parent_id'] );
		}

		if( data['FK_project'] !== null )
		{
			$('#bind-with-project').attr( 'checked', true );
			$('#row-project').removeAttr('hidden');
			$('#project-list').val( data['FK_project'] );
		}
	}

	function isNumber(n)
	{
  		return !isNaN( parseFloat(n) ) && isFinite( n );
	}

	
});