﻿/**
 * 
 */

/*window.onunload(alert());
$(document).onunload(alert("AAA"));*/

window.onbeforeunload = function (evt) {

	//Уходить нельзя, информация изменена и не сохранена
	if ($('#can_leave').val()==0) {
		var message = "Данные были изменены, но не сохранены. ";
		if (typeof evt == "undefined") {
			evt = window.event;
		}
		if (evt) {
			evt.returnValue = message;
		}
		//$("#add-client-modal").modal();
		return evt.closeText;//message;
	}
	else
	console.log("Всё в порядке, уходим!");
}

	//$(document).ready(function(){ListModalWorker('#request-fk_client','#add-client-modal','#submit-org');});
$(document).on('pjax:complete', reattachHandlers);

    $('#draft-grid-pjax').on('pjax:complete', function(e){
      //  alert('hi');
        $.pjax.reload('#form-process-grid-pjax', { timeout: 5000 });
    });
	reattachHandlers();

	function reattachHandlers()
	{



		$('#submit-client').unbind('click' );
		$('#submit-client').bind('click', submitClient );


		$('#submit-org').unbind('click' );
		$('#submit-org').bind('click', submitOrg );
		
		$('#submit-draft').unbind('click');
		$('#draft-file').unbind('change');
		$('#submit-process').unbind( 'click' );
		$('#submit-process').bind('click', submitProcess );

		$('#submit-draft').bind('click', submitDraft );

		$('#draft-file').bind('change', catchDraftFile );

		/*$('#area').unbind('change', MyTestUpdate);
		$('#area').bind('change', MyTestUpdate);*/

		$('#requestdraft-price').unbind('change', CostUpdate);
		//$('#requestdraft-price').bind('change', ToCNum('#requestdraft-price'));
		$('#requestdraft-price').bind('change', CostUpdate);

		$('#requestdraft-amount').unbind('change', CostUpdate);
		$('#requestdraft-amount').bind('change', CostUpdate);
		$('#requestdraft-blank_space').unbind('change');
		$('#requestdraft-blank_space').bind('change', CostUpdate);

		$('#requestdraft-handle_work_price').unbind('change');
		$('#requestdraft-handle_work_price').bind('change', CostUpdate);

		$('#requestdraft-width').unbind('blur', AreaUpdate);
		$('#requestdraft-width').bind('blur', AreaUpdate);
		$('#requestdraft-width').unbind('blur', CostUpdate);
		$('#requestdraft-width').bind('blur', CostUpdate);
		$('#requestdraft-price_blank_space').unbind('blur');
		$('#requestdraft-price_blank_space').bind('blur', CostUpdate);


		$('#requestdraft-height').unbind('blur', AreaUpdate);
		$('#requestdraft-height').bind('blur', AreaUpdate);
		$('#requestdraft-blank_space').unbind('change', AreaUpdate);
		$('#requestdraft-blank_space').bind('change', AreaUpdate);

		$('#requestdraft-fk_material').unbind('change', UnitMaterialUp);
		$('#requestdraft-fk_material').bind('change', UnitMaterialUp);

		$('#requestdraft-fk_work_type').unbind('change', UnitProcessUp);
		$('#requestdraft-fk_work_type').bind('change', UnitProcessUp);


		$('#draft-file2').unbind('change', setFile);
		$('#draft-file2').bind('change', setFile);
		
		$('#requestprocess-fk_draft').unbind('change', getProcessTypeByDraft);
		$('#requestprocess-fk_draft').bind('change',getProcessTypeByDraft);
		$('#requestprocess-fk_draft').unbind('click', getProcessTypeByDraft);
		$('#requestprocess-fk_draft').bind('click',getProcessTypeByDraft);
	//	$( "*" ).bind('change',LockLeave);


		/*TODO:
		 выбивает ошибку JQuery property toLowerCase
		 Сам вызов функции ListModalWorker отрабатывае нормально,
		 но из-за указанной ошибки валится функция
		 $('#request-fk_client').bind('change', ListModalWorker('#request-fk_client','#add-client-modal','#submit-org') );
		* */
		//$('#request-fk_client').bind('change', ListModalWorker('#request-fk_client','#add-client-modal','#submit-org') );
		$( "[id*=requestdraft]" ).bind('change',LockLeave);
		$( "[id*=requestprocess]" ).bind('change',LockLeave);

		//$( "*[class*='dencity']" ).addClass('hidden')
		$( "[id*=usext]" ).unbind('change');

		$( "[id*=usext]").bind('change',AllowLeave);
		$( "[id*=organisation]").bind('change',AllowLeave);
		//$( "[id*=usext]" ).bind('change',function(){alert();});
		//id="usext-lastname"
		$( "[id*=request]" ).bind('change',LockLeave);
		$( "[id*=btn_save_request]" ).bind('click',AllowLeave);
		$('#request-fk_client').unbind();
		$('#request-fk_client').bind('click',AllowLeave);
		$('#request-fk_client').bind('change',AllowLeave);
		MyModalWorker('#request-fk_client',
			'#submit_new_type',
			"#add-client-modal");
		//if ($('#requestprocess-fk_draft').length>1)
		   getProcessTypeByDraft();

	}

function CanLeave(){
	return $('#can_leave').val()==0;
}
function LockLeave(){
	//alert("LockLeave");
	if (CanLeave()) ;
	else
	$('#can_leave').val(0);
	console.log("Can Leave = "+$('#can_leave').val());
}
 function AllowLeave(){

		$('#can_leave').val(1);
	console.log("Can Leave = "+$('#can_leave').val());
}
  var OrgKey=0;
	
	function submitOrg(){
		var data = new FormData( document.querySelector('#org-create') );
//		console.log($(data).get('organisation-short_name'));
		$.ajax({
			url: "index.php?r=organisation/create",
			type: 'POST',
			dataType: 'json',
			data: data,
			processData: false,
			contentType: false,
			timeout: 5000,
			complete:function( response ) {
				console.log( response );
				
				  $.pjax ({
		            	//reload('#request-fk_client', { timeout: 5000 })
		            	container:'#short_name'
		            	});
	            //$.pjax.reload('#short_name');//, { timeout: 5000 });
	            //setTimeout(function, delay)
				  
				  var items = "";
				  console.log(response.responseText);//['orgId']);
				 // Как вот это {"orgId":71} разобрать в json виде
				  console.log($.parseJSON(response.responseText)['orgId']);
				  OrgKey=$.parseJSON(response.responseText)['orgId'];
				  $('#organisation-short_name').val(OrgKey);
				 // $('#short_name').append(OrgKey)
				  $('#short_name').val(OrgKey)
				  submitClient();
				 /* $.each( $.parseJSON(response), function(key, val){
				    items += key + " " + val;
				  });
				 $('#usext-patname').val(items);*/

				  //$('#usext-patname').val(response['orgId']);



			},
			/*success: function( response ) {
				console.log( response );

				  $.pjax ({
		            	//reload('#request-fk_client', { timeout: 5000 })
		            	container:'#short_name'
		            	});
	            //$.pjax.reload('#short_name');//, { timeout: 5000 });
	            //setTimeout(function, delay)

				  var items = "";

				  $.each(JSON.parse(response), function(key, val){
				    items += key + " " + val;
				  });
				 $('#usext-patname').val(items);



			},
	        error:function(response){
				//console.log( response );
				console.log("error");
				 $.pjax ({
		            	//reload('#request-fk_client', { timeout: 5000 })
		            	container:'#short_name'
		            	});
				// $.pjax.reload('#short_name', { timeout: 5000 });
				 $('#usext-patname').val(response);
				 //alert('Ошибка обновления списка!');
	        }*/

	        
		});
		return false;
	}
function submitClient(){
	//submitOrg();
	
	var data = new FormData( document.querySelector('#client-create') );
	var orgname=$('#organisation-short_name').val();
	console.log(orgname);
	//if (orgname  == "")
	$.ajax({
		url: "index.php?r=us-ext/create-phantom",
		type: 'POST',
		dataType: 'json',
		data: data,
		processData: false,
		contentType: false,
		success: function( response ) {
			console.log( response );
            $.pjax ({
            	//reload('#request-fk_client', { timeout: 5000 })
            	container:'#request-fk_client'
            	});

//			$('#can_leave').val()==0;
		},
        error:function(response){
			//console.log( response );
			console.log("error");
			// $.pjax.reload('#request-fk_client', { timeout: 5000 });
			 $.pjax ({
	            	//reload('#request-fk_client', { timeout: 5000 })
	            	container:'#request-fk_client'
	            	});
			 //alert('Ошибка обновления списка!');
        }

        
	});
	return false;
}
function getProcessTypeByDraft(){
	var code=$('#requestprocess-fk_draft').val();
	console.log('code='+code);
	if (code!=null)
	$.ajax({
		url: "index.php?r=request/get-process-type-by-draft",
		type: 'GET',
		//dataType: 'json',
		data: 'code='+code,// data,
		processData: false,
		contentType: false,
		complete: function( response ) {
			//console.log( response );
			$('#draft_process_type').val(response.responseText)
			//$.pjax.reload('#draft-grid-pjax', { timeout: 5000 });


		},



	});
	else
		$('#draft_process_type').val('');
	return false;
}
function UnitMaterialUp()
{

	$.ajax({
		url: "index.php?r=request/get-material-unit",
		type: 'GET',
		dataType: 'json',
		data: 'code='+$('#requestdraft-fk_material').val(),// data,
		processData: false,
		contentType: false,
		success: function( response ) {
			//console.log( response );
			$('#unit').val(response.name);
			$('#requestdraft-price_blank_space').val(response.sellprice);

		//	alert(response.sellprice);
			$.pjax.reload('#draft-grid-pjax', { timeout: 5000 });
			/*$.pjax ({
            	//reload('#request-fk_client', { timeout: 5000 })
            	container:'#draft-grid-pjax'
            	});*/


		},
		error:function(response){
			//console.log( response );
			//console.log('Ошибка обновления списка в  UnitMaterialUp!');
			//alert ('Ошибка обновления списка!');
		}


	});
	//Получим цену продажи самого материала
	//А так же тут будем получать необходимые для участка работ параметры,
	// как то ширина, длина (для своб. поля)
	$.ajax({
		url: "index.php?r=request/get-material-info-for-draft",
		type: 'GET',
		dataType: 'json',
		data: 'code='+$('#requestdraft-fk_material').val(),// data,
		processData: false,
		contentType: false,
		success: function( response ) {
			//console.log( response );
			//$('#unit').val(response.name);
			$('#sell_price_material').val(response.sellprice);
			//width_material
			$('#width_material').val(response.width);
			$('#length_material').val(response.length);
			
			
			/*
			 * На будущее динамически уберем блокировку с полей после выбора материала
			 * document.getElementById("unit").removeAttribute("readonly")
			 * и т.д. для остальных полей формы участка, которые можно отдавать в 
			 * редактирование
			 * */
			//alertObj(response);



		},
		error:function(response){
			//console.log( response );
			//console.log('Ошибка обновления списка в  UnitMaterialUp!');
			//alert ('Ошибка обновления списка!');
		}


	});
	return false;
}

function UnitProcessUp()
{

	$.ajax({
		url: "index.php?r=request/get-process-unit",
		type: 'GET',
		dataType: 'json',
		data: 'code='+$('#requestdraft-fk_work_type').val(),// data,
		processData: false,
		contentType: false,
		success: function( response ) {
			//console.log( response );
			$('#process_unit').val(response.name)
			$.pjax.reload('#draft-grid-pjax', { timeout: 5000 });


		},
		error:function(response){
			//console.log( response );
		//	console.log('Ошибка обновления списка в  UnitProcessUp!');
			//alert ('Ошибка обновления списка!');
		}


	});
	return false;}

//Пройдет по числовым полям и заменит запятые на точки. вызывать перед расчетами, после изменения значения поля.
function ConvertNumFields(){
	ToCNum('#requestdraft-price');
	ToCNum('#requestdraft-amount');
	ToCNum('#requestdraft-blank_space');
	ToCNum('#requestdraft-handle_work_price');
	ToCNum('#requestdraft-width');
	ToCNum('#requestdraft-price_blank_space');
	ToCNum('#requestdraft-height');
}
function CostUpdate(){
	//alert(e.args);
	ConvertNumFields();
	var blank=$('#requestdraft-blank_space').val();
	var craft_cost=$('#requestdraft-price').val()*$('#requestdraft-amount').val();
	var blank_cost = blank * $('#requestdraft-price_blank_space').val();
	$('#requestdraft-cost').val(
		Math.round((craft_cost + blank_cost + parseFloat($('#requestdraft-handle_work_price').val()))*100)/100
	);
}
function AreaUpdate(){
	ConvertNumFields();
	//Посчитаем свободное поле
	//requestdraft-blank_space
	//$('#requestdraft-blank_space').val();
	/*
	 * $('#sell_price_material').val(response.sellprice);
			//width_material
			$('#width_material').val(response.width);
			$('#length_material').val(response.length);
	 * */
	//alert($('#requestdraft-width').val());
	if (parseFloat($('#width_material').val()) >= parseFloat($('#requestdraft-width').val()))
		$('#requestdraft-blank_space').val(
			Math.round(
				($('#width_material').val() - $('#requestdraft-width').val())*$('#requestdraft-height').val()
				*100
			)/100
			);
	else{
		//console.log("!!!" );
	/*alert("Ширина изделия слишком велика. Ширина материала = "+$('#width_material').val()+"" +
		"\n Уменьшите значение поля Ширина изделия");*/

		$('#requestdraft-blank_space').val(0);
		//$('#requestdraft-width').val(1);
	//	$('#requestdraft-width').focus()
		//AreaUpdate();
	}
	var free_area=$('#requestdraft-blank_space').val();
	var width = $('#requestdraft-width').val() ;
	var widt_result=0;
	//alert(free_area);
	/*if (free_area >= width)
		widt_result=0;
	else
		widt_result= width - free_area;*/
	
	$('#requestdraft-work_space').val(
			Math.round(width*$('#requestdraft-height').val(),2)
			);

	$('#requestdraft-amount').val($('#width_material').val() * $('#requestdraft-height').val());
}

	var preparedFiles = 0;
	
	function setFile(e){
		$('#draft-file').val("C:\\File_1.txt");//$('#draft-file2').val());
		//$('#draft-file2').val("C:\\File_1.txt");
	}
	function catchDraftFile (e){
		
		preparedFiles = $('#draft-file').val();// e.target.files;
		console.log('hi'+preparedFiles);
		//console.log(preparedFiles);

		//alert("Цепляю файл "+preparedFiles);//e.target.files[0].name);
		//$('#area').val(-1000);
	}
	
	//debug func
	function alertObj(obj) { 
	    var str = ""; 
	    for(k in obj) { 
	        str += k+": "+ obj[k]+"\r\n"; 
	    } 
	    alert(str); 
	} 
	//
	function submitDraft(e) {
		var data = new FormData( document.querySelector('#request-form') );
		//console.log(data);
		//var request = $('input[name="RequestDraft[FK_request]"]').val();
		//preparedFiles = e.target.files;

		//console.log("request = "+request);
		//console.log("filecount = "+preparedFiles);
		//preparedFiles = [1=>];
		//alert(preparedFiles);
		if( preparedFiles !== 0 ) {
			data.append('0',preparedFiles);
		}
		//alert(preparedFiles);
		//alertObj(data);
		$.ajax({
			url: "index.php?r=request/attach-draft",
			type: 'POST',
			dataType: 'json',
			data: data,
			processData: false,
			contentType: false,
			success: function( response ) {
				console.log( response );
                $.pjax.reload('#draft-grid-pjax', { timeout: 5000 });
                
                
			},
            error:function(response){
				//console.log( response );
				console.log("error");
				 $.pjax.reload('#draft-grid-pjax', { timeout: 5000 });
				 console.log('Ошибка обновления списка!');
				 //alert('Ошибка обновления списка!');
            }

            
		});
		return false;
	}

function myFunction() {
	var x = document.getElementById("requestprocess-fk_draft").options[
		document.getElementById("requestprocess-fk_draft").selectedIndex].text;
	var txt = // "All options: ";
	/*var i;
	for (i = 0; i < x.length; i++) {
		txt = txt + "\n" + x.options[i].text;
	}*/

		alert(x);
	//alert(txt);
}

function MsubmitProcess(){
	//console.log('code='+$('#requestprocess-fk_draft').val());
	/*alert('amount='+$('#requestprocess-amount').val()
		+'&price='+$('#price').val()+
		+'&cost='+$('#cost').val()+
		+'&FKProcType='+$('#draft_process_type').val()+
		+'&FKReq='+$('#').val()+
		+'&FKDraft='+$('#select2-drop-mask').val()+
		+'&deadline='+$('#requestprocess-deadline').val());*/
	 //myFunction();

	var ename = document.getElementById("requestprocess-fk_draft").options[
		document.getElementById("requestprocess-fk_draft").selectedIndex].text;
	var ded = document.getElementById("requestprocess-deadline").text;
	var worker = $('#requestprocess-fk_user').val()
	/*var got = document.getElementById("requestprocess-type_resp").options[
		document.getElementById("requestprocess-type_resp").selectedIndex].text;*/
	console.log(ename);
	//console.log(got);
	//Это я пробую к списку ответственностей обратиться.
	//console.log($('#requestprocess-type_resp').val());
	//с кодом s2id_type_resp вариант так же не определяет список
	//alert('code='+$('#requestprocess-fk_draft').options[$('#requestprocess-fk_draft').selectedIndex].text());

	var data = new FormData(document.querySelector("#form-process-grid-pjax"));
	alert(data);
	console.log(data);
	$.ajax({
		url: "index.php?r=request/attach-my-process",
		type: 'POST',
		dataType: 'json',
		/*$amount, $price, $cost, $FKProcType,
		 $FKReq, $FKDraft,$deadline*/
		data: data,/*'fkdraft='+ename+
		      '&deadline='+ded+
		      '&worker='+worker,  /* 'amount='+$('#requestprocess-amount').val()
				+'&price='+$('#price').val()+
				+'&cost='+$('#cost').val()+
				+'&FKProcType='+$('#draft_process_type').val()+
				+'&FKReq='+$('#').val()+
				+'&FKDraft='+$('#FK_draft').val()+
				+'&deadline='+$('#requestprocess-deadline').val(),*/
		processData: false,
		contentType: false,
		success: function( response ) {
		//alert( response );
			console.log( response );
			//$('#draft_process_type').val(response)
			//$.pjax.reload('#draft-grid-pjax', { timeout: 5000 });


		},
		error:function(response){
			alert( response );
			console.log( response );
			console.log('Ошибка обновления списка в  getMaterials!');
			//alert ('Ошибка обновления списка!');
		}


	});
	return false;
}
	function submitProcess()
	{
		
		$('input[name="requestdraft-name"]').val("!!!")
		var data = new FormData( document.querySelector('#request-form') );
				//form-process-grid-pjax'));//request-form') );
		//console.lo
		//var request = $('input[name="RequestProcess[FK_request]"]').val();
		//console.log( "SubProcess" );
		//console.log( request );
		//console.log( $('input[name="RequestProcess[deadline]"]').val());
		$.ajax({
			url: 'index.php?r=request/attach-process',
			data: data,
			type: 'POST',
			dataType: 'json',
			processData: false,
			contentType: false,
			success: function( response ) {
				$.pjax.reload('#request-form', { timeout: 5000 });

//				$.pjax.reload('#process-grid-pjax', { timeout: 3000 });
				if( response.status == 1 ) {
					console.log( "SUCCESS" );
					console.log( response );
					
				} else {
					console.log( response );
				}
				
			},
			error:function(response){
				$.pjax.reload('#process-grid-pjax', { timeout: 5000 });
			}
		});
	}
	
	