// по окончанию загрузки страницы
$(document).ready(function()
{   
    // Идентификатор текущего модального окна
    var currentModalId; 

    /*
    * Этот блок нужен для устранения коллизий между виджиетами, 
    которые используют один AssetBundle, не смог понять почему скрипты не изолируются.
    Возможно в будущем получится устранить этот недостаток.
    */
    var modalIdString = $('.address-widget').attr('id');

    if( modalIdString.match( /address/i ) == null )
    {
        return;
    }
    //--------------------------------------------

        
    // При клике к кноке триггеру устанавливаем идентификатор 
    // текущего модального окна
    $('.modal-trigger').click(function()
    {
        currentModalId = $( this ).attr( 'data-target' ).substr( 1 );
    });

    // Заполняем все списки регионов и типов улиц
    fillRegion();
    fillStreetTypes();
    // Заполнение списка регионов
    function fillRegion()
    {
        $.ajax(
        {
            url: 'index.php?r=kladr/get-regions',  // указываем URL и
            dataType : "html",                     // тип загружаемых данных
            success: function ( htmlSelect, textStatus) 
            { 
                $('.region').html( htmlSelect );
            }               
        });
    };

    // Заполнение списка типов улиц
    function fillStreetTypes()
    {
        $.ajax(
        {
        
            url: 'index.php?r=kladr/get-street-types',  // указываем URL и
            dataType : 'html',                          // тип загружаемых данных
            success: function ( htmlSelect, textStatus) 
            { 
                $('.street-type').html( htmlSelect );
            }               
        });   
    }

    // При выборе региона
    $('.region').change( function(el)
    {
        var code = $(this).val();
        $.ajax(
        {
            url: 'index.php?r=kladr/get-districts&code=' + code,// указываем URL и
            dataType : 'html',                                  // тип загружаемых данных
            success: function ( htmlSelect, textStatus) 
            { 
                $('#'+currentModalId+'-district').removeAttr('disabled');    
                $('#'+currentModalId+'-town').attr( 'disabled', 'true' );
                $('#'+currentModalId+'-district').html( htmlSelect );
                $('#'+currentModalId+'-town').html( htmlSelect.substr( 0, 53 ) );
            }   
        });
    });


    // выбор населенного пункта

    $('.district').change( function(el)
    {
        var code = $(this).val();
        
        $.ajax(
        {
            url: 'index.php?r=kladr/get-towns&code=' + code,// указываем URL и
            dataType : 'html',                          // тип загружаемых данных
            success: function ( htmlSelect, textStatus) // вешаем свой обработчик на функцию success
            { 
                $('#' + currentModalId + '-town').removeAttr("disabled");
                $('#' + currentModalId + '-town').html( htmlSelect );
            }               
        });
    });

    // Валидация формы заполнения адреса
    $('.address-widget-submit').click( function()
    {
        
        var modalId = $(this).closest('.address-widget').attr('id');
        
        if( currentModalId !== modalId )
        {
            return;
        }

        var msg = '<ul>';
        var err = 0;
        
        $('#message-pool').text('');
        // проверяю адрес
        if ( $( '#'+modalId+'-region option:selected' ).val() === '0' ) 
        {
            msg += '<li>Необходимо заполнить поле Край/Область.\n';
            err++;
        }
        if( $('#'+modalId+'-district option:selected' ).val() === '0' )
        {
            msg += '<li>Необходимо заполнить поле Район.';
            err++;
        }
        if( !$( '#'+modalId+'-building' ).val() )
        {
            msg += '<li>Необходимо заполнить поле Номер здания.'
            err++;
        }

        var postIndex = $( '#'+modalId+'-post-index' ).val();
        if( postIndex )
        {       
            if( ( postIndex.length < 6 ) || ( isNaN( postIndex ) ) )
            {
                msg += '<li>Некорректно заполнено поле "Почтовый индекс".';
                err++;
            }
        }

        var streetName = $( '#'+modalId+'-street' ).val();
        var streetSocr = $( '#'+modalId+'-street-type option:selected' ).val();
        if( streetSocr !== '0' )
        {
            if( !streetName )
            {
                msg += '<li>Необходимо выбрать тип улицы и ввести ее название.';
                err++;
            }
        }
        else
        {
            if( streetName )
            {
                msg += '<li>Необходимо выбрать тип улицы и ввести ее название.';
                err++;
            }
        }

        $( '#message-pool' ).html( msg + '</ul>' );

        if( err === 0 ) 
        {
            //saveAddress( modalId );
            var code = $( '#'+modalId+'-town option:selected' ).val();
            var addressField = '#' + $( '#'+ modalId +'-extraData' ).val();
            if( code === '0' )
            {
                code = $( '#'+modalId+'-district option:selected' ).val();
            }
            var fullAddress = ( $( '#'+modalId+'-post-index' ).val() == ''?'':$( '#'+modalId+'-post-index' ).val() + ', ' ) +
                          $( '#'+modalId+'-region option:selected' ).text() + ', ' +
                          $( '#'+modalId+'-district option:selected' ).text() + ', ' +
                          ( $( '#'+modalId+'-town option:selected' ).val() == '0'?'':$('#'+modalId+'-town option:selected').text() + ', ' ) +
                          ( $( '#'+modalId+'-street-type option:selected' ).val() == '0'?'':$('#'+modalId+'-street-type option:selected').text() + '.' + $('#'+modalId+'-street').val() + ', ' ) +
                          'д.' + $( '#'+modalId+'-building' ).val() +
                          ( $( '#'+modalId+'-room' ).val() == ''?'':', п.' + $( '#'+modalId+'-room' ).val() ) +
                          ( $( '#'+modalId+'-other-place' ).val() == ''?'':' (' + $( '#'+modalId+'-other-place' ).val() + ')' );
            $('.modal').modal('hide');
            $( addressField ).html( '<option value='+code+' selected>'+fullAddress+'</option>' );
        }
        return;
    });

    $(document).one( 'submit', function(e)
    {
        e.preventDefault();
        e.stopPropagation();

        var addressWidgetList = $('.address-widget');
           
        $.each( addressWidgetList, function( key, value )
        {
            saveAddress( value.id );
        });
        callSubmitForm();
        //setTimeout( callSubmitForm, 3000 );
    });

    function callSubmitForm()
    {
        $('form').submit();
    }

    function saveAddress( modalId )
    {
        var addressField = '#' + $( '#'+ modalId +'-extraData' ).val();
        var code = $( '#'+modalId+'-town option:selected' ).val();
        if( code === '0' )
        {
            code = $( '#'+modalId+'-district option:selected' ).val();
            if( code == '0' )
                return 0;
        }
        var fullAddress = $( '#'+modalId+'-region option:selected' ).text() + ', ' +
                          $( '#'+modalId+'-district option:selected' ).text() + ', ' +
                          ( $( '#'+modalId+'-town option:selected' ).val() == '0'?'':$('#'+modalId+'-town option:selected').text() + ', ' ) +
                          ( $( '#'+modalId+'-street-type option:selected' ).val() == '0'?'':$('#'+modalId+'-street-type option:selected').text() + '.' + $('#'+modalId+'-street').val() + ', ' ) +
                          'д.' + $( '#' + modalId+'-building' ).val() +
                          ( $( '#'+modalId+'-room' ).val() == ''?'':', п.' + $( '#'+modalId+'-room' ).val() ) +
                          ( $( '#'+modalId+'-other-place' ).val() == ''?'':' (' + $( '#'+modalId+'-other-place' ).val() + ')' );
        var data ='&code=' + code +
                  '&street-type=' + $( '#'+modalId+'-street-type option:selected' ).val() +
                  '&post-index=' + $( '#'+modalId+'-post-index' ).val() +
                  '&street=' + $( '#'+modalId+'-street' ).val() +
                  '&building=' + $( '#'+modalId+'-building' ).val() +
                  '&room=' + $( '#'+modalId+'-room' ).val() +
                  '&other-place=' + $( '#'+modalId+'-other-place' ).val() +
                  '&full-address=' + fullAddress;

        $.ajax( 
        {
            url: 'index.php?r=kladr/save-address',
            dataType: 'html',
            data: data,
            type: 'POST',
            async: false,
            success: function( htmlResponse, textStatus )
            {
                //$('.modal').modal('hide');
                $( addressField ).html( htmlResponse );
            }
        });
    }

});