<?php

namespace common\assets;

use yii\web\AssetBundle;

class MaterialsAsset extends AssetBundle
{
    public $basePath = '@common/JS/materials';
    public $baseUrl = '@common/JS/materials';

    public $css =
        [

        ];

    public $js =
        [
            '../modalShow.js',
            'materials.js',
        ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
?>