<?php 

namespace common\assets;

use yii\web\AssetBundle;

class ModalWidgetAsset extends AssetBundle
{
	public $basePath = '@common/JS';
	public $baseUrl = '@common/JS';

	public $css = 
	[

	];

	public $js = 
	[
		'addressInput.js',
	];

	public $depends = [
        
    ];
}

?>