<?php

namespace common\models1;

use Yii;

/**
 * This is the model class for table "log".
 *
 * @property integer $id
 * @property string $who
 * @property string $place
 * @property string $what
 * @property string $datetime
 * @property string $new_value
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['who', 'place', 'what', 'datetime', 'new_value'], 'required'],
            [['what'], 'string'],
            [['datetime'], 'safe'],
            [['who'], 'string', 'max' => 30],
            [['place'], 'string', 'max' => 50],
            [['new_value'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'who' => 'Кто',
            'place' => 'Откуда (IP)',
            'what' => 'Что сделал',
            'datetime' => 'Дата_время внесения изменений',
            'new_value' => 'Ввод пользователя',
        ];
    }
}
