<?php

use common\modules\storage\Module;
use kartik\datecontrol\Module as DateControlModule;
Yii::setAlias('@temp/parse', realpath(dirname(__FILE__).'/../../'));

$appName = 'dial';

return 
[
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language'=>'ru',
    'params' => [
	    // format settings for displaying each date attribute (ICU format example)
	    'dateControlDisplay' => [
	        DateControlModule::FORMAT_DATE => 'dd.MM.yyyy',
	        DateControlModule::FORMAT_TIME => 'HH:mm:ss a',
	        DateControlModule::FORMAT_DATETIME => 'dd-MM-yyyy HH:mm:ss a', 
	    ],
	    
	    // format settings for saving each date attribute (PHP format example)
	    'dateControlSave' => [
	        DateControlModule::FORMAT_DATE => 'php:Y-m-d', // saves as unix timestamp
	        DateControlModule::FORMAT_TIME => 'php:H:i:s',
	        DateControlModule::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
	    ]
	],
     'modules' => 
	 [
	    'admin'=> 
	    [
	        'class'=>'mdm\admin\Module',
	        'layout'=>'left-menu',
	    	
		],
         'datecontrol' =>
         [
        	'class' => '\kartik\datecontrol\Module'
         ],
	 		
	 	'gridview' => [
	 			'class' => '\kartik\grid\Module'
	 			// enter optional module parameters below - only if you need to
	 			// use your own export download action or custom translatio
	 			// message source
	 			// 'downloadAction' => 'gridview/export/download',
	 			// 'i18n' => []
	 	],
		
        'storage' => 
        [
            'class' => 'common\modules\storage\Module',
            'employeeClass' => 'common\models\User',
            'subdealerClass' => 'common\models\Organisation',
        	'contractClass' => 'common\models\Contract',
        	'contractTypes' => 'common\models\ContractType',
        ],
	 	
	 	'flexform' => [
	 		'class' => 'vendor\novik\flexform\Module',	
	 	],
        
	],
    'components' => 
    [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['user'],
        ],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
	    'cache' => 
	    [
	    	'class' => 'yii\caching\FileCache',
        ],

        /*'view' => [
            'theme' => [
                'pathMap' => ['@app/views' => '@app/themes/modern'],
                'baseUrl' => '@web/themes/modern',
            ],
        ],*/
        
        /*'urlManager' => 
        [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => 
            [
                '<_c:[\w\-]+>/<id:\d+>' => '<_c>/view',
                '<_c:[\w\-]+>' => '<_c>/index',
                '<_c:[\w\-]+>/<_a:[\w\-]+>/<id:\d+>' => '<_c>/<_a>',
            ],
        ],*/
    ],
    'aliases' => [
    	'@appBaseUrl' => $appName, 
        '@common/JS' => '../../common/assets/crmscripts',
      //  '@logo' 	 => $appName.'/backend/web/res/image/logo',
       // '@parsing' 	 => $appName.'/backend/temp/parsing',
    	'@storage'	 => '/common/modules/storage',
    ],

    

    /*'as access' => 
    [
        'class'=>'mdm\admin\components\AccessControl',
        'allowActions'=>
        [
            '*',
        ],
    ],*/
];
