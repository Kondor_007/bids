<?php

/*
*	Виджет с модальным окном добавления. 
	Для использования виджета необходимо:
	
	1. Определить следующие свойства при вызове AddWidget::begin -
	//--------------------------------------------------
	$actionUrl - URL к действию добавления новой записи. 
	
	В случае успешного добавления записи действие должно вернуть либо строку, 
	содержащую доп. информацию для запроса обновления страницы,	который добавится к htmlSourceUrl 
	либо пустую строку.
	
	В случае некорректного запроса, не прохождения валидации и т.д. действие должно ответить с кодом статуса
	BadRequestHttpException.

	//---------------------------------------------------
	$sourceFieldId - поле которое необходимо обновить после выполнения работы виджета.
	
	//----------------------------------------------------
	$modalId - ID модального окна.
	ID модального окна по которому оно будет вызываться  кнопкой.
	
	//----------------------------------------------------
	Добавить на форму кнопку инициирующую показ модального окна 
	( с аттрибутами 'data-toggle'=>'modal', 'data-target' => должен быть равен $modalId )
	
	
	Остальные аттрибуты устанавливаются опционально.
	//-----------------------------------------------------
		
	2. Добавить поля формы.
	Поля добавляются на форму виджета через его свойство $modalForm которое в свoю очередь является
	виджетом ActiveForm, и обращаться с ним следует ровно так же (см. документацию yii2). 
	Вызовы метода modalForm->field() нужно обособить вызовами методов 
	AddWidget::beginModalForm() и AddWidget::endModalForm.
*/

namespace common\components\modalWidgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use common\assets\AddWidgetAsset;

class AddWidget extends Widget
{
	/*-------------------------------------------------
	* 				@properties
	---------------------------------------------------*/
	/*
	* $actionUrl - URL который осуществляет добавление новой записи в БД
	*/
	public $actionUrl;
	/*
	* $sourceFieldId - поле которое необходимо обновить после выполнения работы выиджета, 
	*/
	public $sourceFieldId;	
	/*
	* $modalId - ID модального окна.
	*/
	public $modalId = 'modalAddWidget';
	/*
	* $title - заголовок модального окна.
	*/
	public $title = 'To change this title, set the title property in begin method';
	/*
	* $buttonLabel - Текст кнопки отправки формы.
	*/
	public $buttonLabel = 'Add';
	/*
	* $modalForm - виджет ActiveForm. Пользователь имеет к нему доступ для добавления 
	полей в форму модального окна.
	*/
	public $modalForm;

	/*---------------------------------------------
	* 				@methods
	-----------------------------------------------*/
	/*
	* init() - Переопределенный метод класса Widget.
	*/
	public function init()
	{
		parent::init();	// Вызов родительского метода.
		// Инициализация виджета
		echo $this->render('modalhead', [ 'id' => $this->modalId,
									 		'title' => $this->title,
									 		'widgetClass' => 'add-widget' ]);
	}

	/*
	* beginModalForm() - создает виджет формы.
	*/
	public function beginModalForm()
	{
		$this->modalForm = ActiveForm::begin([
										'id' => 'main-form-'.$this->modalId,
										'action'=> [ $this->actionUrl ],
										]);
	}
	/*
	* endModalForm() - закрывает виджет формы.
	*/
	public function endModalForm()
	{
		ActiveForm::end();
	}
	/*
	* run() - Переопределенный метод класса Widget.
	*/
	public function run()
	{
		$view = $this->getView();
		// Здесь нужно вернуть результат рендеринга виджета
        $view->registerJsFile( '@common/JS/addWidget.js',
        					  ['depends' => [ AddWidgetAsset::className()] ], 
        					  View::POS_END );
		$modalFooter =  $this->render( 'modalfoot', [ 'buttonLabel' => $this->buttonLabel,
											 'formId' => $this->modalForm->id,
											 'data' => $this->sourceFieldId,
											 'modalId' => $this->modalId,
											 'buttonId' => 'add-widget-submit',
											]);
        return $modalFooter;
	}
}

?>