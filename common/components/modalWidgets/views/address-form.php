<?php
use yii\helpers\Html;

// $fieldId - идентификатор поля. Префикс к полям
?>


<div id="AddressForm" >
	
	<div id='container'>
	
		<div class = 'form-group required'>
			<label class="control-label" for= <?= $fieldId.'-region' ?>>Область/Край</label>
			<select class="form-control region" id = <?= $fieldId.'-region' ?>> 
				<option value="0" selected="selected">-- Выберите из списка --</option>
			</select>
			<div class = 'help-block'></div>
		</div>
		
		<div class = 'form-group required'>
			<label class = 'control-label' for = <?= $fieldId.'-district' ?> >Район</label>
			<select class="form-control district"  placeholder='Введите район' disabled = 'true' id = <?= $fieldId.'-district' ?> > 
				<option value="0" selected="selected">-- Выберите из списка --</option>
			</select>
		</div> 

		<div class = 'form-group'>
			<label class = 'control-label' for = <?= $fieldId.'-town' ?> >Наименование населенного пункта</label>
			<select class="form-control town" id=<?= $fieldId.'-town' ?> placeholder='Введите название населенного пункта' disabled = 'true'> 
				<option value="0" selected >-- Выберите из списка --</option>
			</select>
		</div> 

		<div class = 'form-group'>
			<label class = 'control-label' for = <?= $fieldId.'-post-index' ?> >Почтовый индекс</label>
			<input class = 'form-control post-index' id = <?= $fieldId.'-post-index' ?> type='text' placeholder ='Введите почтовый индекс' >
		</div> 

		<div class = 'form-group'>
			<label class = 'control-label'>Улица</label>
			<div class = 'row'>
				<div class = 'col-sm-3'>
					<select class="form-control street-type" id = <?= $fieldId.'-street-type' ?>> 
						<option value="0" selected >-- Выберите из списка --</option>
					</select>
				</div>
				<div class = 'col-sm-9'>
					<input class="form-control street" id = <?= $fieldId.'-street' ?> type='text' placeholder='Введите название улицы'/> 
				</div>
			</div>
		</div>

		<div class = 'form-group'>
			<label class = 'control-label' for = <?= $fieldId.'-building' ?> >Номер здания</label>
			<input class="form-control building" id = <?= $fieldId.'-building' ?> type='text' placeholder='Введите номер здания'/> 
		</div>

		<div class = 'form-group'>
			<label class = 'control-label' for = <?= $fieldId.'-room' ?> >Номер помещения</label>
			<input class="form-control room" id=<?= $fieldId.'-room' ?> type='text' placeholder='Номер помещения'/> 
		</div>

		<div class = 'form-group'>
			<label class = 'control-label' for = <?= $fieldId.'-other-place' ?> >Другое местоположение</label>
			<input class="form-control other-place" id = <?= $fieldId.'-other-place' ?> type='text' placeholder='Введите примечание к адресу'/> 
		</div>
	</div>
</div>