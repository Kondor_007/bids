<?php

use yii\helpers\Html;
/*
*	Нижняя часто модального окна. Содержит скрытое поле с доп. информацией и 
	футер с кнопками закрытия и отправки.
*/
// $buttonLabel - submit button label
// $data - Дополнительная информация.
// $formId - ID формы или модального окна
// $buttonId

if( empty($modalId) )
{
	$modalId = '';
}
else
{
	$modalId .= '-';
}

?>
				<input type = 'hidden' value = '<?= $data != null?$data:null; ?>' id = '<?= $modalId ?>extraData' >
				</div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" >Закрыть</button>
                <button type="button" class= <?= "'btn btn-primary ".$buttonId."'" ?> id= <?= $modalId ?> form = <?= $formId ?>><?= $buttonLabel ?></button>
            </div>
        </div>
      </div>
</div>