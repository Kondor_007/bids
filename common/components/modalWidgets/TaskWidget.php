<?php 

namespace common\components\modalWidgets;

use yii\base\Widget;
use yii\web\View;
use yii\bootstrap\Modal;
use common\assets\TaskAsset;

class TaskWidget extends Widget
{
	public function init()
	{
		parent::init();
	}
	public function run()
	{
		Modal::begin(
			[
				'header'=>'Добавление задачи',
				'footer'=> $this->render('accept-cancel', [ 'submitId' => 'task-create' ]),
				'size'=> 'SIZE_LARGE',	
				'toggleButton'=>['label'=>'Добавить задачу', 
								 'class'=>'btn btn-primary'],
			]);
		echo $this->render( 'task-form' );
		Modal::end();
		$view = $this->getView();
		// Регистрируем файлы скриптов необходимых для виджета
        $view->registerJsFile( '@common/JS/Task/taskManager.js',
        					  ['depends' => [ TaskAsset::className()] ], 
        					  View::POS_END );

	}
}

?>