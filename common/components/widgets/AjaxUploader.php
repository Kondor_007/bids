<?php 

namespace common\components\widgets;

use yii\base\Widget;
use yii\web\View;

use common\assets\AjaxUploaderAsset;

class AjaxUploader extends Widget
{
	public function init()
	{
		parent::init();
	}
	public function run()
	{
		echo $this->render( 'ajax-file-input' );
		$view = $this->getView();
		// Регистрируем файлы скриптов необходимых для виджета
        $view->registerJsFile( '@common/JS/ajaxUploader.js',
        					  ['depends' => [ AjaxUploaderAsset::className()] ], 
        					  View::POS_END );

	}
}

?>