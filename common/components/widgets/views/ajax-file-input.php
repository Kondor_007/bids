<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\modules\storage\models\Provider;
use yii\bootstrap\Modal;
$providerList = ArrayHelper::map( Provider::find()->all(), 'id', 'name' );

$this->registerJsFile( '/crm/common/modules/storage/assets/plugins/jquery.ajaxupload.js', [ 'depends' => [ 'yii\web\JqueryAsset' ], 
																							'position' => yii\web\View::POS_END ] );
$this->registerJsFile( '/crm/common/modules/storage/assets/ajaxUploader.js', [ 'depends' => [ 'yii\web\JqueryAsset' ],
																			  'position' => yii\web\View::POS_END ] );

?>
<?php Modal::begin([
                'id' => 'file-upload',
                'header' => 'Импорт',
                'toggleButton' => [
                    'label' => 'Импорт',
                    'class' => 'btn btn-primary',
                ],

                'footer' => '<div align = "center"><div id="upload" class = "btn btn-primary" >Импорт</div></div>',
            ]); ?>

	<form id = 'upload-form'>
		<div class = 'form-group'>
			<?= Html::dropDownList( 'provider-list', 0, $providerList, [ 'class' => 'form-control' ] ) ?>
		</div>
		<div class = 'form-group'>
			<input class = 'form-control' style="border: 0" name = 'import' type='file'></input>
		</div>
		<font size = 6 color = red >
			&nbsp<span id="status" ></span>
		</font>
		<br>
		<font size = 4 color = red >
			<span id="warning" ></span>
		</font>
	</form>

<?php Modal::end(); ?>