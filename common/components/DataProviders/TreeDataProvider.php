<?php

namespace common\components\DataProviders;

use Yii;
use yii\data\ActiveDataProvider;
use yii\base\Model;
use yii\db\Connection;
use yii\db\QueryInterface;
use yii\di\Instance;

class TreeDataProvider extends ActiveDataProvider
{
	public $childRelation = 'children';

	protected function prepareModels()
	{
        $this->pagination = false;
        
		if (!$this->query instanceof QueryInterface) {
            throw new InvalidConfigException('The "query" property must be an instance of a class that implements the QueryInterface e.g. yii\db\Query or its subclasses.');
        }
        $query = clone $this->query;
        if (($pagination = $this->getPagination()) !== false) {
            $pagination->totalCount = $this->getTotalCount();
            $query->limit($pagination->getLimit())->offset($pagination->getOffset());
        }
        if (($sort = $this->getSort()) !== false) {
            $query->addOrderBy($sort->getOrders());
        }

        $rootCriteria=clone $query;
        $isEmptyCondition=empty($rootCriteria->where);
 
        if ($isEmptyCondition)
            $rootCriteria->where([ 'parent_id' => 0, 'active' => 1 ]);
 
        $items = $rootCriteria->all( $this->db );
 
        if ($isEmptyCondition)
            $items=$this->buildRecursive($items);

        return $items;
        //return $query->all($this->db);
	}

    protected function buildRecursive($items, $level = 0, $foolproof = 20)
    {
        $data = array();
        foreach ( $items as $item )
        {
            $item->level = $level;
            $data[] = $item;
            if ($foolproof && $item->{$this->childRelation})
                $data=array_merge($data, $this->buildRecursive($item->{$this->childRelation}, $level+1, $foolproof-1));
        }
        return $data;
    }
    
	/**
     * @inheritdoc
     */
    protected function prepareKeys($models)
    {
        $keys = [];
        if ($this->key !== null) 
        {
            foreach ($models as $model) 
            {
                if (is_string($this->key)) 
                {
                    $keys[] = $model[$this->key];
                } else {
                    $keys[] = call_user_func($this->key, $model);
                }
            }

            return $keys;
        } elseif ($this->query instanceof ActiveQueryInterface) 
        {
            /* @var $class \yii\db\ActiveRecord */
            $class = $this->query->modelClass;
            $pks = $class::primaryKey();
            if (count($pks) === 1) {
                $pk = $pks[0];
                foreach ($models as $model) 
                {
                    $keys[] = $model[$pk];
                }
            } else {
                foreach ($models as $model) {
                    $kk = [];
                    foreach ($pks as $pk) {
                        $kk[$pk] = $model[$pk];
                    }
                    $keys[] = $kk;
                }
            }

            return $keys;
        } else {
            return array_keys($models);
        }
    }


    /**
     * @inheritdoc
     */
    protected function prepareTotalCount()
    {
        if (!$this->query instanceof QueryInterface) {
            throw new InvalidConfigException('The "query" property must be an instance of a class that implements the QueryInterface e.g. yii\db\Query or its subclasses.');
        }
        $query = clone $this->query;
        return (int) $query->limit(-1)->offset(-1)->orderBy([])->count('*', $this->db);
    }

    /**
     * @inheritdoc
     */
    public function setSort($value)
    {
        parent::setSort($value);
        if (($sort = $this->getSort()) !== false && empty($sort->attributes) && $this->query instanceof ActiveQueryInterface) {
            /* @var $model Model */
            $model = new $this->query->modelClass;
            foreach ($model->attributes() as $attribute) {
                $sort->attributes[$attribute] = [
                    'asc' => [$attribute => SORT_ASC],
                    'desc' => [$attribute => SORT_DESC],
                    'label' => $model->getAttributeLabel($attribute),
                ];
            }
        }
    }
}

?>