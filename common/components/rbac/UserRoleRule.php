<?php
namespace common\components\rbac;
use Yii;
use yii\rbac\Rule;
use yii\helpers\ArrayHelper;
use common\models\User;

class UserRoleRule extends Rule
{
	public $name = 'userRole';
	public function execute( $user, $item, $params )
	{
		//Получаем массив пользователя из БД
		$user = ArrayHelper::getValue( $params, 'user', User::findOne( $user ) );
		if( $user )
		{
			$role = $user->role; //Значение из поля role БД
			if( $item->name === 'admin' )
			{
				return $role == User::ROLE_ADMIN;
			}elseif ( $item->name === 'extUser' )
			{
				return $role == User::ROLE_ADMIN || 
				$role == User::ROLE_EXTUSER;
			}elseif( $item->name === 'user' )
			{
				return $role == User::ROLE_ADMIN ||
				$role == User::ROLE_EXTUSER || $role == User::ROLE_USER;
			}
		}
		return false;
	}
}
