<?php 

//namespace 

interface Contactable
{
	/*
	* Метод должен возвращать ассоциативный массив 
	* значений с соответствующими ключами
	* 
	* $array = [
	*   'email' => 'e@mail.com',
	*   'phone' => '+7953...',
	*	'skype' => 'Скайп',
	*	'web-site' => 'http://site.com', 
	* ];
	*/
	public function getContacts();
}

?>