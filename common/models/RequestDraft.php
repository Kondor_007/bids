<?php

namespace common\models;

//use Yii;

/**
 * This is the model class for table "request_draft".
 *
 * @property integer $id
 * @property integer $FK_material
 * @property integer $FK_request
 * @property integer $amount
 * @property double $work_space
 * @property double $blank_space
 * @property double $height
 * @property double $width
 * @property double $price
 * @property double $cost
 * @property string $file_path
 */
class RequestDraft extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $type_dev, $unit, $process_unit;//, $type_work;
    public $tempF;
    public static function tableName()
    {
        return 'request_draft';
    }

    /**
     * @inheritdoc
     */
    public $handle_work_price=0; //цена того, что работник таскает файлы ручками
    public function rules()
    {
        return [
           [[ 'FK_request'], 'integer'],
            [['FK_request'], 'string'],//'required'],
            [['work_space',
                //Свободное поле и его цена. В расчет стоимости идет цена отсюда,
                //а сюда она приходит из таблицы материалов при создании участка/этапа
                'blank_space', 'price_blank_space',
                 'height', 'width', 'price', 'cost','amount',

            ], 'double'
            		,'message' => 'Пожалуйста, вводите число с разделителем "." (точка)'
            ],
        	
            [['file_path'], 'string', 'max' => 1024],
            [['FK_material',], 'string'],
            [['name'],  'string'],//  'required'],
            [['FKtype_dev'],'integer'],
            [['FK_work_type'],'integer'],
            [['type_work'],'integer'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Назв. Этапа',
            'FK_material' => 'Fk Material',
            'FK_request' => 'Fk Request',
            'amount' => 'Кол-во матер. на этап в ед. изм.',
            'work_space' => 'Площадь изделия, м2',
            'blank_space' => 'Свободное поле, м2',
            'price_blank_space' => 'Цена свободного поля',
            'height' => 'Длина изделия, м',
            'width' => 'Ширина изделия, м',
            'price' => 'Цена за обработку 1 ед. материала',
            'cost' => 'Стоимость этапа',
            'file_path' => 'Файл',
            'FK_draft'  => 'Fk Draft',
        	'material.name' => 'Материал',
        	'materialName' => 'Материал',
        ];
    }
    
    public function getMaterial()
    {
    	return $this->hasOne( Material::className(), ['id' => 'FK_material'] );
    }
    
    public function getMaterialName()
    {
    	if( isset( $this->material ) ) {
    		return $this->material->name;
    	} else {
    		return 'Не задан';
    	}
    }


    public function getFileName()
    {
    	return $this->file_path;// 'file_path';// FIle
    }


    public function getDeviceName()
    {
       // $reqid=$this->FK_request;
        return Machine::findOne(Request::findOne($this->FK_request)->FK_device)->name;
    }
    public function getDeviceType()
    {
        // $reqid=$this->FK_request;
        return MachineType::findOne( Machine::findOne(Request::findOne($this->FK_request)->FK_device)->FK_type)->name;
    }

    public function getWorkType()
    {
        return  ProcessType::find()->where('fk_machine='.$this->FK_work_type)->one()->name;
    }
}
