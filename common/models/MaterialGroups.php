<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "material_groups".
 *
 * @property integer $id
 * @property integer $need_color
 * @property integer $need_width
 * @property integer $need_thickness
 * @property integer $need_length
 * @property integer $need_dencity
 */
class MaterialGroups extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['need_color', 'need_width', 'need_thickness', 'need_length', 'need_dencity'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'need_color' => 'Need Color',
            'need_width' => 'Need Width',
            'need_thickness' => 'Need Thickness',
            'need_length' => 'Need Length',
            'need_dencity' => 'Need Dencity',
        ];
    }
    public static function getGroups(){
       /* $out = MaterialGroups::find()->all();
        $res=json_encode(ArrayHelper::map(MaterialGroups::find()->all(),'id','name'));
        print_r($res);*/
       // exit();
        return MaterialGroups::find()->all();
    }

}
