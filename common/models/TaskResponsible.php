<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "task_responsible".
 *
 * @property integer $id
 * @property integer $FK_user
 * @property integer $FK_task
 * @property integer $type
 * @property string $FK_role
 * 
 * @property UsExt $fKUser
 * @property Task $fKTask
 */
class TaskResponsible extends \yii\db\ActiveRecord
{
    public $_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_responsible';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FK_task'], 'required'],
            [[ 'FK_task', 'type'], 'integer'],
            [['FK_user'], 'typeControl'],
            [['FK_role'],'string']
        ];
    }
    
    public function typeControl()
    {
        if( $this->type == 4 ) {
            $this->FK_role = $this->FK_user;
            $this->FK_user= null;
        }
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'FK_user' => 'ID пользователя',
            'FK_task' => 'ID задачи',
            'type' => 'Флаг ответственного',
            'FK_role' => 'ID группы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKUser()
    {
        return $this->hasOne(User::className(), ['id' => 'FK_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'FK_task']);
    }

    public function getItem()
    {
        return $this->hasOne( AuthItem::className(), ['name' => 'FK_role' ]);
    }
    

    public function getName()
    {
        if ($this->type == 1){
            return $this->fKUser->Name;
        } elseif ($this->type == 4) {
                return $this->item->description;//->description;
//                return 'Групповая '.$this->FK_role;
            } else {return 'Не определена';}
    }
    // связь с request
    public function getRequest()
    {
        return $this->hasOne(Request::className(), ['FK_task' => 'FK_task']);
    }
    
    
    public function getGroupName(){
        if ($this->group !== null ){                    
            return $this->group->Description;
            }
            else {
                return 'Группа не определена';        
                }
    }

}
