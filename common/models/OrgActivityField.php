<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "org_activity_field".
 *
 * @property integer $id
 * @property string $name
 */
class OrgActivityField extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'org_activity_field';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 4096]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Сфера деятельности',
        ];
    }
}
