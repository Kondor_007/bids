<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "material_machine".
 *
 * @property integer $id
 * @property integer $FK_material
 * @property integer $FK_machine
 */
class MaterialMachine extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_machine';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FK_material', 'FK_machine'], 'required'],
            [['FK_material', 'FK_machine'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'FK_material' => 'Fk Material',
            'FK_machine' => 'Fk Machine',
        ];
    }
}
