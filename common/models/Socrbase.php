<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "socrbase".
 *
 * @property integer $id
 * @property string $level
 * @property string $scname
 * @property string $socrname
 * @property string $kod_t_st
 */
class Socrbase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'socrbase';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['level', 'scname', 'name', 'code'], 'required'],
            [['level'], 'string', 'max' => 8],
            [['scname'], 'string', 'max' => 128],
            [['name'], 'string', 'max' => 1024],
            [['code'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'level' => 'Level',
            'scname' => 'Scname',
            'name' => 'Socrname',
            'code' => 'Kod T St',
        ];
    }
}
