<?php

namespace common\models;


use Yii;
use common\models\TaskStatusRef;
/**
 * This is the model class for table "task_status".
 *
 * @property integer $id
 * @property string $date
 * @property integer $FK_user
 * @property integer $FK_status
 * @property integer $FK_task
 * @property integer $is_top
 * @property string $description
 *
 * @property UsExt $fKUser
 * @property TaskStatusRef $fKStatus
 * @property Task $fKTask
 */
class TaskStatus extends \yii\db\ActiveRecord
{
    const TASK_WAIT       = 1;
    const TASK_STARTED    = 2;
    const TASK_PAUSED     = 3;     
    const TASK_FROZEN     = 4;
    const TASK_WAIT_CHECK = 5;
    const TASK_FINISHED   = 6;

    const STATUS_TOP    = 1;

    public $_statusTypeName;
    public $_statusTypeId;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['FK_user', 'FK_status', 'FK_task'], 'required'],
            [['FK_user', 'FK_status', 'FK_task', 'is_top'], 'integer'],
            [['description'], 'string', 'max' => 8192]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Дата и время установки статуса',
            'FK_user' => 'Пользователь установивший статус',
            'FK_status' => 'Тип статуса ',
            'FK_task' => 'ID задачи',
            'is_top' => 'Актуальность статуса',
            'description' => 'Комментарии к статусу',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'FK_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKStatus()
    {
        return $this->hasOne(TaskStatusRef::className(), ['id' => 'FK_status']);
    }

    public function getStatusTypeName()
    {
        
        return $this->fKStatus->name;
    }

    public function getStatusTypeId()
    {
        return $this->fKStatus->id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'FK_task']);
    }
    
        public function getTaskStatusRef()
    {
    	return $this->hasOne( TaskStatusRef::className(), [ 'id' => 'FK_status'] );
    }
    
     public static function active($query)
    {
        $query->andWhere('is_top = 1');
    }
    
    public function getRequest()
    {
    	return $this->hasOne( Request::className(), [ 'FK_task' => 'FK_task'] );
    }

}
