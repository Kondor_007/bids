<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "material".
 *
 * @property integer $id
 * @property string $name
 * @property integer $FK_unit
 * @property double $issue_limit
 * @property double $buy_price
 * @property double $sell_price
 */
class Material extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material';
    }
  public $have_materials, $searchname;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FK_unit'], 'integer'],
           // [['issue_limit', 'buy_price', 'sell_price'], 'number'],
            [['name', 'manufacturer'], 'string', 'max' => 512],
            [['is_del'], 'integer'],
           // [['count'], 'integer'],
           /* [['prop_name'], 'string'],
            [['prop_value'], 'string'],*/
           // [['dencity'], 'double'],
            [['width','length','thickness','dencity',
            		'issue_limit', 'buy_price', 'sell_price',
            		'count'
            ], 'double','message' => 'Пожалуйста, вводите число с разделителем "." (точка)'],

            [['color'], 'string'],
            [['FK_machine_type'], 'string'],
            [['have_materials'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'FK_unit' => 'Fk Unit',
            'issue_limit' => 'Issue Limit',
            'buy_price' => 'Цена покупки',
            'sell_price' => 'Цена продажи',
            'is_del' => 'Удалено',
            'count' => 'Кол-во',
           /* 'prop_name' => 'Главное свойство',
            'prop_value' => 'Его значение',*/
            'dencity' => 'Плотность, г/см3',
            'width' => 'Ширина, м',
        		'length' => 'Длина, м',
            'thickness' => 'Толщина, мм',
            'color' => 'Цвет, покрытие',
            'FK_machine_type'=>'Тип совместимого оборудования',
        ];
    }


    /* Геттер для полного описания*/
    public function getFullNameSelect($typeWork="печать") {
    	$strdencity="";
        $full = $this->getFullName();
    	if (strpos($typeWork, "ечать") or
    			strpos($typeWork, "лоттер") or strpos($typeWork, "аннер"))
    		$strdencity=", плотн. ".$this->dencity." г/см3, ";
    	
    	return $full.$strdencity;/* $this->name." толщина ".$this->thickness."мм, ".$this->color
    	.", ширина ".$this->width."м".", длина ".$this->length."м. ".$strdencity;*/
    }
    public function getFullName() {
       // return $this->first_name . ' ' . $this->last_name;
        $man= $this->manufacturer=="" ? "" : " (".$this->manufacturer.")";
        return $this->name.$man." толщина ".$this->thickness."мм, ".$this->color.",
                плотн. ".$this->dencity.", ширина ".$this->width."м".", длина ".$this->length."мм";
    }

    public function getShortInfo() {
       // $Info = "";
        if ($this->manufacturer=="")
            return $this->name." , ширина ".$this->width."м";
        return $this->name." (".$this->manufacturer."), ширина ".$this->width."м";
    }
    public function getFullInfo($id=0){
        /*echo $id;
        exit();*/
        //$this=Material::findOne($id);
        //Берем тип материала
        $type = $this->FK_group;
        //инициализируем переменную описатель
        $info = $this->name;
       // $arrtags=['dencity','width','length','color'];
        $man= $this->manufacturer=="" ? "" : " (".$this->manufacturer.")";
        $dencity=! $this->IsNeed($this->FK_group,'dencity') ? "" : " (плотн.".$this->dencity." г/см3)";
        $width=! $this->IsNeed($this->FK_group,'width') ? "" : " (шир.".$this->width." м.)";
        $length=! $this->IsNeed($this->FK_group,'length') ? "" : " (длина.".$this->length." м.)";
        $color= !$this->IsNeed($this->FK_group,'color') ? "" : " (".$this->color.")";
        $square=! $this->IsNeed($this->FK_group,'square') ? "" : " (площ.".$this->width*$this->length." м.кв.)";
            //MaterialUnit::findOne($this->FK_unit)->name.")";
        $thickness= !$this->IsNeed($this->FK_group,'thickness') ? "" : " (толщ.".$this->thickness." мм)";
        

        return  $info.$man.$width.$length.$color.$square.$thickness.$dencity;

    }

    public function IsNeed($id=0, $fieldnameToCheck=""){
        //TODO: Дописать поля
        if ($id>0 and $fieldnameToCheck != "")
            switch ($fieldnameToCheck) {
                case 'color':
                    return MaterialGroups::findOne($id)->need_color == 1;
                    break;
                case 'length':
                    return MaterialGroups::findOne($id)->need_length === 1;
                    break;
                case 'width':
                    return MaterialGroups::findOne($id)->need_width == 1;
                    break;
                case 'dencity':
                    return MaterialGroups::findOne($id)->need_dencity == 1;
                    break;
                case 'square':
                    return MaterialGroups::findOne($id)->need_square === 1;
                    break;
                case 'thickness':
                    return MaterialGroups::findOne($id)->need_thickness == 1;
                    break;
                default:
                    return false;
                break;
            }
        else
            return true;

    }
    public function getQualities()
    {
    	return $this->hasMany( MaterialQuality::className(), ['FK_material' => 'id']);
    }


    /*public static function getMaterials($type=null){
        if (!is_null($type))
        $out = ArrayHelper::map(Material::find()
            ->where("is_del = 0 and FK_machine_type=".$type)
            ->all(),'id','name');
        else
            $out = ArrayHelper::map(Material::find()
                ->where("is_del = 0 ")
                ->all(),'id','name');
        return $out;
    }*/
    public  function getMachineTypes()
    {
        return $this->hasOne( MachineType::className(), ['FK_machine_type' => 'id']);
    }

   /* public  function getMachineTypes2()
    {
        return $this->hasOne( MachineType::className(), ['FK_machine_type' => 'id']);
    }*/
}
