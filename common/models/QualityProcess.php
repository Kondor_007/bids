<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "quality_process".
 *
 * @property integer $id
 * @property string $name
 * @property integer $FK_process_type
 * @property integer $is_del
 */
class QualityProcess extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quality_process';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name' ], 'required'],
            [['FK_process_type','is_del'], 'integer'],
            [['name'], 'string', 'max' => 255],
            //[['is_del'], 'default' => 0]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'FK_process_type' => 'Fk Process Type',
            'is_del' => '�������',
        ];
    }
}
