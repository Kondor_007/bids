<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Calls;

/**
 * SearchCalls represents the model behind the search form about `common\models\Calls`.
 */
class SearchCalls extends Calls
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['src', 'dst', 'dt_start', 'dt_talk', 'dt_fin', 'recfile'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchByPhone($phone=null){
        if (is_null($phone))
            return $this->search();
        else
        {
            $query = Calls::find();
            //$query->where(['dst'=>$phone])->orderBy(['id' => SORT_DESC]);
            $query->orderBy(['id'=>SORT_DESC]);
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
            ]);

           /* $this->load($params);



            $query->andFilterWhere([
                'id' => $this->id,
                'src' => $this->dt_start,
                'dest_tl' => $this->dest_tl,
                'dt_talk' => $this->dt_talk,
                'dt_fin' => $this->dt_fin,
            ]);*/



            return $dataProvider;
        }
    }
    public function search($params)
    {
        $query = Calls::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        /*$this->load($params);
        print_r($this);
        exit();
        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }*/
        $query->orderBy(['id'=>SORT_DESC]);
        /*$query->andFilterWhere([
            'id' => $this->id,
            'dt_start' => $this->dt_start,
            //'dest_tl' => $this->dest_tl,
            'dt_talk' => $this->dt_talk,
            'dt_fin' => $this->dt_fin,
        ]);

        $query->andFilterWhere(['like', 'src', $this->src])
            ->andFilterWhere(['like', 'dst', $this->dst])
                ->andFilterWhere(['like', 'recfile', $this->recfile]);*/

        return $dataProvider;
    }
}
