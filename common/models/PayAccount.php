<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pay_account".
 *
 * @property integer $id
 * @property integer $FK_org
 * @property integer $FK_bank
 * @property string $account
 * @property string $description
 *
 * @property Bank $fKBank
 * @property Organisation $fKOrg
 */
class PayAccount extends \yii\db\ActiveRecord
{

    public $_bankName;
    public $_orgName;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pay_account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FK_org', 'FK_bank', 'account'], 'required'],
            [['FK_org', 'FK_bank'], 'integer'],
            [['account'], 'string', 'max' => 1024],
            [['description'], 'string', 'max' => 4096]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'orgName' => 'Организация ',
            'bankName' => 'Банк',
            'FK_org' => 'Организация ',
            'FK_bank' => 'Банк',
            'account' => 'Расчетный счет',
            'description' => 'Примечание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKBank()
    {
        return $this->hasOne(Bank::className(), ['id' => 'FK_bank']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBank()
    {
    	return $this->hasOne(Bank::className(), ['id' => 'FK_bank']);
    }

    public function getBankName()
    {
        return $this->fKBank->name;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKOrg()
    {
        return $this->hasOne(Organisation::className(), ['id' => 'FK_org']);
    }

    public function getOrgName()
    {
        return $this->fKOrg->short_name;
    }
}
