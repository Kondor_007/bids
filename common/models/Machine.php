<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "machine".
 *
 * @property integer $id
 * @property integer $FK_type
 * @property string $name
 * @property integer $FK_user
 */
class Machine extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'machine';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['FK_type', 'FK_user'], 'integer'],
        		[['FK_type'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['is_del'],'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'FK_type' => 'Fk Type',
            'name' => 'Наименование',
            'FK_user' => 'Fk User',
            'is_del' => 'Удалено',
        ];
    }
    
    public function getProcesstypemachine(){
        return $this->hasOne( ProcesstypeMachine::className(), ['id' => 'FK_machine'] );
    }

    
}
