<?php

namespace common\models;

use backend\controllers\ServiceController;
use Yii;
use common\models\Task;
use common\models\TaskStatus;
/**
 * This is the model class for table "request".
 *
 * @property integer $id
 * @property integer $FK_client
 * @property integer $FK_device
 * @property integer $FK_task
 * @property double $draft_cost
 * @property integer $FK_payment_type
 * @property double $payed_part
 * @property integer $FK_user_owner
 * @property string $description
 */
class Request extends \yii\db\ActiveRecord
{
	//public $deadline;
	public $device_type;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FK_client', 'FK_device', 'FK_task', 'FK_payment_type','FK_user_owner' ], 'integer'],
            [['draft_cost', 'payed_part'], 'number'],
            [['description','deadline'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№ заказа',
            'FK_client' => 'Fk Client',
            'FK_device' => 'Fk Device',
            'FK_task' => 'Fk Task',
            //'draft_cost' => 'Цена',
            'FK_payment_type' => 'Fk Payment Type',
            'payed_part' => 'Частичная оплата',
            'FK_user_owner' => 'Owner',            
            'description'=>'Описание',
            'cost' => 'Стоимость',
            'deadline' => 'Срок сдачи',
        		'statusName' => 'Уровень готовности, %',
        		'userownerName' => 'Постановщик',
        		'clientName' => 'Заказчик',
        		
        ];
    }


    public  function getMaterialsList() { // could be a static func as well
        ServiceController::actionAlert("getMaterialsList");
       /* $models = \common\models\City::find()->where(['country_id' => $cat_id])->asArray()->all();
        return ArrayHelper::map($models, 'id', 'name_ru','country_id');*/
    }

    public function getClientName()
    {
    	if( $this->client !== null )
    		return $this->client->fullName;
    	else 
    		return 'Не задан';
    }
    
    public function getClient()
    {
    	return $this->hasOne( UsExt::className(), [ 'FK_Users' => 'FK_client'] );
    }
    
    public function getDeviceName()
    {
    	if( isset( $this->device ) ) {
    		return $this->device->name;
    	} else {
    		return 'Не задан';	
    	}
    }
    
    public function getDevice()
    {
    	return $this->hasOne( Machine::className(), ['id' => 'FK_device'] );
    }
    
    public static function findEmpty()
    {
    	return self::findOne(['FK_client' => null, 'FK_task' => null, 'FK_device' => null, 'FK_user_owner' => \Yii::$app->user->identity->id ]);
    }
//----------------------------------------------
    public function getUserownerName()
    {
    	if( $this->userowner !== null )
    		return $this->userowner->fullName;
    	else 
    		return 'Не задан';
    }

    public function getUserowner()
    {
    	return $this->hasOne( User::className(), [ 'id' => 'FK_user_owner'] );
    }

    public function getOwnerID()
    {
        return $this->FK_user_owner;
    }
    public function getIsOwner($userid)//Это постановщик?
    {
        return Request::findOne($this->id)->getOwnerID() == $userid;
    }
    public function getIsWorker($userid)//Это исполнитель?
    {
        $req=Request::findOne($this->id);
        $arr = RequestProcess::find()->where(
            'FK_user='.$userid
        )->andWhere("FK_request='".$req->id."'")->all();
       /* print_r(  $arr);
        print( count( $arr). " ".$userid);*/
        /*print_r($req);
        exit();*/

        return count( $arr)>0;
    }
    //----------------------------------------
    public function getTask()
    {
    	return $this->hasOne( Task::className(), [ 'id' => 'FK_task'] );
    }
    
    public function getTaskStatus()
    {
    	return $this->hasOne( TaskStatus::className(), [ 'FK_task' => 'FK_task'] );
    }
    
    public function getTaskResponsible()
    {
    	return $this->hasOne( TaskResponsible::className(), [ 'FK_task' => 'FK_task'] );
    }

    public function getTaskStatuses()
    {
        return $this->hasMany(TaskStatus::className(), ['FK_task' => 'FK_task']);
    }

    public function getCurrentStatus()
    {
        foreach ($this->taskStatuses as $key => $status) 
        {
            if( $status->is_top == 1 )
            {
                return $status->statusTypeId;
            }
        }
    }
public function getStatusName(){
        //$model = TaskStatusRef::findOne($this->currentStatus); // модель request
        //$model = \Request::findOne($condition)
        //По невзятым в работу участкам прогресс 0%
        $uchastki=RequestProcess::find()->where('FK_request='.$this->id)->all();
        //return count($uchastki);
        if (count($uchastki)>0){
        //return $model->name;//'не пойму как его вытащить';
        /*
         * */
               $todo='TODO: in getStateRequest - вынести в модель состояний!';
             //FIXED

       // ServiceController::actionConsoleLog($todo);
        $procent=0;
         foreach($uchastki as $iterator){

             $procent+=RequestSectorStates::getProgressByCode($iterator->state);
        	/*if ($iterator->state == 0) $procent+=0; // назначен, но не взят в работу
        	if ($iterator->state == 1) $procent+=10;// взят в работу
        	if ($iterator->state == 2) $procent+=10; // приостановлен
        	if ($iterator->state == 3) $procent+=90; //сдан наконтроль
        	if ($iterator->state == 4) $procent+=100; // контроль прошел (принят)
        	if ($iterator->state == 5) $procent+=0; // Отказ от выполнения
        	 */
        }
        return $procent/count($uchastki);
       } 
        else
        	return "процент готовности";// пусто
           
    
    
}
	   }