<?php

namespace common\models;

use Yii;
use yii\base\Model;



/**
 * This is the model class for table "request_process_type".
 *
 * @property integer $id
 * @property string $name
 */
class ProcessType extends \yii\db\ActiveRecord
{
    public   $mname="";
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request_process_type';
    }

    public function getmname()
    {
        return  $this->mname;
    }
    /**
     * @inheritdoc
     */

//    public  $MachName='test';


    public function rules()
    {
        return [
            [['id','is_del'], 'integer'],
            [['name'], 'string'],
            [['fk_p_type_unit'], 'string'],
            [['fk_machine', 'mname'], 'safe'],
            [['mname'],  'string'],
            [['price_one_unit'],  'double'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Способ обработки',
            'is_del' => 'Удалено',
            'fk_p_type_unit' => 'Fk P Type Unit',
            'fk_machine' => 'Fk Machine',
            'machine_name' => 'Устройство',
            //'mname' => 'УстройствоN',
            'price_one_unit' => 'Цена за 1 ед. изм.',
            //'device'=>""
        ];
    }

    public  function  getUnits()
    {
     //   return $this->hasMany(MaterialUnit::className(), [])
        return false;
    }


    /* Связь с моделью PrecesstypeMachine*/
    public function getFK_machine_type()
    {
        return $this->hasOne(ProcesstypeMachine::className(), ['id' => 'FK_process_type']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkPTypeUnit()
    {
        return $this->hasOne(ReqProcessUnit::className(), ['id' => 'fk_p_type_unit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkMachine()
    {
        //return $this->hasOne(Machine::className(), ['id' => 'fk_machine']);
        return $this->hasOne(MachineType::className(), ['id' => 'fk_machine']);
    }
    //TODO: DBase
    public  function  getProperties(){
        return ['Длина реза', 'Площадь'];
    }
    public function  getPrice(){
        return $this->req_process_unit->price;
    }
    //public function get
    public function getQualityprocess()
    {
        return $this->hasMany( QualityProcess::className(), ['id' => 'FK_process_type']);    
    }

    public function getMachineType () {

        return  MachineType::findOne($this->fk_machine)->name;
    }
}
