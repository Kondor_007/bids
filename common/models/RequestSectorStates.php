<?php

namespace common\models;

use backend\controllers\ServiceController;
use Yii;
use common\models\Task;
use common\models\TaskStatus;


/*
 * В классе RequestSectorStates храню коды состояний этапа, описание и прогресс.
 * Туда же можно добавить и другую инф если понадобится.
 * По идее  RequestSectorStates  есть справочник для  CodesStates, где лежат
 * коды состояний
 * */
class CodesStates{
    public static $NotInWork=0,
        $AlreadyInWork=1,
    $Paused=2,
    $Vidacha=3,
    $Completed=4,
    $Cancelled=5
;
}
class RequestSectorStates
{
   // $en=[]
	//public $deadline;
    public static $States = [
        ['code'=>0, 'label'=>'Назначен, но не взят в работу', 'progress'=>0],
        ['code'=>1, 'label'=>'Взят в работу', 'progress'=>10],
        ['code'=>2, 'label'=>'Приостановлен', 'progress'=>10],
        ['code'=>3, 'label'=>'Сдан на выдачу', 'progress'=>100],//или 100?
        ['code'=>4, 'label'=>'Завершен', 'progress'=>100],
        ['code'=>5, 'label'=>'Отказ от выполнения', 'progress'=>0],

    ];


    public static function getProgressByCode($code){
      //  RequestSectorStates::$States[0]['code']= CodesStates::$NotInWork;
      //  print_r(RequestSectorStates::$States);
      //  print_r(RequestSectorStates::$States[$code]['label']);
      // exit();

        if (isset(RequestSectorStates::$States[$code]))
        return RequestSectorStates::$States[$code]['progress'];
        else
            return 1;
    }
    public static function getLabelByCode($code){
        //print_r(RequestSectorStates::$States);
      //  print_r(RequestSectorStates::$States[$code]['label']);

        if (isset(RequestSectorStates::$States[$code]))
        return RequestSectorStates::$States[$code]['label'];
        else
           return 1;
    }







	   }

