<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "processtype_machine".
 *
 * @property integer $id
 * @property integer $FK_process_type
 * @property integer $FK_machine
 */
class ProcesstypeMachine extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'processtype_machine';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FK_process_type', 'FK_machine'], 'required'],
            [['FK_process_type', 'FK_machine'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'FK_process_type' => 'Fk Process Type',
            'FK_machine' => 'Fk Machine',
        ];
    }
    
    public function getProcesstype()
    {
        return $this->hasOne( ProcessType::className(), ['FK_process_type' => 'id'] );
    }


    public  function  getMachine(){
        return $this->hasOne(Machine::className(), ['id' => 'FK_process_type']);
    }

    public function getName()
    {
        if( isset( $this->processtype) ) {
    		return $this->processtype->name;
    	} else {
    		return '��� �������� �����������';	
    	}
    }
}
