<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "request".
 *
 * @property integer $id
 * @property integer $FK_client
 * @property integer $FK_device
 * @property integer $FK_task
 * @property double $draft_cost
 * @property integer $FK_payment_type
 * @property double $payed_part
 */
class RequestSearch extends Request
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FK_client', 'FK_device', 'FK_task', 'FK_payment_type'], 'integer'],
            [['draft_cost', 'payed_part'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'FK_client' => 'Fk Client',
            'FK_device' => 'Fk Device',
            'FK_task' => 'Fk Task',
            'draft_cost' => 'Draft Cost',
            'FK_payment_type' => 'Fk Payment Type',
            'payed_part' => 'Payed Part',
        ];
    }
    
    public function search($params)
    {
    	$query = Request::find();
    	
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    	]);
       	
    	$this->load($params);
    	
    	if( !$this->validate() ){
    		return $dataProvider;
    	}
    	
        //$list = ArrayHelper::getColumn(\backend\models\rbac\AuthAssignment::findAll(['user_id'=>\yii::$app->user->identity->id]),'item_name');

        
                
        if (in_array('manager',UsExt::getItemName())){
            $query->joinWith(['client','userowner', 'task','taskStatuses','taskResponsible'],false,' INNER JOIN');
                
            
        } else {
            // ��� ������� �� ������������ � �� ������� ������������
            $query->joinWith(['client','userowner', 'task','taskStatuses','taskResponsible'],false,' INNER JOIN')
                    ->andWhere(['task_responsible.FK_user'=>\yii::$app->user->identity->id])
                    ->orWhere(['task_responsible.FK_role' => UsExt::getItemName()]);
                    
            
        } 
        
    	return $dataProvider;
    }
}







