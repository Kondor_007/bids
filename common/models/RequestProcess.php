<?php

namespace common\models;

use common\models\RequestDraft;

use Yii;

/**
 * This is the model class for table "request_process".
 *
 * @property integer $id
 * @property integer $amount
 * @property double $price
 * @property double $cost
 */
class RequestProcess extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request_process';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount','type_resp', 'FK_request', 'FK_process_type', 'FK_draft'], 'integer'],
            [['price', 'cost'], 'number'],
        	['FK_role','string'],
        		['was_in_work','integer']
        ];
    }

    /**
     * @inheritdoc
     */

    public function attributeLabels()
    {
        $label_sector_rodit_padej = 'этапа';
        return [
            'id' => 'ID',
            'amount' => 'Количество повторений '.$label_sector_rodit_padej,
        		'deadline' => 'Дата завершения '.$label_sector_rodit_padej,
            'price' => 'Цена',
            'cost' => 'Стоимость '.$label_sector_rodit_padej,
        	'typeName' => 'Тип работы',
        		'FK_role'=>'Код группы',
        ];
    }
    
    public function typeControl()
    {
    	if( $this->type == 4 ) {
    		$this->FK_role = $this->FK_user;
    		$this->FK_user= null;
    	}
    }
    public function getType()
    {
    	return $this->hasOne( ProcessType::className(), ['id' => 'FK_process_type'] );
    }
    
    public function getDraftName(){        
        if( isset( $this->draft ) ) {
    		return $this->draft->name;
    	} else {
    		return 'Не установлен';	
    	}
    }
    
    
    public function getDraft()
    {
    	return $this->hasOne( RequestDraft::className(), [ 'id' => 'FK_draft'] );
    }
    
}
