<?php

namespace common\models;

use Yii;
use common\models\User;
use backend\models\rbac\AuthItem;
use backend\models\rbac\AuthAssignment;
use yii\helpers\ArrayHelper;

/**
 * Расширение таблицы пользователей ( profile )
 *
 * @property integer $ID
 * @property integer $FK_Users
 * @property string $LastName
 * @property string $FirstName
 * @property string $PatName
 * @property integer $FK_org
 * @property string $Photo
 */
class UsExt extends \yii\db\ActiveRecord
{
    public $_fullName;      // ФИО
    public $_shortName;
    public $_jobs;
    public $_orgName;

    public $_currentUserId;

    // Свойства для добавления и обновления связанной таблицы
    public $newUsername;    // Новое имя пользователя
    public $newEmail;       // Новый email
    public $newPassword;    // Новый пароль

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'us_ext';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'FK_Users' ], 'required'],
            [['FK_Users', 'FK_org'], 'integer'],
            [['newPassword', 'jobs', 'orgName'], 'string', 'max'=>1024 ],
            [['newEmail', 'newUsername'], 'required', 'on' => 'userUpdate'],
            [['FirstName'], 'required', 'on' => 'createByManager'],
            [['LastName', 'FirstName', 'PatName'], 'string', 'max' => 1024],
            [['Photo'], 'string', 'max' => 4096],
            [['phone'], 'string', 'max' => 4096]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            // Поля таблицы
            'ID' => 'ID',
            'LastName' => 'Фамилия',
            'FirstName' => 'Имя',
            'PatName' => 'Отчество',
            'Photo' => 'Фото',
            // Добавленные свойства класса
            'fullName' => 'ФИО',
            'newUsername'=>'Логин',
            'newPassword'=>'Пароль',
            'newEmail'=>'Email',
            // Связанные поля
            // User
            'username'=>'Логин',
            'email'=>'Email',
            'phone'=>'Телефон',
            // UsJobs
            'jobs' => 'Должность',
        		'FK_org' => 'Организация',
        		'orgName' => 'Организация',
        ];
    }

    //------- Событие afterSave()
    public function afterSave( $insert, $changedAttributes )
    {
        parent::afterSave( $insert, $changedAttributes );
        
        if( !$insert )
        {
            $model = User::findOne( $this->FK_Users );
            $model->username = $this->newUsername;
            $model->email = $this->newEmail;

            $model->update();
        }  
    }

    public function generateUsername()
    {
        $initials = $this->LastName;
        $firstName = $this->FirstName;
        $patName = $this->PatName;
        $initials .= trim( substr( $firstName, 0, 2 ) );
        $initials .= trim( substr( $patName, 0, 2 ) );

        return $this->rus2translit( $initials );
    }
    
    //---- Геттер ФИО
    public function getFullName()
    {
        return $this->LastName.' '.$this->FirstName.' '.$this->PatName;
    }

    //---- Геттер "Имя Фамилия"
    public function getShortName()
    {
        return $this->FirstName.' '.$this->LastName;
    }
    
    /**
     * Для совместимости 'ID' и 'id'
     * @return number
     */
    public function getId()
    {
    	return $this->ID;
    }


    /************************************************************
    --------- Геттеры таблицы пользователей ---------------------
    *************************************************************/
    
    //--- Геттер связи с таблицей таблицей пользователей ----------
    public function getLoginInfo()
    {
        return $this->hasOne( User::className(), ['id' => 'FK_Users'] );
    }
    
    //---- Имя пользователя
    public function getUsername()
    {
        return $this->loginInfo->username;
    }

    
    //---- Email
    public function getEmail()
    {
        return $this->loginInfo->email;
    }

    /************************************************************
    ----- Геттеры таблицы должностей ( роль == должность )-------
    *************************************************************/
    public function getAssignment()
    {
        return $this->hasMany( AuthAssignment::className(), [ 'user_id' => 'FK_Users' ] );
    }

    //--- Геттер строки должностей раздел. ',' ----------
    public function getJobs()
    {
        $jobsString = '';

        foreach ($this->assignment as $value) 
        {
            $jobsString .= ' '.$value->roleDescription.',';
        }
                
        $jobsString = trim( $jobsString, ', ' );
        return $jobsString;
    }

    /************************************************************
    ----- Геттеры таблицы организаций ---------------------------
    *************************************************************/
    public function getOrg()
    {
        return $this->hasOne( Organisation::className(), ['id' => 'FK_org']  );
    }

    public function getOrgName()
    {
        if ( $this->org != null )
            return $this->org->short_name;
    }

    /*
    * ************** Статические методы *******************
    */
    //---- Статический геттер массива сотрудников организации текущего пользователя
    public static function getOwnOrgEmployees()
    {
        $model = UsExt::getCurrentUserProfile();
        
        return UsExt::getEmployeesByOrgId( $model->FK_org );
    }
    
    public static function getClients()
    {
    	$user = self::getCurrentUserProfile();
    	return self::find()->where("FK_org <> $user->FK_org")->all();
    }
    
    public static function getFullList()
    {
    	return ArrayHelper::map( self::find()->all(), 'ID', 'fullName' );
    }

    //---- Статический геттер массива сотрудников по ID организации
    public static function getEmployeesByOrgId( $orgId )
    {
        $employeesArray = UsExt::find()->where([ 'FK_org' => $orgId ])->all();

        return $employeesArray;
    }

    //----- Статический геттер ID профиля текущего пользователя
    public static function getCurrentUserId()
    {
        return UsExt::getCurrentUserProfile()->ID;
    }

    //----- Статический геттер телефона текущего пользователя
    public static function getCurrentUserPhone()
    {
        return UsExt::getCurrentUserProfile()->phone;
    }
//----- Статический геттер ринггруппы текущего пользователя
    public static function getCurrentUserRgroup()
    {
        return UsExt::getCurrentUserProfile()->ringG;
    }
    //----- Статический геттер модел профиля
    public static function getCurrentUserProfile()
    {
        return UsExt::find()->where(['FK_Users' => \Yii::$app->user->identity->id ])->one();
    }
    //-----------------------------------------------------------


    private function rus2translit($string) {
        $converter = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '',    'ы' => 'y',   'ъ' => '',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
        
        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '',  'Ы' => 'Y',     'Ъ' => '',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        );
        return strtr($string, $converter);
    }
    
    public static function getItemName(){
        return ArrayHelper::getColumn(\backend\models\rbac\AuthAssignment::findAll(['user_id'=>\yii::$app->user->identity->id]),'item_name');
    }

}
