<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "calls".
 *
 * @property integer $id
 * @property string $src
 * @property string $dst
 * @property string $dt_start
 * @property integer $dest_tl
 * @property string $dt_talk
 * @property string $dt_fin
 * @property string $recfile
 */
class Calls extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        //Table was renamed calls=>info
        return 'info';
        //return 'calls';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['src', 'dst', 'dt_start', 'dest_tl', 'dt_talk', 'dt_fin', 'recfile'], 'required'],
            [['dt_start', 'dt_talk', 'dt_fin'], 'safe'],

            [['src', 'dst'], 'string', 'max' => 100],
            [['recfile'], 'string', 'max' => 256]

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№ записи',
            'src' => '№ тел. откуда',
            'dst' => '№ тел. куда',
            'dt_start' => 'Дата_время начала звонка',

            'dt_talk' => 'Дата_время начала разговора',
            'dt_fin' => 'Дата_время окончания',
            'recfile' => 'Путь к файлу записи разговора',
        ];
    }

    //пропущен если есть дата окончания и нет даты поднятия трубки
    public function IsMissed(){
        return is_null($this->dt_talk) && !is_null($this->dt_fin);
    }

    //Состояние звонка - пропущен, звонит, принят
    public function getStateCall(){
        $state="Ожидает";
        if (is_null($this->dt_talk) && is_null($this->dt_fin))
        $state= "Ожидает";
        else
          ($this->IsMissed()) ? $state= "Пропущен" :  $state="Принят";
        return $state;
    }
}
