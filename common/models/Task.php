<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property string $name
 * @property integer $FK_project
 * @property integer $parent_id
 * @property integer $FK_originator
 * @property string $desription
 * @property string $date
 *
 * @property Project $fKProject
 * @property Task $parent
 * @property Task[] $tasks
 * @property UsExt $fKOriginator
 * @property TaskResponsible[] $taskResponsibles
 * @property TaskStaus[] $taskStatuses
 */
class Task extends \yii\db\ActiveRecord
{
    public $_originatorName;
    public $_responsiblesString;

    public $_currentStatus;

    const TASK_SHOW_ALL         = 0;
    const TASK_SHOW_INSTRUCTED  = 1;
    const TASK_SHOW_EXEC        = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'FK_originator'], 'required'],
            [['FK_project', 'parent_id', 'FK_originator','FK_responsible', 'priority'], 'integer'],
            [['date', 'originatorName'], 'safe'],
            [['name'], 'string', 'max' => 4096],
            [['FK_cid'], 'string', 'max' => 256],
            [['description'], 'string', 'max' => 8192],
            [['fk_info'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'originatorName' => 'Постановщик',
            'name' => 'Название задачи',
            'FK_project' => 'Внешний ключ на проект',
            'parent_id' => 'Внутренний ключ, обеспечивает иерархию задач',
            'FK_originator' => 'ID пользователя создавшего задачу',
            'FK_responsible' => 'ID пользователя принявшего заявку',
            'description' => 'Описание задачи',
            'date' => 'Дата постановки задачи',
            'priority' => 'Приоритет',
            'FK_cid'=>'Звонивший №',
            'fk_info' => 'Ссылка на запись в табилце Info',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'FK_project']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Task::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKOriginator()
    {
        return $this->hasOne(User::className(), ['id' => 'FK_originator']);
    }

    public function getOriginatorName()
    {
        return $this->fKOriginator->profile->fullName;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskResponsibles()
    {
        return $this->hasMany(TaskResponsible::className(), ['FK_task' => 'id'])->where( ['type' => 1] );
    }

    public function getTaskResponsible()
    {
        return $this->hasOne(TaskResponsible::className(), ['FK_task' => 'id'])->where( ['type' => 1] );
    }

    public function getChildren()
    {
        return $this->hasMany( Task::className(), [ 'parent_id' => 'id' ] )->where( [ 'active' => 1 ] );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskStatuses()
    {
        return $this->hasMany(TaskStatus::className(), ['FK_task' => 'id']);
    }

    public function getCurrentStatus()
    {
        foreach ($this->taskStatuses as $key => $status) 
        {
            if( $status->is_top == 1 )
            {
                return $status->statusTypeId;
            }
        }
    }

    public function getResponsibleName()
    {
        $respString = '';
        $usres=UsExt::findOne($this->FK_responsible);
        if (!is_null($usres))
            return  $usres->getFullName();
        else
            return "НЕ задан";
       /* foreach ($this->taskResponsibles as $value)
        {
            $respString .= ' '.$value->name.',';
        }

        $respString = trim( $respString, ', ' );
        return $respString;*/
    }
    public function getResponsiblesString()
    {
        $respString = '';

        foreach ($this->taskResponsibles as $value) 
        {
            $respString .= ' '.$value->name.',';
        }
                
        $respString = trim( $respString, ', ' );
        return $respString;
    }

    public function isOriginator()
    {
        return ( \Yii::$app->user->identity->getId() == $this->FK_originator );
    }

    public function isResponsible()
    {
        $userId = \Yii::$app->user->identity->id;
        foreach ($this->taskResponsibles as $key => $value) 
        {
            if( $value->FK_user === $userId )
            {
                return true;
            }
        }
        return false;
    }
}
