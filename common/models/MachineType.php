<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "machine_type".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 */
class MachineType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'machine_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias'], 'string', 'max' => 255],
        		['name','required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название типа',
            'alias' => 'Псевдоним',
        ];
    }
    
    public function getMachines()
    {
    	return $this->hasMany( Machine::className(), ['FK_type' => 'id'] );
    }

    public function getMachineType () {

            return  MachineType::findOne($this->fk_machine)->name;
    }
}
