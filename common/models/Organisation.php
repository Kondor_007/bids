<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "organisation".
 *
 * @property integer $id
 * @property string $short_name
 * @property string $full_name
 * @property integer $FK_us_ext
 * @property integer $FK_org_type
 * @property string $INN
 * @property string $OGRN
 * @property string $KPP
 * @property string $OKATO
 * @property string $act_on_authority
 * @property string $phone
 * @property string $email
 * @property string $description
 *
 * @property UsExt $fKUsExt
 * @property OrgType $fKOrgType
 */
class Organisation extends \yii\db\ActiveRecord
{
	const TYPE_OWNER 	  = 1;
	
    public $_headName;
    public $_orgType;
    public $_headJobs;
    public $_payAccCount;

    public $_legalAdr;
    public $_localAdr;
    public $_postAdr;

    public $_fieldName;
    public $_headcount;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organisation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['short_name'], 'required'],
            [['FK_us_ext', 'FK_org_type' ], 'integer'],
            [['short_name', 'full_name', 'INN', 'OGRN', 'KPP', 'OKATO', 'act_on_authority', 'phone', 'email', 'headName'], 'string', 'max' => 1024],
            [['description', 'headJobs'], 'string', 'max' => 4096],
            [['address', 'web_site', 'address_legal', 'address_post'], 'string', 'max' => 4096],
            [['FK_field_of_activity', 'FK_headcount'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'short_name' => 'Краткое название',
            'full_name' => 'Полное название',
            'FK_us_ext' => 'Руководитель',
            'FK_org_type' => 'Тип организвации',
            'INN' => 'ИНН',
            'headName' => 'Руководитель',
            'orgType' => 'Тип организации',
            'headJobs'=> 'Должность руководителя',
            'OGRN' => 'ОГРН',
            'KPP' => 'КПП',
            'OKATO' => 'ОКАТО',
            'act_on_authority' => 'Действует на основании',
            'address_legal' => 'Юридический адрес',
            'address_post' => 'Почтовый адрес',
            'legalAdr' => 'Юридический адрес',
            'localAdr' => 'Фактический адрес',
            'postAdr' => 'Почтовый адрес',
            'FK_headcount' => 'Количество сотрудников',
            'FK_field_of_activity' => 'Область деятельности',
            'phone' => 'Контактный телефон',
            'email' => 'Email',
            'web_site' => 'Веб-сайт',
            'description' => 'Описание',
            'address' => 'Фактический адрес',
            'payAccCount' => 'Количество расч. счетов',
            'fieldName' => 'Сфера деятельности',
            'headcount' => 'Количество сотрудников'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKUsExt()
    {
        return $this->hasOne( UsExt::className(), ['ID' => 'FK_us_ext'] );
    }

    public function getHeadName()
    {
        if ( $this->fKUsExt != null ) 
            return $this->fKUsExt->fullName;
    }

    public function getHeadJobs()
    {
        if ( $this->fKUsExt != null ) 
            return $this->fKUsExt->jobs;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKOrgType()
    {
        return $this->hasOne( OrgType::className(), ['id' => 'FK_org_type'] );
    }    

    public function getFKFieldOfActivity()
    {
        return $this->hasOne( OrgActivityField::className(), [ 'id' => 'FK_field_of_activity' ] );
    }

    public function getFKHeadcount()
    {
        return $this->hasOne( OrgHeadcount::className(), ['id' => 'FK_headcount'] );
    }

   
    public function getOrgType()
    {
        return $this->fKOrgType->name;
    }

    public function getPayAccCount()
    {
        return count( PayAccount::find()->where( [ 'FK_org'=>$this->id ] )->asArray()->all() );
    }
    
    public function getPayAccount()
    {
    	return $this->hasOne( PayAccount::className(), ['FK_org' => 'id'] );
    }

    public function getFieldName()
    {
        if( $this->FK_field_of_activity !== NULL )
            return $this->fKFieldOfActivity->name;
    }

    public function getHeadcount()
    {
        if( $this->FK_headcount !== NULL )
            return $this->fKHeadcount->count;
        else
            return 'Неизвестно';
    }
    
    public function getName()
    {
    	return $this->short_name;
    }
    
    public function getContracts()
    {
    	return $this->hasMany( Contract::className(), ['FK_organisation'=>'id'] );
    }
    
    public function getContract()
    {
    	return $this->hasOne( Contract::className(), ['FK_organisation'=>'id'] );
    }
    
    public static function getContractById( $id )
    {
    	return Contract::findOne($id);
    }
}
