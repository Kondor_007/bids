<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "document".
 *
 * @property integer $id
 * @property integer $FK_user
 * @property integer $FK_request
 * @property integer $FK_material
 * @property integer $FK_type
 * @property integer $amount
 */
class Document extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FK_user', 'FK_request', 'FK_material', 'FK_type', 'amount'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'FK_user' => 'Fk User',
            'FK_request' => 'Fk Request',
            'FK_material' => 'Fk Material',
            'FK_type' => 'Fk Type',
            'amount' => 'Amount',
        ];
    }
}
