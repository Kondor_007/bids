<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "req_process_unit".
 *
 * @property integer $id
 * @property string $name
 * @property double $price
 *
 * @property RequestProcessType[] $requestProcessTypes
 */
class ReqProcessUnit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'req_process_unit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['price'], 'number'],
            [['name'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessTypes()
    {
        return $this->hasMany(RequestProcessType::className(), ['fk_p_type_unit' => 'id']);
    }
}
