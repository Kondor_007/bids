<?php

namespace common\models;

use backend\controllers\ServiceController;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Material;

/**
 * SearchMaterial represents the model behind the search form about `common\models\Material`.
 */
class SearchMaterial extends Material
{
    /**
     * @inheritdoc
     */

    /* ����������� ���� */
    public $fullName;
    public function rules()
    {
        return [
            [['id', 'FK_unit', 'FK_machine_type', 'is_del', 'count'], 'integer'],
            [['name', 'prop_name', 'prop_value', 'color'], 'safe'],
            [['issue_limit', 'buy_price', 'sell_price', 'dencity', 'thickness', 'width'], 'number'],
            [['fullName'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //ServiceController::actionAlert("!!!");
        $query = Material::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'query'
        ]);
        ServiceController::actionAlert("SearchModel".$this->name);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

            //$query->and
        $query->andFilterWhere([
            'id' => $this->id,
            'FK_unit' => $this->FK_unit,
            'name' => $this->FK_unit,
            'issue_limit' => $this->issue_limit,
            'buy_price' => $this->buy_price,
            'sell_price' => $this->sell_price,
           // 'manufacturer'=>$this->manufacturer,
            'FK_machine_type' => $this->FK_machine_type,
            'is_del' => $this->is_del,
            'count' => $this->count,
            'dencity' => $this->dencity,
            'thickness' => $this->thickness,
            'width' => $this->width,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name
        ])
           // ->andFilterWhere(['like', 'prop_name', $this->prop_name])
            //->andFilterWhere(['like', 'prop_value', $this->prop_value])
               //$query->andFilterWhere(['like','CONCAT(name,manufacturer,width,color,thickness)',$filter]);
            ->andFilterWhere(['=', 'color', $this->color]);


      /*  $query->andWhere('name LIKE "%' . $this->fullName . '%" ' .
            'OR color LIKE "%' . $this->fullName . '%"'
        );*/
        return $dataProvider;
    }

    public function searchByLike($params)
    {

        $query = Material::find()
        ->andFilterWhere(['=', 'is_del', 0]);
        if (isset($params['name']))
            $query->andFilterWhere(['like','CONCAT(name,manufacturer,width,color,thickness)',$params['name']]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
