<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "request_process_type".
 *
 * @property integer $id
 * @property string $name
 * @property integer $is_del
 * @property integer $fk_p_type_unit
 * @property integer $fk_machine
 *
 * @property MachineType $fkMachine
 * @property ReqProcessUnit $fkPTypeUnit
 */
class RequestProcessTypeMy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request_process_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_del', 'fk_p_type_unit', 'fk_machine'], 'required'],
            [['is_del', 'fk_p_type_unit', 'fk_machine'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'is_del' => 'Is Del',
            'fk_p_type_unit' => 'Fk P Type Unit',
            'fk_machine' => 'Fk Machine',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkMachine()
    {
        return $this->hasOne(MachineType::className(), ['id' => 'fk_machine']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkPTypeUnit()
    {
        return $this->hasOne(ReqProcessUnit::className(), ['id' => 'fk_p_type_unit']);
    }
}
