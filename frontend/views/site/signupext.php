<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UsExt */
/* @var $form ActiveForm */
?>
<div class="usext-UserList">
    <h2>Форма регистрации</h2>
    <?php $form = ActiveForm::begin(); ?>
        <div class="form-group">
            <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary', 'name'=>'signup-button']) ?>
        </div>
        
        <div class = 'form-data-block'>
            <h3>Данные для входа</h3>
            <?= $form->field($model, 'username') ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
        </div>  
        <br>  
        <div class = 'form-data-block'>
            <h3>Фамилия имя отчество пользователя</h3>
            <?= $form->field($model, 'lastName') ?>
            <?= $form->field($model, 'firstName') ?>
            <?= $form->field($model, 'patName') ?>
        </div>
        <br>  
        <div class = 'form-data-block'>
            <h3>Контакты</h3>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'userPhone') ?>
            <?= $form->field($model, 'photo') ?>
        </div>
        <br>  
    <?php ActiveForm::end(); ?>

</div><!-- usext-UserList -->
