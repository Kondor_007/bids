<?php 

namespace common\assets;

use yii\web\AssetBundle;

class TaskAsset extends AssetBundle
{
	public $basePath = '@common/JS/Task';
	public $baseUrl = '@common/JS/Task';

	public $css = 
	[

	];

	public $js = 
	[
		'taskManager.js',
		'addTaskModal.js',
	];

	public $depends = [
    ];
}
?>