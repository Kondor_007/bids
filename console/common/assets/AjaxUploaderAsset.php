<?php 

namespace common\assets;

use yii\web\AssetBundle;

class AjaxUploaderAsset extends AssetBundle
{
	public $basePath = '@common/JS';
	public $baseUrl = '@common/JS';

	public $css = 
	[

	];

	public $js = 
	[	
		'plugins/jquery.ajaxupload.js',
		'ajaxUploader.js',
	];

	public $depends = [
		'yii\web\JqueryAsset',
    ];
}

?>