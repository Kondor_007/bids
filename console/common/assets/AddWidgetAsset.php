<?php 

namespace common\assets;

use yii\web\AssetBundle;

class AddWidgetAsset extends AssetBundle
{
	public $basePath = '@common/JS';
	public $baseUrl = '@common/JS';

	public $css = 
	[

	];

	public $js = 
	[
		'addWidget.js',
	];

	public $depends = [
        
    ];
}

?>