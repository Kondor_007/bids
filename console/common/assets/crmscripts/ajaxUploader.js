$( document ).ready( function(){
	var btnUpload = $('#upload');
	var status = $('#status');

	btnUpload.click( startImport(e) );

	function startImport( e )
	{
		$('#upload-form').ajaxUpload(
		{
			url: 'index.php?r=storage/sklad/import',
			beforeSend: function( input ){
				$('body').prepend('<div class="overlay">');
				$('body').append('</div>');
				$(".overlay").css({
			    "position": "absolute", 
			    "width": $(document).width(), 
			    "height": $(document).height(),
			    "z-index": 99999, 
				}).fadeTo(0, 0.8);
				status.text('Загрузка...');
				$('#warning').text('Не закрывайте и не обновляйте данную стрницу!');
			},
			success: function ( responseData, statusText )
			{	
				var response = JSON.parse( responseData );
				//console.log( response );
				if( response.status == 1 )
				{	
					$('#fileName').text( response.file );
					status.html('<font color = green>Загружено</font>');
					parseData();
				}
				else 
				{
					success = 'true';
					$(".overlay").remove();
					status.html('<font color = green>Ошибка</font>');
					return;
				}
			},
			complete: function( responseData ){
				//return responseData;
				//console.log( responseData );
				//Очищаем текст статуса
				//var response = JSON.parse( responseData );
				
			},

		});
	}

	function parseData()
	{
		$.ajax({
			url: 'index.php?r=storage/sklad/parse-temp-table',
			type: 'POST',
			success: function( responseData, statusText )
			{
				var response = JSON.parse( responseData );
				
				switch( response.status )
				{
					case 0:
						$(".overlay").remove();
						$console.log( response.message );
						$.pjax.reload({container:'#sklad-grid'});
					break;

					case 1:
						$('#status').html('<font color = blue>Обрабатывается</font>&nbsp' + response.process );
						parseData();
					break;

					case 99:
						$(".overlay").remove();
						$('#status').html('<font color = red>Ошибка</font>&nbsp' + response.error );
					break;

				}
			}
		});
	}
});
