$(document).ready( function()
{
	var userId = getUserId();
	var editingRecord = false;

	$('#originator').text( 'Постановщик: ' + getUserName() );

	fillResponsibleList();
	fillProjectList();
	fillTaskList();

	$('.editTaskButton').click( setEditingFlag );
	/*
	* На изменение состояния чекбокса "Крайний срок"
	*/
	$('#is-deadline').change( function()
	{
		// Если чекнут показываем блок с датой
		if( $(this).is(':checked') )
		{
			$('#row-deadline').removeAttr('hidden');
		}
		else //либо НЕТ
		{
			$('#row-deadline').attr('hidden', true);
		}
	});

	/*
	* На изменение состояния чекбокса "Ассоциировать с проектом"
	* показываем/скрываем блок со списком проектов.
	*/
	$('#bind-with-project').change( function()
	{
		if( $(this).is(':checked') )
		{
			$('#row-project').removeAttr('hidden');
		}
		else
		{
			$('#row-project').attr('hidden', true);
		}
	});

	/*
	* На изменение состояния чекбокса "Указать базовую задачу"
	* показываем/скрываем блок со списком задач.
	*/
	$('#choose-parent-task').change( function()
	{
		if( $(this).is(':checked') )
		{
			$('#row-parent-task').removeAttr('hidden');
		}
		else
		{
			$('#row-parent-task').attr('hidden', true);
		}
	});

	/*
	* Событие клика по кнопке "Принять"
	*/
	$('#task-create').click( function()
	{
		if( validateForm() )
		{
			submitForm();
		}
	});

	/*
	* Склеивае6м запрос к серверу на добавление задачи и отправляем его
	*/
	function submitForm()
	{
		var requestData = '&Task[name]=' + $('#task-name').val() +
				   '&Task[priority]=' + $('.priority-select > :checked').val() +
				   '&Task[description]=' + $('#task-description').text() + 
				   '&responsible=' + $('#responsible').val();
		
		if( $('#bind-with-project').is(':checked') )
		{
			requestData += '&Task[FK_project]=' + $('#project-list').val();
		}

		if( $('#choose-parent-task').is(':checked') )
		{
			requestData += '&Task[parent_id]=' + $('#task-list').val();
		}
		if( $('#is-deadline').is(':checked') )
		{
			//Разобраться как вытащить из виджета дату
		}

		var actionUrl = 'index.php?r=task/create';
		
		if( editingRecord !== false )
		{
			actionUrl = 'index.php?r=task/update&id='+editingRecord;
		}

		$.ajax(
		{
			url:actionUrl,
			data: requestData,
			dataType:'text',
			type: 'POST',
			success: function( response, error )
			{
				$('.modal').modal('hide');
				$.pjax.reload({container:'#task-grid'});
				console.log( response );
				editing = false;
			},
		});
		$('.editTaskButton').click( setEditingFlag );
	}

	/*
	* Валидация формы
	*/
	function validateForm()
	{
		var success = true;
		$.each( $('input:required'), function( key, input )
		{
			if( !input.value  )
			{
				$( '#'+input.id ).next('.help-block').html( '"'+$('label[for=\''+input.id+'\']').text()+'" обязательно к заполнению.');
				success = false;
			}
		});
		return success;
	}

	/*
	* Запрашивает с сервера ID текущего пользователя.
	*/
	function getUserId()
	{
		return $.ajax(
				{
					url:'index.php?r=us-ext/get-current-user-id',
					dataType: 'text',
					async: false,
				}).responseText;
	}

	/*
	* Запрашивает с сервера фамилию и имя текущего пользователя.
	*/
	function getUserName()
	{
		return $.ajax(
				{
					url:'index.php?r=us-ext/get-current-user-name',
					dataType: 'text',
					async: false,
				}).responseText;
	}

	/*
	* Запрашивает с сервера список ответственных
	* и вставляет его в страничку
	*/
	function fillResponsibleList()
	{
		$.ajax(
		{
			url:'index.php?r=us-ext/get-own-org-employees',
			success: function( responseData, textStatus )
			{
				var response = JSON.parse( responseData );
				var select = '';
				var selectedVal = response.currentUserId;
				delete response.currentUserId;
				for( var key in response )
				{
					select += '<option value =\'' + key + '\'' +
							  (key == selectedVal?' selected':'') + '>' +
							  response[key] + '</option>';
				}
				$('#responsible').html( select );
			},
		});
	}

	/*
	* Запрашивает с сервера список проектов
	* и вставляет его в страничку
	*/
	function fillProjectList()
	{
		$.ajax(
		{
			url:'index.php?r=task/get-project-list',
			dataType: 'text',
			success: function( responseData, textStatus )
			{
				var response = JSON.parse( responseData );
				var select = '';
				for( var key in response )
				{
					select += '<option value =\'' + key + '\'>' +
							  response[key] + '</option>';
				}
				$('#project-list').html( select );
			}
		});
	}
	
	/*
	* Запрашивает с сервера список задач
	* и вставляет его в страничку
	*/
	function fillTaskList()
	{
		$.ajax(
		{
			url:'index.php?r=task/get-task-list',
			dataType: 'text',
			success: function( responseData, textStatus )
			{
				var response = JSON.parse( responseData );
				var select = '';
				for( var key in response )
				{
					select += '<option value =\'' + key + '\'>' +
							  response[key] + '</option>';
				}
				$('#task-list').html( select );
			}
		});
	}

	
	function setEditingFlag(e)
	{
		editingRecord = this.name;
		console.log('df');
	}
});
