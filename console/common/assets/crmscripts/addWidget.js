    /*$(document).submit(function(e)
    { 
        if( $(this).closest('.modal') !== 'undefined' ) 
        {
            console.log( $(this).closest('.modal') );
            e.preventDefault(); 
        }
    });*/
    // Слушаем клики кнопок с индентификатором sumbitModalForm
    $('.add-widget-submit').click(function( e ) 
    {   
        // Упаковываем данные из формы в строку
        var formObj = this.form;

        if( (typeof formObj === 'undefined') || ( formObj == null )  )
        {
            return;
        }
        var data_input = $(this.form).serialize();
        var sourceFieldId = $( '#'+$(this).attr('id')+'extraData' ).attr('value');
        console.log( $(this).attr('id') );

        var modalForm = $('.modal');
        //  Составляем ajax запрос 
        $.ajax(
        {
            url: $(this.form).attr('action'),       // url экшена
            datatype:"text",                        // тип ожидаемых от сервера данных
            type:'POST',                            // тип запроса
            data:data_input,                        // данные отправляемые на сервер

            success:function( serverResp, textStatus ) 
            {
                formObj.reset();    
                modalForm.modal('hide');
                var jsonResp = JSON.parse( serverResp );
                var selectedId = jsonResp.selected;
                delete jsonResp.selected;
                console.log('server response: '+ jsonResp);
                
                var selectTag = '<option value>-- Выберите пункт списка --</option>';
                $.each( jsonResp, function( key, value )
                {
                    selectTag += '<option value=' + key + ( key == selectedId?' selected':'' ) + '>' +
                                 value + '</option>';
                });

                $( '#' + sourceFieldId ).html( selectTag );
            },

            error:function( jqXHR, textStatus, errorThrown) {
                alert('Ошибка. Проверьте правильность заполнения полей. Если все верно - проблема в запросе.');
            }
        });
    });

