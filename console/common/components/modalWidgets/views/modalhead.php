<?php
use yii\helpers\Html;
/* 
*	Верхняя чать модального окна. включает открывающие теги модального окна.
*/
// $id - modal div ID
// $title - modal title
// $widgetClass - class of widget

if( $widgetClass == null )
{
	$widgetClass = 'modal-widget';
}

?>
<div class="modal fade <?= $widgetClass ?>" id = <?= $id ?>  tab_index="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><?= $title ?></h4>
                    <div id = 'message-pool' style="color: red;"></div>
                </div>
                <!-- Тело модального окна -->
                <div class="modal-body">