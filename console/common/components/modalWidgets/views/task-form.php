<?php 
use yii\helpers\Html;
use kartik\widgets\DateTimePicker;
?>

<div id="TaskForm" >
	
	<div class='modal-container'>
		<label id = 'originator'>Постановщик: </label>
		
		<div class = 'form-group field-task-name required'>
			<label class = "control-label" for = 'task-name'>Название</label>
			<input type = 'text' id = 'task-name' class = 'form-control' placeholder = 'Введите название задачи' required>
			<div class = 'help-block'></div>
		</div>

		<div class = 'row form-group'>
			<div class = 'col-lg-5 field-responsible'>
				<label class = "control-label" for = 'responsible'>Ответственный</label>
				<select id = 'responsible' class = 'form-control'>
				</select>
				<div class = "help-block"></div>
			</div>

			<div class = 'col-lg-7'>
				<label class = "control-label">Приоритет</label>
				<div class = 'form-group priority-select'>
					<input type = 'radio' id = 'priority-1' value = '1' name = 'priority-radio'>
					<font color=grey>
					<label for = 'priority-1'>Низкий</label>
					</font>
					<input type = 'radio' id = 'priority-2' value = '2' name = 'priority-radio' checked>
					<font color=blue>
					<label for = 'priority-2'>Средний</label>
					</font>
					<input type = 'radio' id = 'priority-3' value = '3' name = 'priority-radio'>
					<font color=red>
					<label for = 'priority-3' >Высокий</label>
					</font>
				</div>
				<div class = 'help-block'></div>
			</div>
		</div>

		<div class = 'row'>
			<div class = 'col-sm-6 field-is-deadline'>
				<input type = 'checkbox' id = 'is-deadline' value = 'deadline'>
				<label class = 'control-label' for = 'is-deadline'>Установить крайний срок</label>
			</div>
		</div>
		<div class = 'row' id = 'row-deadline' hidden>
			<div class = 'col-sm-6 field-deadline' >
				<?= DateTimePicker::widget([
					'name' => 'deadline',
					'value' => date( 'd-M-y' ),
					'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
					'readonly' => true,
					'removeButton' => false,
					'convertFormat' => true,
					'pluginOptions' => [ 
						'format' => 'dd/MM/yyyy HH:i P',
						'autoclose' => true,
						'todayBtn' => true,
					]
				]) ?>
				<div class = 'help-block'></div>
			</div>
		</div>

		<div class = 'row '>
			<div class = 'col-sm-6 field-bind-with-project'>
				<input type = 'checkbox' id = 'bind-with-project' value = 'bind'>
				<label class = 'control-label' for = 'bind-with-project'>Ассоциировать с проектом</label>
				<div class = 'help-block'></div>
			</div>
		</div>
		<div class = 'row ' id = 'row-project' hidden>
			<div class = 'col-sm-6 field-project' >
				<select id = 'project-list' class = 'form-control'>
					<option value></option>
				</select>
				<div class = 'help-block'></div>
			</div>
		</div>

		<div class = 'row'>
			<div class = 'col-sm-6 field-choose-parent-task'>
				<input type = 'checkbox' id = 'choose-parent-task' value = 'parent'>
				<label class = 'control-label' for = 'choose-parent-task'>Указать базовую задачу</label>
			</div>
		</div>
		<div class = 'row' id = 'row-parent-task' hidden>
			<div class = 'col-sm-6 field-task' >
				<select id = 'task-list' class = 'form-control'>
					<option value></option>
				</select>
				<div class = 'help-block'></div>
			</div>
		</div>


		<div class = 'form-group'>
			<label class = 'control-label' for = 'task-description'>Описание</label>
			<textarea id = '#task-description' class = 'form-control' rows = '6' style='resize:none' ></textarea>
		</div>




	</div>
</div>