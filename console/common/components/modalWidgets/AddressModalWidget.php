<?php

/*
*	Виджет модального окна с формой ввода адреса. 
	Для использования виджета необходимо:
	
	1. Определить следующие свойства при вызове AddressModalWidget::widget -
	//--------------------------------------------------
	$modalId - уникальный идентификатор модального окна. 
	
	Это свойство не обязательно определять если на странице находится единственный виджет этого класса.
	Но для единообразия кода и исключения ошибок в будущем рекомендуется его определять явно.	

	//---------------------------------------------------
	$addressFieldId - уникальный идентификатор элемента <select> страницы с которым связывается виджет. 
	После заполнения и отправки формы модального окна на сервер данное поле будет обновлено в соответствии с 
	введенными данными.
	
	//----------------------------------------------------

	Виджет имеет постоянно видимый элемент button, у которого определены атрибуты: 
	data-toggle = 'modal'
	data-target = '#modalId' 
	class = 'btn btn-primary modal-trigger'
	и который отображается в том месте документа где был создан	виджет. 
	Так же виджет выводит в текст документа код модального окна отображение которого 
	инициируется кликом по кнопке триггеру.

*/

namespace common\components\modalWidgets;

use yii\base\Widget;
use yii\web\View;
use yii\helpers\Html;
use common\assets\ModalWidgetAsset;


class AddressModalWidget extends Widget
{
	public $modalId;		// ID модального окна
	public $addressFieldId;	// ID элемента <select> связанного с виджетом
	
	public function init()
	{
		parent::init();
		if( $this->modalId == null )
		{
			// ID модального окна по умолчанию
			$this->modalId = 'addressModalWidget';
		}
	}

	public function run()
	{
		// Регистрируем файлы скриптов необходимых для виджета
		$view = $this->getView();
        $view->registerJsFile( '@common/JS/addressInput.js',
        					  ['depends' => [ ModalWidgetAsset::className()] ], 
        					  View::POS_READY );
        // Выводим кнопку триггер
        echo Html::button( 'Ввести адрес', [ 	
    											'class' => 'btn btn-primary modal-trigger', 
    											'data-toggle' => 'modal',
    											'data-target' => '#'.$this->modalId,
    										]);
        // Выводим шапку модального окна
		echo $this->render( 'modalhead', [ 
											'id' => $this->modalId, 
											'title' => 'Форма заполнения адреса',
											'widgetClass' => 'address-widget'
										 ]);
		// Выводим тело модального окна
		echo $this->render( 'address-form', ['fieldId' => $this->modalId, ] );
		// Выводим футер модального окна
		return $this->render( 'modalfoot', ['buttonLabel' => 'Сохранить',
											'data' => $this->addressFieldId,
											'formId' => $this->modalId,
											'modalId'=>$this->modalId,
											'buttonId'=>'address-widget-submit',
											]);
	}
}

?>