<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property integer $id
 * @property integer $FK_kladr
 * @property integer $FK_socr
 * @property string $street
 * @property string $building
 * @property string $room
 * @property string $other_place
 *
 * @property Socrbase $fKSocr
 * @property Kladr $fKKladr
 */
class Address extends \yii\db\ActiveRecord
{
    public $_fullAddress;
    public $_oterPlace;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FK_kladr'], 'required'],
            [['FK_kladr', 'FK_socr'], 'integer'],
            [['street'], 'string', 'max' => 1024],
            [['building'], 'string', 'max' => 512],
            [['room'], 'string', 'max' => 2048],
            [['other_place'], 'string', 'max' => 4096],
            [['full_address'], 'string', 'max' => 9192],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'FK_kladr' => 'Внешний ключ КЛАДР',
            'FK_socr' => 'Внешний ключ к SOCR',
            'street' => 'Название улицы',
            'building' => 'Здание (корпус/подъезд и т.д.)',
            'room' => 'Номер помещения (офиса)',
            'other_place' => 'Другое местоположение',
            'full_address' => 'Полный адрес',
        ];
    }


    public function getFullAddress()
    {
        $addressString = '';
        if( empty( $this->post_index ) == true )
        {
            $index = $this->fKKladr->indexAnyway;
            if( empty( $index ) == false )
            {
                $addressString .= $index.', ';
            }
        }
        $addressString .= $this->full_address;

        return $addressString;
    }

    public function getOtherPlace()
    {
        $text = '';
        if( empty( $this->other_place ) == false )
        {
            $text .= '('.$this->other_place.')';
        }
        return $text;
    }

    



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKSocr()
    {
        return $this->hasOne(Socrbase::className(), ['id' => 'FK_socr']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKKladr()
    {
        return $this->hasOne(Kladr::className(), ['id' => 'FK_kladr']);
    }
}
