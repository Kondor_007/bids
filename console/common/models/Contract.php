<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "organisation".
 *
 * @property integer $id
 * @property string $name
 * @property integer $FK_organisation
 * @property integer $FK_type
 * @property string $description
 *
 */
class Contract extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contract';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'FK_organisation'], 'required', 'on' => 'creating'],
        	[['FK_organisation'], 'required', 'on' => 'optional'],
        	[['name', 'number'], 'safe'],
            [['FK_organisation', 'FK_type'], 'integer'],
            [['description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'number' => 'Номер',
            'FK_organisation' => 'Организация',
            'FK_type' => 'Тип договора',
            'date_start' => 'Дата начала',
        	'date_end' => 'Дата окончания',
        	'file_path' => 'Файл',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganisation()
    {
        return $this->hasOne( Organisation::className(), ['id' => 'FK_organisation'] );
    }

    public function getType()
    {
        return $this->hasOne( ContractType::className(), ['id' => 'FK_type'] );
    }
}
