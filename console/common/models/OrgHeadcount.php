<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "org_headcount".
 *
 * @property integer $id
 * @property string $count
 *
 * @property Organisation[] $organisations
 */
class OrgHeadcount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'org_headcount';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['count'], 'required'],
            [['count'], 'string', 'max' => 4096]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'count' => 'Количество',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganisations()
    {
        return $this->hasMany(Organisation::className(), ['FK_headcount' => 'id']);
    }
}
