<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "task_responsible".
 *
 * @property integer $id
 * @property integer $FK_user
 * @property integer $FK_task
 * @property integer $type
 *
 * @property UsExt $fKUser
 * @property Task $fKTask
 */
class TaskResponsible extends \yii\db\ActiveRecord
{
    public $_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_responsible';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FK_user', 'FK_task'], 'required'],
            [['FK_user', 'FK_task', 'type'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'FK_user' => 'ID пользователя',
            'FK_task' => 'ID задачи',
            'type' => 'Флаг ответственного',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKUser()
    {
        return $this->hasOne(UsExt::className(), ['ID' => 'FK_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'FK_task']);
    }

    public function getName()
    {
        return $this->fKUser->shortName;
    }
}
