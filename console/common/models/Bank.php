<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bank".
 *
 * @property integer $id
 * @property string $name
 * @property string $BIK
 * @property string $cor_account
 * @property string $address
 * @property string $description
 *
 * @property PayAccount[] $payAccounts
 */
class Bank extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'BIK', 'cor_account', 'address'], 'required'],
            [['name', 'BIK', 'cor_account'], 'string', 'max' => 1024],
            [['address', 'description'], 'string', 'max' => 4096]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование банка',
            'BIK' => 'БИК',
            'cor_account' => 'Кор. счет',
            'address' => 'Адрес банка',
            'description' => 'Примечание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayAccounts()
    {
        return $this->hasMany(PayAccount::className(), ['FK_bank' => 'id']);
    }
}
