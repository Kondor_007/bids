<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "task_status_ref".
 *
 * @property integer $id
 * @property string $name
 *
 * @property TaskStatus[] $taskStatuses
 */
class TaskStatusRef extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_status_ref';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 4096]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название статуса',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskStatuses()
    {
        return $this->hasMany(TaskStatus::className(), ['FK_status' => 'id']);
    }
}
