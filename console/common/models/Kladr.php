<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kladr".
 *
 * @property integer $id
 * @property string $name
 * @property string $socr
 * @property string $code
 * @property string $indx
 * @property string $gninb
 * @property string $uno
 * @property string $ocatd
 * @property string $status
 */
class Kladr extends \yii\db\ActiveRecord
{
    public $_regionCode;
    public $_districtCode;
    public $_indexAnyway;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kladr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'socr', 'code', 'indx', 'gninb', 'uno', 'ocatd', 'status'], 'required'],
            [['name'], 'string', 'max' => 1024],
            [['socr', 'code', 'indx', 'gninb', 'uno', 'ocatd'], 'string', 'max' => 512],
            [['status'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'socr' => 'Socr',
            'code' => 'Code',
            'indx' => 'Index',
            'gninb' => 'Gninb',
            'uno' => 'Uno',
            'ocatd' => 'Ocatd',
            'status' => 'Status',
        ];
    }

    public function getRegionCode()
    {
        return substr( $this->code, 0, 2 ).'00000000000';
    }

    public function getDistrictCode()
    {
        return substr( $this->code, 0, 8 ).'00000';
    }

    public function getRegionName()
    {
        $region = Kladr::find()->where( [ 'code' => $this->regionCode ] )->one();

        return $region->name.' '.$region->socr;
    }

    public function getDistrictName()
    {
        $district = Kladr::find()->where( [ 'code' => $this->districtCode ] )->one();
        $name ='';

        if( $district->socr == 'г' )
        {
            $name .= $district->socr.'.'.$district->name;
        }
        else
        {
            $name .= $district->name.' '.$district->socr;
        }
        return $name;
    }

    public function getTownName()
    {
        return $this->socr.'.'.$this->name;
    }

    public function getIndexAnyway()
    {
        $index = $this->indx;

        if( empty( $index )  )
        {
            $index = Kladr::find()->where([ 'code' => $this->districtCode ])->one()->indx;
        }

        if( empty( $index ) )
        {
            $index = Kladr::find()->where([ 'code' => $this->regionCode  ])->one()->indx;    
        }

        return $index;
    }

}
