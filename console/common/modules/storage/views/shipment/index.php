<?php


/*
* $stores
*/
$this->title = 'Отгрузка';
$this->params['breadcrumbs'][] = ['label' => 'Склад', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs("
	$( '#shipment-submit' ).click( function( e )
	{
		console.log('ok');
		$.ajax({
			url: 'index.php?r=storage/shipment/realize',
			data: '&operation-id=' + $('#operation-id').text(),
			type: 'POST',
			dataType: 'json',
			success: function( response ){
				if( response.status == 1 )
				{
					$('#result-message').text ( response.messasge );
				}
				else if( response.status == 99 )
				{
					$('#result-message').text ( response.error );	
				}

			}
		});
		return false;
	});
	");
?>

	<div class = 'col-md-9' >
		<h2>Отгрузка товара</h2>
	</div>
	<div class = 'col-md-3' style="margin-top:20px">
		<?= $this->render( 'incomplete-shipments', [ 'incomplete' => $incompleteShipments ] ) ?>
	</div>
<div class="row">
	<div class="col-md-6">
		<?= $this->render( 'store-select', [ 'storeList' => $stores ] ) ?>
	</div>

</div>
<div class = "row">
	<div >
		<?= $this->render('form') ?>
	</div>

</div>

<div id = 'preorder-table'>
	<?= $this->render( 'preorder-table', [ 'dataProvider' => $preorderProvider ] ) ?>
</div>

<div id = 'shipment-table'>
	<?= $this->render( 'shipment-table', [ 'dataProvider' => $shipmentProvider ] ) ?>
</div>

<button class="btn btn-success" id = "shipment-submit" >Оформить отгрузку</button>
<span id = 'result-message' style = "color: green"></span>