<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
/*
*	$dataProvider
*/

?>
<h3>Отгруженные</h3>
<?php Pjax::begin(['id' => 'shipment-grid-pjax']) ?>
<?= GridView::widget([
			'id' => 'shipment-table',
	        'dataProvider' => $dataProvider,
        	'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'],
            [
                'attribute'=>'providerNameFilter',
                'value'=>'providerName',
                'label' => 'Провайдер',
            ],
            [
	            'attribute'=>'tovarNameFilter',
	            'value'=>'tovarName',
	            'label' => 'Тариф',
            ],
            'sim_number',
            'serial',
            //'registration_date',
            //'activation_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
]);?>
<?php Pjax::end() ?>