<?php

use yii\helpers\Html;

$this->registerJs("
	
	$('#from-serial').change( validateSerial );
	$('#from-serial').keyup( validateSerial );
	$('#to-serial').change( validateSerial );
	$('#to-serial').keyup( validateSerial );

	$('#show-to').change( function(e)
	{
		if( $(this).is(':checked') )
		{
			$('#to-input').removeAttr('hidden');
			$('#to-serial').val('');
		}
		else
		{
			$('#to-input').attr('hidden', true);
		}
	});

	$('#ship-submit').click( function(e)
	{
		$.ajax({
			url: 'index.php?r=storage/shipment/ship',
			type: 'POST',
			data: $(this.form).serialize(),
			dataType: 'json',
			success: function( response )
			{	
				console.log(response);
				if ( response.status == 1 )
				{
					html = response.html;
					$('#preorder-table').html ( html.preorderTable );
					$('#shipment-table').html ( html.shipmentTable );
				}
				else if( response.status == 99 )
				{
					$('.help-block').text( response.error );

				}
			},

		});
		return false;
	});

	function validateSerial()
	{
		if( ( $('#from-serial').val().length == 19 ) && 
			( ( $('#to-serial').val().length == 19 ) || ( $('#to-serial').val().length == 0 ) 
													 || ( $('#to-serial').val() == undefined ) ) )
		{
			$('#ship-submit').removeAttr('disabled');
		}
		else
		{
			$('#ship-submit').attr('disabled', true);	
		}
	}
	");

?>

<form action="storage/shipment/shp">
	<div class="row form-group"  style = 'margin-left:13px'>
		<span class = 'col-md-4'>
			<label for = 'from-serial' >Серийный номер</label>
			<?= 
			\yii\widgets\MaskedInput::widget([
					'mask' => '9999999999999999999',
					'name' => 'from',
					'options' => [
						'id' => 'from-serial',	
						'placeholder' => '- Серийный номер товара -',
						'class' => 'form-control',
					],
			 ]);
			?>
			<span class = 'help-block'></span>
		</span>
		<span class = 'col-md-2'>
			<?= Html::checkbox('show-to', false, [
												  'id' => 'show-to',
												  'label' => 'Отгрузить диапазон',
												  'labelOptions' => ['style' => 'margin-top:20px;'],
												  ] ) ?>
		</span>
		<div id = 'to-input' class = 'col-md-4' hidden>			
			<label for = 'to-serial' >До</label>								
			<?= 
			\yii\widgets\MaskedInput::widget([
					'mask' => '9999999999999999999',
					'name' => 'to',
					'options' => [
						'id' => 'to-serial',	
						'placeholder' => '- Серийный номер товара -',
						'class' => 'form-control',
					],
			 ]);
			?>
		</div>
	</div>
	<button id = 'ship-submit' class = 'btn btn-primary' style = 'margin-left:24px'>Отгрузить</button>
</form>
