<?php

use yii\grid\GridView;

/*
*	$dataProvider
*/

?>

<h3>Предзаказ</h3>
<?= GridView::widget([
			'id' => 'preorder-grid',
			'dataProvider' => $dataProvider,
			'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'],
            'provider',
            'name',
            'inStorage',
            'ordered',
            [
	            'attribute'=>'shipped',
	            'value'=>'shipped',
	            'label' => 'Отгружено',
            ],
            
            [
	            'label' => 'Статус',
	            'content' => function( $model, $key, $index )
	            {
	            	$result = '';
	            	if( $model['shipped'] == $model['ordered'] )
	            		return '<span class = "glyphicon glyphicon-ok" style = "color:green" title = "Предзаказ удовлетворен" ></span>';
	            	else if ( $model['shipped'] > $model['ordered'] )
	            		return '<span class = "glyphicon glyphicon-remove" style = "color:red" title = "Предзаказ превышен" ></span>';
	            	else
	            		return '<span class = "glyphicon glyphicon-shopping-cart" style = "color:gray" title = "Заполнение предзаказа" ></span>';
	            }
            ],
        ],
]);
?>