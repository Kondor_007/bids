<?php

/*
* $incomplete - массив незвершенных 
*/
use yii\bootstrap\Dropdown;
use common\modules\storage\Module;

$this->registerJs("
	$( '.old-op' ).click( function( e )
	{
		$.ajax({
			url: 'index.php?r=storage/shipment/refresh-preorder',
			data: '&store='+$(this).attr('value'),
			type: 'POST',
			dataType: 'json',
			success: function( response ){
				if( response.status == 1 )
				{
					html = response.html;
					$('#preorder-table').html ( html.preorderTable );
					$('#shipment-table').html ( html.shipmentTable );
				}
			}
		});
		return false;
	});
	");

if( count($incomplete) == 0 ): ?>
	№<span id = 'operation-id'><?= date('ymdhis') ?></span><br>
<?php else: 
$dropdown = [];
$userFirstName = call_user_func( Module::getInstance()->employeeClass . '::find', 
									['id' => \Yii::$app->user->identity->id ])
								->one()->getName();

foreach ($incomplete as $key => $value) 
{
	$dropdown[] = 	[
			            'label' => "<span class = 'glyphicon glyphicon-shopping-cart'></span> ".$value['store']." (${value['count']})", 
			            'url' => '', 
			            'linkOptions' => [
			            					'class' => 'old-op',
			                                'title' => 'Продолжить работу',
			                                'name' => 'store',
			                                'value' => $value['storeId'],
			                             ],
				    ];
}


?>
<style>
	.notification{
		width: 175px;
		padding: 5px;
		border: 1px solid white;
	}
	.notification:hover{
		box-shadow: 0px 0px 30px #599FFF;
		border: 1px solid #8B8989;
		border-radius: 5px;
	}
</style>

<div class="dropdown">
	№<span id = 'operation-id'><?= date('ymdhis') ?></span><br>
	<div class = 'notification' >
		<a data-toggle="dropdown" href="#" style = 'text-decoration: none; color: #8B8989;' >
			<?= $userFirstName ?>, 
			<br>Количество не закрытых докуметов: <?= count($incomplete) ?> 
			<span class="glyphicon glyphicon-exclamation-sign" style = "color: red" ></span>
		</a>
		<?= Dropdown::widget([
			                'items' => $dropdown,
			                'encodeLabels' => false,
			            ]) ?>
	</div>
</div>
<?php endif; ?>
