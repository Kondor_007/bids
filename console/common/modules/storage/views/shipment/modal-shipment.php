<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;

$listBoxOptions = [ 'class' => 'form-control', 
					'size' => '15', 
					'multiple' => true, 
				];

$this->registerJs("
		$('#ship').click( function(e)
		{
			var data = $(\"select[name='in-storage-serials[]']\").serialize();
			$.ajax({
				url: 'index.php?r=storage/shipment/quick-ship',
				type: 'POST',
				data: data,
				dataType: 'json',
				success: function( response )
				{	
					console.log(response);
					if( response.status == 1 )
					{
						$(\"select[name='shipped-serials[]']\").empty();
						$(\"select[name='in-storage-serials[]']\").empty();
						$.each(response.shipped, function( val, text) {
							$(\"select[name='shipped-serials[]']\").append( $('<option></option>').val(val).html(text) );
						});
						
						$.each(response.inStorage, function( val, text) {
							$(\"select[name='in-storage-serials[]']\").append( $('<option></option>').val(val).html(text) );
						});
					}
					else
					{
						console.log( response );
					}
				},
			});
		
			return false;
		});

		$('#sahip').click( function(e)
		{
			var data = $(\"select[name='shipped-serials[]']\").serialize();
			$.ajax({
				url: 'index.php?r=storage/shipment/recall',
				type: 'POST',
				data: data,
				dataType: 'json',
				success: function( response )
				{
					if( response.status == 1 )
					{	
						$.each(response.shipped, function( val, text) {
							$(\"select[name='shipped-serials[]']\").empty();
							$(\"select[name='shipped-serials[]']\").append( $('<option></option>').val(val).html(text) );
						});
					}
					else
					{
						console.log( response );
					}
				},
			});
		
			return false;
		});
	");

?>
<div id = 'modal-shipment-view'>
<?php Modal::begin([
                'id' => 'modal-shipment',
                'header' => '<h2 align=center>Отгрузка</h2>',
                'size' => 'LARGE',
                'toggleButton' => [
                    'label' => 'Show',
                    'class' => 'btn btn-primary',
                ],
            ]) ?>
<div class = 'row'>
	<div class = 'col-md-4 col-md-offset-1'>
		<?= Html::listBox( 'shipped-serials', [], $shipped, $listBoxOptions ) ?>
	</div>
	<div class = 'col-md-1'>
		<div style = 'margin-top:35px;'>
			<?= Html::a( '<span class = "glyphicon glyphicon-chevron-left"></span>', '', [ 'class' => 'btn btn-primary', 'id' => 'ship', 'title' => 'Отгрузить' ] ) ?>
			<br>
			<br>
			<?= Html::a( '<span class = "glyphicon glyphicon-chevron-right"></span>', '', [ 'class' => 'btn btn-primary', 'id' => 'recall', 'title' => 'Вернуть' ] ) ?>
		</div>
	</div>
	<div class = 'col-md-4'>
		<?= Html::listBox( 'in-storage-serials', [], $inStorage, $listBoxOptions  ) ?>
	</div>
</div>
<?php Modal::end() ?>
</div>