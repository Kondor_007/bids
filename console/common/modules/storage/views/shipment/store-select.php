<?php

use kartik\widgets\Select2;

/*
*	$storeList
*/

$this->registerJs("

	$( '.store-filter' ).click( function( e )
	{
		var currentBtn = this;
		$.ajax({
			url: 'index.php?r=storage/shipment/refresh-stores',
			type: 'POST',
			data: '&filter=' + $(this).attr('filter'),
			dataType: 'json',
			success: function ( response )
			{
				if( response.status == 1 )
				{
					$('.store-filter').css('box-shadow', '');
					$(currentBtn).css('box-shadow', 'inset 0px 0px 15px #222222');
					var select = '';
					for( var key in response.stores )
					{
                        select += '<option value =\'' + key + '\'>' +
                                   response.stores[key] + '</option>';
                    }
				}
				$('#store-select').html( select ).select2();
				$('#store-select').change();

			}
		});
	});

    $('#store-select').change( function(e)
    {
        $.ajax({
            url:'index.php?r=storage/shipment/refresh-preorder',
            type: 'POST',
            dataType: 'json',
            data: '&store=' + this.value,
            success: function( response )
            {
                if( response.status == 1 )
                {
                	$('#preorder-table').html( response.html.preorderTable );
                	$('#shipment-table').html( response.html.shipmentTable );
                }
                else
                {
                    console.log('Refresh preorder failed');
                }
            }
        });
    });
    
    ");

?>

<div class="btn-group" role="group" aria-label = "Filters" style = "margin-left: 15px; 
																	margin-bottom: 10px;">
  	<button type="button" class="btn btn-default store-filter" filter = 'all' style="padding-left: 30px;padding-right: 30px;">Все</button>
  	<!-- <button type="button" class="btn btn-success store-filter" filter = 'complete'>Отгруженные</button> -->
	<button type="button" class="btn btn-danger store-filter" filter = 'incomplete' >Не закрытые</button>
</div>

<div id = 'store-unit-select' class="col-md-9 form-group" >
	<label for = 'store-select' >Торговые точки</label>
	<?= Select2::widget([
	'id' => 'store-select',
    'name' => 'store-select',
    'value' => '',
    'data' => $storeList,
    'options' => [ 'placeholder' => '- Выберите торговую точку -',],
	])	?>
</div>