<?php

/*
* $complete - массив завершенных
*/
use yii\bootstrap\Dropdown;
use common\modules\storage\Module;

$this->registerJs("
	$( '.finished-orders' ).click( function( e )
	{   
		var store = $(this).attr('store-unit')
		console.log(store);
		$.ajax({
			url: 'index.php?r=storage/preorder/recall',
			data: /*'&'+this.name+'='+$(this).attr('value') + */'&store='+ store,
			type: 'POST',
			dataType: 'json',
			success: function( response ){
				console.log( response );
				if( response.status == 1 )
				{
				    //$.pjax.reload({container: '#preorder-grid'})
					$('#store-select').val( store );
					$('#preorder-table').html( response.html.table );
					$('#incomplete-box').html( response.html.incomplete );
					$('#complete-box').html( response.html.complete );
					$('#filters').html( response.html.filters );
				}
			}
		});
		return false;
	});
	");

if( count($complete) == 0 ): ?>
<?php else: 
$dropdown = [];

foreach ($complete as $key => $value) 
{
	$dropdown[] = 	[
			            'label' => '<span class = "glyphicon glyphicon-paperclip"> '.$value['name']." (${value['count']})".'</span>', 
			            'url' => '', 
			            'linkOptions' => [
			            					'class' => 'finished-orders',
			                                'title' => 'Редактировать предзаказ',
			                                'name' => 'preorder-number',
			                                'value' => $value['op_date'],
			                                'store-unit' => $value['storeId'],
			                             ],
				    ];
}

?>
<style>
	.notification-comp{
		width: 175px;
		padding: 5px;
		border: 1px solid white;
	}
	.notification-comp:hover{
		box-shadow: 0px 0px 30px #66FF33;
		border: 1px solid #8B8989;
		border-radius: 5px;
	}
</style>

<div class="dropdown">
	<div class = 'notification-comp'>
		<a data-toggle="dropdown" href="#" style = 'text-decoration: none; color: #8B8989;' >
			Сформированные: <?= count($complete) ?> 
			<span class="glyphicon glyphicon-ok" style = "color: green" ></span>
		</a>
		<?= Dropdown::widget([
			                'items' => $dropdown,
			                'encodeLabels' => false,
			            ]) ?>
	</div>
</div>
<?php endif; ?>

<?php 
/*function parseDate( $date )
{
	return substr( $date, 4, 2 ).'-'
			   .substr( $date, 2, 2).'-'
			   .substr( $date, 0, 2).' '
			   .substr( $date, 6, 2).':'
			   .substr( $date, 8, 2).':'
			   .substr( $date, 10, 2 );
}*/
?>
