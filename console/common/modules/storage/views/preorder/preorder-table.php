<script src="/crm/backend/web/assets/946a662f/jquery.js"></script>
<?php

use yii\grid\GridView;
use kartik\widgets\TouchSpin;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;

$this->registerJs("

    $('#submit-preorder').click( function(e)
    {
        $.ajax({
            url:'index.php?r=storage/preorder/realize',
            type: 'POST',
            dataType: 'json',
            success: function( response )
            {
                if( response.status == 1 )
                {
                    $.pjax.reload({container:'#preorder-grid'});
                    $('#result').css('color', 'green');
                    $('#result').text( 'Количество товаров в предзаказе: '+response.number );
                }
                else
                {
                    $('#result').css('color', 'red');
                    $('#result').text( 'Ошибка: '+ response.message );   
                }
            }
        });
    });
    
    var timer;
    function test()
    {   
        clearTimeout(timer);
        $(this).css('color', 'red');
        $('.order-value').attr('disabled', true);
        $(this).removeAttr('disabled');
        timer = setTimeout( update, 200, this );

        function update( element )
        {
            $.ajax({
                url: 'index.php?r=storage/preorder/update',
                data: '&productId=' + $(element).attr('id') +
                      '&amount=' + $(element).val(),
                type: 'POST',
                dataType: 'json',
                success: function( response )
                {
                    console.log(response);
                    $(element).css('color', 'rgb(85, 85, 85)');
                    $('.order-value').removeAttr('disabled');
                },
            });
        }
    }


    ");

Pjax::begin();
echo GridView::widget([
        'id' => 'preorder-grid',
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'],
            'provider',
            'name',
            'inStorage',
            'inStoreUnit',
            ['class' => 'yii\grid\DataColumn',
             'content' => function ( $model, $key, $index, $column )
             			{
             				return TouchSpin::widget([
                                        'name' => 't6',
                                        'options' => [ 'id' => $model['tovarId'], 'class' => 'order-value' ],
								        'pluginOptions' => ['verticalbuttons' => true,
								        					'initval' => ( $model['ordered'] == NULL )?0:$model['ordered'],
								        					'min' => 0,
								        					'max' => 999999,
								        					'step' => 1,],

                                        'pluginEvents' => [
                                            'change' => 'test',
                                        ],

								]);
             			},
             'contentOptions' => ['width' => '100px'],
 			],

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
Pjax::end();

    Modal::begin([
            'id' => 'preview-modal',
        ]);
?>
<div id = 'preview-list'>
</div>
<?php
    Modal::end();

?>
<button id = 'submit-preorder' class="btn btn-success" <?= ( $dataProvider->count == 0?'disabled':'') ?> >Сформировать предзаказ</button>
<span id = 'result'></span>