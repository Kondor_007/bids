<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$providers['0'] = 'Все';

$this->registerJs("
	$( '#subdealer-select' ).change( function( e )
	{
		$.ajax({
			url: 'index.php?r=storage/subdealer/get-stores',
			type: 'POST',
			dataType: 'json',
			data: '&id='+ this.value,
			success: function( response ){
				var select = '';
				if( ( response.status == 1 ) && (!( response.stores == '' )) )
                {   
                    for( var key in response.stores )
                    {
                    	select += '<option value =\'' + key + '\'>' +
                        		  response.stores[key] + '</option>';
                    }
                }
                else
                {
                    select = '<option value = 0>-- Нет торговых точек --</option>';
                }
                $('#store-select').html( select );
			}
		});
		return false;
	});
	
	function refreshTable( e )
	{
		var data = $('#store-select').val() == ''?'&new=1':'&store='+ $('#store-select').val() +
				   ($('#provider-select').val() == '0'?'':'&providerId='+$('#provider-select').val());

		$.ajax({
			url: 'index.php?r=storage/preorder/refresh',
			type: 'POST',
			dataType: 'json',
			data: data,
			success: function( response ){
                if( response.status == 1 )
				{
					$('#preorder-table').html( response.html.table );
					$('#incomplete-box').html( response.html.incomplete );
					$('#complete-box').html( response.html.complete );
				}
			}
		});
		return false;
	}

	$( '#store-select' ).change( refreshTable );
	$( '#provider-select' ).change( refreshTable );

	");

?>

<!-- <div class="btn-group" role="group" aria-label = "Filters" style = "margin-left: 15px; margin-bottom: 10px;">
  	<button type="button" class="btn btn-default store-filter" filter = 'all' style="padding-left: 30px;padding-right: 30px;">Все</button>
  	<button type="button" class="btn btn-success store-filter" filter = 'complete'>Отгруженные</button>
	<button type="button" class="btn btn-danger store-filter" filter = 'incomplete' >Не закрытые</button>
</div> -->

<?php $form = ActiveForm::begin() ?>

	<div class = 'row form-group'>
		<div class = 'col-md-4'>
			<label for = 'subdealer-select' >Субдилер</label>
			<?= Html::dropDownList( 'subdealer-select', 0, $subdealers, [ 
																	'id' => 'subdealer-select', 
																	'class' => 'form-control',
																	'prompt' => '- Субдилеры -',
																 ] ); ?>
		</div>
		<div class = 'col-md-5'>
			<label for = 'store-select' >Торговыe точки</label>
			<?= Html::dropDownList( 'store-select', isset( $current_store )?$current_store:0, $stores, [ 'id' => 'store-select', 
																 'class' => 'form-control',
																 'prompt' => '- Торговые точки -' ] ); ?>
		</div>

	</div>
	<div class="row form-group">
		<div class = 'col-md-3'>
			<label for = 'provider-select' >Провайдер</label>
			<?= Html::dropDownList( 'provider-select', 0, $providers, [ 'id' => 'provider-select', 
																  'class' => 'form-control' ] ); ?>
		</div>
	</div>
<?php ActiveForm::end(); ?>