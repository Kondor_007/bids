<?php 

$this->title = 'Предзаказ';
$this->params['breadcrumbs'][] = ['label' => 'Склад', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
	<div class = 'col-md-9'>
		<div id = 'filters'>
		<?= $this->render( 'filters', [ 
										'subdealers' => $subdealers,
										'stores' => $stores,
										'providers' => $providers,
									  ]); ?>
		</div>
	</div>
	<div  class="col-md-3">
		<div id = 'incomplete-box' style = 'margin: 10px 0; color: #8B8989' >
			<?= $this->render('incomplete-orders', [
														'incomplete' => $incomplete,
														'current' => $current,
													]); ?>
		</div>
		<div id = 'complete-box' style = 'margin: 10px 0; color: #8B8989' >
			<?= $this->render('complete-orders', [
													'complete' => $complete,
												]); ?>
		</div>
	</div>
</div>


<div id = 'preorder-table'>
<?=
     $this->render( 'preorder-table', 
					[ 'dataProvider' => $dataProvider,]);

?>
</div>

