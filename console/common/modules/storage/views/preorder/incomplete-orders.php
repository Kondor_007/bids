<?php

/*
* $incomplete - массив незвершенных
* $current - текущая операция 
*/
use yii\bootstrap\Dropdown;
use common\modules\storage\Module;
use common\modules\storage\classes\UniqueController;

$this->registerJs("
	$( '.old-op' ).click( function( e )
	{   
		var store = $(this).attr('store-unit');
		var data = store?'&store='+store:'';
		$.ajax({
			url: 'index.php?r=storage/preorder/refresh',
			data: /*'&'+this.name+'='+$(this).attr('value') +*/ data,
			type: 'POST',
			dataType: 'json',
			success: function( response ){
				if( response.status == 1 )
				{
					$('#store-select').val( store );
					$('#preorder-table').html( response.html.table );
					$('#complete-box').html( response.html.complete );
					$('#incomplete-box').html( response.html.incomplete );
				}
			}
		});
		return false;
	});
	");

$uploadDate = $current;

if( count($incomplete) == 0 ): ?>
	№<span id = 'operation-id' > <?= $current ?> </span>
<?php else: 
$dropdown = [];
$userFirstName = call_user_func( Module::getInstance()->employeeClass . '::find', ['id' => \Yii::$app->user->identity->id ])->one()->getName();

foreach ($incomplete as $key => $value) 
{
	$dropdown[] = 	[
			            'label' => '<span class = "glyphicon glyphicon-paperclip"> '.$value['name']." (${value['count']})".'</span>', 
			            'url' => '', 
			            'linkOptions' => [
			            					'class' => 'old-op',
			                                'title' => 'Продолжить работу',
			                                'name' => 'preorder-number',
			                                'value' => $value['op_date'],
			                                'store-unit' => $value['storeId'],
			                             ],
				    ];
}

$dropdown[] = 	[
					'label' => '<span class = "glyphicon glyphicon-repeat"></span> Начать новую операцию', 
			        'url' => '', 
			        'linkOptions' =>[
			            				'class' => 'old-op',
			                            'title' => 'Сгенерировать новый код операции',
			                            'name' => UniqueController::GENERATE_NEW,
			                            'value' => 1,
			                        ],
				];

?>
<style>
	.notification{
		width: 175px;
		padding: 5px;
		border: 1px solid white;
	}
	.notification:hover{
		box-shadow: 0px 0px 30px #FF3300;
		border: 1px solid #8B8989;
		border-radius: 5px;
	}
</style>

<div class="dropdown">
	№<span id = 'operation-id'><?= $uploadDate ?></span><br>
	<div class = 'notification' >
		<a data-toggle="dropdown" href="#" style = 'text-decoration: none; color: #8B8989;' >
			<?= $userFirstName ?>, 
			<br>Количество не закрытых докуметов: <?= count($incomplete) ?> 
			<span class="glyphicon glyphicon-exclamation-sign" style = "color: red" ></span>
		</a>
		<?= Dropdown::widget([
			                'items' => $dropdown,
			                'encodeLabels' => false,
			            ]) ?>
	</div>
</div>
<?php endif; ?>

<?php 
/*function parseDate( $date )
{
	return substr( $date, 4, 2 ).'-'
			   .substr( $date, 2, 2).'-'
			   .substr( $date, 0, 2).' '
			   .substr( $date, 6, 2).':'
			   .substr( $date, 8, 2).':'
			   .substr( $date, 10, 2 );
} 
*/?>
