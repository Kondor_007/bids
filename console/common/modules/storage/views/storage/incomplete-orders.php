<?php

/*
* $incomplete - массив незвершенных 
*/
use yii\bootstrap\Dropdown;
use common\modules\storage\Module;

$this->registerJs("
	$( '.old-op' ).click( function( e )
	{
		$.ajax({
			url: 'index.php?r=storage/storage/orders-table',
			data: '&'+this.name+'='+$(this).attr('value'),
			type: 'POST',
			dataType: 'html',
			success: function( responseData ){
				response = JSON.parse( responseData );
				if( response.status == 1 )
				{
					//$('.storage-index').html( response.table );
					$('#incomplete-box').html( response.incomplete );
				}
				$.pjax.reload({container:'#storage-grid-pjax'});
			}
		});
		return false;
	});
	");

$uploadDate = $current;

if( count($incomplete) == 0 ): ?>
	№<span id = 'operation-id' > <?= $current ?> </span>
<?php else: 
$dropdown = [];
$userFirstName = call_user_func( Module::getInstance()->employeeClass . '::find', ['id' => \Yii::$app->user->identity->id ])->one()->getName();

foreach ($incomplete as $key => $value) 
{
	$dropdown[] = 	[
			            'label' => '<span class = "glyphicon glyphicon-paperclip"> '.parseDate( $value['operation_id'] )." (${value['count']})".'</span>', 
			            'url' => '', 
			            'linkOptions' => [
			            					'class' => 'old-op',
			                                'title' => 'Продолжить работу',
			                                'name' => 'operation-id',
			                                'value' => $value['operation_id'],
			                             ],
				    ];
}

$dropdown[] = 	[
					'label' => '<span class = "glyphicon glyphicon-repeat"></span> Начать новую операцию', 
			        'url' => '', 
			        'linkOptions' =>[
			            				'class' => 'old-op',
			                            'title' => 'Сгенерировать новый код операции',
			                            'name' => 'new',
			                            'value' => 1,
			                        ],
				];

?>
<style>
	.notification{
		width: 175px;
		padding: 5px;
		border: 1px solid white;
	}
	.notification:hover{
		box-shadow: 0px 0px 30px #599FFF;
		border: 1px solid #8B8989;
		border-radius: 5px;
	}
</style>

<div class="dropdown">
	№<span id = 'operation-id'><?= $uploadDate ?></span><br>
	<div class = 'notification' >
		<a data-toggle="dropdown" href="#" style = 'text-decoration: none; color: #8B8989;' >
			<?= $userFirstName ?>, 
			<br>Количество не закрытых докуметов: <?= count($incomplete) ?> 
			<span class="glyphicon glyphicon-exclamation-sign" style = "color: red" ></span>
		</a>
		<?= Dropdown::widget([
			                'items' => $dropdown,
			                'encodeLabels' => false,
			            ]) ?>
	</div>
</div>
<?php endif; ?>

<?php 
function parseDate( $date )
{
	return substr( $date, 4, 2 ).'-'
			   .substr( $date, 2, 2).'-'
			   .substr( $date, 0, 2).' '
			   .substr( $date, 6, 2).':'
			   .substr( $date, 8, 2).':'
			   .substr( $date, 10, 2 );
} 
?>
