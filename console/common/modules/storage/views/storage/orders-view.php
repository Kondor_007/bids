<?php 
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\helpers\ArrayHelper;
    use yii\widgets\ActiveForm;
    use yii\bootstrap\Modal;
    use kartik\widgets\Select2;
    use yii\widgets\Pjax;
    use common\components\widgets\AjaxUploader;

    use common\modules\storage\models\Provider;
    use common\modules\storage\models\Tovar;
/*
    $model - storage\models\Storage
    $tovar - storage\models\Tovar
    $incomplete
    $current
*/

    $this->title = 'Оформление товара';
    $this->params['breadcrumbs'][] = ['label' => 'Склад', 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;

?>

<style>
#overlay {
    background-color: rgba(0, 0, 0, 0.8);
    z-index: 999;
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    /*display: none;*/
}​
</style>

<div class="page-header">
    <div class = 'row'>
        <div class = 'col-md-9'>
            <h1><?= $this->title ?></h1>
            <?php
                $providerList = ArrayHelper::map( Provider::find()->all(), 'id', 'name' ); 
                echo $this->render( 'ajax-file-input', [ 'current' => $current ] );//echo AjaxUploader::widget();
            ?>
        </div>
        <div id = 'incomplete-box' class="col-md-3" style = 'margin: 30px 0; color: #8B8989' >
            <?= $this->render( 'incomplete-orders', [ 'incomplete' => $incomplete, 'current' => $current ] ) ?>
        </div>
    </div>
</div>

<div class = 'manual-creating'>
    <?= $this->render( 'add-form', [ 'model' => $model ] ) ?>
</div>
<br>
<div class="storage-index">
    <?= 
        $this->render('orders-table', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel ] )
    ?>
</div>
<button class = 'btn btn-primary' id = 'submit-realize'>Сформировать накладную</button>
<span id = 'result' style = 'color:green' ></span>