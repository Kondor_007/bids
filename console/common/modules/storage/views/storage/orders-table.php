<script src="/crm/backend/web/assets/946a662f/jquery.js"></script>
<?php
use yii\widgets\Pjax;
/*
* $dataProvider
* $searchModel
*/

use kartik\grid\GridView;

Pjax::begin(['id' => 'storage-grid-pjax']);
$dataProvider->pagination->setPageSize(3);
echo GridView::widget([
        'id' => 'storage-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'],

            [
            'attribute'=>'tovarNameFilter',
            'value'=>'tovarName',
            'label' => 'Тариф',
            ],
            /*[
                'attribute'=>'providerNameFilter',
                'value'=>'providerName',
                'label' => 'Провайдер',
            ],*/
            'sim_number',
            'serial',
            //'registration_date',
            // 'activation_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
Pjax::end();
?>