<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Provider */

$this->title = $model->serial;
$this->params['breadcrumbs'][] = ['label' => 'Оформление товара', 'url' => ['orders-view']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provider-view">

    <?= DetailView::widget([
        'model' => $model,
    	'enableEditMode' => true,
    	'bootstrap' => true,
    	'panel' => [
    		'type' => DetailView::TYPE_PRIMARY,
    	],
        'attributes' => [
            'id',
            'serial',
        ],
    ]) ?>

</div>
