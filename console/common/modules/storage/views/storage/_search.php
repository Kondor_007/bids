<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SkladSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sklad-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'FK_tovar') ?>

    <?= $form->field($model, 'FK_provider') ?>

    <?= $form->field($model, 'FK_document') ?>

    <?= $form->field($model, 'account') ?>

    <?php // echo $form->field($model, 'serial') ?>

    <?php // echo $form->field($model, 'FK_user') ?>

    <?php // echo $form->field($model, 'registration_date') ?>

    <?php // echo $form->field($model, 'activation_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
