<?php

use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use kartik\widgets\Select2;
use yii\widgets\Pjax;
use yii\helpers\Html;

use common\modules\storage\models\Provider;
use common\modules\storage\models\Tovar;

$providerList = ArrayHelper::map( Provider::find()->all(), 'id', 'name' ); 
$itemList = ArrayHelper::map( Tovar::find()->all(), 'id', 'name' ); 

$this->registerJsFile( '/crm/common/modules/storage/assets/orders-view.js',
                       ['depends' => [ 'yii\web\JqueryAsset' ] ] );

$form = ActiveForm::begin(); ?>
<div class = 'row'>
    <div class="col-sm-2">
        <?= $form->field( ( new Provider() ), 'id' )->dropDownList( $providerList, [ 'prompt' => '-- Выберите провайдера --' ] );  ?>
    </div>
    <?php Pjax::begin(['id' => 'tovar-list-pjax']) ?>
    <div id ='tovar-list' class="col-sm-3">
        <?= $form->field( $model, 'FK_tovar' )->widget( Select2::className(), ['data'=>$itemList] );  ?>
    </div>
    <?php Pjax::end(); ?>
    <div class="col-sm-2">
        <?= $form->field($model, 'sim_number')->widget( \yii\widgets\MaskedInput::classname(), [
                                                        'mask' => '9999999999' ]); ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'serial')->widget( \yii\widgets\MaskedInput::classname(), [
                                                        'mask' => '9999999999999999999' ]); ?>
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <br>
            <?= Html::submitButton( '<span class="glyphicon glyphicon-plus"></span>&nbspДобавить', [ 'class' => 'btn btn-success', 'id' => 'submit-button', 'value' => '<span class="glyphicon glyphicon-plus"></span>' ]) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<div class = 'row'>
    <div class = 'col-sm-2'></div>
    <div class= 'col-sm-3' >
    <?php $tovar = new Tovar();
          Modal::begin([
                'id' => 'add',
                'header' => 'Добавление тарифа',
                'toggleButton' => [
                    'label' => 'Добавить тариф',
                    'class' => 'btn btn-primary',
                ],
                'options' => [
                    'data-backdrop' => 'static',
                ],
                'footer' => '<div align=center>'.Html::submitButton( 'Добавить', [ 'class' => 'btn btn-success', 'id' => 'submit-tariff-button', 'form' => 'tariff-form' ]).'</div>',
            ]);
          $tariff = ActiveForm::begin(['id' => 'tariff-form']) ?>
        <div class="row">
            <div class="col-sm-5">
                <?= $tariff->field( $tovar, 'FK_provider' )->dropDownList( $providerList, [ 'prompt' => '-- Выберите провайдера --' ] ) ?>
            </div>
            <div class="col-sm-6">
                <?= $tariff->field( $tovar, 'name' )->textInput(['placeholder' => 'Введите название тарифа']) ?>
            </div>
        </div>
    <?php ActiveForm::end();
          Modal::end(); ?>
    </div>
</div>

