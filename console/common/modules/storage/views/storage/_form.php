<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\storage\models\Tovar;
use common\modules\storage\models\Provider;
use common\modules\storage\models\Doc;
use common\modules\storage\Module;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Sklad */
/* @var $form yii\widgets\ActiveForm */
?>
<?php 
    //call_user_func(array($class_name, 'doSomething')); php ver < 5.2.3
    $tovarName = ArrayHelper::map(Tovar::find()->all(), 'id', 'name');
    //$userName = ArrayHelper::map( call_user_func(Module::getInstance()->userClass.'::find')->all(), 'id', 'username');
    $providerName = ArrayHelper::map(Provider::find()->all(), 'id', 'name');
    $documentName = ArrayHelper::map(Doc::find()->all(), 'id', 'number');
?>
<div class="sklad-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'FK_tovar')->widget(Select2::className(),['data'=>$tovarName]) ?>

    <?= $form->field($model, 'FK_provider')->dropDownList($providerName) ?>

    <?= $form->field($model, 'FK_document')->widget(Select2::className(),['data'=>$documentName]) ?>

    <?= $form->field($model, 'account')->textInput() ?>

    <?= $form->field($model, 'serial')->textInput() ?>

    <?= $form->field($model, 'registration_date')->textInput() ?>

    <?= $form->field($model, 'activation_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
