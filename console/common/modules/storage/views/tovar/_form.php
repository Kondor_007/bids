<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Provider;

/* @var $this yii\web\View */
/* @var $model common\models\Tovar */
/* @var $form yii\widgets\ActiveForm */
?>
<?php 
	$providerName = ArrayHelper::map(Provider::find()->all(), 'id', 'name'); ?>
<div class="tovar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'FK_provider')->dropDownList($providerName) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
