<?php

use kartik\grid\GridView;
use kartik\widgets\Select2;
use yii\widgets\Pjax;

$addHref = "index.php?r=storage/store-unit/create";
if( isset( $searchModel->FK_org ) )
{
	$addHref .= "&FK_org=".$searchModel->FK_org; 
}

?>

<div class = 'row'>
	<div class = 'col-md-3'>
		<a href=<?= $addHref ?> class = "btn btn-primary">Добавить торговую точку</a>
	</div>
</div>
<br>
<?php 
	echo GridView::widget([
		'dataProvider' => $dataProvider, 
		'condensed' => false,
		'columns' => [
			'address',	
			'brokerName',
		],
	]);
