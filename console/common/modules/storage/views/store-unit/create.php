<?php

$href = 'list-of'; 

$this->title = 'Добавление торговой точки';

$this->params['breadcrumbs'][] = ['label' => 'Торговые точки', 'url' => [$href]];
$this->params['breadcrumbs'][] = $this->title;
?>

<h2><?php echo $this->title ?></h2>
<?php 
echo $this->render('_form', ['model' => $model,
						   		'refs' => $refs,
	]);