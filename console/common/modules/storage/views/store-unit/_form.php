<?php

use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use yii\jui\DatePicker;
use yii\helpers\Html;


$form = ActiveForm::begin();
?>
<?= $form->field( $model, 'name' )->textInput() ?>
<?= $form->field( $model, 'address' )->textInput() ?>
<?= $form->field( $model, 'FK_org' )
		 ->widget( Select2::className(), 
				[ 'data' => $refs['subdealers'],
				  'options' => [
						'placeholder' => 'Субдилер'
				  ],
 ] ) ?>
 
 <?= $form->field( $model, 'FK_broker' )
		->widget( Select2::className(), 
				[ 'data' => $refs['brokers'],
				  'options' => [
						'placeholder' => 'Торговый представитель'
				  ]
 ] ) ?>
  
<?= $form->field( $model, 'open_date' )->widget( DatePicker::className(), 
												[
													'dateFormat' => 'yyyy-MM-dd',
													'options' => [
															'class' => 'form-control'
													]													
												]) ?>
<?= $form->field( $model, 'close_date' )->widget( DatePicker::className(),[
													'dateFormat' => 'yyyy-MM-dd',
													'options' => [
															'class' => 'form-control'
													]													
												]) ?>

<?= $form->field( $model, 'phone' )->widget( MaskedInput::className(), [
														'mask' => '(999) 999-99-99'
												] ) ?>
												
<?= $form->field( $model, 'email' )->widget( MaskedInput::className(), [
														'clientOptions' => [
															'alias' => 'email'
														]
												] ) ?>
 
<?= $form->field( $model, 'FK_ref_location' )->dropDownList( $refs['locations'], ['prompt' => "-- Выберите местоположение --"] )->label('Местоположение') ?>
<?= $form->field( $model, 'FK_ref_price_group' )->dropDownList( $refs['prices'], ['prompt' => "-- Выберите ценовую группу --"] )->label('Ценовая группа') ?>
<?= $form->field( $model, 'FK_ref_spot_spec' )->dropDownList( $refs['profs'], ['prompt' => "-- Выберите профильность --"] )->label('Профильность') ?>

<?= $form->field( $model, 'description' )->textarea(['rows' => 6]) ?>

<div class="form-group">
	<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php 
ActiveForm::end();

