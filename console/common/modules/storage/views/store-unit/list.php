<?php

use kartik\grid\GridView;
use kartik\widgets\Select2;
use yii\widgets\Pjax;

$this->title = "Торговые точки";
$addHref = "index.php?r=storage/store-unit/create";
if( isset( $searchModel->FK_org ) )
{
	$this->title .= ' '.$searchModel->subdealerName;
	$addHref .= "&FK_org=".$searchModel->FK_org; 
}

$this->registerJs("
			$('#subdealer-select').change( function(e)
			{
				var data = '&FK_org='+$(this).val();
				$.pjax.reload({container:'#w0-pjax', data: data});
			});
		");


?>
<h2><?= $this->title ?></h2>
<div class = 'row'>
	<div class = 'col-md-3'>
		<a href=<?= $addHref ?> class = "btn btn-primary">Добавить торговую точку</a>
	</div>
	<div class = 'col-md-5'>
		<?= Select2::widget([
				'id' => 'subdealer-select',
				'name' => 'subdealer-id',
				'data' => $subdealers,
				'options' => [
					'placeholder' => 'Субдилер',		
				]
		])?>
	</div>
</div>
<br>
<?php 
	echo GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'condensed' => true,
		'pjax' => true,
		'responsive' => true,
		'columns' => [
			'subdealerName',
			'name',
			'address',	
			'brokerName',
		],
	]);
