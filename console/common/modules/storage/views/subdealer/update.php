<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Provider */

$this->title = 'Редактирование профиля субдилера: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Субдилеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $subdealerModel->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="subdealer-update">

    <h1><?= Html::encode($this->title) ?></h1>
<?php 
echo $this->render('_form', [
								 'model' => $model,
								 'userModel' => $userModel,
								 'userController' => $userController,
								 'contractModel' => $contractModel,
								 'subdealerModel' => $subdealerModel,
	]);
?>

</div>
