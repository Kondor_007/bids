<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Organisation */

$this->title = $model->name;

$this->params['breadcrumbs'][] = ['label' => 'Субдилеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organisation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php 
        $fields = [ 'name', 'headMasterName', 'hasStamp' ];

        if( Yii::$app->user->can( 'showRequisite' ) )
        {
            $fields[] = 'INN';
            $fields[] = 'OGRN';
            $fields[] = 'OKATO';
            $fields[] = 'KPP';
            $fields[] = 'mts_condition';
            $fields[] = 'meg_condition';
            $fields[] = 'bee_condition';
            $fields[] = 'tel_condition';
            $fields[] = [
            				'attribute' => 'contractLinks',
            				'format' => 'html',
            			];
            $fields[] = 'address';
            $fields[] = 'addressLegal';
            $fields[] = 'addressPost';
        }

        if( Yii::$app->user->can( 'showContacts' ) )
        {  
            $fields[] = 'phone';
            $fields[] = 'email:email';
        }
    ?>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $fields
    ]) ?>

</div>
