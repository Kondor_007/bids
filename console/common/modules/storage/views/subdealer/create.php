<?php

use yii\helpers\Html;
use kartik\widgets\Select2;

$this->title = 'Новый субдилер';
$this->params['breadcrumbs'][] = ['label' => 'Субдилеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs("
		$('#clone-check').change(function(e)
		{	
			$('#clone-subdealer').slideToggle();
			$('#new-subdealer').slideToggle(600);	
		});
		");

		

?>

<h2><?= $this->title ?></h2>

<?php echo Html::checkbox('clone-existing', false, ['id' => 'clone-check',
													'label' => 'Связать с организацией'
													]) ?>
<div id = 'clone-subdealer' hidden>
	<div class = 'col-md-6'>
	<?= Select2::widget([
			'id' => 'FK_org',
			'name' => 'FK_organisation',
			'data' => $unbindedOrgs,
	]) ?>
	</div>
	<div class = 'form-group'>
		<button id = 'submit-clone' class = 'btn btn-success '>Связать</button>
	</div>
</div>
													
													
<div id = 'new-subdealer'>
<?php 
echo $this->render('_form', [
								 'model' => $model,
								 'userModel' => $userModel,
								 'userController' => $userController,
								 'subdealerController' => $subdealerController,
								 'contractModel' => $contractModel,
								 'subdealerModel' => $subdealerModel,
	]);
?>
</div>