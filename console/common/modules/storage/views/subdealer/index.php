<?php

use vendor\novik\flexform\FlexibleGrid;
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Субдилеры';

$this->registerJs("
		$('.subdealer-row').mousedown( function( e )
		{
			$(this).css({'border-color': 'red'});
		});
		$('.subdealer-row').dblclick(function(e)
		{
			window.location.href = 'index.php?r=storage/store-unit/list-of&FK_org='+$(this).attr('value');
		});");
?>

<h2><?= $this->title ?></h2>
<?php
echo FlexibleGrid::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'templateName' => 'yyy',
		'responsive' => true,
		'hover' => true,
		'condensed' => true,
		'toolbar' => [
			[
				'content' =>'<a class = "btn btn-primary" href = "index.php?r=storage/subdealer/create"><span class = "glyphicon glyphicon-plus" ></span></a>',					
			],
			'{export}',
			'{toggleData}',
		],
		'panel'=> [
	        'type'=> FlexibleGrid::TYPE_PRIMARY,
    	],
		'rowOptions' => function($model, $key ){
			return [
					'class' => 'subdealer-row',
					'value' => $key,
					'title' => 'Двойной щелчек для просмотра ТТ'
			];	
		},
		
		'columns' => [
				[ 'class' => 'yii\grid\SerialColumn' ],
				[ 
						'class' => 'kartik\grid\ExpandRowColumn',
						'value' => function( $model )
						{
							return kartik\grid\GridView::ROW_COLLAPSED;
						},
						'detailUrl' => 'index.php?r=storage/store-unit/stores-list',
				],
				[
					'attribute' =>	'name',
				],
				[
					'attribute' => 'contractLinks',
					'content' => function( $model, $key, $index ){
						return $model->contractLinks;
					},
				],
				'headMasterName',
				'storesCount',
				[
					'attribute'=>'has_stamp',
					'content' => function ( $model, $key, $index ){
						return ($model->has_stamp == 1)?"Есть печать":"Нет печати";
					}
				],
				'address',
				'addressLegal',
				'addressPost',
				'INN',
				'KPP',
				'OGRN',
				'OKATO',
				'mts_condition',
				'meg_condition',
				'bee_condition',
				'tel_condition',
				
				[
						'class' => 'kartik\grid\ActionColumn',
						'dropdown' => true,
						'buttons'=>
						[
							'attach-contract'=> function ( $url, $model, $key )
							{
								return Html::a( '<span class="glyphicon glyphicon-cog"></span>', $url,
										[ 'title'=>Yii::t('yii', 'Assign role' ),] );
							}
						],
            			'template' => '{view} {update} {delete} {attach-contract}',
						'controller'=>'subdealer',
				],
		],
]);