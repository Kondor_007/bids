<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;
use common\components\modalWidgets\AddWidget;
use yii\bootstrap\Modal;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model subdealerClass instance */
/* @var $userModel employeer profile class instance */
/* @var $form yii\widgets\ActiveForm */ 

$this->registerJs("
		$('#attach-contract-check').change(function(e)
		{
			$('div#contract-block').slideToggle();
		});
		
		$('#submit-clone').click(function(e)
		{
			cloneSub($('#FK_org').val());
		});
		
		function cloneSub( id )
		{
			$.ajax({
				url: 'index.php?r=storage/subdealer/create',
				type: 'POST',
				data: $('#w0').serialize() + '&clone-existing=1&FK_organisation='+id,
				dataType: 'json',
				success: function(response)
				{
					if( response.status = 99 )
					{
						alert('Error during binding');
					}
					else
					{
						window.location.replace('index.php?r=storage/subdealer/index'); 
					}
				}
			});
		}");

?>

<div class="organisation-form">

    <?php $form = ActiveForm::begin([
    		'action' => 'index.php?r=storage/subdealer/'.( $model->isNewRecord?'create':'update&id='.$subdealerModel->id ),
    		'options' => [ 
    				'enctype' => 'multipart/form-data'
    				]
    ]); ?>
    <!-- =============== -->
    <!-- Блок о компании -->
    <!-- =============== -->
        <h3>О компании</h3>
        <?= $form->field($model, 'short_name')->textInput(['maxlength' => 1024]) ?>

        <?= $form->field($model, 'full_name')->textInput(['maxlength' => 1024]) ?>
    
        <?php 
        	        	
            $userList = ArrayHelper::map( $userModel::find()->all(), 'id', 'fullName' );
            Pjax::begin(['id' => 'users-pjax']);
            echo $form->field($model, 'FK_us_ext')
                                ->dropDownList(
                                $userList,           
                                ['id'=>'user-head-list',
                                'prompt'=>'-- Выберите пункт списка --']
                                );
            Pjax::end();

            // Button trigger modal
            echo '<div class="form-group">'.Html::button( Yii::t( 'yii', 'Добавить руководителя' ), [ 
                            'class' => 'btn btn-primary',
                            'data-toggle'=>'modal', 
                            'data-target' => '#addUserModal',
                            ]).'</div>';
        ?>
    
    <!-- =============== -->
    <!-- Блок реквизиты  -->
    <!-- Видят только пользователи с разрешением -->
    <?php if( \Yii::$app->user->can('showRequisite') ): ?>
    <div class = 'form-data-block'>
        <h3>Реквизиты</h3>

        <?= $form->field($model, 'INN')->textInput(['maxlength' => 1024]) ?>

        <?= $form->field($model, 'OGRN')->textInput(['maxlength' => 1024]) ?>

        <?= $form->field($model, 'KPP')->textInput(['maxlength' => 1024]) ?>

        <?= $form->field($model, 'OKATO')->textInput(['maxlength' => 1024]) ?>

        <?= $form->field($model, 'act_on_authority')->textInput(['maxlength' => 1024]) ?>
    </div>
    <?php endif;?>
    <!-- =============== -->
    <!-- Блок контактная информация -->
    <!-- =============== -->
    <!-- Видят только пользователи с разрешением -->
    <h3>Контактная информация</h3>
    <?php if( \Yii::$app->user->can('showContacts') ): ?>
        <?= $form->field($model, 'address')->textInput(['maxlength' => 4096]) ?>
        <?= $form->field($model, 'address_legal')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'address_post')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'phone')->widget(MaskedInput::classname(), [
                'name' => 'phone-input',
                'mask' => '+7(999) 999-99-99',
        ]);?>
        <?= $form->field($model, 'web_site')->textInput(['maxlength' => 4096]) ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => 1024]) ?>
    <?php endif; ?>
    
    <?= $form->field($subdealerModel, 'mts_condition' )->textInput(['maxlength' => 255]) ?>
    <?= $form->field($subdealerModel, 'meg_condition' )->textInput(['maxlength' => 255]) ?>
    <?= $form->field($subdealerModel, 'bee_condition' )->textInput(['maxlength' => 255]) ?>
    <?= $form->field($subdealerModel, 'tel_condition' )->textInput(['maxlength' => 255]) ?>
	
	<div class = 'form-group'>
    	<label for = 'attach-contract-check'><input type = 'checkbox' name = 'attach-contract' id = 'attach-contract-check'> Прикрепить договор</label>
    </div>
    <div class = 'form-group' id = 'contract-block' hidden>
	    <div class = 'col-md-4'>
	    	<?= $form->field( $contractModel, 'name')->textInput() ?>
	    </div>
	    <div class = 'col-md-4'>
	    	<?= $form->field( $contractModel, 'number')->textInput() ?>
	    </div>
	    <div class = 'col-md-4'>
	    	<?= $form->field( $contractModel, 'file_path')->widget( FileInput::className(), [
													    		'pluginOptions' => [
													    			'showUpload' => false,
													    			'browseLabel' => '',
													    			'removeLabel' => '',
													    			]
													    		]) ?>
		   
		    <?php FileInput::widget([
		    		'id' => 'contracts-files',
		    		'name' => 'contract-file',
		    		'pluginOptions' => [
		    			'showUpload' => false,
		    			'browseLabel' => '',
		    			'removeLabel' => '',
		    		],
		    ]) ?>
	    </div>
    </div>
	    
    <?= $form->field($model, 'description')->textArea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', [ 'id' => $model->isNewRecord ? 'subdealer-submit':'subdealer-update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php 
        ActiveForm::end(); 

        Modal::begin([
        		'id' => 'addUserModal',
        		'header' => "<h2>Добавить</h2>",
        		'footer' => "<button id = 'user-modal-submit' class = 'btn btn-success'>Добавить</button>"	
        ]);
        
        $form = ActiveForm::begin(['id' => 'phantom-form']);
                
        echo $form->field( $userModel, 'LastName' )->textInput(['maxlength' => 1024]);
        echo $form->field( $userModel, 'FirstName' )->textInput(['maxlength' => 1024]);
        echo $form->field( $userModel, 'PatName' )->textInput(['maxlength' => 1024]);
		ActiveForm::end();
        Modal::end();
        
        $this->registerJs("
        		$('#user-modal-submit').click(function(e)
        		{
        			e.preventDefault();
        			$.ajax({
        				url: 'index.php?r={$userController}/create-phantom',
        				type: 'POST',
        				data: $('#phantom-form').serialize(),
        				success: function(response)
        				{
        					$.pjax.reload({container:'#users-pjax'});
        					$('.modal').modal('hide');
        				}
        			});
        		});
        		");

    ?>
</div>
	