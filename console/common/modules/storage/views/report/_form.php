<?php

use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\jui\DatePicker;
use vendor\novik\flexform\FlexibleForm;
use yii\helpers\Url;

$this->registerJs("
		$('#flexform-submit').click( function(e)
		{
			var data = $('#w0').serialize();
			$.pjax({
				url: 'index.php?r=storage/report/report-view',
				data: data,
				container: '#flexible-grid-pjax',				
			});
		});");


$form = FlexibleForm::begin(['formModel' => $reportModel, 'submitTitle' => 'Найти']);

?>
<?= $form->field( $reportModel, 'subdealer' )->widget( Select2::className(), [
													'data' => $filterList['subdealers'], 
													'options' => [ 'id' => 'subdealer-id', 'placeholder' => 'Субдилер' ], 		
													]) ?>
<?= $form->field( $reportModel, 'store' )->widget( DepDrop::className(), [
													'data' => $filterList['stores'],
													'type' => DepDrop::TYPE_SELECT2,
													'options' => [
															'id' => 'stores-id',
															'placeholder' => 'Торговые точки'
													],
													'select2Options' => [
														'pluginOptions' => ['allowClear' => true]	
													],
													'pluginOptions' => [
														'depends' => ['subdealer-id'],
														'url' => Url::to(['/storage/subdealer/get-stores']),	
													],
													
											 ] ) ?>
<?= $form->field( $reportModel, 'provider' )->widget( Select2::className(), [
													'data' => $filterList['providers'], 
													'options' => [ 'id' => 'provider-id', 'placeholder' => 'Провайдер' ], 		
													]) ?>
													
<?= $form->field( $reportModel, 'FK_tovar' )->widget( DepDrop::className(), [
													'data' => $filterList['products'],
													'type' => DepDrop::TYPE_SELECT2,
													'options' => [
															'id' => 'product-id',
															'placeholder' => 'Номенклатура'
													],
													'select2Options' => [
														'pluginOptions' => ['allowClear' => true]	
													],
													'pluginOptions' => [
														'depends' => ['provider-id'],
														'url' => Url::to(['/storage/provider/get-products']),	
													],
													
											 ] ) ?>
<?= $form->field( $reportModel, 'states' )->widget( Select2::className(), [
													'data' => $filterList['states'], 
													'options' => [ 'id' => 'states-id', 'multiple' => true, 'placeholder' => 'Состояние товара' ], 		
													]) ?>

<?= $form->field( $reportModel, 'serial' )->textInput(['maxlength' => 19 ]) ?>

<?= $form->field( $reportModel, 'sim_number' )->textInput(['maxlength' => 13 ]) ?>

<div class = "row">
	<div class = "col-md-6">
		<?= $form->field( $reportModel, 'date_from' )->widget( DatePicker::className(),[
																'dateFormat' => 'yyyy-MM-dd',
																'options' => [
																		'class' => 'form-control'
																]
		] ) ?>
	</div>
	<div class = "col-md-6">
		<?= $form->field( $reportModel, 'date_to' )->widget( DatePicker::className(),[
																'dateFormat' => 'yyyy-MM-dd',
																'options' => [
																		'class' => 'form-control'
																]
		] ) ?>
	</div>
</div>

<?= $form->field( $reportModel, 'activation_date' )->widget( DatePicker::className(),[
														'dateFormat' => 'yyyy-MM-dd',
														'options' => [
																'class' => 'form-control'
														]
] ) ?>

<?= $form->field( $reportModel, 'registration_date' )->widget( DatePicker::className(),[
															'dateFormat' => 'yyyy-MM-dd',
															'options' => [
																	'class' => 'form-control'
															]
] ) ?>

<?php 
FlexibleForm::end();
