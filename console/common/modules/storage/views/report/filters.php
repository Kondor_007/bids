<?php

use kartik\widgets\Select2;
use yii\widgets\ActiveForm;

$this->registerJs("
	$('#submit-filter').click( function(e)
	{
		e.preventDefault();
		$.ajax({
			url: 'index.php?r=storage/report/view',
			data: $(this.form).serialize(),
			type: 'POST',
			dataType: 'json',
			success: function( response )
			{
				if( response.status == 1 )
				{
					console.log(response.msg);
					$('#table-block').html( response.html.table );
				}
				else
				{
					console.log(response);
				}
			}
		});
	});
	
	$('#broker-select').change(function(e)
	{
		$.ajax({
			url: 'index.php?r=storage/report/get-stores',
			data: '&broker=' + $(this).val(),
			dataType: 'json',
			type: 'POST',
			success: function ( response )
			{
				console.log(response);
				if( response.status == 1 )
				{
					var select = '';
					for( var key in response.stores )
					{
                        select += '<option value =\'' + key + '\'>' +
                                   response.stores[key] + '</option>';
                    }
					$('#store-select').html( select ).select2();
					$('#store-select').change();
				}
			}
		});
	});

	$('#in-store-option').change( function(e)
	{
		$( '#store-filter-block' ).removeAttr('hidden');
	});

	$('#in-storage-option').change( function(e)
	{
		$('#store-filter-block').attr('hidden', true);	
	});

	");

?>

<?php ActiveForm::begin(['action' => 'index.php?r=storage/report/view', 
						 'options'=>['name' => 'filter-form',
						 			 'data-pjax' => true, 
						 			 ]]) ?>
<div class="row">
	<div class="col-md-8">
		<div class = 'form-group'>
			<div class="btn-group btn-group-justified" data-toggle="buttons">
	  			<label class="btn btn-default active">
	    		<input type="radio" name="searchLocation" value = 'storage' id="in-storage-option" autocomplete="off" checked> Склад
	  			</label>
	  			<label class="btn btn-default">
	    		<input type="radio" name="searchLocation" value = 'store' id="in-store-option" autocomplete="off" > Торговые точки
	  			</label>
			</div>
		</div>
	</div>
</div>
<div id = 'store-filter-block' class="row" hidden>
	<div class="col-md-4">
		<label for = "broker-select">Торговые представители</label>
		<?= Select2::widget([
			'id' => 'broker-select',
			'name' => 'broker',
			'data' => $filterList['brokers'],
			'options' => [ 'placeholder' => 'Выберите торгового представителя'],
		]); ?>
	</div>
	<div class="col-md-4">
		<div class = 'form-group'>
			<label for = "store-select">Торговые точки</label>
			<?= Select2::widget([
				'id' => 'store-select',
				'name' => 'store',
				'data' => $filterList['stores'],
				'options' => ['placeholder' => 'Выберите торговую точку'],
			]); ?>
		</div>
	</div>
	<div class = 'col-md-4'>
		<label for = "state-select">Состояние товара</label>
		<?= Select2::widget([
			'id' => 'state-select',
			'name' => 'states',
			'data' => $filterList['states'],
			'options' => ['placeholder' => 'Выберите состояния', 'multiple' => true ],
		]); ?>
	</div>
</div>

<div class="row form-group">
	<div class = 'col-md-4'>
		<label for = "provider-select">Провайдер</label>
		<?= Select2::widget([
			'id' => 'provider-select',
			'name' => 'providers',
			'data' => $filterList['providers'],
			'options' => ['placeholder' => 'Выберите провайдера', 'multiple' => true ],
		]); ?>
	</div>
	<div class="col-md-4">
		<label for = "product-select">Продукт</label>
		<?= Select2::widget([
			'id' => 'product-select',
			'name' => 'products',
			'data' => $filterList['products'],
			'options' => ['placeholder' => 'Выберите продукт', 'multiple' => true ],
		]); ?>
	</div>
	<div class="col-md-4">
	</div>
	<div class="col-md-4">
	</div>
</div>

<div class="row">
	<div class = 'col-md-4'>
		
	</div>
	<div class="col-md-4">
	</div>
	<div class="col-md-4">
	</div>
</div>
<div class = 'form-group'>
	<button id = 'submit-filter' class="btn btn-primary">Сформировать</button>
</div>
<?php ActiveForm::end(); ?>