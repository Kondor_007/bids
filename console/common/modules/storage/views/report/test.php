<?php

use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use common\modules\storage\models\Broker;
//use common\modules\storage\components\flexform\FlexibleForm;
use vendor\novik\flexform\FlexibleForm;
use common\modules\storage\models\ReportSearch;

$model = new ReportSearch;
?>
<div class = 'col-md-10'>
<?php

$brokers = ArrayHelper::map( Broker::find()->all(), 'id', 'name' );

$filter = FlexibleForm::begin([ 'formModel' => $model ]);
?>
	<?= $filter->field( $model, 'broker' )->widget( Select2::className(), 
											[
												'data' => $brokers, 
												'options' => [
												'multiple' => true,
												'placeholder' => 'Брокер',
												]
											]) ?>
	<?= $filter->field( $model, 'store' )->dropdownList($brokers) ?>
	<?= $filter->field( $model, 'searchLocation' )->textInput() ?>
<?php
FlexibleForm::end();
?>
</div>
