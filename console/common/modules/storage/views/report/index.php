<?php


$this->title = 'Отчеты';
$this->params['breadcrumbs'][] = ['label' => 'Склад', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h2>Отчеты</h2>
<div id = 'filter-block' class = 'row'>
	<?= $this->render('_form', [ 'reportModel' => $reportModel, 'filterList' => $filterList ]); ?>
</div>

<div id = 'table-block' class = 'row'>
	
</div>