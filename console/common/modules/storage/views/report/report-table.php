<?php

use vendor\novik\flexform\FlexibleGrid;
use yii\widgets\Pjax;

$gridColumns = [
	[
		'class' => '\kartik\grid\DataColumn',
		'attribute' => 'brokerName',
	],
	[
		'class' => '\kartik\grid\DataColumn',
		'attribute' => 'subdealerName',	
	],
	[
		'class' => '\kartik\grid\DataColumn',
		'attribute' => 'storeName',
	],
	
	[
		'class' => '\kartik\grid\DataColumn',
		'attribute' => 'providerName',
	],
	[
		'class' => '\kartik\grid\DataColumn',
		'attribute' => 'productName',
	],
	[
		'class' => '\kartik\grid\DataColumn',
		'attribute' => 'serial',
	],
	[
		'class' => '\kartik\grid\DataColumn',
		'attribute' => 'sim_number',
	],
		
	[
		'class' => '\kartik\grid\DataColumn',
		'attribute' => 'birthday',
	],
];
echo FlexibleGrid::widget([
	'id' => 'report-grid',
	'templateName' => 'reports',
    'dataProvider'=> $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumns,
    'hover' => true,
    'condensed' => true,
    'panel' => [
			        'type' => FlexibleGrid::TYPE_PRIMARY,
			        //'heading' => $heading,
				],
	
]);
?>