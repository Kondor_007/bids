<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\modules\storage\models\Provider;

$this->registerJsFile( '/crm/common/modules/storage/assets/plugins/jquery.ajaxupload.js', [ 'depends' => [ 'yii\web\JqueryAsset' ], 
																							'position' => yii\web\View::POS_END ] );
$this->registerJs( "
		$('#updating-submit').click( startImport );


		function startImport( e )
		{
			e.preventDefault();
			$('#upload-form').ajaxUpload(
			{
				url: 'index.php?r=storage/updating/import',
				beforeSend: function( input ){
					$('body').prepend('<div class=\'overlay\'>');
					$('body').append('</div>');
					$('.overlay').css({
				    'position': 'absolute',
				    'width': $(document).width(), 
				    'height': $(document).height(),
				    'z-index': 99999, 
					}).fadeTo(0, 0.8);
					$('#status').text('Загрузка...');
					$('#warning').text('Не закрывайте и не обновляйте данную страницу!');
				},
				success: function ( responseData, statusText )
				{	
					var response = JSON.parse( responseData );
					console.log( response );
					if( response.status == 1 )
					{	
						//$('#fileName').text( response.file );
						$('#status').html('<font color = green>Загружено</font>');
						parseData();
					}
					else 
					{
						success = 'true';
						$('.overlay').remove();
						$('#status').html('<font color = green>Ошибка</font>');
						return;
					}
				},
				complete: function( responseData ){
					//return responseData;
					//console.log( responseData );
					//Очищаем текст статуса
					//var response = JSON.parse( responseData );
					
				},

			});
		}

		function parseData()
		{
			$.ajax({
				url: 'index.php?r=storage/updating/update',
				type: 'POST',
				success: function( responseData, statusText )
				{
					var response = JSON.parse( responseData );
					console.log( response );
					switch( response.status )
					{
						case 0:
							$('.overlay').remove();
							console.log( response.message );
							$('#status').html('<font color = green>Актуализация завершена</font>&nbsp' );
							//$('#unequal-table').html( response.html.unequal );
							//$('#missing-table').html( response.html.missing );
							$('#info').html( response.html.info );
							$('#warning').empty();
						break;

						case 1:
							$('#status').html('<font color = blue>Обрабатывается</font>&nbsp' + response.process );
							console.log( response.error );
							parseData();
						break;

						case 99:
							$('.overlay').remove();
							$('#status').html('<font color = red>Ошибка</font>&nbsp' + response.error );
						break;

					}
				}
			});
		}

		$( '#provider-list' ).change( function(e) 
			{
				if( $(this).val() )
				{
					$('#updating-submit').removeAttr('disabled');
				}
				else
				{
					$('#updating-submit').attr('disabled', true );	
				}
			});
	", yii\web\View::POS_END );
?>

<form id = 'upload-form' enctype="multipart/form-data">
	<input name = 'operation-id' type = 'hidden' ></input>
	<div class = 'form-group'>
		<?= Html::dropDownList( 'provider-id', 0, $providerList, [ 'class' => 'form-control',
																	 'id' => 'provider-list',
																	 'prompt' => '-- Выберите провайдера --' ] ) ?>
	</div>
	<div class = 'form-group'>
		<input class = 'form-control' style="border: 0" name = "import" type="file" ></input>
		<button class = "btn btn-primary form-control" id = 'updating-submit'>Запустить актуализацию</button>
	</div>
	<font size = 6 color = red >
		&nbsp<span id="status" ></span>
	</font>
	<br>
	<font size = 4 color = red >
		<span id="warning" ></span>
	</font>
</form>

