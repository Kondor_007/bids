<?php

use kartik\grid\GridView;
use kartik\export\ExportMenu;
/*$this->registerJs( "
		");*/
$this->title = 'Несоответствующие даты';
$this->params['breadcrumbs'][] = ['label' => 'Склад', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<h2>Несоответствующие даты</h2>
<?php
	
	$columns =[
	            [
	            	'value'=>'product',
	            	'label' => 'Тариф',
	            ],
	            [
	                'value'=>'phone_number',
	                'label' => 'Абонентский номер',
	            ],
	            [
	            	'value' => 'serial',
	            	'label' => 'Серийный номер',
	            ],
	            'active',
	            'tempActive',
	            'reg',
	            'tempReg',
	            //'registration_date',
	            // 'activation_date',

	        ];

	echo GridView::widget([
        'id' => 'unequal-grid',
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'export' => false,
    ]);

    echo ExportMenu::widget([
    		'dataProvider' => $dataProvider,
    		'columns' => $columns,
    		'target' => ExportMenu::TARGET_SELF,
    		'initProvider' => true,
    		'showConfirmAlert' => false,
    		'exportConfig' => [
    			ExportMenu::FORMAT_TEXT => false,
    			ExportMenu::FORMAT_PDF => false,
    			ExportMenu::FORMAT_CSV => false,
    			ExportMenu::FORMAT_HTML => false,
    			ExportMenu::FORMAT_EXCEL_X => [
										        'label' => 'Excel 2007',
										        'icon' => 'file-excel-o',
										        'iconOptions' => ['class' => 'text-success'],
										        'linkOptions' => [],
										        'options' => ['title' => 'Microsoft Excel 2007+ (xlsx)'],
										        'alertMsg' => 'The EXCEL 2007+ (xlsx) export file will be generated for download.',
										        //'mime' => 'application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
										        'extension' => 'xlsx',
										        'writer' => 'Excel2007'
										    ],
    		],
    		'onRenderDataCell' => function ( $cell, $content )
    		{ 
	    		$cell->setValueExplicit( $content, PHPExcel_Cell_DataType::TYPE_STRING );
    		},
    	]);
?>

