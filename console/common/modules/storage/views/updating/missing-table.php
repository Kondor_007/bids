<?php

use kartik\grid\GridView;
use kartik\export\ExportMenu;
/*$this->registerJs( "
		");*/
$this->title = 'Отсутствующие на складе';
$this->params['breadcrumbs'][] = ['label' => 'Склад', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h2>Отсутствующие на складе</h2>
<?php
	
	$columns =[
				[ 'class' => 'kartik\grid\CheckboxColumn' ],
	            [
	            	'value'=>'product',
	            	'label' => 'Тариф',
	            ],
	            [
	            	'value' => 'abon',
	            	'label' => 'Тел. номер',
	            ],
	            [
	            	'value' => 'serial',
	            	'label' => 'Серийный номер',
	            ],
	            
	            //'registration_date',
	            // 'activation_date',

	        ];

	echo GridView::widget([
        'id' => 'missing-grid',
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'floatHeader' => true,
        'responsive' => true,
        'hover' => true,
        'pjax' => true,
        'panel' => [
			        'type' => GridView::TYPE_PRIMARY,
			        //'heading' => $heading,
				    ],
		'export' => false,
    ]);

    echo ExportMenu::widget([
    		'dataProvider' => $dataProvider,
    		'columns' => $columns,
    		'target' => ExportMenu::TARGET_SELF,
    		'showConfirmAlert' => false,
    		'initProvider' => true,
    		'exportConfig' => [
    			ExportMenu::FORMAT_TEXT => false,
    			ExportMenu::FORMAT_PDF => false,
    			ExportMenu::FORMAT_CSV => false,
    			ExportMenu::FORMAT_HTML => false,
    			ExportMenu::FORMAT_EXCEL_X => [
										        'label' => 'Excel 2007',
										        'iconOptions' => ['class' => 'text-success'],
										        'linkOptions' => [],
										        'options' => ['title' => 'Microsoft Excel 2007+ (xlsx)'],
										        'alertMsg' => 'The EXCEL 2007+ (xlsx) export file will be generated for download.',
										        'extension' => 'xlsx',
										        'writer' => 'Excel2007'
										    ],
    		],
    		'onRenderDataCell' => function ( $cell, $content )
    		{ 
	    		$cell->setValueExplicit( $content, PHPExcel_Cell_DataType::TYPE_STRING );
    		},
    	]);
?>


