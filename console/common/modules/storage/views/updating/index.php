<?php

$this->title = 'Актуализация склада';
$this->params['breadcrumbs'][] = ['label' => 'Склад', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<h2><?= $this->title ?></h2>
<div class = "row">
	<div class = "col-md-5">
		<?= $this->render( 'import', [ 'providerList' => $providerList ] ) ?>
	</div>
	<div class = "col-md-5" id = "info">
	</div>
</div>
