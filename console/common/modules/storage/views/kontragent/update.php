<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Kontragent */

$this->title = 'Update Kontragent: ' . ' ' . $model->id_kontragent;
$this->params['breadcrumbs'][] = ['label' => 'Kontragents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_kontragent, 'url' => ['view', 'id' => $model->id_kontragent]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kontragent-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
