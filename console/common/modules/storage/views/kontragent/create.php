<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Kontragent */

$this->title = 'Create Kontragent';
$this->params['breadcrumbs'][] = ['label' => 'Kontragents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kontragent-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
