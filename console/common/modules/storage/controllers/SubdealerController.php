<?php

namespace common\modules\storage\controllers;

use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

use common\modules\storage\models\Subdealer;
use common\modules\storage\models\SubdealerSearch;
use common\modules\storage\classes\DepDropFormatter;
use yii\web\NotFoundHttpException;


class SubdealerController extends Controller
{
	
	public function actionIndex()
	{
		
		$searchModel = new SubdealerSearch();
		
		$dataProvider = $searchModel->search(\Yii::$app->request->queryParams );
		
		return $this->render( 'index', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel] );
	}
	
	public function actionView( $id )
	{
		return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
	}
	
    public function actionGetStores()
    {
    	$stores = [];
    	if( !empty( $_POST['depdrop_parents'] ) )
    	{
    		$subdealerId = $_POST['depdrop_parents'][0];
    		
    		$out = ArrayHelper::map( Subdealer::findOne( $subdealerId )->stores, 'id', 'name' );
    		echo Json::encode(['output' => DepDropFormatter::format( $out ), 'selected' => '' ]);
    		return;
    	}
    	
    	if( isset($_POST['id']) )
    	{
    		$model = Subdealer::findOne( $_POST['id'] );
	    	
	    	if( isset( $model ) )
	    		$stores = ArrayHelper::map( $model->stores, 'id', 'name' );
    	}

        return json_encode([ 'status' => 1, 'stores' => $stores ]);
    }
    
    public function actionCreate()
    {
    	$orgClass = $this->module->subdealerClass;
    	$model = new $orgClass();
    	
    	if( $model->load( \Yii::$app->request->post() ) && $model->save() )
    	{
    		$model->refresh();
    		$subModel = new Subdealer();
    		$subModel->FK_organisation = $model->id;
    		$subModel->save();
    		
    		if( $_POST['attach-contract'] )
    			$this->uploadContracts( $model->id );
    		
    		return $this->redirect(['index']);
    	}
    	else if( isset($_POST['FK_organisation']) && isset( $_POST['clone-existing'] ) )
    	{
    		$subModel = new Subdealer();
    		$subModel->load($_POST);
    		$subModel->FK_organisation = $_POST['FK_organisation'];
    		if( $subModel->save() )		
    			return $this->redirect(['index']);
    		else
    			return json_encode(['status' => 99, 'error' => print_r( $subModel->getErrors(), true ) ]);
    	}
    	
    	$userClass = $this->module->employeeClass;
    	$profileClass = get_class( $userClass::find()->one()->profile );
    	$contractClass = $this->module->contractClass;
    	$contractModel = new $contractClass;
    	$userModel = new $profileClass; 
    	$userModel->scenario = 'createByManager';
    	
    	$unbindedOrgs = ArrayHelper::map( $orgClass::find()->where('id NOT IN (select FK_organisation from storage_subdealer)')->all(), 'id', 'name');
    	
    	return $this->render('create', ['model' => $model,
    									'userModel' => $userModel,
    									'contractModel' => $contractModel,
    									'subdealerController' => $this->module->subdealerController,
    									'userController' => $this->module->employeeController,
    									'unbindedOrgs' => $unbindedOrgs,
    									'subdealerModel' => (new Subdealer), 
    	]);
    } 
    
    private function uploadContracts( $id )
    {
    	if( isset( $_FILES['Contract'] ) )
    	{
    		$allowed_filetypes = [ '.doc','.docx', '.pdf', '.odt' ];
    		$file_name = $_FILES['Contract']['name']['file_path'];
    		$destinationPath = \Yii::$app->basePath . '\\files\\documents\\contracts\\';
    		$ext = substr($file_name, strpos($file_name,'.'), strlen($file_name)-1);
    
    		//---- Валидация и перемещение файла в постоянную директорию
    		if( !in_array( $ext, $allowed_filetypes ) )
    		{
    			return 'Некорректный формат файла';
    		}
    		if ( ! move_uploaded_file($_FILES["Contract"]["tmp_name"]["file_path"],
    				$destinationPath.$file_name ) )
    		{
    			return "Не удалось загрузить файл";
    		}
    		$class = $this->module->contractClass;
    		$model = new $class;
    		$model->load($_POST);
    
    		$model->name = isset($model->name)?$model->name:$file_name;
    		$model->number = isset($model->number)?$model->number:date('ymdhis');
    
    		$model->FK_organisation = $id;
    		$model->file_path = $destinationPath.$file_name;
    
    		$model->save();
    
    		return "";
    	}
    	return 'Error';
    }

    public function actionDownloadContract( $id )
    {
    	$class = $this->module->subdealerClass;
    	$contract = $class::getContractById($id);
    	$file = $contract->file_path;
    	if (file_exists($file)) {
    		// сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
    		// если этого не сделать файл будет читаться в память полностью!
    		if (ob_get_level()) {
    			ob_end_clean();
    		}
    		// заставляем браузер показать окно сохранения файла
    		header('Content-Description: File Transfer');
    		header('Content-Type: application/octet-stream');
    		header('Content-Disposition: attachment; filename=' . basename($file));
    		header('Content-Transfer-Encoding: binary');
    		header('Expires: 0');
    		header('Cache-Control: must-revalidate');
    		header('Pragma: public');
    		header('Content-Length: ' . filesize($file));
    		// читаем файл и отправляем его пользователю
    		readfile($file);
    		exit;
    	}
 
    }
    
    public function actionUpdate( $id )
    {
    	$subdealerModel = $this->findModel($id);
    	$orgClass = $this->module->subdealerClass;
    	$model = $orgClass::findOne( $subdealerModel->FK_organisation );
    	
    	if ( ($model->load(\Yii::$app->request->post()) && $model->save()) &&
    		($subdealerModel->load(\Yii::$app->request->post()) && $subdealerModel->save()) ) {
    		if( $_POST['attach-contract'] )
    			$this->uploadContracts( $model->id );
    		
    		return $this->redirect(['view', 'id' => $subdealerModel->id]);
    	}
    	
    	$userClass = $this->module->employeeClass;
    	$profileClass = get_class( $userClass::find()->one()->profile );
    	$contractClass = $this->module->contractClass;
    	$contractModel = new $contractClass;
    	$userModel = new $profileClass;
    	$userModel->scenario = 'createByManager';
    	 
    	$unbindedOrgs = ArrayHelper::map( $orgClass::find()->where('id NOT IN (select FK_organisation from storage_subdealer)')->all(), 'id', 'name');
    	 
    	return $this->render('update', [
    			'model' => $model,
    			'userModel' => $userModel,
    			'contractModel' => $contractModel,
    			'userController' => $this->module->employeeController,
    			'subdealerModel' => $subdealerModel,
    	]);
    }
    		
    public function actionDelete( $id )
    {
    	$this->findModel($id)->delete();
    	return $this->redirect(['index']);
    	
    }

    protected function findModel($id)
    {
    	if (($model = Subdealer::findOne($id)) !== null) {
    		return $model;
    	} else {
    		throw new NotFoundHttpException('The requested page does not exist.');
    	}
    }
}
