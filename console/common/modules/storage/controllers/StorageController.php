<?php

namespace common\modules\storage\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\Session;
use common\modules\storage\models\Storage;
use common\modules\storage\models\Provider;
use common\modules\storage\models\StorageSearch;
use common\modules\storage\models\TempExel;
use common\modules\storage\models\Tovar;
use common\modules\storage\Module;
use common\modules\storage\classes\ChunkReadFilter;
use common\modules\storage\models\StorageOperation;
use common\modules\storage\models\StoragePreference;
use common\modules\storage\models\Doc;
use common\modules\storage\classes\XlsToCsvConverter;


require_once(Yii::getAlias('@vendor/phpoffice/phpexcel/Classes/PHPExcel.php'));
/**
 * StorageController implements the CRUD actions for Storage model.
 */
class StorageController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Creates a new Storage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Storage();
        $session = new Session;
        $session->open();
        if( !isset( $session['operation-id'] ) )
        {
            return json_encode( [ 'status' => 99, 'error' => 'Код операции отсутствует' ] );
        }

        if ( !( $model->load(Yii::$app->request->post()) && $model->save() ) )
        {
            return json_encode( [ 'status' => 99, 'error' => $model->getErrors() ] );
        } 

        $model->refresh();

        $error = $this->registerStorageOperation( $model->id, $session['operation-id'], \Yii::$app->user->identity->id );

        return json_encode( [ 'status' => 0, 'id' => $model->id, 'error' => $error ] ); 
    }


    /**
     * Deletes an existing Storage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['orders-view']);
    }
    
    public function actionView( $id )
    {
    	$model = $this->findModel($id);
    	return $this->render('view', ['model'=>$model]);
    }

    public function actionOrdersView()
    {
        $session = new Session;

        $session->open();

        $model = new Storage();
        $searchModel = new StorageSearch();
        $incompleteArray = StorageOperation::find()->select('operation_id, count(*) as count')->where(['FK_doc' => null])->groupBy('operation_id')->asArray()->all();

        if( isset( $_POST['operation-id'] ) )
        {
            $session['operation-id'] = $_POST['operation-id'];
        }
        else
        {
            if( !isset( $session['operation-id'] ) ) 
            {
                $session['operation-id'] = date('ymdhis');   
            }
        }

        $searchModel->opCodeFilter = $session['operation-id'];

        $searchModel->userIdFilter = \Yii::$app->user->identity->id;
        $dataProvider = $searchModel->search();
        $dataProvider->pagination->pageSize = 5;
        
        return $this->render('orders-view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'incomplete' => $incompleteArray,
            'current' => $session['operation-id'],
        ]);
    }

    public function actionOrdersTable()
    {
        $session = new Session;
        $session->open();

        $searchModel = new StorageSearch();
        $incompleteArray = StorageOperation::find()->select('operation_id, count(*) as count')->where([ 'FK_doc' => null ])->groupBy('operation_id')->asArray()->all();

        if( isset( $_POST['operation-id'] ) )
        {
            $session['operation-id'] = $_POST['operation-id'];
        }
        else
        {
            if( !isset( $session['operation-id'] ) || isset($_POST['new']) )  
            {
                $session['operation-id'] = date('ymdhis');   
            }
        }
        $searchModel->opCodeFilter = $session['operation-id'];
        $searchModel->realized = false;
        $searchModel->type = 1;
        $searchModel->userIdFilter = \Yii::$app->user->identity->id;
        $dataProvider = $searchModel->search();
        
        $dataProvider->pagination->pageSize = 5;

        $tableHtml = $this->renderAjax('orders-table', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

        $incompleteHtml = $this->renderAjax('incomplete-orders', [
            'incomplete' => $incompleteArray,
            'current' => $session['operation-id'],
        ]);

        return json_encode( [ 'status' => '1', 'table' => $tableHtml, 'incomplete' => $incompleteHtml ] );
    }


    /*
    * Загрузка файла на сервер
    */
    public function actionImport()
    {
        if ( !Yii::$app->request->isPost ) 
        {
            return json_encode( [ 'status' => 99, 'error' => 'POST required, something else taken' ] );    
        }

        $allowed_filetypes = [ '.xls','.xlsx' ];
        $file_name = $_FILES['import']['name'];
        $destinationPath = Yii::$app->basePath . '/temp/parsing/';
        $ext = substr($file_name, strpos($file_name,'.'), strlen($file_name)-1);
        
        //---- Валидация и перемещение файла в постоянную директорию
        if( !in_array( $ext, $allowed_filetypes ) )
        {
            return json_encode( ['status' =>99, 'error' => 'unknown extension' ]);       
        }
        if ( ! move_uploaded_file($_FILES["import"]["tmp_name"], 
                                $destinationPath.'temp'. $ext) )
        {                
            return json_encode( ['status' => 99, 'name' => 'upload failed' ]);
        }

        $session = new Session;
        $session->open();
        
        $session['provider-id'] = $_POST['provider-id'];
        //$session['operation-id'] = $_POST['operation-id'];
        $session['settings'] = $this->getImportSettings( $_POST['provider-id'] );


        $csvFilePath = str_replace( "\\", "/", XlsToCsvConverter::convert( $destinationPath.'temp'. $ext ) );
        $skipRow = - 1 + $session['settings']['start-row'];
        $columns = '(id, ';
        
        /*
        * Команда LOAD DATA INFILE 
        */
        if( ( $max = $this->findMaxField( $session['settings'] ) ) <= 10 )
        {
            for ( $i = 2; $i <= $max;  $i++ )
            { 
                $columns .= 'col'.$i.',';
            }

            $columns = rtrim( $columns, ',' ).')';
        }
        //TempExel::resetTable();
        \Yii::$app->db->createCommand("
            LOAD DATA INFILE '$csvFilePath' INTO TABLE temp_exel
            CHARACTER SET 'utf8'
            FIELDS TERMINATED BY ','
            ENCLOSED BY '\"'
            IGNORE $skipRow LINES
            ${columns}
         ")->execute();

        return json_encode( [ 'status' => 1, 'message' => 'upload success' ] );
    }

    /*
    * Заполняем таблицу склада из временной таблицы
    */
    public function actionParseTempTable()
    {
        $error = '';

        $session = new Session();
        $session->open();
        
        $currentOperationId = $session['operation-id']; 
        $providerId = $session['provider-id'];
        $userId = \Yii::$app->user->identity->id;
        
        $temp = TempExel::find()->where([ 'col'.$session['settings']['activation-date'] => '' ] )->where( "substring( col{$session['settings']['serial']}, 1, 19) not in (select serial from storage)" )->all();
        $count = count( $temp );

        $tariffCol = 'col'.$session['settings']['tariff'];
        $abonCol = 'col'.$session['settings']['abon-number'];
        $serialCol = 'col'.$session['settings']['serial'];
        
        $limit = ($count <= 300)?$count:300;
        
        if( $count !== 0 )
        {
            for ( $i = 0; $i < $limit; $i++) 
            { 
                $model = new Storage();
                $record = $temp[ $i ];
                $tariffName = trim($record->$tariffCol);
                $tariffId = Tovar::getIdByName( $tariffName, $providerId );
                
                if( $tariffId == null )
                {
                    $tariffId = $this->createNewTariff( $tariffName, $providerId );
                    if( $tariffId == null )
                    {
                        $error .= "${i}. Не удалось добавить новую номенклатуру: $tariffName"
                                  .PHP_EOL.print_r( $model->getErrors(), true ).PHP_EOL;
                        continue;
                    }
                }

                $model->sim_number = rtrim( ltrim( $record->$abonCol, '+7 ' ) );
                $model->FK_tovar = $tariffId;
                $model->serial = substr( trim( $record->$serialCol ), 0, 19 );
                

                if( !$model->save() )
                {
                    $error .= "${i}. Не удалось добвать запись в склад.".PHP_EOL
                              .print_r( $model->getErrors(), true ).PHP_EOL;
                }

                $model->refresh();
                $this->registerStorageOperation( $model->id, $currentOperationId, $userId );
                
                $record->delete();
                unset( $model );
            }
            return json_encode( [ 'status' => 1, 'process' => $count ] );
        }
        else
        {
            $this->cleanAfterImport();
            return json_encode( [ 'status' => 0, 'msg' => 'success' ] );
        }
    }

    private function cleanAfterImport()
    {
        $session = new Session;
        $session->open();
        
        unset( $session['abon-number'] );
        unset( $session['activation-date'] );
        unset( $session['tariff'] );
        unset( $session['serial'] );
        unset( $session['start-row'] );
        
        TempExel::resetTable();
    }

    /*
    *   Получает настройки импорта для данного провайдера.
    */
    private function getImportSettings( $providerId )
    {
        $providerAlias = Provider::find( $providerId )->one()->alias;

        $settings['abon-number'] = StoragePreference::getProperty( 'product-acceptance', 'abon-number-'.$providerAlias );
        $settings['tariff'] = StoragePreference::getProperty( 'product-acceptance', 'tariff-'.$providerAlias );                
        $settings['serial'] = StoragePreference::getProperty( 'product-acceptance', 'serial-'.$providerAlias);
        $settings['activation-date'] = StoragePreference::getProperty( 'product-acceptance', 'activation-date-'.$providerAlias);
        $settings['start-row'] = StoragePreference::getProperty( 'product-acceptance', 'start-row-'.$providerAlias);        

        return $settings;
    }

    /*
    * Возвращает номер максимального столбца
    */
    private function findMaxField( $settings )
    {
        $max = -1;
        unset($settings['start-row']);
        foreach ($settings as $key => $value) 
        {
            if( $value > $max )
                $max = $value;
        }
        return $max;
    }
    /*
    *   Добавляет номенклатуру в таблицу.
    */
    private function createNewTariff( $name, $providerId )
    {
        $model = new Tovar();

        $model->name = $name;
        $model->FK_provider = $providerId;

        if( $model->save() )
        {
            $model->refresh();
            return $model->id;
        }
        return null;
    }

    /*
    *   Регистрация складской операции.
    */
    private function registerStorageOperation( $storageId, $operationId, $userId, $type = 1 )
    {
        StorageOperation::updateAll(['current' => 0], 'FK_storage = '.$storageId);
        $model = new StorageOperation();
        $model->FK_storage = $storageId;
        $model->operation_id = $operationId;
        $model->FK_user = $userId;
        $model->op_type = $type;

        if( $model->save() )
            return true;
        else
            return $model->getErrors();

    }

    public function actionRealize()
    {
        $session = new Session;
        $session->open();

        if( !isset( $session['operation-id'] ) )
        {
            return json_encode(['status' => 99, 'message' => 'Отутствуют принятые товары' ]);
        }
        
        $operationId = $session['operation-id'];
        $model = new Doc;

        $model->number = '№'.$operationId;

        if ( !$model->save() )
        {
            return json_encode(['status' => 99, 'message' => 'Не удалось создать документ.' ]);
        }
        $model->refresh();
        $number = StorageOperation::updateAll([ 'FK_doc' => $model->id ], 'FK_doc IS NULL AND op_type = 1' );

        return json_encode(['status' => 1, 'number' => $number ]);
    }

    /**
     * Finds the Storage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Storage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Storage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
