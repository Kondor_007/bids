<?php

namespace common\modules\storage\controllers;

use yii\web\Controller;
use yii\helpers\ArrayHelper;

use common\modules\storage\models\StoreUnit;
use common\modules\storage\models\StoreUnitSearch;
use common\modules\storage\models\Subdealer;
use common\modules\storage\models\Broker;
use common\modules\storage\models\StorageReference;

class StoreUnitController extends Controller
{	
	/**
	 * 
	 */
	public function actionIndex()
	{
		
	}
	
	/**
	 * Отображает список отфильтрованных и отсортированных ТТ
	 * 
	 * @return Ambigous <string, string>
	 */
	public function actionListOf()
	{
		$searchModel = new StoreUnitSearch();
		
		if( isset( $_GET['FK_org'] ) && ( $_GET['FK_org'] != 0 ) )
		{
			$searchModel->FK_org = $_GET['FK_org'];
		}

		$dataProvider = $searchModel->search( \Yii::$app->request->queryParams );
		
		$subdealers = ArrayHelper::map( Subdealer::find()->all(), 'id', 'name' );
		$subdealers[0] = '-- Все --';
		ksort( $subdealers ); 
		
		return $this->render('list', [
				'dataProvider' => $dataProvider,
				'searchModel' => $searchModel,
				'subdealers' => $subdealers,
		]);
	}
	
	
	public function actionStoresList()
	{
		if( isset($_POST['expandRowKey']) )
		{
			$searchModel = new StoreUnitSearch();
			$searchModel->FK_org = $_POST['expandRowKey'];
			
			$dataProvider = $searchModel->search([]);
			
			return $this->renderAjax('stores-list', [
					'dataProvider' => $dataProvider,
					'searchModel' => $searchModel,
			]); 
		}
	}
	
	/**
	 * Создает новую и ли связывает существующую организуацию с субдилером
	 * @return \yii\web\Response|Ambigous <string, string>
	 */
	public function actionCreate()
	{
		$model = new StoreUnit();
		
		if( $model->load(\Yii::$app->request->post()) && $model->save() )
		{	
			return $this->redirect( ['store-unit/list-of', 'FK_org' => $model->FK_org ] );
		}
		
		if( isset( $_GET['FK_org'] ) )
		{
			$model->FK_org = $_GET['FK_org'];
		}
		
		$refs = [];
		$refs['subdealers'] =  ArrayHelper::map( Subdealer::find()->all(), 'id', 'name');
		$refs['brokers'] = ArrayHelper::map( Broker::find()->all(), 'id', 'name');
		$refs['locations'] = ArrayHelper::map( StorageReference::getReferenceBlock( StorageReference::STORE_LOCATION ), 'id', 'value' );
		$refs['prices'] = ArrayHelper::map( StorageReference::getReferenceBlock( StorageReference::PRICE_GROUP ), 'id', 'value' );
		$refs['profs'] = ArrayHelper::map( StorageReference::getReferenceBlock( StorageReference::PROF_GROUP ), 'id', 'value' );
		
		return $this->render('create', ['model' => $model,
										'refs' => $refs,
		]);
	}
}