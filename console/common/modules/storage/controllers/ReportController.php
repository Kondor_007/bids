<?php

namespace common\modules\storage\controllers;

use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\web\Session;
use common\modules\storage\models\Provider;
use common\modules\storage\models\Subdealer;
use common\modules\storage\models\Broker;
use common\modules\storage\models\StoreUnit;
use common\modules\storage\models\Storage;
use common\modules\storage\models\Tovar;
use common\modules\storage\models\ReportSearch;
use common\modules\storage\models\Report;
use common\modules\storage\models\common\modules\storage\models;

class ReportController extends Controller
{
	
	public function actionReportView()
	{
		$model = new Report();
		
		$filterList = [];
		$filterList['states'] = [ ReportSearch::STATE_ACTIVATED => 'Активированные',
								  ReportSearch::STATE_REGISTRATED => 'Зарегистрированные' ];
		$filterList['subdealers'] 	= ArrayHelper::map( Subdealer::find()->all(), 'id', 'name' );
		$filterList['providers'] 	= ArrayHelper::map( Provider::find()->all(), 'id', 'name' );
		$filterList['brokers'] 		= ArrayHelper::map( Broker::find()->all(), 'id', 'name' );
		
		$filterList['stores'][0]    = 'Склад';  
		$filterList['stores']['Торговые точки']		= ArrayHelper::map( StoreUnit::find()->all(), 'id', 'name' );
		$filterList['products'] 	= ArrayHelper::map( Tovar::find()->all(), 'id', 'name' );
		
		$searchModel = new Report();
		$dataProvider = $searchModel->search( \Yii::$app->request->queryParams );
		
		return $this->render( 'report-view', [
												'reportModel' => $model,
												'filterList' => $filterList,
												'dataProvider' => $dataProvider,
												'searchModel' => $searchModel,
		 ] );
	}

	public function actionGetStores()
	{
		if( !isset( $_POST['broker'] ) )
		{
			return json_encode( [ 'status' => 99, 'error' => 'Отсутствует идентификатор брокера' ] );
		}

		$stores = ArrayHelper::map( StoreUnit::find()->where(['FK_broker' => $_POST['broker']])->all(), 'id', 'name' );
		$stores['0'] = '-- ВСЕ --';
		return json_encode( [ 'status' => 1, 'stores' => $stores ] );
	}

}
?>