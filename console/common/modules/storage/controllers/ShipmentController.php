<?php

namespace common\modules\storage\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\web\Session;
use common\modules\storage\models\Storage;
use common\modules\storage\models\StorageOperation;
use common\modules\storage\models\StorageSearch;
use common\modules\storage\models\Request;
use common\modules\storage\models\RequestSearch;
use common\modules\storage\models\Doc;
use common\modules\storage\models\common\modules\storage\models;

class ShipmentController extends Controller
{
	public function actionIndex()
	{
		$session = new Session;
		$session->open();

		$session['store'] = isset( $_POST['store'] )?$_POST['store']:
							(isset( $session['store'] )?$session['store']:'0');

		$stores = ArrayHelper::map( Request::getStores(), 'id', 'name' );
		$preorderProvider = ( new RequestSearch)->search( $_POST );
		$shipmentSearch = new StorageSearch;

		$incompleteShipments = StorageOperation::getIncompleteShipments();

		$shipmentSearch->opCodeFilter = 0;
		$shipmentProvider = $shipmentSearch->search();
		return $this->render( 'index', 
									[
										'stores' => $stores,
										'preorderProvider' => $preorderProvider,
										'shipmentProvider' => $shipmentProvider,
										'incompleteShipments' => $incompleteShipments,
									] );
	}

	public function actionRefreshPreorder()
	{
		$session = new Session;
		$session->open();
		
		$session['store'] = isset( $_POST['store'] )?$_POST['store']:
							(isset( $session['store'] )?$session['store']:'0');
		
		$preorderProvider = ( new RequestSearch )->search( $session );

		$shipmentSearch = new StorageSearch;
		$shipmentSearch->storeFilter = $session['store'];
		$shipmentSearch->type = 2;
		$shipmentProvider = $shipmentSearch->search();
		//$shipmentProvider->setPagination( [ 'pageSize' => 5 ] );
		$result['shipmentTable'] = $this->renderAjax( 'shipment-table', ['dataProvider' => $shipmentProvider, ] );
		$result['preorderTable'] = $this->renderAjax( 'preorder-table', ['dataProvider' => $preorderProvider,] );

		return json_encode([ 'status' => 1, 'html' => $result ]);
	}

	public function actionTest()
    {
    	$response = $this->actionRefreshGoods();

        return $this->render( 'modal-shipment', ['shipped' => $response['shipped'],
        										 'inStorage' => $response['inStorage'] ] );
    }

    public function actionQuickShip()
	{
		$session = new Session;
		$session->open();

		if( isset( $session['store'] ) && isset( $_POST['in-storage-serials'] ) )
		{
			$modelArray = Storage::find()->where(['id' => $_POST['in-storage-serials']])->all();
			
			foreach ( $modelArray as $key => $model ) 
			{
				$this->registerShipOperation(  $model->id, \Yii::$app->user->identity->id, $session['store'] );
			}
			
			$response = $this->actionRefreshGoods();
			$response['status'] = 1;

			return json_encode( $response );
		}
		return json_encode( ['status' => 99] );
	}

    public function actionRefreshGoods()
	{
		$session = new Session;
		$session->open();

		if( !isset($session['store']) )
		{
			return [];
		}

		$shippedList = Storage::find()->leftJoin('storage_operation st_op', 
									   'st_op.FK_storage = storage.id 
									   AND st_op.current = 1 
									   AND st_op.FK_doc IS NULL
									   AND st_op.FK_store_unit ='.$session['store'])
									->orderBy(['storage.sim_number' => SORT_DESC ])
									->where('st_op.op_type = 2')->all();
    	$inStorageList = Storage::find()->leftJoin('storage_operation st_op', 
										   'st_op.FK_storage = storage.id 
										   AND st_op.current = 1 
										   AND st_op.FK_doc IS NOT NULL')
								    	->orderBy(['storage.sim_number' => SORT_DESC ])
    									->where('st_op.op_type = 1')->all();
    	$result['shipped'] = ArrayHelper::map( $shippedList, 'id', 'sim_number' );
    	$result['inStorage'] = ArrayHelper::map( $inStorageList, 'id', 'sim_number' );

    	return $result;
	}


	public function actionRefreshStores()
	{	
		$storeList = [];

		switch ( $_POST['filter'] ) {
			case 'incomplete':
				$storeList = ArrayHelper::map( StorageOperation::getIncompleteShipments(), 'storeId', 'store' );
			break;
			
			case 'complete':
				$storeList = ArrayHelper::map( StorageOperation::getCompleteShipments(), 'storeId', 'store' );
			break;
			
			default:
				$storeList = ArrayHelper::map( Request::getStores(), 'id', 'name' );
			break;
		}

		return json_encode( ['status' => 1, 'stores' => $storeList ] );
	}

	public function actionShip()
	{
		$session = new Session;
		$session->open();

		if( isset( $session['store'] ) && isset( $_POST['from'] ) )
		{
			if( isset( $_POST['to'] ) && $_POST['to'] != '' )
			{
				$modelArray = Storage::find()->where("serial BETWEEN ${_POST['from']} AND ${_POST['to']}")->all();
				
				if( empty( $modelArray ) )
				{
					return json_encode( ['status' => 99, 'error' => 'На складе отсутствуют товары с серийными номерами в заданном диапазоне.' ] );
				}

				foreach ( $modelArray as $key => $model ) 
				{
					$this->registerShipOperation(  $model->id, \Yii::$app->user->identity->id, $session['store'] );
				}
			}
			else
			{
				$model = Storage::getProductBySerial( $_POST['from'] );
				if( empty( $model ) )
				{
					return json_encode( ['status' => 99, 'error' => 'На складе отсутствует товар с данным серийным номером.' ] );
				}
				$this->registerShipOperation(  $model->id, \Yii::$app->user->identity->id, $session['store'] );
			}
			
			return $this->actionRefreshPreorder();
		}

	}

	public function actionDelete( $id )
	{
		StorageOperation::find()->where(['FK_storage' => $id, 'op_type' => 2 ])->one()->delete();
		$shipmentSearch = new StorageSearch();
		$dataProvider = $shipmentSearch->search();
		return $this->actionIndex();
	}

	public function actionRealize()
	{
		$session = new Session;
		$session->open();

		if( !( isset( $session['store'] ) && isset( $_POST['operation-id'] ) ) )
		{
			return json_encode(['status' => 99, 'error' => 'Отсутсвует информация о торговой точке']);
		}
		$storeId = $session['store'];

		$model = new Doc();
		$model->FK_store = $storeId;
		$model->number = '№'.$_POST['operation-id'];
		$model->shipping_date = date('yy-mm-dd'); // Здесь нужен пользовательский ввод

		if ( !$model->save() )
        {
            return json_encode(['status' => 99, 'message' => 'Не удалось создать документ.' ]);
        }

        $model->refresh();
        /*
         * Нужно почистить таблицу request
         * $shippedProducts = StorageOperation::find()
        					->where(['FK_doc' => null,
        							 'op_type' => 2,
        							 'FK_store_unit' => $storeId,							
        					])-all();*/
        //Request::deleteAll([''])
        $number = StorageOperation::updateAll([ 'FK_doc' => $model->id ], "FK_doc IS NULL 
        																  AND op_type = 2 
        																  AND FK_store_unit = ${storeId}" );
		
        
        
        return json_encode(['status' => 1, 'number' => $number ]);

	}

	/*
    *   Регистрация складской операции.
    */
    private function registerShipOperation( $storageId, $userId, $storeId )
    {
    	StorageOperation::updateAll(['current' => 0], 'FK_storage = '.$storageId);
        $model = new StorageOperation();

        $model->FK_storage = $storageId;
        $model->FK_user = $userId;
        $model->FK_store_unit = $storeId;
        $model->op_type = 2;

        if( $model->save() )
            return true;
        else
            return $model->getErrors();

    }
}

?>