<?php

namespace common\modules\storage\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Session;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

use arturoliveira\ExcelView;

use common\modules\storage\models\Provider;
use common\modules\storage\models\StoragePreference;
use common\modules\storage\models\Storage;
use common\modules\storage\models\StorageOperation;
use common\modules\storage\models\TempExel;
use common\modules\storage\classes\XlsToCsvConverter;


/**
 * TovarController implements the CRUD actions for Tovar model.
 */
class UpdatingController extends Controller
{
    const SERIAL_MISSING   = 1;
    const ACTIV_UNEQUAL    = 2;
    const REG_UNEQUAL      = 3;
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tovar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $providerList = ArrayHelper::map( Provider::find()->all(), 'id', 'name' );
        return $this->render( 'index', [ 'providerList' => $providerList  ] );
    }

    public function actionImport()
    {
        if ( !Yii::$app->request->isPost ) 
        {
            return json_encode( [ 'status' => 99, 'error' => 'POST required, something else taken' ] );    
        }

        $allowed_filetypes = [ '.xls','.xlsx' ];
        $file_name = $_FILES['import']['name'];
        $destinationPath = Yii::$app->basePath . '/temp/parsing/';
        $ext = substr($file_name, strpos($file_name,'.'), strlen($file_name)-1);
        
        //---- Валидация и перемещение файла в постоянную директорию
        if( !in_array( $ext, $allowed_filetypes ) )
        {
            return json_encode( ['status' => 99, 'error' => 'unknown extension' ]);       
        }
        if ( ! move_uploaded_file($_FILES["import"]["tmp_name"], 
                                $destinationPath.'temp'. $ext) )
        {                
            return json_encode( ['status' => 99, 'name' => 'upload failed' ]);
        }

        $session = new Session;
        $session->open();
        
        $session['provider-id'] = $_POST['provider-id'];
        $session['operation-id'] = date('ymdhis');
        $session['settings'] = $this->getImportSettings( $_POST['provider-id'] );


        $csvFilePath = str_replace( "\\", "/", XlsToCsvConverter::convert( $destinationPath.'temp'. $ext ) );
        $skipRow = - 1 + $session['settings']['start-row'];
        
        $columns = '( ';
        
        /*
        * Команда LOAD DATA INFILE 
        */
        if( ( $max = $this->findMaxField( $session['settings'] ) ) <= 20 )
        {
            for ( $i = 1; $i <= $max;  $i++ )
            { 
                $columns .= 'col'.$i.',';
            }

            $columns = rtrim( $columns, ',' ).')';
        }
        TempExel::deleteAll();
        TempExel::dropErrorField();
        \Yii::$app->db->createCommand("
            LOAD DATA INFILE '$csvFilePath' INTO TABLE temp_exel
            CHARACTER SET 'utf8'
            FIELDS TERMINATED BY ','
            ENCLOSED BY '\"'
            IGNORE $skipRow LINES
            ${columns}
         ")->execute();
        
        TempExel::createErrorField();
        TempExel::markMissing( 'col'.$session['settings']['serial'], self::SERIAL_MISSING );

        return json_encode( [ 'status' => 1, 'message' => 'upload success' ] );
    }

    public function actionUpdate()
    {
        $error = '';

        $session = new Session();
        $session->open();
        
        $currentOperationId = $session['operation-id']; 
        $providerId = $session['provider-id'];
        $userId = \Yii::$app->user->identity->id;
        
        $serialCol = 'col'.$session['settings']['serial'];
        $activateCol = 'col'.$session['settings']['activation-date'];
        $registerCol = 'col'.$session['settings']['registration-date'];
        
        $temp = TempExel::find()->where( "substring( col{$session['settings']['serial']}, 1, 19) in (select serial from storage)" )
                                ->andWhere( "error IS NULL" )
                                ->andWhere( "{$activateCol} IS NOT NULL OR {$registerCol} IS NOT NULL" )->all();
        $count = count( $temp );

        
        $limit = ($count <= 300)?$count:300;
        
        if( $count !== 0 )
        {
            for ( $i = 0; $i < $limit; $i++) 
            { 
                $record = $temp[ $i ];
                $tempSerial = substr( $record->$serialCol, 0, 19);

                $tempActivDate = $this->convertExcelDate( $record->$activateCol );
                $tempRegDate = $this->convertExcelDate( $record->$registerCol );
                $model = Storage::find()->where([ 'serial' => $tempSerial ])->one();
                
                if( ( $model->activation_date == $tempActivDate ) &&
                    ( $model->registration_date == $tempRegDate ) )
                {
                    $record->delete();
                    unset( $model );
                    continue;
                }

                if( ( !is_null($model->activation_date) ) && 
                    ( $model->activation_date != $tempActivDate ) )
                {
                    $error .= "ERR";
                    $record->error = self::ACTIV_UNEQUAL;
                    $record->save();
                    continue;
                }

                $model->activation_date = $tempActivDate;

                if( ( !is_null( $model->registration_date ) ) && 
                    ( $model->registration_date != $tempRegDate ) )
                {
                    $record->error = self::REG_UNEQUAL;
                    $record->save();
                    continue;
                }

                $model->registration_date = $tempRegDate;

                if( !$model->save() )
                {
                    $error .= $model->id.". Не удалось обновить запись.".PHP_EOL
                              .print_r( $model->getErrors(), true ).PHP_EOL;
                    continue;
                }

                $model->refresh();
                $this->registerUpdateOperation( $model->id, $currentOperationId, $userId );
                
                $record->delete();
                unset( $model );
            }
            return json_encode( [ 'status' => 1, 'process' => $count, 'error' => $error ] );
        }
        else
        {
            $html = [];
            $unequalCount = count(TempExel::find()->where("error = 2 OR error = 3")->all());
            $missingCount = count(TempExel::find()->where("error = 1")->all());
            //$this->cleanAfterImport();
            $html['info'] = $this->renderAjax( 'update-info', 
                                                [ 'unequal_count' => $unequalCount,
                                                  'missing_count' => $missingCount]);
            return json_encode( [ 'status' => 0, 'msg' => 'success', 'html' => $html ] );
        }   
    }

    public function actionUnequalView()
    {
        $session = new Session;
        $session->open();
        return $this->render( 'unequal-table', [
                                'dataProvider' => TempExel::getUnequal( $session['settings'] )              
                            ]);
    }

    public function actionMissingView()
    {
        $session = new Session;
        $session->open();
        return $this->render( 'missing-table', [
                                'dataProvider' => TempExel::getMissing( $session['settings'] )
                            ]);
    }

    /*
    *   Получает настройки актуализации для данного провайдера.
    */
    private function getImportSettings( $providerId )
    {
        $providerAlias = Provider::find( $providerId )->one()->alias;

        $settings['registration-date'] = StoragePreference::getProperty( 'product-updating', 
                                                              'registration-date-'.$providerAlias );                
        $settings['serial'] = StoragePreference::getProperty('product-updating', 
                                                             'serial-'.$providerAlias);
        $settings['product'] = StoragePreference::getProperty('product-updating', 
                                                             'tariff-'.$providerAlias);
        $settings['activation-date'] = StoragePreference::getProperty('product-updating', 
                                                                      'activation-date-'.$providerAlias);
        $settings['start-row'] = StoragePreference::getProperty( 'product-updating', 
                                                                 'start-row-'.$providerAlias);        

        $settings['abon-number'] = StoragePreference::getProperty( 'product-updating', 
                                                                 'abon-number-'.$providerAlias);        

        return $settings;
    }


    /*
    *   Регистрация складской операции.
    */
    private function registerUpdateOperation( $storageId, $operationId, $userId )
    {
        $model = new StorageOperation();

        $model->FK_storage = $storageId;
        $model->operation_id = $operationId;
        $model->FK_user = $userId;
        $model->op_type = 3;

        if( $model->save() )
            return true;
        else
            return $model->getErrors();

    }

    private function convertExcelDate( $date )
    {
        if( $date == null )
            return null;
        $year = substr( $date, 6, 4 );
        $day = substr( $date, 0, 2 );
        $month = substr( $date, 3, 2 );

        $hour = substr( $date, 12, 2 );
        $min  = substr( $date, 15, 2 );
        $sec  = substr( $date, 18, 2 );

        return $year.'-'.$month.'-'.$day.' '.(stristr($hour, ' ')?'0'.trim($hour):$hour).':'.$min.':'.$sec;
    }

    private function cleanAfterImport()
    {
        $session = new Session;
        $session->open();
        
        unset( $session['settings'] );
        
        //TempExel::resetTable();
    }

    /*
    * Возвращает номер максимального столбца
    */
    private function findMaxField( $settings )
    {
        $max = -1;
        unset($settings['start-row']);
        foreach ($settings as $key => $value) 
        {
            if( $value > $max )
                $max = $value;
        }
        return $max;
    }

    
}

