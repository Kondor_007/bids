<?php

namespace common\modules\storage\controllers;

use Yii;
use common\modules\storage\models\Storage;
use common\modules\storage\models\StorageSearch;
use common\modules\storage\models\Request;
use common\modules\storage\models\Provider;
use common\modules\storage\models\Subdealer;
use common\modules\storage\models\Broker;
use common\modules\storage\models\StoreUnit;
use common\modules\storage\models\Doc;
use common\modules\storage\models\StorageNumberSearch;
use common\modules\storage\classes\UniqueController;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Session;
use yii\filters\VerbFilter;


/*
* На мой взгляд поле op_date лишнее и только усложняет архитекуру базы данных 
* и код обработки. Записи в таблице request можно с тем же успехом уникализировать с помощью поля 
  FK_store_unit. Сейчас это более-менее стабильная версия кода, но содержит очень не очевидный код
  и логику работы, что в дальнейшем неминуемо приведет к куче багов и головняках при модификации кода,
  поэтому настоятельно рекомендую рефакторинг.
*/

class PreorderController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                
            ],
        ];
    }

    public function actionIndex()
    {
        $session = new Session;
        $session->open();
        
        $preorderId = UniqueController::control( 'preorder-number', $_POST );

        $incompleteOrders = \Yii::$app->db->createCommand("
        SELECT op_date, su.name, count(*) count, su.id storeId FROM request
        LEFT JOIN store_unit su ON su.id = request.FK_store_unit
        WHERE request.FK_doc IS NULL
        GROUP BY op_date
        ")->queryAll();

        $completeOrders = \Yii::$app->db->createCommand("
        SELECT op_date, su.name, count(*) count, su.id storeId FROM request
        LEFT JOIN store_unit su ON su.id = request.FK_store_unit
        WHERE request.FK_doc IS NOT NULL
        GROUP BY op_date
        ")->queryAll();


        $model = new Request();
        $searchModel = new StorageNumberSearch();
        
        $_POST['store'] = ( isset($_POST['store'])?$_POST['store']:$session['store'] );
        $dataProvider = $searchModel->search();
        $brokerList = ArrayHelper::map( Broker::find()->all(), 'id', 'name' );
        $storeList = ArrayHelper::map( StoreUnit::find()
                    ->innerJoin( 'request rq', 'rq.FK_store_unit = store_unit.id AND FK_doc IS NULL' )
                    ->groupBy('store_unit.name')
                    ->all(), 'id', 'name' );
        $subdealers = ArrayHelper::map( Subdealer::find()->all(), 'id', 'name' );
        $providerList = ArrayHelper::map( Provider::find()->all(), 'id', 'name' );
        
        return $this->render('index', [
                                       'model' => $model,
                                       'brokers' => $brokerList,
                                       'stores' => $storeList,
                                       'providers' => $providerList,
                                       'dataProvider' => $dataProvider,
                                       'current' => $preorderId,
                                       'incomplete' => $incompleteOrders,
                                       'complete' => $completeOrders,
                                       'subdealers' => $subdealers,
                                    ] );
    }

    
    /*
    *   Обновление данных таблицы
    */
    public function actionRefresh()
    {
        $session = new Session;
        $session->open();

        if( isset( $_POST['store'] ) )
        {
            //Если в запросе присутствует идентификатор торговой точки обновляем значение сессии
            $session['store'] = $_POST['store'];
            $model = Request::find()->where([ 'FK_store_unit' => $session['store'] ])
                                    ->groupBy('FK_store_unit')->one();
            if( $model != null )
            {
                $_POST['preorder-number'] = $model->op_date;
            }
            else
            {
                $_POST['new'] = true;
            }
        }
        
        $preorderId = UniqueController::control( 'preorder-number', $_POST );

        //--- Добавляем в $_POST идентификатор текущей операции
        $_POST['op_date'] = $preorderId;
        $searchModel = new StorageNumberSearch();
        $dataProvider = $searchModel->search( $_POST );

        return json_encode( [ 'status' => 1, 'html' => $this->renderTable( $dataProvider ) ]);
    }

    private function renderTable( $dataProvider )
    {
            
        $incompleteOrders = \Yii::$app->db->createCommand("
        SELECT op_date, su.name, count(*) count, su.id storeId FROM request
        LEFT JOIN store_unit su ON su.id = request.FK_store_unit
        WHERE request.FK_doc IS NULL
        GROUP BY op_date
        ")->queryAll();
    

    
        $completeOrders = \Yii::$app->db->createCommand("
        SELECT op_date, su.name, count(*) count, su.id storeId FROM request
        LEFT JOIN store_unit su ON su.id = request.FK_store_unit
        WHERE request.FK_doc IS NOT NULL
        GROUP BY op_date
        ")->queryAll();

        $result['incomplete'] = $this->renderAjax('incomplete-orders', [
                                                    'incomplete' => $incompleteOrders,
                                                    'current' => UniqueController::getUnique('preorder-number'),
                                                ]);
        $result['complete'] = $this->renderAjax( 'complete-orders', [ 'complete' => $completeOrders ] );

        $result['table'] = $this->renderAjax('preorder-table', [
                                 'dataProvider' => $dataProvider,
                                ]);
        
        return $result;
    }

    public function actionRecall()
    {
        if( !isset($_POST['store']) )
        {
            return json_encode( [ 'status' => 99, 'message' => 'Не указана торговая точка' ]);
        }
        $request = Request::find()->where(['FK_store_unit' => $_POST['store']])
                                  ->groupBy('FK_store_unit')->one();
        $docId = $request->FK_doc;
        $preorderId = $request->op_date;
        Request::updateAll( [ 'FK_doc' => null ], "FK_store_unit = ${_POST['store']}" );
        Doc::findOne( $docId )->delete();

        //--- Добавляем в $_POST идентификатор текущей операции
        $_POST['op_date'] = $preorderId;
        $preorderId = UniqueController::control( 'preorder-number', $_POST );
        $searchModel = new StorageNumberSearch();
        $dataProvider = $searchModel->search( $_POST );
        $brokerList = ArrayHelper::map( Broker::find()->all(), 'id', 'name' );
        $subdealers = ArrayHelper::map( Subdealer::find()->all(), 'id', 'short_name' );
        $storeList  = ArrayHelper::map( StoreUnit::find()
                    ->innerJoin( 'request rq', 'rq.FK_store_unit = store_unit.id AND FK_doc IS NULL' )
                    ->groupBy('store_unit.name')
                    ->all(), 'id', 'name' );
        $providerList = ArrayHelper::map( Provider::find()->all(), 'id', 'name' );
        $result = $this->renderTable( $dataProvider );
        $result['filters'] = $this->renderAjax( 'filters', [ 
                                            'brokers' => $brokerList, 
                                            'providers' => $providerList, 
                                            'current_store' => $_POST['store'],
                                            'stores' => $storeList,
                                            'subdealers' => $subdealers ]);

        return json_encode( [ 'status' => 1, 'html' => $result ]);
    }

    public function actionRealize()
    {
        $session = new Session;
        $session->open();

        if( !($preorderId = UniqueController::getUnique('preorder-number')) )
        {
            return json_encode(['status' => 99, 'message' => 'Отутствуют предзаказы' ]);
        }

        $model = new Doc;

        $model->number = '№'.$preorderId;
        $model->FK_store = $session['store'];

        if ( !$model->save() )
        {
            return json_encode(['status' => 99, 'message' => 'Не удалось создать документ.' ]);
        }

        $model->refresh();
        $number = Request::updateAll([ 'FK_doc' => $model->id ], 
                                    'FK_doc IS NULL AND op_date ='.$preorderId );
        UniqueController::renew( 'preorder-number' );
        unset($session['store']);
        return json_encode(['status' => 1, 'number' => $number ]);
    }
    
    public function actionUpdate()
    {
        $session = new Session;
        $session->open();
        $preorderId = UniqueController::getUnique( 'preorder-number' );
        $model;
        if( ( $model = Request::find()->where([
                                            'FK_product' => $_POST['productId'],
                                            'FK_store_unit' => $session['store'],
                                            'op_date' => $preorderId,
                                        ])->one()) == null )
        {
            $model = new Request();
            $model->FK_product = $_POST['productId'];
            $model->FK_store_unit = $session['store'];
            $model->op_date = $preorderId;
        }

        if( $_POST['amount'] == 0 )
        {
            if( !$model->getIsNewRecord() )    
                $model->delete();

            return json_encode([ 'status' => 1, 'message' => 'order deleted' ]);
        }
        else
        {
            $model->amount = $_POST['amount'];

            if( $model->validate() && $model->save() )
            {
                return json_encode([ 'status' => 1, 'message' => 'success' ]);
            }
            return json_encode([ 'status' => 99, 'message' => 'error' ]);
        }

    }
    
}
