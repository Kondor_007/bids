<?php

namespace common\modules\storage\controllers;

use yii\web\Controller;
use yii\helpers\ArrayHelper;
use common\modules\storage\models\Broker;

class BrokerController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGetStores()
    {
    	if( !isset( $_POST['id'] ) || ( $_POST['id'] == '' ) )
    	{
    		return json_encode([ 'status' => 99, 'error' => 'no id' ]);
    	}
    	else
    	{
    		$stores = ArrayHelper::map( Broker::find()->where(['id' => $_POST['id']])
    												  ->one()->storeUnits, 'id', 'name' );
    		return json_encode( [ 'status' => 1, 'data' => $stores ] );
    	}
    }
}
