<?php

namespace common\modules\storage;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\storage\controllers';
    
    public $employeeClass;
    public $subdealerClass;
    public $contractClass;
    
    public $employeeController = 'us-ext';
    public $subdealerController = 'organisation';

    public function init()
    {
        parent::init();
        \Yii::configure($this, require(__DIR__ . '/config.php'));

        //Это не обязательно если у приложения и модуля одна тема
        /*$this->layoutPath = '@app/themes/modern/layouts';
        $this->layout = 'main';*/
    }

    public function getSubdealerClassName()
    {
        echo $this->subdealerClass;
    }
}
