<?php

namespace common\modules\storage\models;

use Yii;

/**
 * This is the model class for table "storage_operation".
 *
 * @property integer $id
 * @property integer $FK_storage
 * @property integer $FK_user
 * @property integer $FK_doc
 * @property string $operation_id
 * @property integer $op_type
 *
 * @property Storage $fKStorage
 * @property User $fKUser
 * @property Doc $fKDoc
 */
class StorageOperation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'storage_operation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FK_storage', 'FK_user'], 'required'],
            [['FK_storage', 'FK_user', 'FK_doc', 'op_type'], 'integer'],
            [['operation_id'], 'string', 'max' => 12]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'FK_storage' => 'Fk Storage',
            'FK_user' => 'Таблица пользователей',
            'FK_doc' => 'Fk Doc',
            'operation_id' => 'Operation ID',
            'op_type' => 'Op Type',
        ];
    }

    public static function getIncompleteShipments()
    {
        return \Yii::$app->db->createCommand("
                                            SELECT su.name store, st_op.FK_store_unit storeId, count(*) count 
                                            FROM storage_operation st_op
                                            LEFT JOIN store_unit su ON su.id = st_op.FK_store_unit
                                            WHERE st_op.FK_doc IS NULL 
                                                AND st_op.op_type = 2
                                                AND st_op.FK_store_unit <> 0
                                            GROUP BY st_op.FK_store_unit
                                            ")->queryAll(); 
    }

    public static function lastOperationIdOf( $id )
    {
        return self::find()->where(['FK_storage' => $id])->orderBy(['id' => SORT_DESC])->limit(1)->one()->id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'FK_storage']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKUser()
    {
        return $this->hasOne(User::className(), ['id' => 'FK_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKDoc()
    {
        return $this->hasOne(Doc::className(), ['id' => 'FK_doc']);
    }
    
    public function getDocument()
    {
    	return $this->hasOne(Doc::className(), ['id' => 'FK_doc']);
    }
    
    public function getStore()
    {
    	return $this->hasOne( StoreUnit::className(), ['id' => 'FK_store_unit'] );
    }
    
    public function getStoreName()
    {
    	if( $this->store )
    	{
    		return $this->store->name;
    	}
    	else 
    	{
    		'Склад';
    	}
    }
}
