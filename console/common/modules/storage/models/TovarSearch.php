<?php

namespace common\modules\storage\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\storage\models\Tovar;

/**
 * TovarSearch represents the model behind the search form about `common\models\Tovar`.
 */
class TovarSearch extends Tovar
{
    /**
     * @inheritdoc
     */
    public $providerNameFilter; //Поле фильтрации по имени провайдера
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'providerNameFilter'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tovar::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->joinWith('fKProvider');

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere([ 'like', 'tovar.name', $this->name ]);
        $query->andFilterWhere([ 'like', 'provider.name', $this->providerNameFilter ]);
        return $dataProvider;
    }
}
