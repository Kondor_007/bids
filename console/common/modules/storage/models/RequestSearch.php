<?php

namespace common\modules\storage\models;

use Yii;
use yii\base\Model;
use yii\data\SqlDataProvider;
use common\modules\storage\models\Request;

/**
 * RequestSearch represents the model.
 */
class RequestSearch extends Request
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = 
        "SELECT pv.name AS provider, rq.FK_store_unit AS store, tv.name AS name, rq.amount AS ordered
        ,(SELECT count(*) 
            FROM storage AS st
            INNER JOIN storage_operation AS st_op ON st_op.FK_storage = st.id AND st_op.id = 
            (SELECT st_op2.id 
                FROM storage_operation st_op2 
                WHERE st_op2.FK_storage = st.id 
                ORDER BY st_op2.id 
                DESC LIMIT 1 )
                WHERE st.FK_tovar = tv.id AND st_op.op_type = 1 AND st_op.FK_doc IS NOT NULL ) AS inStorage

        ,(SELECT count(*) FROM storage AS st
            INNER JOIN storage_operation AS st_op ON st_op.FK_storage = st.id AND st_op.id = 
            (SELECT st_op2.id
                FROM storage_operation st_op2 
                WHERE st_op2.FK_storage = st.id 
                ORDER BY st_op2.id 
                DESC LIMIT 1 )
            WHERE st.FK_tovar = tv.id 
            AND st_op.op_type = 2 
            AND st_op.FK_doc IS NULL
            AND st_op.FK_store_unit = :store ) AS shipped
        FROM request AS rq
        LEFT JOIN tovar AS tv ON rq.FK_product = tv.id
        LEFT JOIN provider AS pv ON tv.FK_provider = pv.id
        WHERE rq.FK_store_unit = :store";

        $dataProvider = new SqlDataProvider([
        'sql' => isset($params['store'])?$query:'select * from storage where 1 = 0',
                'params' => [
                             ':store' => (isset($params['store'])?$params['store']:-10),
                            ],
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);

        return $dataProvider;
    }
}
