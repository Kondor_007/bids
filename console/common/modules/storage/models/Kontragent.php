<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kontragent".
 *
 * @property integer $id_kontragent
 * @property string $kontragent
 *
 * @property Sklad[] $sklads
 */
class Kontragent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kontragent';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kontragent'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kontragent' => 'Id Kontragent',
            'kontragent' => 'Kontragent',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSklads()
    {
        return $this->hasMany(Sklad::className(), ['FK_kontragent' => 'id_kontragent']);
    }
}
