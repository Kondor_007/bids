<?php

namespace common\modules\storage\models;

use yii\data\SqlDataProvider;
use yii\db\Query;
use yii\base\Model;
class ReportSearch extends Model
{
	public $subdealer;
    public $broker;
	public $store;
    public $states;

	public $products;
	public $providers;
    public $searchLocation;

    const STATE_ACTIVATED = 1;
    const STATE_REGISTRATED = 2;

	/**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'store', 'products', 'providers', 'states', 'broker'], 'safe'],
            [['searchLocation'], 'safe'],
        ];
    }

    /*
    * Почему то обязательно. По идее этот массив должен генериться автоматически.
    * но не тут то было. Короче говоря, без этого не работает массовое присвоение атрибутов,
    * setAttributes();, что очень мешает.
    */
    public function attributes()
    {
        $attr = [   
                'store',
                'products',
                'providers',
                'states',
                'searchLocation',
                'broker',
            ];
        return $attr;
    }
    
    public function attributeLabels()
    {
    	return [
    		'subdealer' => 'Субдилер',
    		'store' => 'Торговая точка',
    		'providers'	=> 'Провайдер',
    		'products' => 'Товар',
    		'states' => 'Состояние товара',
    	];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search( $params = null )
    {
        $query = new Query();
        $query->select('st.serial serial, st.sim_number phone, 
                        pv.name provider, tv.name product')
        	  ->from('storage as st')
        	  ->leftJoin('tovar as tv', 'st.FK_tovar = tv.id')
        	  ->leftJoin('provider as pv', 'tv.FK_provider = pv.id')
        	  ->leftJoin('storage_operation st_op', 'st_op.FK_storage = st.id AND st_op.current = 1');
        	  //->rightJoin('store_unit su', 'su.id = st_op.FK_store_unit');
        
        
        if( !is_null( $params ) )
        {
            $this->setAttributes( $params, false );
        }

        if( $this->providers ) 
        {
            $query->andWhere( 'pv.id IN ('.implode( ',', $this->providers ).')' );
        }

        if( $this->products )
        {
            $query->andWhere('tv.id IN ('.implode( ',', $this->products ).')' );
        }

        if( isset( $this->store ) )
        {
            $query->addSelect('su.name store')
                  ->rightJoin('store_unit su', 'su.id = st_op.FK_store_unit');
            
            if( $this->store )        
                $query->andWhere('su.id = '.$this->store);
            if( $this->broker )        
                $query->andWhere('su.FK_broker = '.$this->broker);
            
            if( isset($this->states) )
            {
                if( in_array( self::STATE_ACTIVATED, $this->states ) )
                    $query->andWhere('st.activation_date IS NOT NULL');
                
                if( in_array( self::STATE_REGISTRATED, $this->states ) )
                    $query->andWhere('st.registration_date IS NOT NULL');
            }
            
            $query->andWhere('st_op.op_type = 2 OR st_op.op_type = 3' );   
        } 
        else 
        {
            $query->andWhere('st_op.op_type = 1' );   
        }

        $command = $query->createCommand();
        $dataProvider = new SqlDataProvider([
            'sql' => $command->sql,
            'pagination' => [
                'pageSize' => 10,
            ],
            'totalCount' => count( $command->queryAll() ),
        ]);

        

        return $dataProvider;
    }
}

?>