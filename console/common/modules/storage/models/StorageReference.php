<?php

namespace common\modules\storage\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "storage_preference".
 *
 * @property integer $id
 * @property integer $block
 * @property string $block_name
 * @property string $value
 */
class StorageReference extends \yii\db\ActiveRecord
{
	const STORE_LOCATION = 1;
	const PRICE_GROUP = 2;
	const PROF_GROUP = 3;

	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'storage_reference';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['block', 'block_name', 'value'], 'required'],
            [['block'], 'integer'],
            [['block_name', 'value'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'block' => 'Block',
            'block_name' => 'Block Name',
            'value' => 'Prop Val',
        ];
    }
    
    public static function getReferenceBlock( $block )
    {
    	return self::find()->where([ 'block' => $block ])->all();
    }

    public static function getProperty( $block = 'product-acceptance', $alias )
    {
        return self::find()->where([ 'prop_alias' => $alias, 
                                     'block_alias' => $block ])->one()->prop_val;
    }
    
}
