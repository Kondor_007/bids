<?php

namespace common\modules\storage\models;

use Yii;

/**
 * This is the model class for table "tovar".
 *
 * @property integer $id
 * @property string $name
 * @property integer $FK_provider
 *
 * @property Sklad[] $sklads
 * @property Provider $fKProvider
 */
class Tovar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tovar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FK_provider'], 'integer'],
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'FK_provider' => 'Provider',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSklads()
    {
        return $this->hasMany(Sklad::className(), ['FK_tovar' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKProvider()
    {
        return $this->hasOne(Provider::className(), ['id' => 'FK_provider']);
    }
    public function getProviderName()
    {
        if($this->fKProvider !== null)
            return $this->fKProvider->name;
        else
            return 'Провайдер не задан';
    }

    /*
    *   Возвращает id товара с соответсвующими
    *   name и FK_provider, если не найдено возвращает null
    */
    public static function getIdByName( $name, $providerId )
    {
        $model = Tovar::find()->where( [ 'name' => $name, 'FK_provider' => $providerId ] )->one();
        if( $model == null )
        {
            return null; 
        }
        else
        {
            return $model->id;
        }
    }
}
