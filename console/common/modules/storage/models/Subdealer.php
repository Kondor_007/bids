<?php

namespace common\modules\storage\models;

use Yii;
use common\modules\storage\Module;
use common\modules\storage\models\StoreUnit;

/**
 * This is the model class for table "storage_subdealer".
 *
 * @property integer $id
 * @property string $FK_organisation
 *
 * @property Stores[] $stores
 */
class Subdealer extends \yii\db\ActiveRecord
{
	private $externalClass;
	
	public function init()
	{
		parent::init();
		
		$this->externalClass = Module::getInstance()->subdealerClass;
	}
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
    	return 'storage_subdealer';
    }
    
    public function rules()
    {
    	return [
    		[['mts_condition', 'bee_condition', 'meg_condition', 'tel_condition'], 'safe'],
    	];	
    }
    
    public function attributeLabels()
    {
    	return [
    			'headMasterName' => 'Директор',
    			'name' => 'Название организации',
    			'storesCount' => 'Количество ТТ',
    			'address' => 'Фактический адрес',
    			'addressLegal' => 'Юридический адрес',
    			'addressPost' => 'Почтовый адрес',
    			'OKATO' => 'ОКАТО',
    			'OGRN' => 'ОГРН',
    			'KPP' => 'КПП',
    			'INN' => 'ИНН',
    			'hasStamp' => 'Наличие печати',
    			'has_stamp' => 'Наличие печати',
    			'mts_condition' => 'МТС',
    			'meg_condition' => 'Мегафон',
    			'bee_condition' => 'Билайн',
    			'tel_condition' => 'Теле2',
    			'contractLinks' => 'Договоры',
    	];
    }
	
    /**
     * Связь к таблице организаций 
     * @return ActiveQuery
     */
    public function getOrganisation()
    {
    	return $this->hasOne( $this->externalClass, ['id' => 'FK_organisation'] );
    }
    
    public function getContracts()
    {
    	return $this->organisation->contracts;
    }
    
    
    public function getContractLinks()
    {
    	$contracts = '';
    	foreach ( $this->contracts as $key => $model )
    	{
    		$contracts .= "<a href = 'index.php?r=storage/subdealer/download-contract&id=$model->id' data-pjax = false download = '$model->file_path'>$model->name</a>, ";
    	}
    	return trim( $contracts, ', ');
    }
    
    /**
     * 
     */
    public function getHeadMasterName()
    {
    	return isset( $this->organisation )?$this->organisation->headName:'';
    }
    
    /**
     * Связь к таблице торговых точек
     * @return \yii\db\ActiveQuery
     */
    public function getStores()
    {
        return $this->hasMany( StoreUnit::className(), ['FK_org' => 'id']);
    }
    

   
    
    public function beforeDelete()
    {
    	if( parent::beforeDelete() )
    	{
    		StoreUnit::deleteAll([ 'FK_org' => $this->id ]);
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
    
    /**
     *
     */
    public function getName()
    {
    	if( isset( $this->organisation ) )
    		return $this->organisation->short_name;
    	else
    		return '';
    }
    
    /**
     * 
     * @return string
     */
    public function getOKATO()
    {
    	return isset( $this->organisation )?$this->organisation->OKATO:null;
    }
    
    public function getINN()
    {
    	return isset( $this->organisation )?$this->organisation->INN:null;
    }
    
    public function getOGRN()
    {
    	return isset( $this->organisation )?$this->organisation->OGRN:null;
    }
    
    public function getKPP()
    {
    	return isset( $this->organisation )?$this->organisation->KPP:null;
    }
    
    public function getPhone()
    {
    	return isset( $this->organisation )?$this->organisation->phone:null;
    }
    
    public function getEmail()
    {
    	return isset( $this->organisation )?$this->organisation->email:null;
    }
    
    public function getStoresCount()
    {
    	return count( $this->stores );
    }
    
    public function getAddress()
    {
    	return $this->organisation->address;
    }
    
    public function getAddressPost()
    {
    	return $this->organisation->address_post;
    }
    
    public function getAddressLegal()
    {
    	return $this->organisation->address_legal;
    }

    public function getHasStamp()
    {
    	return ( $this->has_stamp == 1 )?'Есть печать':'Нет печати';
    }
}


?>