<?php

namespace common\modules\storage\models;

use Yii;
use common\modules\storage\Module;
/**
 * This is the model class for table "broker".
 *
 * @property integer $id
 * @property integer $FK_user
 *
 * @property User $fKUser
 * @property StoreUnit[] $storeUnits
 */
class Broker extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'broker';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FK_user'], 'required'],
            [['FK_user'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'FK_user' => 'Торговый представитель',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKUser()
    {
        return $this->hasOne( Module::getInstance()->employeeClass, ['id' => 'FK_user']);
    }

    public function getName()
    {
        return $this->fKUser->profile->shortName;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreUnits()
    {
        return $this->hasMany( StoreUnit::className(), ['FK_broker' => 'id'] );
    }


}
