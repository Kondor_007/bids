<?php

namespace common\modules\storage\models;

use Yii;
use yii\base\Model;
use yii\data\SqlDataProvider;
use common\modules\storage\models\Storage;

/**
 * StorageSearch represents the model behind the search form about `common\models\Storage`.
 */
class StorageNumberSearch extends Storage
{
    /**
     * @inheritdoc
     */

    public $tovarNameFilter;
    public $providerNameFilter;
    public $documentNameFilter;
    public $userNameFilter;
    public $userIdFilter;
    
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['registration_date','tovarNameFilter','providerNameFilter','documentNameFilter','userNameFilter', 'activation_date'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'providerName' => 'Провайдер',
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search( $params = '' )
    {
        $query = 
        "select pv.name as provider, tv.id as tovarId, tv.name as name, rq.amount as ordered
        ,(SELECT count(*) FROM storage as st
        inner join storage_operation as st_op on st_op.FK_storage = st.id 
            and st_op.id = (select st_op2.id from storage_operation st_op2 where st_op2.FK_storage = st.id order by st_op2.id desc limit 1 )
        where st.fk_tovar=tv.id and st_op.op_type=1 and st_op.FK_doc IS NOT NULL ) as inStorage

        ,(SELECT count(*) FROM storage as st
        inner join storage_operation as st_op on st_op.FK_storage = st.id and st_op.id = (select st_op2.id from storage_operation st_op2 where st_op2.FK_storage = st.id order by st_op2.id desc limit 1 )
        inner join doc on doc.id = st_op.FK_doc
        where st.fk_tovar=tv.id and st_op.op_type=2 and doc.FK_store = :store) as inStoreUnit
        FROM tovar as tv
        JOIN provider as pv ON tv.FK_provider = pv.id
        LEFT JOIN request as rq ON rq.FK_store_unit = :store 
                                AND rq.FK_product = tv.id 
                                AND FK_doc IS NULL"
        .( isset( $params['op_date'] )?" AND rq.op_date =".$params['op_date']:"" )
        .( isset($params['providerId'])?" WHERE tv.FK_provider = ${params['providerId']} ":" ")
        ."group by tv.name";
                  

        $dataProvider = new SqlDataProvider([
        'sql' => isset($params['store'])?$query:'select * from storage where 1 = 0',
                'params' => [
                             ':store' => (isset($params['store'])?$params['store']:-10),
                            ],
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);

        if( $params !== '' )
        {
            $this->load($params);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
