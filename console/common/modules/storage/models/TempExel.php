<?php

namespace common\modules\storage\models;

use Yii;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
/**
 * This is the model class for table "temp_exel".
 *
 * @property integer $id
 */
class TempExel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'temp_exel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
        ];
    }

    public static function createErrorField()
    {   
        \Yii::$app->db->createCommand("
            ALTER TABLE temp_exel ADD error INT(2) DEFAULT NULL
        ")->execute();
    }

    public static function dropErrorField()
    {
        \Yii::$app->db->createCommand("
            ALTER TABLE temp_exel DROP COLUMN error
        ")->execute();
    }

    public static function resetTable()
    {
        self::deleteAll();
        self::dropErrorField();
    }

    public static function markMissing( $serialCol, $mark )
    {
        self::updateAll( [ 'error' => $mark ],
                         "substring( ${serialCol}, 1, 19 ) NOT IN ( SELECT serial FROM storage )" );
    }

    public static function getUnequal( $cols )
    {
        $query = 
                "SELECT tv.name product, st.sim_number phone_number, st.serial serial, 
                        st.activation_date active, tm.col{$cols['activation-date']} AS tempActive, 
                        st.registration_date reg, tm.col{$cols['registration-date']} AS tempReg
                FROM temp_exel tm
                LEFT JOIN storage st ON st.serial = substring(tm.col{$cols['serial']}, 1, 19)
                LEFT JOIN tovar tv ON st.FK_tovar = tv.id
                WHERE tm.error = 2 OR tm.error = 3";

        $dataProvider = new SqlDataProvider([
        'sql' => $query,
                
            ]);

        return $dataProvider;
    }

    public static function getMissing( $cols )
    {
        $count = \Yii::$app->db->createCommand("
                SELECT COUNT(*) FROM ".self::tableName()."
                WHERE error = 1
            ")->queryScalar();

        $query = "SELECT col${cols['product']} product,
                        1 as f, 
                        col${cols['serial']} serial, 
                        col${cols['abon-number']} abon,
                        col${cols['activation-date']} active,
                        col${cols['registration-date']} reg
                FROM temp_exel 
                WHERE error = 1";

        $dataProvider = new SqlDataProvider([
        'sql' => $query,
        'totalCount' => $count,
        'pagination' => [
            'pageSize' => 20,
            ],
        ]);

        return $dataProvider;
    }
}
