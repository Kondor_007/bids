<?php

namespace common\modules\storage\models;

use Yii;
use common\modules\storage\Module;

/**
 * This is the model class for table "store_unit".
 *
 * @property integer $id
 * @property string $adress
 * @property string $name
 * @property integer $FK_org
 * @property integer $FK_broker
 * @property string $open_date
 * @property string $close_date
 * @property integer $has_stamp
 * @property string $email
 * @property string $phone
 * @property string $description
 * @property integer $FK_ref_price_group
 * @property integer $FK_ref_spot_spec
 * @property integer $FK_ref_location
 *
 * @property Broker $fKBroker
 * @property Organisation $fKOrg
 * @property StorageReference $storeLocation
 * @property StorageReference $priceGroup
 * @property StorageReference $profGroup
 */
class StoreUnit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_unit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address', 'name', 'FK_org'], 'required'],
            [['FK_org', 'FK_broker', 'has_stamp', 'FK_ref_price_group', 'FK_ref_spot_spec', 'FK_ref_location'], 'integer'],
            [['open_date', 'close_date'], 'safe'],
            [['description'], 'string'],
            [['address'], 'string', 'max' => 4096],
            [['name'], 'string', 'max' => 1024],
            [['email'], 'string', 'max' => 512],
            [['phone'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Адрес',
            'name' => 'Название',
            'FK_org' => 'Субдилер',
        	'subdealerName' => 'Субдилер',
            'FK_broker' => 'Торговый представитель',
        	'brokerName' => 'Торговый представитель',
        	'open_date' => 'Дата открытия',
            'close_date' => 'Дата закрытия',
            'has_stamp' => 'Есть печать',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'description' => 'Описание',
        	
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKBroker()
    {
        return $this->hasOne(Broker::className(), ['id' => 'FK_broker']);
    }
    
    public function getBrokerName()
    {
    	return isset($this->fKBroker)?$this->fKBroker->name:'Не указан';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKOrg()
    {
        return $this->hasOne( Subdealer::className(), ['id' => 'FK_org']);
    }
	
    public function getSubdealerName()
    {
    	return isset( $this->fKOrg )?$this->fKOrg->name:'Не задано';
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(StorageReference::className(), ['id' => 'FK_ref_location'])->andWhere( ['block' => StorageReference::STORE_LOCATION ] );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceGroup()
    {
        return $this->hasOne(StorageReference::className(), ['id' => 'FK_ref_price_group'])->andWhere( ['block' => StorageReference::PRICE_GROUP ] );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfGroup()
    {
        return $this->hasOne(StorageReference::className(), ['id' => 'FK_ref_spot_spec'])->andWhere(['block' => StorageReference::PROF_GROUP ]);
    }
}
