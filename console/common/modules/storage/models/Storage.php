<?php

namespace common\modules\storage\models;

use Yii;
use common\modules\storage\Module;

/**
 * This is the model class for table "Storage".
 *
 * @property integer $id
 * @property integer $FK_tovar
 * @property integer $FK_provider
 * @property integer $FK_document
 * @property integer $sim_number
 * @property integer $serial
 * @property integer $FK_user
 * @property string $registration_date
 * @property string $activation_date
 *
 * @property Doc $fKDocument
 * @property Provider $fKProvider
 * @property Tovar $fKTovar
 * @property User $fKUser
 */
class Storage extends \yii\db\ActiveRecord
{
    public $FK_provider;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'storage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FK_tovar', 'FK_provider'], 'integer'],
            [[ 'sim_number', 'serial'], 'required'],
            [['registration_date', 'activation_date'], 'safe'],
            [['providerName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'FK_tovar' => 'Тариф',
            'FK_provider' => 'Провайдер',
            'sim_number' => 'Тел. номер',
            'serial' => 'Серийный номер',
            'registration_date' => 'Дата регистрации',
            'activation_date' => 'Дата активации',
            'providerName' => 'Провайдер',
        	'subdealerName' => 'Субдилер',
        	'storeName' => 'Торговая точка',
        	'productName' => 'Товар',
        	'birthday' => 'Дата оформления',
        ];
    }
    
    public static function getProductBySerial( $serial )
    {
        return self::find()->where(['serial' => $serial ])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKTovar()
    {
        return $this->hasOne(Tovar::className(), ['id' => 'FK_tovar']);
    }

    public function getProviderName()
    {
        if($this->fKTovar !== null)
            return $this->fKTovar->providerName;
        else
            return 'Провайдер не задан';
    }

    public function getTovarName()
    {
        if($this->fKTovar !== null)
            return $this->fKTovar->name;
        else
            return 'Название не задано';
    }
    
    public function getProductName()
    {
    	if($this->fKTovar !== null)
    		return $this->fKTovar->name;
    	else
    		return 'Название не задано';
    }
	
    public function getFKStorageOperations()
    {
        return $this->hasMany( StorageOperation::className(), ['FK_storage' => 'id']);
    }
    
    public function getStorageOperation()
    {
    	return $this->hasOne(StorageOperation::className(), ['FK_storage' => 'id'])->where(['current' => 1 ]);
    } 
    
    public function getFKStorageOperation()
    {
    	return $this->hasOne(StorageOperation::className(), ['FK_storage' => 'id'])->where(['current' => 1 ]);
    }
    
    public function getFirstOperation()
    {
    	return $this->hasOne(StorageOperation::className(), ['FK_storage' => 'id'])->where(['op_type' => 1 ]);
    }
    
    public function getStore()
    {
    	return $this->storageOperation->store;
    }
    
    public function getStoreName()
    {
    	if( $this->store )
    		return $this->store->name;
    	else
    		return 'Склад';
    }
    
    public function getSubdealerName()
    {
    	if( $this->store )
    		return $this->store->subdealerName;
    	else
    		return 'Склад';
    }
    
    public function getBrokerName()
    {
    	if( $this->store )
    		return $this->store->brokerName;
    	else 
    		return 'Не отгружен';
    	
    }
    
    public function getBirthday()
    {
    	if( isset( $this->firstOperation->document ) )
    		return $this->firstOperation->document->datetime;
    	else
    		return null;
    }
    
    public function beforeDelete()
    {
    	if( parent::beforeDelete() )
    	{
    		StorageOperation::deleteAll([ 'FK_storage' => $this->id ]);
    		return true;
    	}
    	return false;
    }
     
}
