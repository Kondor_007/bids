<?php

namespace common\modules\storage\models;

use Yii;

/**
 * This is the model class for table "request".
 *
 * @property integer $id
 * @property integer $FK_product
 * @property integer $FK_store_unit
 * @property integer $FK_doc
 * @property integer $amount
 * @property string $op_date
 */
class Request extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FK_product', 'FK_store_unit', 'amount'], 'required'],
            [['FK_product', 'FK_store_unit', 'FK_doc', 'amount'], 'integer'],
            [['op_date'], 'safe']
        ];
    }

    public static function getStores()
    {
        $preorderedStores = self::find()->where('FK_doc IS NOT NULL')->groupBy('FK_store_unit')->all();
        $stores = [];

        foreach ($preorderedStores as $key => $value) 
        {
            $stores[ $value->FK_store_unit ] = StoreUnit::findOne( $value->FK_store_unit );
        }
        return $stores;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'FK_product' => 'Внешний ключ к таблице номенклатуры',
            'FK_store_unit' => 'Внешняя связь к таблице торговых точек',
            'FK_doc' => 'Внешняя связь к таблице документов',
            'amount' => 'Количество товара',
            'op_date' => 'Дата операции',
        ];
    }
}
