<?php

namespace common\modules\storage\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * StoreUnitSearch represents the model behind the search form about `StoreUnit`.
 */
class StoreUnitSearch extends StoreUnit
{	
	
	public $subdealerName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
        	[['name', 'address', 'subdealerName', 'FK_org'], 'safe'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoreUnit::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere([
        		'FK_org' => $this->FK_org,
        ]);
        $query->andFilterWhere(['like',	'name', $this->name ]);
        $query->andFilterWhere(['like',	'store_unit.address', $this->address ]);
        
        $query->joinWith(['fKOrg' => function($q)
        {
        	$q->joinWith(['organisation' => function($qr)
        	{
        		$qr->andFilterWhere(['like', 'short_name', $this->subdealerName]);
        	}]);
        	 
        }]);
        

        return $dataProvider;
    }
}
