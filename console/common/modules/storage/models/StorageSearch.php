<?php

namespace common\modules\storage\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\storage\models\Storage;

/**
 * StorageSearch represents the model behind the search form about `common\models\Storage`.
 */
class StorageSearch extends Storage
{
    /**
     * @inheritdoc
     */

    public $tovarNameFilter;
    public $providerNameFilter;
    public $documentNameFilter;
    public $userNameFilter;
    public $userIdFilter;
    public $storeFilter;

    public $opCodeFilter;
    public $realized;
    public $type;
    
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['registration_date','tovarNameFilter','providerNameFilter','documentNameFilter','userNameFilter', 'activation_date'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'providerName' => 'Провайдер',
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search( $params = '' )
    {
        $query = Storage::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if( $params !== '' )
        {
            $this->load($params);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('fKTovar');
        $query->joinWith('fKStorageOperation');

        $query->andFilterWhere([
            'id' => $this->id,
            'sim_number' => $this->sim_number,
            'serial' => $this->serial,
            'registration_date' => $this->registration_date,
            'activation_date' => $this->activation_date,
            'storage_operation.operation_id' => $this->opCodeFilter,
            'storage_operation.FK_user' => $this->userIdFilter,
            'storage_operation.op_type' => $this->type,
            'storage_operation.FK_store_unit' => $this->storeFilter,
        ]);

        if( !$this->realized )
            $query->andWhere( 'storage_operation.FK_doc IS NULL' );

        $query->andFilterWhere([ 'like', 'tovar.name', $this->tovarNameFilter ]);
        $query->andFilterWhere([ 'like', 'user.username', $this->userNameFilter ]);

        return $dataProvider;
    }
}
