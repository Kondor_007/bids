<?php

namespace common\modules\storage\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SubdealerSearch represents the model behind the search form about `Subdealer`.
 */
class SubdealerSearch extends Subdealer
{
	public $name;
	public $OKATO;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
        	[['name', 'OKATO'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Subdealer::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
        
        
        $query->andFilterWhere([
            'id' => $this->id,
        ]);
		
        $query->joinWith(['organisation' => function ($q){
        			$q->where("short_name LIKE \"%{$this->name}%\"");
        		}
        ]);

        return $dataProvider;
    }
}
