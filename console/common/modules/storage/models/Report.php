<?php

namespace common\modules\storage\models;

use yii\data\ActiveDataProvider;

class Report extends Storage
{
	public $store, $states, $subdealer, $provider, $broker;
	public $date_from, $date_to;
	
	public function rules()
	{
		return [
			[
			 ['serial', 'sim_number', 'FK_tovar', 
			  'activation_date', 'registration_date',
			  'store', 'states', 'subdealer',
			  'provider',
			  'date_from', 'date_to',
			 ], 'safe']	
		];
	}
	
	public function attributeLabels()
	{
		return array_merge( parent::attributeLabels(), [
				'subdealer' => 'Субдилер',
				'broker' => 'Торговый представитель',
				'store' => 'Торговая точка',
				'provider' => 'Провайдер',
				'states' => 'Состояние товара',
				'date_from' => 'Начиная с даты',
				'date_to' => 'Заканчивая датой',
		] );
	}
	
	public function search( $params )
	{
		$query = Storage::find();
		
		$dataProvider = new ActiveDataProvider([
				'query' => $query,
		]);
		
		$this->load($params);
		
		if (!$this->validate()) {
			// uncomment the following line if you do not want to any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		
		
		
			$query->andFilterWhere([
					'serial' => $this->serial,
					'sim_number' => $this->sim_number,
					'FK_tovar' => $this->FK_tovar,
					'activation_date' => $this->activation_date,
					'registration_date' => $this->registration_date,
			]);
			
			$query->andFilterWhere(['like', 'sim_number', $this->sim_number]);
			
			$query->joinWith(['fKTovar' => function( $q ){
				$q->from('tovar tv')->andFilterWhere([
						'FK_provider' => $this->provider,
				]);
			},
			'storageOperation' => function( $q ){
				$q->from('storage_operation st_op')->andFilterWhere(['FK_store_unit' => $this->store]);
			},
			
			'storageOperation.document' => function ( $q ){
				$q->from('doc');
				if( $this->store == 0 )
					$q->andFilterWhere([ 'between', 'doc.datetime', $this->date_from, $this->date_to ]);
				else 
					$q->andFilterWhere([ 'between', 'doc.datetime', $this->date_from, $this->date_to ]);
			},
			
			'fKTovar.fKProvider' => function ( $q )
			{
				$q->from('provider pv');
			}], true, [
					'storageOperation' => 'RIGHT JOIN',
			]);
							
		return $dataProvider;
	}
}