<?php

namespace common\modules\storage\models;

use Yii;

/**
 * This is the model class for table "storage_preference".
 *
 * @property integer $id
 * @property integer $block
 * @property string $block_name
 * @property string $prop_name
 * @property string $prop_val
 * @property string $prop_alias
 * @property string $description
 */
class StoragePreference extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'storage_preference';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['block', 'block_name', 'prop_name', 'prop_val', 'prop_alias', 'description'], 'required'],
            [['block'], 'integer'],
            [['description'], 'string'],
            [['block_name', 'prop_name', 'prop_val', 'prop_alias'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'block' => 'Block',
            'block_name' => 'Block Name',
            'prop_name' => 'Prop Name',
            'prop_val' => 'Prop Val',
            'prop_alias' => 'Prop Alias',
            'description' => 'Description',
        ];
    }

    public static function getProperty( $block = 'product-acceptance', $alias )
    {
        return self::find()->where([ 'prop_alias' => $alias, 
                                     'block_alias' => $block ])->one()->prop_val;
    }
}
