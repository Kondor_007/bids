<?php

namespace common\modules\storage\models;

use Yii;

/**
 * This is the model class for table "targets".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Sklad[] $sklads
 */
class Targets extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'targets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSklads()
    {
        return $this->hasMany(Sklad::className(), ['FK_target' => 'id']);
    }
}
