<?php

namespace common\modules\storage\models;

use Yii;

/**
 * This is the model class for table "doc".
 *
 * @property integer $id
 * @property integer $number
 * @property string $datetime
 * @property string $description
 * @property string $shipping_date
 *
 * @property Sklad[] $sklads
 */
class Doc extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doc';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['datetime', 'shipping_date'], 'safe'],
            [['description', 'number'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Number',
            'datetime' => 'Datetime',
            'description' => 'Description',
            'shipping_date' => 'Shipping Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorages()
    {
        return $this->hasMany( Storage::className(), ['FK_doc' => 'id']);
    }
}
