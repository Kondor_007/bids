
$('#provider-id').change( function(e)
{
    $.ajax({
            url: 'index.php?r=storage/tovar/get-by-provider',
            type: 'POST',
            dataType: 'json',
            data: '&provider-id='+this.value,
            success: function( response ) 
            {
                var select;
                if( response.status == 1 )
                {   
                    if( !(response.providers == '') )
                    {
                        for( var key in response.providers )
                        {
                            select += '<option value =\'' + key + '\'>' +
                                      response.providers[key] + '</option>';
                        }
                    }
                    else
                    {
                        select = '<option value = 0>-- Нет продуктов --</option>';
                    }
                    $('#storage-fk_tovar').html( select ).select2();
                }
                //$.pjax.reload({container:'#tovar-list'});   
            },
            error: function(response) {
                console.log( 'error' );
            }
    });
    
    return false; 
});

$('#submit-button').click(function( e ){
    var form = e.currentTarget.form;

    $.ajax({
            url: 'index.php?r=storage/storage/create',
            type: 'POST',
            dataType: 'json',
            data: $( form ).serialize(),
            success: function(response) {
                console.log( response );
                $.pjax.reload({container:'#storage-grid-pjax'});   
            },
            error: function(response) {
                console.log( 'error' );
            }
    });
   return false;
});

$('#submit-tariff-button').click(function( e ){
    var form = e.currentTarget.form;
    $.ajax({
            url: 'index.php?r=storage/tovar/create',
            type: 'POST',
            dataType: 'json',
            data: $( form ).serialize(),
            success: function(response) {
                console.log( response );
                $.pjax.reload({container:'#tovar-list'});   
            },
            error: function(response) {
                console.log( 'error' );
            }
    });
   return false;
});

$('#submit-realize').click( function(e){
    $.ajax({
        url: 'index.php?r=storage/storage/realize',
        type: 'POST',
        dataType: 'json',
        success: function( response )
        {
            if( response.status == 1 )
            {    
                $.pjax.reload({container:'#storage-grid'});
                $('#result').css('color', 'green');
                $('#result').text( 'Количество оформленных товаров: '+response.number );
            }
            else
            {
                $('#result').css('color', 'red');
                $('#result').text( 'Ошибка: '+ response.message );    
            }
        }
    });

});