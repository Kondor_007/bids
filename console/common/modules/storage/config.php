<?php
return [
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module'
        // enter optional module parameters below - only if you need to  
        // use your own export download action or custom translatio
        // message source
        // 'downloadAction' => 'gridview/export/download',
        // 'i18n' => []
        ],
        'flexform' => [
            'class' => 'vendor\novik\flexform\Module',//'common\modules\storage\components\flexform\Module',
        ],
    		
    	'aliases' => [
    		'@storage/root' => '/crm/common/modules/storage/',
    	],
    ],

];