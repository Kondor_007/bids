<?php

namespace common\modules\storage\classes;

class DepDropFormatter
{
	public static function format( $array )
	{
		$result = [];
		foreach ( $array as $key => $value )
		{
			$result[] = [
				'id' => $key,
				'name' => $value,	
			];
		}
		return $result;
	}
}