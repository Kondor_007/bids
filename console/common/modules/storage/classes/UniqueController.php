<?php

namespace common\modules\storage\classes;

use yii\web\Session;

class UniqueController
{
	const GENERATE_NEW = 'new';

	private function __construct(){}

	public static function control( $varName, $params )
	{
		$session = new Session;
		$session->open();
		
		if( isset( $params[ $varName ] ) )
		{
			$session[ $varName ] = $params[ $varName ];
		}
		else if( !isset( $session[ $varName ] ) || isset( $params[ self::GENERATE_NEW ] ) )
		{
			$session[ $varName ] = self::generateNew();
		}

		return $session[ $varName ];
	}

	public static function getUnique( $varName )
	{
		$session = new Session;
		$session->open();
		return isset($session[ $varName ])?$session[ $varName ]:false;
	}

	public static function renew( $varName )
	{
		$session = new Session;
		$session->open();
		$session[ $varName ] = self::generateNew();
	}

	private static function generateNew()
	{
		return date('ymdhis');
	}
}

?>
