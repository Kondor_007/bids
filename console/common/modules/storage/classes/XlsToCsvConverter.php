<?php

namespace common\modules\storage\classes;

use common\modules\storage\classes\ChunkReadFilter;

require_once( \Yii::getAlias('@vendor/phpoffice/phpexcel/Classes/PHPExcel.php') );

class XlsToCsvConverter
{
	private function __construct(){}

	/*
    *   Конвертирование файла XLS в CSV
    * c последующим удалением исходного файла.
    */
    public static function convert( $inputFileName, $startRow = 0, $endRow = 0, $columns = [] )
    {
        $error = '';
        /**  Определяем тип входного файла  **/
        $inputFileType = \PHPExcel_IOFactory::identify($inputFileName);
        /**  Создаем Reader нужного типа  **/
        $reader = \PHPExcel_IOFactory::createReader($inputFileType);

        // Читаем только данные, без форматирования
        $reader->setReadDataOnly(true);
        
        // Создаем объект фильтра 
        $readFilter = new ChunkReadFilter();

        $readFilter->setRows( $startRow, $endRow );
        if( !empty( $columns ) )
        	$readFilter->setCols( $columns );

        $reader->setReadFilter( $readFilter );

        $doc = $reader->load( $inputFileName );

        $worksheet = $doc->getActiveSheet();
        
        $writer = \PHPExcel_IOFactory::createWriter( $doc, 'CSV');

        $resultPath = pathinfo( $inputFileName, PATHINFO_DIRNAME ). '/temp.csv';

        unlink( $inputFileName );
        
        $writer->save( $resultPath );

        return $resultPath;
        
    }
}

?>