<?php


use yii\db\Migration;

class m150827_120000_spravochniki extends Migration
{
    public function up()
    {
        $this->addColumn('material', 'is_del', 'integer DEFAULT 0');
        $this->addColumn('machine', 'is_del', 'integer DEFAULT 0');
        $this->addColumn('request_process_type', 'is_del', 'integer DEFAULT 0');
                
    	}

    public function down()
    {
        $this->dropColumn('material', 'is_del');
        $this->dropColumn('machine', 'is_del');
        $this->dropColumn('request_process_type', 'is_del');
        
        
        
    }
}
