<?php

use yii\db\Schema;
use yii\db\Migration;

class m150626_070500_add_processes extends Migration
{
    public function up()
    {
    	$this->insert('request_process_type', [
    			'id' => 1,
    			'name' => 'Фрезеровка',
    	]);
    	
    	$this->insert('request_process_type', [
    			'id' => 2,
    			'name' => 'Люверсировка',
    	]);
    	
    	$this->insert('request_process_type', [
    			'id' => 3,
    			'name' => 'Ламинация',
    	]);
    	
    	$this->insert('payment_type', [
    			'id' => 1,
    			'name' => 'Наличный',
    	]);
    	
    	$this->insert('payment_type', [
    			'id' => 2,
    			'name' => 'Безналичный',
    	]);
    }

    public function down()
    {
        $this->delete('request_process_type', ['id' => [1, 2, 3]]);
        $this->delete('payment_type', ['id' => [1, 2]]);
    }
}
