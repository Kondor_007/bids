<?php

use yii\db\Schema;
use yii\db\Migration;

class m151224_070725_material_add_manufacturer extends Migration
{
    public function up()
    {
        //ALTER TABLE  `material` ADD  `manufacturer` VARCHAR( 50 ) NULL DEFAULT "" ;
      $this->addColumn('material','manufacturer', 'VARCHAR( 50 ) NULL DEFAULT ""'
          );
    }

    public function down()
    {
       // echo "m151224_070725_test cannot be reverted.\n";
        $this->dropColumn('material','manufacturer');
        //return false;
    }
    

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }

}
