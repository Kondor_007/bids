<?php

use yii\db\Schema;
use yii\db\Migration;

class m150625_114825_dial_fix4 extends Migration
{
    public function up()
    {
		$this->addColumn('request_process', 'FK_request', 'integer');
		$this->addColumn('material', 'FK_machine_type', 'integer');
    }

    public function down()
    {
        $this->dropColumn('request_process', 'FK_request');
        $this->dropColumn('material', 'FK_machine_type');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
