<?php

use yii\db\Schema;
use yii\db\Migration;

class m150624_134742_dial_fix2 extends Migration
{
    public function up()
    {
		$this->addColumn('request_process', 'FK_process_type', 'integer');
		$this->createTable('request_process_type', [
				'id' => 'pk',
				'name' => 'string',
		], "DEFAULT CHARSET = utf8");
    }

    public function down()
    {
        $this->dropColumn('request_process', 'FK_process_type');
        $this->dropTable('request_process_type');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
