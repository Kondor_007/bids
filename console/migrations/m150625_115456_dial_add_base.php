<?php

use yii\db\Schema;
use yii\db\Migration;

class m150625_115456_dial_add_base extends Migration
{
    public function up()
    {
		$this->insert('machine_type', [
				'id' => '1',
				'name' => 'Фрезер',
		]);
		
		$this->insert('machine_type', [
				'id' => '2',
				'name' => 'Плоттер',
		]);
		
		$this->insert('machine', [
				'name' => '1',
				'FK_type' => 1,
		]);
		
		$this->insert('machine', [
				'name' => '2',
				'FK_type' => 1,
		]);
		
		$this->insert('machine', [
				'name' => '1',
				'FK_type' => 2,
		]);
		
		$this->insert('machine', [
				'name' => '2',
				'FK_type' => 2,
		]);
		
		$this->insert('machine', [
				'name' => '3',
				'FK_type' => 2,
		]);
		
		$this->insert('material', [
				'id' => '1',
				'name' => 'Материал для фрезера 1',
				'FK_machine_type' => 1,
		]);
		
		$this->insert('material', [
				'id' => '2',
				'name' => 'Материал для фрезера 2',
				'FK_machine_type' => 1,
		]);
		
		$this->insert('material', [
				'id' => '3',
				'name' => 'Материал для плоттера 1',
				'FK_machine_type' => 2,
		]);
		
		$this->insert('material', [
				'id' => '4',
				'name' => 'Материал для плоттера 2',
				'FK_machine_type' => 2,
		]);
		
		$this->insert('material_quality', [
				'id' => '1',
				'name' => '800x600',
				'FK_material' => 1,
		]);
		
		$this->insert('material_quality', [
				'id' => '2',
				'name' => '1024x4096',
				'FK_material' => 1,
		]);
		
		$this->insert('material_quality', [
				'id' => '3',
				'name' => '200x300',
				'FK_material' => 2,
		]);
		
		$this->insert('material_quality', [
				'id' => '4',
				'name' => '640x480',
				'FK_material' => 2,
		]);
    }

    public function down()
    {
        $this->delete('machine_type', ['id' => [1, 2]]);
        $this->delete('machine', ['FK_type' => [1, 2]]);
        $this->delete('material', ['FK_machine_type' => [1, 2]]);
        $this->delete('material_quality', ['FK_material' => [1, 2]]);
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
