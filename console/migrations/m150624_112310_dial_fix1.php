<?php

use yii\db\Schema;
use yii\db\Migration;

class m150624_112310_dial_fix1 extends Migration
{
    public function up()
    {
    	$this->addColumn('request_draft', 'FK_quality', 'integer');
    	$this->addColumn('request', 'deadline', 'date');
    	$this->createTable('material_quality', [
    			'id' => 'pk',
    			'FK_material' => 'integer',
    			'name' => 'string',
    	], "DEFAULT CHARSET = utf8");
    }

    public function down()
    {
    	$this->dropColumn('request', 'deadline');
        $this->dropColumn('request_draft', 'FK_quality');
        $this->dropTable('material_quality');
    }
}
