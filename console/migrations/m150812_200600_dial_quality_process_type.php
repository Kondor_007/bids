<?php

use yii\db\Schema;
use yii\db\Migration;

class m150812_200600_dial_quality_process_type extends Migration
{
    public function up()
    {
        $this->createTable('quality_process', [
    			'id' => 'pk',
    			'name' => 'string(255',
                'FK_process_type' => 'integer'
    	], "DEFAULT CHARSET = utf8");


        $this->createTable('processtype_machine', [
    			'id' => 'pk',
    			'FK_machine' => 'integer',
                'FK_process_type' => 'integer'
    	], "DEFAULT CHARSET = utf8");

        $this->createTable('material_machine', [
    			'id' => 'pk',
    			'FK_machine' => 'integer',
                'FK_material' => 'integer'
    	], "DEFAULT CHARSET = utf8");
        
    	}

    public function down()
    {
        $this->dropTable('quality_process');
        $this->dropTable('processtype_machine');
        $this->dropTable('material_machine');
        
        
    }
}

/*
CREATE TABLE IF NOT EXISTS `quality_process` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `FK_process_type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `processtype_machine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FK_process_type` int(11) NOT NULL,
  `FK_machine` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `material_machine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FK_material` int(11) NOT NULL,
  `FK_machine` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

*/