<?php

use yii\db\Schema;
use yii\db\Migration;

class m150624_150245_dial_fix3 extends Migration
{
    public function up()
    {
		$this->addColumn('request', 'description', 'text');
		$this->addColumn('request', 'cost', 'float');
    }

    public function down()
    {
    	$this->dropColumn('request', 'description');
    	$this->dropColumn('request', 'cost');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
