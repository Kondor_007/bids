<?php

use yii\db\Schema;
use yii\db\Migration;

class m150624_081437_request_table extends Migration
{
    public function safeUp()
    {
    	$this->createTable('material_unit', [
    			'id' => 'pk',
    			'name' => 'string',
    	], "DEFAULT CHARSET = utf8");
    	
    	$this->createTable('material', [
    			'id' => 'pk',
    			'name' => 'string(512)',
    			'FK_unit' => 'integer',
    			'issue_limit' => 'float',
    			'buy_price' => 'float DEFAULT 0',
    			'sell_price' => 'float DEFAULT 0', 
    	], "DEFAULT CHARSET = utf8" );
    	
    	$this->createTable('document', [
    			'id' => 'pk',
    			'FK_user' => 'integer',
    			'FK_request' => 'integer',
    			'FK_material' => 'integer',
    			'FK_type' => 'integer',
    			'amount' => 'smallint',
    	], "DEFAULT CHARSET = utf8");
    	
    	$this->createTable('request_draft', [
    			'id' => 'pk',
    			'FK_material' => 'integer',
    			'FK_request' => 'integer NOT NULL',
    			'amount' => 'smallint DEFAULT 1',
    			'work_space' => 'float',
    			'blank_space' => 'float',
    			'height' => 'float',
    			'width' => 'float',
    			'price' => 'float',
    			'cost' => 'float',
    			'file_path' => 'string(1024)'
    	], "DEFAULT CHARSET = utf8");
    	
    	$this->createTable('machine_type', [
    			'id' => 'pk',
    			'name' => 'string',
    			'alias' => 'string',
    	], "DEFAULT CHARSET = utf8");
    	
    	$this->createTable('machine', [
    			'id' => 'pk',
    			'FK_type' => 'integer NOT NULL',
    			'name' => 'string NOT NULL',
    			'FK_user' => 'integer'
    	], "DEFAULT CHARSET = utf8");
    	
    	$this->createTable('request_process', [
    			'id' => 'pk',
    			'amount' => 'smallint',
    			'price' => 'float',
    			'cost' => 'float',
    	], "DEFAULT CHARSET = utf8");
    	
    	$this->createTable('payment_type', [
    			'id' => 'pk',
    			'name' => 'string',
    	], "DEFAULT CHARSET = utf8");
    	
    	$this->createTable( 'request', [
    			'id' => 'pk',
    			'FK_client' => 'integer',
    			'FK_device' => 'integer',
    			'FK_task' => 'integer',
    			'draft_cost' => 'float',
    			'FK_payment_type' => 'integer',
    			'payed_part' => 'float',
    	], "DEFAULT CHARSET = utf8");
    	
    }
    
    public function safeDown()
    {
    	echo "Impossible";
  		return false;
    }
}
