<?php

use yii\db\Schema;
use yii\db\Migration;

class m150722_132600_dial_task_add_newfields extends Migration
{
    public function up()
    {
    	$this->addColumn('task_responsible', 'FK_role', 'string');
        $this->addColumn('request_process', 'FK_draft', 'integer');
        $this->addColumn('request_draft', 'name', 'string(255)');
    	}

    public function down()
    {
    	$this->dropColumn('task_responsible', 'FK_role');
        $this->dropColumn('request_process', 'FK_draft');
        $this->dropColumn('request_draft', 'name');
        
    }
}
