<?php

use yii\db\Schema;
use yii\db\Migration;

class m150520_122520_org_string_addr extends Migration
{
    public function safeUp()
    {
		$this->addColumn( 'organisation', 'address_legal', 'string');
		$this->addColumn( 'organisation', 'address_post', 'string');
    }

    public function safeDown()
    {
    	$this->dropColumn('organisation', 'address_legal');
    	$this->dropColumn('organisation', 'address_post');
    }
}
