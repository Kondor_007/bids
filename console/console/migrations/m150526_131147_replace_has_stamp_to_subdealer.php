<?php

use yii\db\Schema;
use yii\db\Migration;

class m150526_131147_replace_has_stamp_to_subdealer extends Migration
{
    public function up()
    {
		$this->dropColumn('store_unit', 'has_stamp');
		$this->addColumn('storage_subdealer', 'has_stamp', 'boolean DEFAULT 0');
    }

    public function down()
    {
    	$this->dropColumn('storage_subdealer', 'has_stamp');
    	$this->addColumn('store_unit', 'has_stamp', 'boolean DEFAULT 0');
    }
    
}
