<?php

use yii\db\Schema;
use yii\db\Migration;

class m150527_063724_contracts_table extends Migration
{
    public function safeUp()
    {
    	$this->createTable( 'contract_type', [
    			'id' => 'pk',
    			'name' => 'string NOT NULL',
    	], 'DEFAULT CHARSET = utf8' );
    	
		$this->createTable( 'contract', [
				'id' => 'pk',
				'name' => 'string NOT NULL',
				'FK_organisation' => 'integer NOT NULL',
				'number' => 'string NOT NULL',
				'date_start' => 'date DEFAULT NULL',
				'date_end' => 'date DEFAULT NULL',
				'FK_type' => 'integer DEFAULT 1',
				'file_path' => "string(2048)",
				'description' => 'string(2048)',
		], 'DEFAULT CHARSET = utf8' );
		
		$this->insert('contract_type', [
				'name' => 'C комиссионером',
		]);
		$this->insert('contract_type', [
				'name' => 'С поставщиком',
		]);
		$this->insert('contract_type', [
				'name' => 'Купля/Продажа',
		]);
		
    }

    public function safeDown()
    {
        $this->dropTable('contract_type');
        $this->dropTable('contract');
    }
}
