<?php

use yii\db\Schema;
use yii\db\Migration;

class m150519_123130_subdealer_table extends Migration
{
	public function safeUp()
    {
        $this->createTable( 'storage_subdealer', [
                'id' => 'pk',
                'FK_organisation' => 'integer NOT NULL',
            ], 'DEFAULT CHARSET = utf8' );
    }

    public function safeDown()
    {
        $this->dropTable('storage_subdealer');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
