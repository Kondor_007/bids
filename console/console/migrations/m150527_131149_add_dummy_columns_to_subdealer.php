<?php

use yii\db\Schema;
use yii\db\Migration;

class m150527_131149_add_dummy_columns_to_subdealer extends Migration
{
    public function safeUp()
    {
    	$this->addColumn('storage_subdealer', 'mts_condition', 'string DEFAULT NULL');
    	$this->addColumn('storage_subdealer', 'bee_condition', 'string DEFAULT NULL');
    	$this->addColumn('storage_subdealer', 'meg_condition', 'string DEFAULT NULL');
    	$this->addColumn('storage_subdealer', 'tel_condition', 'string DEFAULT NULL');
    }
    
    public function safeDown()
    {
    	$this->dropColumn('storage_subdealer', 'mts_condition');
    	$this->dropColumn('storage_subdealer', 'bee_condition');
    	$this->dropColumn('storage_subdealer', 'meg_condition');
    	$this->dropColumn('storage_subdealer', 'tel_condition');
    }
}
