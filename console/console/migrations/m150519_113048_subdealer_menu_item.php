<?php

use yii\db\Schema;
use yii\db\Migration;

class m150519_113048_subdealer_menu_item extends Migration
{
	public function safeUp()
    {
        $this->insert('auth_item', [
        			'name' => '/storage/subdealer/index',
        			'type' => 2,
        ]);
        
        $this->insert('menu', [
        			'name' => 'Субдилеры',
        			'parent' => 16,
        			'route' => 	'/storage/subdealer/index',	
        ] );
    }

    public function safeDown()
    {
        $this->delete('menu', [ 'parent' => 16, 'route' => '/storage/subdealer/index' ] );
        $this->delete('auth_item', [ 'name' => '/storage/subdealer/index', 'type' => 2 ] );
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
