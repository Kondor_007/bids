<?php

use yii\db\Schema;
use yii\db\Migration;

class m150526_111853_report_menu_rerouting extends Migration
{
    public function up()
    {
    	$this->insert('auth_item', ['type' => 2, 'name' => '/storage/report/report-view']);
		$this->update('menu', ['route' => '/storage/report/report-view'], ['name' => "Формирование отчетов"]);
		
    }

    public function down()
    {
    	$this->update('menu', ['route' => '/storage/report/index'], 'name = "Формирование отчетов"');
        $this->delete('auth_item', ['type' => 2, 'name' => '/storage/report/report-view']);
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
