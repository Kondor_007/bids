<?php
namespace console\controllers;
use Yii;
use yii\console\Controller;
use common\components\rbac\UserRoleRule;

class RbacController extends Controller
{
	public function actionInit()
	{
		$auth = Yii::$app->authManager;
		$auth->removeAll();	// Удаляем старые данные 
		//Доступ к админке
		$dashboard = $auth->createPermission( 'dashboard' );
		$dashboard->description = 'Админ панель';
		$auth->add( $dashboard );

		// Видимость полей
		$lastName = $auth->createPermission( 'lastName' );
		$lastName->description = 'Видимость поля фамилия';
		$auth->add( $lastName );

		$firstName = $auth->createPermission( 'firstName' );
		$firstName->description = 'Видимость поля имя';
		$auth->add( $firstName );

		$patName = $auth->createPermission( 'patName' );
		$patName->description = 'Видимость поля отчество';
		$auth->add( $patName );

		$photo = $auth->createPermission( 'photo' );
		$photo->description = 'Видимость поля фото';
		$auth->add( $photo );

		// Включаем наш обработчик
		//$rule = new UserRoleRule();
		//$auth->add( $rule );

		//Добавляем роли
		$user = $auth->createRole( 'user' );
		$user->description = 'Пользователь';
		//$user->ruleName = $rule->name;
		$auth->add( $user );
		$auth->addChild( $user, $firstName );
		$auth->addChild( $user, $lastName );

		$extUser = $auth->createRole( 'extUser' );
		$extUser->description = 'Продвинутый пользователь';
		//$extUser->ruleName = $rule->name;
		$auth->add( $extUser );
		$auth->addChild( $extUser, $user );
		$auth->addChild( $extUser, $patName );

		$admin = $auth->createRole( 'admin' );
		$admin->description = 'Администратор';
		//$admin->ruleName = $rule->name;
		$auth->add( $admin );
		$auth->addChild( $admin, $extUser );
		$auth->addChild( $admin, $photo );
		$auth->addChild( $admin, $dashboard );

		// Назначаем роли пользователям по ID
		$auth->assign( $extUser, 1 );
		$auth->assign( $admin, 11 );
		$auth->assign( $user, 12 );
		$auth->assign( $user, 13 );

	}
}