<?php

namespace backend\controllers;

use Yii;
use common\models\PayAccount;
use common\models\Organisation;
use common\models\Bank;
use backend\models\PayAccountSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * PayAccountController implements the CRUD actions for PayAccount model.
 */
class PayAccountController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PayAccount models.
     * @return mixed
     */
    public function actionIndex( $id )
    {
        $dataProvider = new ActiveDataProvider( [ 'query' => PayAccount::find()->where([ 'FK_org' => $id ]) ] );
        return $this->render( 'index', [ 'dataProvider' => $dataProvider, 'model' => Organisation::findOne($id) ] );
    }

    /**
     * Displays a single PayAccount model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PayAccount model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate( $id )
    {
        $model = new PayAccount();

        if ($model->load(Yii::$app->request->post()) && $model->save()) 
        {
            return $this->redirect(['view', 'id' => $model->id]);
        } 
        else 
        {
            $model->FK_org = $id;
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PayAccount model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PayAccount model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionAddbank()
    {
        $model = new Bank();

        if ( $model->load(Yii::$app->request->post() ) && $model->save() ) 
        {
            
            $model->refresh();
            
            $banksList = ArrayHelper::map( Bank::find()->all(), 'id', 'name' );

            $banksList['selected'] = $model->id;
            return  json_encode( $banksList );
        }
        
        throw new yii\web\BadRequestHttpException;
        return;
    }

    /**
     * Finds the PayAccount model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PayAccount the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if ( ( $model = PayAccount::findOne( $id ) ) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
