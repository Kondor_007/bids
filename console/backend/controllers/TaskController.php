<?php

namespace backend\controllers;

use Yii;
use common\models\Task;
use backend\models\TaskSearch;
use common\models\TaskResponsible;
use common\models\TaskStatus;
use common\models\Project;
use common\models\UsExt;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class TaskController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreate()
    {
        $model = new Task();
        $post = \Yii::$app->request->post();
        
        if( $model->load( Yii::$app->request->post() ) == false )
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $parentId = $model->parent_id;

        if( !empty( $parentId ) )
        {
            $parentTask = Task::find()->where( [ 'id' => $parentId ] )->one();
            $model->level = $parentTask->level + 1;
        }

        $model->FK_originator =  ( new UsExt )->getCurrentUserId();
        if( $model->save() == false )
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $model->refresh();

        $taskStatus = new TaskStatus();

        $taskStatus->FK_user = $model->FK_originator;
        $taskStatus->FK_task = $model->id;
        $taskStatus->FK_status = TaskStatus::TASK_WAIT;
        $taskStatus->description = 'Задача ожидает выполнения';

        if( $taskStatus->save() == false )
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $responsibleModel = new TaskResponsible();

        $responsibleModel->FK_task = $model->id;
        $responsibleModel->FK_user = $post['responsible'];

        if( $responsibleModel->save() == false )
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

    }

    public function actionUpdate( $id )
    {   
        $model = $this->findModel( $id );

        $newModel = new Task();

        $post = Yii::$app->request->post();

        if( $newModel->load( Yii::$app->request->post() ) )
        {
            $newModel->id = $id;
            $newModel->date = $model->date;
            $newModel->version = $model->version + 1;
            $newModel->FK_originator = $model->FK_originator;
            $newModel->FK_us_edit = \Yii::$app->user->identity->profileId;//( new UsExt )->getCurrentUserId();

            $parentId = $newModel->parent_id;

            if( !empty( $parentId ) )
            {
                $parentTask = Task::find()->where( [ 'id' => $parentId ] )->one();
                $newModel->level = $parentTask->level + 1;
            }

            if( $this->changeResponsible( $post['responsible'], $model->id ) == false )
            {
                return 'Error: cant modify responsible';
            }

            if( $newModel->save() )
            {
                $model->active = 0;
                $model->save();
            }

            return 'OK';
        }
        
        return $this->getErrorsArray( $model );
    }

    public function actionEdit( $id )    
    {
        $model = $this->findModel( $id );
        $oldAttr = $model->attributes;

        unset( $oldAttr['date_edit'] );

        $newModel = new Task();

        $newModel->setAttributes( $oldAttr, false );
        //$newModel->level = $model->level;
        $post = Yii::$app->request->post();

        if( $newModel->load( Yii::$app->request->post() ) )
        {
            //$newModel->id = $id;
            //$newModel->date = $model->date;
            $newModel->version = $model->version + 1;
            //$newModel->FK_originator = $model->FK_originator;
            $newModel->FK_us_edit = \Yii::$app->user->identity->profileId;

            $parentId = $newModel->parent_id;

            if( !empty( $parentId ) )
            {
                $parentTask = Task::find()->where( [ 'id' => $parentId ] )->one();
                $newModel->level = $parentTask->level + 1;
            }

            if( isset($post['responsible']) )
            {
                if( $this->changeResponsible( $post['responsible'], $model->id ) == false )
                {
                    return 'Error: can\'t modify responsible';
                }
            }

            if( $newModel->save() )
            {
                $model->active = 0;
                $model->save();
                return $this->redirect( [ 'view-all' ] );
            }

            return $this->render('edit-task', [ 'model' => $newModel ]);
        }
        else
        {
            return $this->render('edit-task', [ 'model' => $newModel ]);
        }
        
        return $this->getErrorsArray( $model );
    }

    private function changeResponsible( $userId, $taskId )
    {

        $oldResponsible = TaskResponsible::find()->where([ 'FK_task' => $taskId, 'type' => 1 ])->one();
        if( $oldResponsible->FK_user == $userId )
        {
            return true;
        }

        $model = new TaskResponsible();

        $model->FK_task = $taskId;
        $model->FK_user = $userId;

        if( $model->save() )
        {
            $oldResponsible->type = 0;
            if( $oldResponsible->save() == false )             
            {
                return false;
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }

        return true;
    }

    public function actionGetTaskInfo()
    {
        $post = \Yii::$app->request->post();

        if( !isset( $post['id'] ) )
        {
            return 'Не получен ID задачи.';
        }

        $model = Task::find()->where( ['id' => $post['id'], 'active' => 1 ] )->one();
        if( empty( $model ) )
        {
            return 'Не удалось найти модель';
        }

        $responseArray = $model->getAttributes(['FK_project', 'parent_id', 'description', 'name', 'priority']);
        
        if( $model->taskResponsibles )
        {
            foreach ($model->taskResponsibles as $key => $value) 
            {
                $responseArray['responsible'] = $value->FK_user;
            }
        }
        
        return json_encode( $responseArray );
    }

    public function actionViewAll()
    {
        $searchModel = new TaskSearch();
        $searchModel->FK_originator = UsExt::getCurrentUserId();
        $searchModel->FK_responsible = UsExt::getCurrentUserId();
        $dataProvider = $searchModel->search( '', true );
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionViewTasks()
    {
        $post = Yii::$app->request->post();

        $searchModel = new TaskSearch();
        
        switch( $post['view-option'] )
        {
            case Task::TASK_SHOW_ALL:
                $searchModel->FK_originator = UsExt::getCurrentUserId();
                $searchModel->FK_responsible = UsExt::getCurrentUserId();
                $searchModel->active = 1;
                $dataProvider = $searchModel->search( '', true );
            break;
            
            case Task::TASK_SHOW_INSTRUCTED:
                $searchModel->FK_originator = UsExt::getCurrentUserId();
                $searchModel->active = 1;
                $dataProvider = $searchModel->search();
            break;
            
            case Task::TASK_SHOW_EXEC:
                $searchModel->FK_responsible = UsExt::getCurrentUserId();
                $searchModel->active = 1;
                $dataProvider = $searchModel->search();
            break;

            default:
                $searchModel->FK_originator = UsExt::getCurrentUserId();
                $searchModel->FK_responsible = UsExt::getCurrentUserId();
                $searchModel->active = 1;
                $dataProvider = $searchModel->search( '', true );
            break;
        }
        
        
        return $this->renderPartial('view-all', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionIsOriginator()
    {
        $post = Yii::$app->request->post();
        if( isset( $post['id'] ) )
        {
            $taskModel = Task::findOne( $post['id'] );
            return $taskModel->isOriginator();
        }
        return 'Не удалось обработать запрос';
    }

    /*
    * Список проектов
    */
    public function actionGetProjectList()
    {
        return json_encode( ArrayHelper::map( Project::find()->all(), 'id', 'name' ) );
    }

    /*
    * Список задач
    */
    public function actionGetTaskList()
    {
        return json_encode( ArrayHelper::map( Task::find()->all(), 'id', 'name' ) );
    }


    public function getErrorsArray( $model )
    {
        $errors = 'Task model errors:\n ';
        foreach ( $model->getErrors() as $key => $err) 
        {
            $errors .= $err[0].'\n';
        }
        return $errors;
    }

    protected function findModel($id)
    {
        if (($model = Task::find()->where( [ 'id' => $id, 'active' => 1 ] )->one() ) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
