<?php

namespace backend\controllers;

use Yii;
use common\models\UsExt;
use common\models\User;
use backend\models\UsExtSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * UsExtController implements the CRUD actions for UsExt model.
 */
class UsExtController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UsExt models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsExtSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UsExt model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UsExt model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UsExt();
        $model->scenario = 'userUpdate';
        if ( $model->load( Yii::$app->request->post() ) )
        { 
            $userModel = new User();
            
            $userModel->username = $model->newUsername;
            $userModel->email = $model->newEmail;
            $userModel->setPassword( $model->newPassword );
            $userModel->generateAuthKey();
            
            if( $userModel->save() )
            {
                $model->FK_Users = $userModel->id;

                if ( $model->save()) 
                {
                    return $this->redirect(['view', 'id' => $model->ID]);
                } 
                else 
                {
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            }
        }
        else
        {
            return $this->render('create', [
                        'model' => $model,
                    ]);   
        }
    }

    public function actionCreatePhantom()
    {    
        $model = new UsExt();

        $model->scenario = 'createByManager';
        $errors = '';
        if( $model->load( Yii::$app->request->post() ) )
        {
            $userModel = new User();

            $userModel->username = $model->generateUsername();
            $userModel->email = $userModel->username.'@undefined.ru';
            $userModel->setPassword( 'qwerty159357' );
            $userModel->generateAuthKey();

            if( $userModel->save() ) 
            {
                $userModel->refresh();
                $model->FK_Users = $userModel->id;
                if( $model->save() )
                {
                    $model->refresh();
                    $userList = ArrayHelper::map( UsExt::find()->all(), 'ID', 'fullName' );
                    $userList['selected'] = $model->ID;
                    return json_encode( $userList );
                }
            }
        }
    }

    /**
     * Updates an existing UsExt model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'userUpdate';
        $userModel = User::findOne( $model->FK_Users );
        $model->newUsername = $userModel->username;
        $model->newEmail = $userModel->email;

        if ($model->load(Yii::$app->request->post()) && $model->save()) 
        {

            return $this->redirect(['view', 'id' => $model->ID]);
        } else 
        {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UsExt model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionAssign($id)
    {
        $model = $this->findModel($id);

        $this->redirect(['admin/assignment/view', 'id'=>$model->FK_Users]);
    }

    /**
     * Finds the UsExt model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UsExt the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UsExt::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function getCurrentUserId()
    {
        $userProfileModel = UsExt::find()->where(['FK_Users' => \Yii::$app->user->identity->id ])->one();
        return $userProfileModel->ID;    
    }

    public function actionGetCurrentUserId()
    {
        return $this->getCurrentUserId();
    }

    public function actionGetCurrentUserName()
    {
        $userProfileModel = UsExt::find()->where(['FK_Users' => \Yii::$app->user->identity->id ])->one();
        return $userProfileModel->shortName;
    }

    public function actionGetOwnOrgEmployees()
    {
        $employees = UsExt::getOwnOrgEmployees();

        $resultArray = ArrayHelper::map( $employees, 'ID', 'shortName' );
        $resultArray['currentUserId'] = UsExt::getCurrentUserId();

        return json_encode( $resultArray );
    }
}
