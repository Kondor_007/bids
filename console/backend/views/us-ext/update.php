<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UsExt */

$this->title = 'Update Us Ext: ' . ' ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Us Exts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="us-ext-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
