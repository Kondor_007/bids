<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UsExtSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Us Exts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="us-ext-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Us Ext', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'username',
            'email',
            'fullName',
            'orgName',
            // 'Photo',

            ['class' => 'yii\grid\ActionColumn',
             'buttons'=>[
                        'assign'=> function ( $url, $model, $key )
                            {
                                return Html::a( '<span class="glyphicon glyphicon-cog"></span>', $url,
                                                [ 'title'=>Yii::t('yii', 'Assign role' ),] );
                            }
                        ],
            'template' => '{view} {update} {delete} {assign} ',
            'controller'=>'usext',
            ],
        ],
    ]); ?>

</div>
