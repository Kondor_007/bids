<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PayAccount */

$this->title = 'Счет '.$model->bankName;
$this->params['breadcrumbs'][] = ['label' => 'Расчетные счета '.$model->orgName, 'url' => ['index', 'id' => $model->FK_org]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pay-account-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a( Yii::t('yii', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary'] ) ?>
        <?= Html::a( Yii::t('yii', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'orgName',
            'bankName',
            'account',
            'description',
        ],
    ]) ?>

</div>
