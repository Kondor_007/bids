<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Organisation */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Расчетные счета ';
$this->params['breadcrumbs'][] = ['label' => 'Организации', 'url' => ['organisation/index'] ];
$this->params['breadcrumbs'][] = ['label' => $model->short_name, 'url' => ['organisation/view', 'id' => $model->id] ];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pay-account-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить расчетный счет', ['create', 'id' => $model->id ], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            'bankName',
            'orgName',
            'account',
            'description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
