<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PayAccount */

$this->title = 'Добавить расчетный счет';

$this->params['breadcrumbs'][] = ['label' => 'Организации', 'url' => ['organisation/index'] ];
$this->params['breadcrumbs'][] = ['label' => $model->orgName, 'url' => ['organisation/view', 'id' => $model->FK_org] ];
$this->params['breadcrumbs'][] = ['label' => 'Расчетные счета ', 'url' => ['index', 'id' => $model->FK_org]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pay-account-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
