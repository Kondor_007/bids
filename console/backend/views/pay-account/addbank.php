<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\assets\AppAsset;
/* @var $this yii\web\View */
/* @var $model common\models\Bank */
 ?>   
    <div class="modal fade" id="addBankModal" tab_index="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><?=Yii::t( 'yii', 'Add bank' ) ?></h4>
                </div>
                <!-- Тело модального окна -->
                <div class="modal-body">
        
                    <?php
                    use common\models\Bank;

                    $modelBank = new Bank();

                    $form = ActiveForm::begin(
                            ['id' => 'crm-form-add-bank',
                            'action' => ['pay-account/addbank']
                            ]); 
                    ?>

                    

                    <input type = 'hidden' name = 'addBankModal' value = 'bank/getbanks' id = 'extraDataField' >

                    <?php ActiveForm::end(); ?>
                
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" >Закрыть</button>
                <button type="button" class="btn btn-primary" id="submitModalForm" form = "crm-form-add-bank"><?= Yii::t( 'yii', 'Add bank') ?></button>
            </div>
        </div>
      </div>

</div>


