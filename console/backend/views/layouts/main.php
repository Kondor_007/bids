<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use kartik\widgets\SideNav;


use mdm\admin\components\MenuHelper;
AppAsset::register($this);
/* @var $this \yii\web\View */
/* @var $content string */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap kv-header">
        <?php
            NavBar::begin([
                'brandLabel' => 'IT-INTEGO',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);


            $menuItems = [
                ['label' => 'Главная', 'url' => ['/site/index'],
                ],
            ];
           
            if (Yii::$app->user->isGuest) 
            {
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
            } 
            else 
            {
                $menuItems[] = [
                    'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]); 

            NavBar::end();
        ?>
                    
        
        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <div class = 'row'>
                <div class = 'col-md-2'>
                    <?= SideNav::widget([
                                'items'=>MenuHelper::getAssignedMenu(Yii::$app->user->id, null, null, true), 
                                'type' => SideNav::TYPE_PRIMARY]) ?>
                </div>
                <div class = 'col-md-10'>
                    <?= $content ?>
                </div>
            </div>
        </div>

    <footer class="footer">
        <div class="container">
        <p class="pull-left">&copy; IT-INTEGO <?= date('Y') ?></p> 
        <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
