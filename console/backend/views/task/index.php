<?php

use yii\helpers\Html;
use yii\bootstrap\Dropdown;
use common\components\modalWidgets\TaskWidget;
use common\models\Task;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tasks';
$this->params['breadcrumbs'][] = $this->title;

$viewOptions = [
    Task::TASK_SHOW_ALL => 'Все',
    Task::TASK_SHOW_INSTRUCTED => 'Поручил',
    Task::TASK_SHOW_EXEC => 'Выполняю',
];

?>
<h1><?= Html::encode($this->title) ?></h1>

    <p>
        <div class = 'row'>
        	<div class = 'col-xs-2'>
		        <?= TaskWidget::widget() ?>
		    </div>
	        <div class = 'col-xs-2'>
		        
                <?= Html::dropDownList('view-tasks-options', 0, $viewOptions, 
                                        [ 
                                            'encode' => true, 
                                            'class' => 'form-control',
                                            'id' => 'view-options', 
                                        ]) ?>
	    	    
            </div>
        </div>
    </p>

    <?= $this->render('view-all', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]) ?>
