<?php

use yii\helpers\Html;
use yii\bootstrap\Dropdown;
use yii\grid\GridView;
use yii\grid\DataColumn;
use common\components\modalWidgets\TaskWidget;
use common\models\TaskStatus;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
const CHILD_IDENT = '&nbsp&nbsp&nbsp&nbsp';
?>

<div class="task-view-all" id='task-view-all'>

    <?php \yii\widgets\Pjax::begin() ?>
    <?php 
       echo GridView::widget([
        'id' => 'task-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\CheckBoxColumn'],
            [
                 'class'=>'yii\grid\DataColumn',
                 'attribute'=>'name',
                 'content' => function( $model )
                             {
                                $content = $model->name;
                                if( $model->currentStatus == TaskStatus::TASK_FINISHED )  
                                {
                                    return '<strike>'.$content.'</strike>';
                                }
                                
                                return str_repeat( CHILD_IDENT, $model->level )."<input type = 'checkbox'>&nbsp".$model->name;
                             }
            ],
            'originatorName',
            'responsiblesString',
            [
                'class' => 'yii\grid\DataColumn',
                'content' => function( $model, $key )
                          {
                            return "<div class = 'task-actions-column'"
                            ." id='task-actions-".$model->id."'>"
                            .$this->render('action-field',
                                        [ 'model' => $model,
                                          'key' => $model->id,
                                        ]).'</div>';
                          }

            ],
            //'date',
            //'description',
        ],
    ]);
    ?>
    <?php \yii\widgets\Pjax::end() ?>

</div>
