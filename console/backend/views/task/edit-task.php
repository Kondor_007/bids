<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Organisation */

$this->title = 'Редактирование задачи';
$this->params['breadcrumbs'][] = ['label' => 'Задачи', 'url' => ['view-all']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-edit">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>