<?php 
namespace app\rbac;

use yii\rbac\Rule;
use common\models\User;

class ResponsibleRule extends Rule
{
	public $name = 'isResponsible';

	/**
     * @param string|integer $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {   
    	$id = User::findOne($user)->profileId;
        return isset($params['task']) ? $params['task']->taskResponsible->fKUser->ID == $id : false;
    }
}
?>