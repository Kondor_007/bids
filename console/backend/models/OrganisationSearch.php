<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Organisation;

/**
 * OrganisationSearch represents the model behind the search form about `common\models\Organisation`.
 */
class OrganisationSearch extends Organisation
{
    public $headName;
    public $orgType;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'FK_us_ext', 'FK_org_type', 'FK_adr_legal', 'FK_adr_local', 'FK_adr_post'], 'integer'],
            [[ 'short_name', 'full_name', 'act_on_authority', 'phone', 'email', 'description'], 'safe'],
            [['INN', 'OGRN', 'KPP', 'OKATO'], 'safe' ],
            [['headName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Organisation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            $dataProvider->joinWith( [ 'fKUsExt' ] );
            $dataProvider->joinWith( [ 'fKOrgType' ] );
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'FK_org_type' => $this->FK_org_type,
            'FK_adr_legal' => $this->FK_adr_legal,
            'FK_adr_local' => $this->FK_adr_local,
            'FK_adr_post' => $this->FK_adr_post,
        ]);

        $query->andFilterWhere(['like', 'short_name', $this->short_name])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'INN', $this->INN])
            ->andFilterWhere(['like', 'OGRN', $this->OGRN])
            ->andFilterWhere(['like', 'KPP', $this->KPP])
            ->andFilterWhere(['like', 'OKATO', $this->OKATO])
            ->andFilterWhere(['like', 'act_on_authority', $this->act_on_authority])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'description', $this->description]);

            // Без анонимной функции не пашет
        $query->joinWith(['fKUsExt'=>
            function($q)
            {
                if( $this->headName != null )
                    $q->where('us_ext.LastName LIKE "%'.$this->headName.'%"');
                else
                    return;

            } 
            ]);
        
        $query->joinWith(['fKOrgType'=>
            function($q)
            {
                $q->where('org_type.name LIKE "%'.$this->orgType.'%"');
            } 
            ]);

        return $dataProvider;
    }
}

