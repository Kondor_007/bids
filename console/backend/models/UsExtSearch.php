<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UsExt;

/**
 * UsExtSearch represents the model behind the search form about `common\models\UsExt`.
 */
class UsExtSearch extends UsExt
{
    // Поля связанных таблиц
    // User
    public $username;
    public $email;

    // Jobs
    public $jobName;

    public $orgName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'FK_Users' ], 'integer'],
            [['LastName', 'FirstName', 'PatName', 'Photo', 'newUsername', 'newPassword'], 'safe'],
            [['username', 'email', 'orgName'], 'safe' ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Создает dataProvider при каждом запросе поиска
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search( $params )
    {
        $query = UsExt::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            $query->joinWith(['loginInfo']);
            $query->joinWith(['org']);
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'FK_Users' => $this->FK_Users,
            'FK_org' => $this->FK_org,
        ]);

        $query->andFilterWhere(['like', 'LastName', $this->LastName])
            ->andFilterWhere(['like', 'FirstName', $this->FirstName])
            ->andFilterWhere(['like', 'PatName', $this->PatName])
            ->andFilterWhere(['like', 'Photo', $this->Photo]);

        // Без анонимной функции не пашет
        $query->joinWith(['loginInfo'=>
            function($q)
            {
                $q->where('user.username LIKE "%'.$this->username.'%" AND
                            user.email LIKE "%'.$this->email.'%"');
            } 
            ]);
        
        $query->joinWith(['org'=>
            function($q)
            {
                if( $this->orgName != null )
                    $q->where('organisation.short_name LIKE "%'.$this->orgName.'%"');
                else
                    return;
            } 
            ]); // eagerLoading = true и LEFT JOIN по дефолту
        

        return $dataProvider;
    }
}
