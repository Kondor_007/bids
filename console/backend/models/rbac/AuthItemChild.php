<?php

namespace backend\models\rbac;

use Yii;


/**
 * This is the model class for table "auth_item_child".
 *
 * @property string $parent
 * @property string $child
 *
 * @property AuthItem $parent0
 * @property AuthItem $child0
 */
class AuthItemChild extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_item_child';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent', 'child'], 'required'],
            [['parent', 'child'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parent' => 'Parent',
            'child' => 'Child',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent0()
    {
        return $this->hasOne(AuthItem::className(), ['name' => 'parent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChild0()
    {
        return $this->hasOne(AuthItem::className(), ['name' => 'child']);
    }

    //-- Удаляет записи из таблицы authItemChild где значение 
    //   в поле parent == $parent
    public function removeAllChildren( $parent )
    {
        $childrenItems = $this->findAll( [ 'parent'=>$parent ] );
        foreach ( $childrenItems as $item ) 
        {
            $item->delete();
        }
    }


    //-- Добавляет записи в таблицу AuthItemChild
    //   записывая в поле parent = $parent, child = $child 
    public function addChildren( $parent, $childrenArray )
    {
        foreach ( $childrenArray as $child ) 
        {
            $childItem = new AuthItemChild();

            $childItem->parent = $parent;
            $childItem->child = $child;

            $childItem->save();
        }
    }
}
