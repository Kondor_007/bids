<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PayAccount;

/**
 * PayAccountSearch represents the model behind the search form about `common\models\PayAccount`.
 */
class PayAccountSearch extends PayAccount
{
    public $orgName;
    public $bankName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'FK_org', 'FK_bank'], 'integer'],
            [[ 'orgName', 'bankName', 'account', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PayAccount::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            $query->joinWith(['fKOrg']);
            $query->joinWith(['fKBank']);
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'account', $this->account])
            ->andFilterWhere(['like', 'description', $this->description]);

        $query->joinWith(['fKOrg'=>
            function($q)
            {
                $q->where('organisation.short_name LIKE "%'.$this->orgName.'%"');
            } 
            ]);
        $query->joinWith(['fKBank'=>
            function($q)
            {
                $q->where('bank.name LIKE "%'.$this->bankName.'%"');
            } 
            ]);

        return $dataProvider;
    }
}
