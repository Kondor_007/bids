<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use common\models\UsExt;

/**
 * This is the model class for table "us_ext".
 *
 * @property integer $ID
 * @property integer $FK_Users
 * @property string $LastName
 * @property string $FirstName
 * @property string $PatName
 * @property string $Photo
 */
class SignUpExt extends Model
{
    public $username;
    public $email;
    public $password;

    public $lastName;
    public $firstName;
    public $patName;
    public $userPhone;

    public $photo;

    public function rules()
    {
        return [
            [['username', 'password', 'email', 'lastName', 'firstName', 'patName', 'photo'], 'required'],
            [['lastName', 'firstName', 'patName', 'userPhone'], 'string', 'max' => 1024],
            [['photo'], 'string', 'max' => 4096]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'password' => 'Пароль',
            'email' => 'e-mail',
            'lastName' => 'Фамилия',
            'firstName' => 'Имя',
            'patName' => 'Отчество',
            'userPhone' => 'Телефон',
            'photo' => 'Фото',
        ];
    }

    public function signupext()
    {
        $error = '';
        if( $this->validate() )
        {
            $user = new User();

            $user->username = $this->username;
            $user->setPassword($this->password);
            $user->email = $this->email;
            $user->generateAuthKey();
            
            if( $user->save() )
            {
                
                $userExt = new UsExt();

                $userExt->FK_Users = $user->id;
                $userExt->LastName = $this->lastName;
                $userExt->FirstName = $this->firstName;
                $userExt->PatName = $this->patName;
                $userExt->phone = $this->userPhone;
                $userExt->Photo = $this->photo;

                if ( $userExt->save() )
                {
                    return $user;
                }
                else
                {
                    $error .= 'Can\'t save usExt: ';
                    foreach ($userExt->errors as $key => $value) {
                        $error .= $value[0].'<br>';    
                    };
                }
            }
            else
            {
                $error .= 'Can\'t save user';
            }
        }
        return $error;
    }
}
