<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\UsExt */
/* @var $form ActiveForm */
?>
<div class="UserInfoEdit">

    <?php $form = ActiveForm::begin(); ?>
        <?php $allow = \Yii::$app->user->can( 'lastName' ); 
            if( $allow ): ?>
            <?= $form->field($model, 'LastName')->textInput( ['readonly'=>\Yii::$app->user->can( 'lastName' )] ) ?>
        <?php endif; ?>
        <?php if ( \Yii::$app->user->can( 'firstName' ) ): ?>
            <?= $form->field($model, 'FirstName') ?>
        <?php endif; ?>
        <?php if ( \Yii::$app->user->can( 'patName' ) ): ?>
            <?= $form->field($model, 'PatName') ?>
        <?php endif; ?>

        <?php if ( \Yii::$app->user->can( 'photo' ) ): ?>
            <?= $form->field($model, 'Photo') ?>
        <?php endif; ?>            
    
        <div class="form-group">
            <?= Html::submitButton('Применить   ', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- UserInfoEdit -->
