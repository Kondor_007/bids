<?php

namespace vendor\novik\repbuilder\models;

use yii\db\ActiveRecord;

/**
 * @author Novikov A.S
 *
 * @var $id integer
 * @var $type boolean or model (0) either dataProvider (1)
 * @var $value string report model attribute name
 * @var $name string field name
 * @var $cell string excel cell address, or a column name if type = 1 (dataProvider)
 * @var $FK_template foreign key to ReportTemplate
 * @var $defaultValue string a value that would be 
 *      printed into document if value property was not specified in report model 
 */

class ReportContent extends ActiveRecord
{
	const STATIC_DATA = 0;
	const DYNAMIC_DATA = 1;
	
	public static function tableName()
	{
		return 'report_content';
	}

	public function rules()
	{
		return [
				[['type', 'id'], 'integer'],
				[['name', 'cell'], 'string'],
		];
	}
	
}