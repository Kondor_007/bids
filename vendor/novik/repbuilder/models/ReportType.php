<?php

namespace vendor\novik\repbuilder\models;

use yii\db\ActiveRecord;

/**
 * @author Novikov A.S
 *
 * @property $id integer
 * @property $name string field name
 * @property $alias
 * @property $ext string out file extension  
 */

class ReportType extends ActiveRecord
{
	public static function tableName()
	{
		return 'report_type';
	}

	public function rules()
	{
		return [
				[['id'], 'integer'],
				[['name', 'alias'], 'string'],
		];
	}
	
}