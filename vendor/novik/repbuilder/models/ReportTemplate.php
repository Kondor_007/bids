<?php

namespace vendor\novik\repbuilder\models;

use yii\db\ActiveRecord;

/**
 * @author Novikov A.S
 *
 * @property $id integer
 * @property $type integer
 * @property $file string
 * @property $alias string
 * @property $name string
 * @property $data_start_row integer
 */

class ReportTemplate extends ActiveRecord
{
	public static function tableName()
	{
		return 'report_template';
	}
	
	public function rules()
	{
		return [
			[['type', 'id'], 'integer'],
			[['name', 'alias', 'file'], 'string'],	
		];
	}
	
	/**
	 * 
	 * @return ActiveQuery
	 */
	public function getCellContent()
	{
		return $this->hasMany( ReportContent::className(), ['FK_template' => 'id' ]);
	}
	
	/**
	 * 
	 */
	public function getStaticCellContent()
	{
		return $this->getCellContent()->where(['type' => ReportContent::STATIC_DATA ]);
	}
	
	/**
	 * 
	 */
	public function getDynamicCellContent()
	{
		return $this->getCellContent()->where(['type' => ReportContent::DYNAMIC_DATA ]);
	}
	
	public function getTypeModel()
	{
		return $this->hasOne( ReportType::className(), ['id' => 'type'] );
	}
	

}