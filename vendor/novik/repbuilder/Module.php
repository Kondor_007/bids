<?php

namespace vendor\novik\repbuilder;

class Module extends \yii\base\Module
{
	const MODULE = "repbuilder";

	public $templateController = "repbuilder/report";
	public $downloadAction = 'repbuilder/report/download';
	public $reportDirectory = '/reports';

    public function init()
    {
        parent::init();
        
        if( isset( $this->module ) )
        	$this->templateController = $this->module->id."/".$this->templateController; 
        
        //\Yii::configure($this, require(__DIR__ . '/flex_config.php'));
        //Это не обязательно если у приложения и модуля одна тема
        /*$this->layoutPath = '@app/themes/modern/layouts';
        $this->layout = 'main';*/
    }
}

?>