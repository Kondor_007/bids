<?php

namespace vendor\novik\repbuilder;

use yii\base\Widget;
use yii\base\Model;
use yii\base\InvalidConfigException;
use yii\base\InvalidArgumentException;
use yii\base\InvalidValueException;
use yii\data\DataProviderInterface;

use vendor\novik\repbuilder\models\ReportTemplate;
use vendor\novik\repbuilder\classes\NumToWordConverter;


require_once(\Yii::getAlias('@vendor/phpoffice/phpexcel/Classes/PHPExcel.php'));

/**
 * 
 * @author Novikov A.S
 *
 */

class ReportBuilder extends Widget
{
	// Modifiers
	const MODIFIER_NONE = 0;
	const MODIFIER_HANWDRITING_NUMBER = 1;
	const MODIFIER_HANWDRITING_MONEY = 2;
	/**
	 * @var string name of the template to use 
	 */
	public $templateName;
	
	/**
	 * Data provider contents data.
	 * @var yii\data\DataProviderInterface
	 */
	public $dataProvider;
	
	/**
	 * 
	 * @var vendor\novik\repbuilder\ReportTemplate
	 */
	public $template;
	
	/**
	 * 
	 * @var string
	 */
	public $reportName = 'report';
	
	/**
	 * Report model
	 * @var yii\base\Model
	 */
	public $reportModel;
	
	public $reportPath;
	
	protected $_module;
	
	
	/**
	 * (non-PHPdoc)
	 * @see \yii\base\Object::init()
	 */
	public function init()
	{
		parent::init();
		if( !isset( $this->reportModel ) || !isset( $this->templateName ) || !isset( $this->dataProvider ) )
			throw new InvalidConfigException("The property \$reportModel, \$templateName and \$dataProvider need to be initialized.");
		if( !( $this->reportModel instanceof Model ) || !( $this->dataProvider instanceof DataProviderInterface ) )
			throw new InvalidArgumentException("\$reportModel must be a descedant of yii\base\Model, and \$dataProvider must be an implementation of DataProviderInterface.");
		
		$this->template = ReportTemplate::find()->where(['alias' => $this->templateName])->one();
		
		$this->_module = Config::initModule( Module::classname() );
		
		$this->reportPath = $this->_module->reportDirectory.date('ymdhis').'_'.$this->reportName.$this->template->typeModel->ext;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \yii\base\Widget::run()
	 */
	public function run()
	{
		$this->generateReport();
		
		return $this->renderDownloadButton();
	}
	
	private function generateReport()
	{
		$reader = $this->getReaderObj();
		$excelObj = $reader->load( $this->template->file );
		
		$excelObj->setActiveSheetIndex(0);
		
		// In that order only!!!!
		$this->fillStaticData( $excelObj );
		$this->fillDynamicData($excelObj);
		
		$writer = $this->getWriterObj($excelObj);
		$writer->save( $this->reportPath );
	}
	
	/**
	 * Fills $excelObj by static data from $reportModel
	 * @param $excelObj
	 * @return $excelObj filled
	 */
	private function fillStaticData( $excelObj )
	{
		foreach ( $this->template->staticCellContent as $key => $content )
		{
			$value = '';
			$attr = $content->value;
			if( $this->reportModel->canGetProperty( $attr ) )
				$value = $this->reportModel->$attr;
			else 
				$value = $content->defaultValue;
			
			$value = $this->modify($value, $content->modifier);
			
			if( isset( $content->end_cell ) && ( $content->end_cell !== $content->cell ) ) {
				$excelObj->getActiveSheet()->mergeCells( "{$content->cell}:{$content->end_cell}" );
			}
			$excelObj->getActiveSheet()->setCellValue( $content->cell, $value );
		}
		
	}
	
	private function fillDynamicData( $excelObj )
	{
		$this->dataProvider->prepare();
		
		// Inserting rows after data_start_row is because 
		// we need to get cells style and apply it to cell on the next row
		$excelObj->getActiveSheet()->insertNewRowBefore( $this->template->data_start_row + 1, $this->dataProvider->totalCount );
		$rowCounter = $this->template->data_start_row + 1;
		foreach ( $this->dataProvider->models as $key => $model ) {
			$this->renderRow( $model, $key, $rowCounter, $excelObj);
			$rowCounter++;
		}
		// Removing row after
		$excelObj->getActiveSheet()->removeRow( $this->template->data_start_row );
	}
	
	/**
	 * 
	 * @param Model $model
	 * @param integer $index
	 * @param integer $row number of current row
	 * @param unknown $excelObj
	 */
	private function renderRow( $model, $index, $row, $excelObj )
	{
		foreach ( $this->template->dynamicCellContent as $key => $content )
		{
			$value = "";
			$attr = $content->value;
			if( $model->canGetProperty( $attr ) ) {
				$value = $model->$attr;
			} else {
				if( $attr == 'orderNumber' )
					$value = $index + 1;
				else 
					$value = $content->defaultValue;
			}
			$cellStyle = $excelObj->getActiveSheet()->getStyle("$content->cell".($row-1));
			$excelObj->getActiveSheet()->duplicateStyle( $cellStyle, "{$content->cell}{$row}" );
			if( isset( $content->end_cell ) && ( $content->end_cell !== $content->cell ) ) {
				$excelObj->getActiveSheet()->mergeCells( "{$content->cell}{$row}:{$content->end_cell}{$row}" );
			} 
			
			// Here I appending a blank to value, because othervice large numbers diplaying in exp form 
			$excelObj->getActiveSheet()->getStyle( "$content->cell".$row )->getNumberFormat()->setFormatCode( \PHPExcel_Style_NumberFormat::FORMAT_TEXT );
			$excelObj->getActiveSheet()->setCellValueExplicit( "$content->cell".$row, $value." ", \PHPExcel_Cell_DataType::TYPE_STRING2 );
			
		}
	}
	
	
	
	/**
	 * 
	 * @return PHPExcel_Reader_IReader
	 */
	private function getReaderObj()
	{
		return \PHPExcel_IOFactory::createReader( $this->template->typeModel->alias );
	}
	
	private function getWriterObj( $excelObj )
	{
		return \PHPExcel_IOFactory::createWriter($excelObj, $this->template->typeModel->alias );
	}
	
	private function modify( $value, $modifier )
	{
		switch( $modifier )
		{
			case self::MODIFIER_HANWDRITING_NUMBER:
				return NumToWordConverter::num2strAmount( $value );
			
			case self::MODIFIER_HANWDRITING_MONEY:
				return NumToWordConverter::num2str( $value );
		
			case self::MODIFIER_NONE:
				return $value;
		}
	}
	
	private function renderDownloadButton()
	{
		$downloadUrl = substr( $this->reportPath, strpos( $this->reportPath, \Yii::getAlias('@appBaseUrl') ) );
		return "<a class = 'btn btn-success' href = '{$downloadUrl}'  download>Загрузить( $this->reportName )</a>";
	}
}