<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\modules\storage\models\Provider;

$this->registerJsFile( \Yii::getAlias('@appBaseUrl').'/vendor/novik/importer/assets/plugins/jquery.ajaxupload.js', [ 'depends' => [ 'yii\web\JqueryAsset' ], 
																							'position' => yii\web\View::POS_END ] );

$functionPrefix = str_replace( '-', '_', $template_name ).'_';

$this->registerJs( "
		$('#{$template_name}-submit').click( {$functionPrefix}startImport );


		function {$functionPrefix}startImport( e )
		{
			e.preventDefault();
			$('#{$template_name}-form').ajaxUpload(
			{
				url: 'index.php?r={$importController}/upload',
				beforeSend: function( input ){
					$('body').prepend('<div class=\'overlay\'>');
					$('body').append('</div>');
					$('.overlay').css({
				    'position': 'absolute',
				    'width': $(document).width(), 
				    'height': $(document).height(),
				    'z-index': 99999, 
					}).fadeTo(0, 0.8);
					$('#{$template_name}-status').text('Загрузка...');
					$('#{$template_name}-warning').text('Не закрывайте и не обновляйте данную страницу!');
				},
				success: function ( responseData, statusText )
				{	
					var response = JSON.parse( responseData );
					console.log( response );
					if( response.status == 1 )
					{	
						$('#{$template_name}-status').html('<font color = green>Загружено</font>');
						{$functionPrefix}read();
					}
					else 
					{
						success = 'true';
						$('.overlay').remove();
						$('#{$template_name}-status').html('<font color = green>Ошибка</font>');
						return;
					}
				},

			});
		}
		
		function {$functionPrefix}read()
		{
			$.ajax({
				url: 'index.php?r={$importController}/read',
				type: 'POST',
				dataType: 'json',
				success: function( response ){
					if( response.status == 0 ) {
						$('#{$template_name}-status').html('<font color = blue>Данные загружаются:'+response.process+'</font>&nbsp' );
						{$functionPrefix}read();
					} else if( response.status == 1 ){
						$('#{$template_name}-status').html('<font color = green>Данные загружены</font>&nbsp' );
						{$functionPrefix}importData();
					} else {
						console.log( response );
					}
				}
				
			});
		}

		function {$functionPrefix}importData()
		{
			$.ajax({
				url: 'index.php?r={$importController}/import',
				type: 'POST',
				success: function( responseData, statusText )
				{
					var response = JSON.parse( responseData );
					console.log( response );
					switch( response.status )
					{
						case 0:
							$('.overlay').remove();
							console.log( response.errors );
							$('#{$template_name}-status').html('<font color = green>Импорт завершен</font>&nbsp' );
							$('#{$template_name}-warning').empty();
						break;

						case 1:
							$('#{$template_name}-status').html('<font color = blue>Обрабатывается</font>&nbsp' + response.process );
							//console.log( response.errors );
							{$functionPrefix}importData();
						break;

						case 99:
							$('.overlay').remove();
							$('#{$template_name}-status').html('<font color = red>Ошибка</font>&nbsp' + response.error );
						break;

					}
				}
			});
		}

	", yii\web\View::POS_END );
?>

<form id = '<?= $template_name ?>-form' enctype="multipart/form-data">
	<div class = 'form-group'>
		<input type = hidden name = 'template' value = '<?= $template_name ?>'>
		<input class = 'form-control' style="border: 0" name = "import" type="file" ></input>
		<button class = "btn btn-primary form-control" id = '<?= $template_name ?>-submit'>Импорт</button>
	</div>
	<font size = 6 color = red >
		&nbsp<span id="<?= $template_name ?>-status" ></span>
	</font>
	<br>
	<font size = 4 color = red >
		<span id="<?= $template_name ?>-warning" ></span>
	</font>
</form>

