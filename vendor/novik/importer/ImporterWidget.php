<?php

namespace vendor\novik\importer;

use yii\base\Widget;
use yii\base\InvalidConfigException;
use yii\base\InvalidArgumentException;
use yii\base\InvalidValueException;
use vendor\novik\importer\models\ImportTemplate;
use vendor\novik\importer\components\Importer;
use yii\base\yii\base;



/**
 * 
 * @author Novikov A.S
 *
 * ImporterWidget.
 *
 */

class ImporterWidget extends Widget
{	
	public $templateName;
	
	protected $_module;
	
	/**
	 * (non-PHPdoc)
	 * @see \yii\base\Object::init()
	 */
	public function init()
	{
		parent::init();
		if( !isset( $this->templateName ) )
			throw new InvalidConfigException("The property \$templateName need to be initialized.");
		
		$this->_module = Config::initModule( Module::classname() );
		$template = ImportTemplate::getByAlias( $this->templateName );
		if( $template == null ) {
			throw new InvalidConfigException("Template with alias \"$this->templateName\" does not exist");
		} else {
			$this->isValidImporterClass( $template->importer );
		}
		
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \yii\base\Widget::run()
	 */
	public function run()
	{	
		return $this->renderImportField();
	}
	
	/**
	 * 
	 * @return string
	 */
	private function renderImportField()
	{
		return $this->render( 'import', ['template_name' => $this->templateName,
										 'importController' => $this->_module->importController,
		 ]);
	}
	
	/**
	 * 
	 * @param string $class given importer class
	 * @throws InvalidConfigException
	 */
	private function isValidImporterClass( $class )
	{
		$importer = new $class();
		if( !($importer instanceof Importer) ) {
			throw new InvalidConfigException("Invalid importer class. Importer class must be inherited from class vendor\novik\importer\components\Importer.");
		}
	}
	
}