<?php

namespace vendor\novik\importer\controllers;

use yii\base\Controller;
use yii\helpers\Json;

use vendor\novik\importer\models\ImportTemplate;
use vendor\novik\importer\classes\XlsToCsvConverter;
use vendor\novik\importer\models\ImportTemp;
use yii\web\Session;



class ImportController extends Controller
{
	
	public function actionUpload()
	{
		if ( !\Yii::$app->request->isPost && ( isset($_POST['template'] ) ) ){
			return json_encode( [ 'status' => 99, 'error' => 'POST required, something else taken' ] );
		}
		
		if( !ImportTemplate::exists( $_POST['template'] ) ) {
			return json_encode( [ 'status' => 99, 'error' => 'Template doesn\'t exist' ] ); 
		}
		
		$allowed_filetypes = [ '.xls', '.xlsx', '.txt' ];
		$file_name = $_FILES['import']['name'];
		$destinationPath = \Yii::$app->basePath . '/temp/parsing/';
		$ext = substr($file_name, strpos($file_name,'.'), strlen($file_name)-1);
		$file = "";
		//---- Валидация и перемещение файла в постоянную директорию
		if( !in_array( $ext, $allowed_filetypes ) )
		{
			$file = copy( $_FILES['import']['tmp_name'], $destinationPath.$file_name.$ext );
			return json_encode( ['status' => 99, 'error' => 'unknown extension' ]);
		}
		
		
		$session['import'] = [
				'csv_file' => $this->prepareFile( ImportTemplate::getByAlias($_POST['template'] ), $_FILES['import']['tmp_name'], $destinationPath),
				'template_name' => $_POST['template'], 
				
		];
		
		return Json::encode(['status' => 1]);
	}
	
	public function actionRead()
	{
		$session = new Session();
		$session->open();
		$template = ImportTemplate::getByAlias( $session['import']['template_name'] );
		
		$skipRows = $template->start_row;

		ImportTemp::resetTable();
		
		ImportTemp::createFields( $template->fieldsCount );
		 
		$columns = '('.implode( ',', ImportTemp::getDynamicFields() ).')';
		$delimiter = $template->delimiter;
		
		\Yii::$app->db->createCommand("
				LOAD DATA INFILE '".$session['import']['csv_file']."' INTO TABLE ".ImportTemp::tableName()."
				CHARACTER SET 'utf8'
				FIELDS TERMINATED BY '$delimiter'
				ENCLOSED BY '\"'
				IGNORE $skipRows LINES
				${columns}
		")->execute();
		//unset( $session['import']['csv_file'] );
		return Json::encode(['status' => 1]);
	}
	
	public function actionImport()
	{
		$session = new Session();
		$session->open();
		$importer = ImportTemplate::getByAlias( $session['import']['template_name'] )->importer;
		return ( new $importer([
				'template' => ImportTemplate::getByAlias( $session['import']['template_name'] ), 
		]) )->import();
	}
	
	private function prepareFile( $template, $file, $destination )
	{
		$new_path = "";
		switch( $template->extension )
		{
			case ImportTemplate::EXT_XLS:
				$new_path = copy( $file, $destination.'temp.csv' ); 
				$new_path = str_replace( "\\", "/", XlsToCsvConverter::convert( $file, 0, 0, [], $destination ) );
			break;
			
			case ImportTemplate::EXT_SEMICOL:
				copy( $file, $destination.'temp.csv' );
				$new_path = str_replace( "\\", "/", $destination.'temp.csv' );
			break;
			
			case ImportTemplate::EXT_TAB:
				copy($file, $destination.'temp.csv' );
				$new_path = str_replace( "\\", "/", $destination.'temp.csv' );
			break;
		}
		
		return $new_path;
	}
	
}