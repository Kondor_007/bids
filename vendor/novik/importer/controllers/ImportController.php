<?php

namespace vendor\novik\importer\controllers;

use yii\base\Controller;
use yii\helpers\Json;

use vendor\novik\importer\models\ImportTemplate;
use vendor\novik\importer\classes\XlsToCsvConverter;
use vendor\novik\importer\models\ImportTemp;
use yii\web\Session;
use yii\web\yii\web;



class ImportController extends Controller
{

	const XLS_CHUNK_SIZE = 300;
	
	public function actionUpload()
	{
		$session = new Session();
		$session->open();
		if ( !\Yii::$app->request->isPost && ( isset($_POST['template'] ) ) ){
			return json_encode( [ 'status' => 99, 'error' => 'POST required, something else taken' ] );
		}
		
		if( !ImportTemplate::exists( $_POST['template'] ) ) {
			return json_encode( [ 'status' => 99, 'error' => 'Template doesn\'t exist' ] ); 
		}
		
		$allowed_filetypes = [ '.xls', '.xlsx', '.txt' ];
		$file_name = $_FILES['import']['name'];
		$destinationPath = \Yii::$app->basePath . '/temp/parsing/';
		$ext = substr($file_name, strpos($file_name,'.'), strlen($file_name)-1);
		
		//---- Валидация и перемещение файла в постоянную директорию
		if( !in_array( $ext, $allowed_filetypes ) ) {
			return json_encode( ['status' => 99, 'error' => 'unknown extension' ]);
		}
		if( !move_uploaded_file($_FILES["import"]["tmp_name"],
				$destinationPath.$file_name) ) {
			json_encode(['status' => 99, 'error' => 'Can\'t save file']);
		}
		
		$session['import'] = [
			'path' => $destinationPath,
			'uploaded_file' => $destinationPath.$file_name,
			'template_name' => $_POST['template'], 
		];
		
		return Json::encode(['status' => 1]);
	}
	
	/**
	 * 
	 * @return string
	 */
	public function actionRead()
	{
		$session = new Session();
		$session->open();
		$template = ImportTemplate::getByAlias( $session['import']['template_name'] );
		$skipRows = 0;
		$import_data = $session['import'];
		
		if( !isset( $import_data['row'] ) ) {
			ImportTemp::resetTable();
			$skipRows = $template->start_row;
			ImportTemp::createFields( $template->fieldsCount );
		} else {
			$skipRows = $import_data['row'];
		}

		// Метод prepareFile возвращает либо путь к файлу, либо false, если не удалось прочитать файл
		$file_path = $this->prepareFile( ImportTemplate::getByAlias( $import_data['template_name'] ), $import_data['uploaded_file'], $import_data['path']);
		
		// Если файл удачно считан
		if( $file_path != false ) {
			// Загружаем его в БД
			$columns = '('.implode( ',', ImportTemp::getDynamicFields() ).')';
			$delimiter = $template->delimiter;
			
			\Yii::$app->db->createCommand("
					LOAD DATA INFILE '".$file_path."' INTO TABLE ".ImportTemp::tableName()."
					CHARACTER SET 'utf8'
					FIELDS TERMINATED BY '$delimiter'
					ENCLOSED BY '\"'
					IGNORE $skipRows LINES
					${columns}
			")->execute();
			
			if( isset( $session['import']['row'] ) ) {
				return Json::encode(['status' => 0, 'process' => $session['import']['row'] ]);
			} else {
				return Json::encode(['status' => 1]);
			}
		} else {
			unset( $_SESSION['import']['row'] );
			return Json::encode(['status' => 1]);
		}	
	}
	
	/**
	 * 
	 */
	public function actionImport()
	{
		$session = new Session();
		$session->open();
		$importer = ImportTemplate::getByAlias( $session['import']['template_name'] )->importer;
		return ( new $importer([
				'template' => ImportTemplate::getByAlias( $session['import']['template_name'] ), 
		]) )->import();
	}
	
	/**
	 * 
	 * Подготавливает файл к обработке, возвращает путь к подготовленному файлу
	 * или false, если файл не удалось считать
	 * 
	 * @param ImportTemplate $template
	 * @param string $file full path to file
	 * @param string $destination destination path
	 * @return Ambigous <string, mixed>
	 */
	private function prepareFile( $template, $file, $destination )
	{
		$new_path = "";
		$row;
		
		switch( $template->extension )
		{
			case ImportTemplate::EXT_XLS:
				$new_path = $this->partialConversion($file, $destination, 'Excel5');
			break;
			
			case ImportTemplate::EXT_XLSX:
				$new_path = $this->partialConversion($file, $destination, 'Excel2007');
			break;
			
			case ImportTemplate::EXT_SEMICOL:
				copy($file, $destination.'temp.csv' );
				$new_path = str_replace( "\\", "/", $destination.'temp.csv' );
			break;
			
			case ImportTemplate::EXT_TAB:
				copy($file, $destination.'temp.csv' );
				$new_path = str_replace( "\\", "/", $destination.'temp.csv' );
			break;
		}
		
		return $new_path;
	}
	
	/**
	 * Конвертирование по частям.
	 * Возвращает либо путь к подготовленному файлу, либо false, если больше не обнаружено частей для подготовки
	 * 
	 * @param string $file
	 * @param string $destination
	 * @param string $file_type
	 * @return mixed
	 */
	private function partialConversion( $file, $destination, $file_type )
	{
		if( !isset( $_SESSION['import']['row'] ) )
			$_SESSION['import']['row'] = 0;
			
		$row = $_SESSION['import']['row'];
		$result = str_replace( "\\", "/", XlsToCsvConverter::convert( $file, $row, self::XLS_CHUNK_SIZE, [], $destination, $file_type ) );
		
		$_SESSION['import']['row'] += self::XLS_CHUNK_SIZE;
		
		if( $result == false ) {
			$_SESSION['import']['row'] = 0;
		}
		return $result;
	}
	
}