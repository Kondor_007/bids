<?php

namespace vendor\novik\importer;

class Module extends \yii\base\Module
{
	const MODULE = "importer";

	public $importController = "importer/import";

    public function init()
    {
        parent::init();
        
        if( isset( $this->module ) )
        	$this->importController = $this->module->id."/".$this->importController; 
        
        //\Yii::configure($this, require(__DIR__ . '/import_config.php'));
        //Это не обязательно если у приложения и модуля одна тема
        /*$this->layoutPath = '@app/themes/modern/layouts';
        $this->layout = 'main';*/
    }
}

?>