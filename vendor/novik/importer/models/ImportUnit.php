<?php

namespace vendor\novik\importer\models;

use yii\db\ActiveRecord;

/**
 * 
 * @author Программист
 *
 * @property integer id 
 * @property integer FK_template
 * @property string alias
 * @property integer col_number
 *  
 */

class ImportUnit extends ActiveRecord
{
	public static function tableName()
	{
		return 'import_unit';
	}
	
	public function getTemplate()
	{
		return $this->hasOne( ImportTemplate::className(), ['id' => 'FK_template'] );
	}
}