<?php

namespace vendor\novik\importer\models;

use yii\db\ActiveRecord;

/**
 * 
 * @author Программист
 *
 * @property id integer
 * @property alias string
 * @property string model_class
 * 
 * 
 */

class ImportTemplate extends ActiveRecord
{
	const EXT_XLS 	  = 1;
	const EXT_SEMICOL = 2;
	const EXT_TAB 	  = 3;
	const EXT_XLSX	  = 4;
	
	
	public static function tableName()
	{
		return 'import_template';
	}

	public function getTemplateUnits()
	{
		return $this->hasMany( ImportUnit::className(), ['FK_template' => 'id' ] );
	}
	
	public function getFieldsCount()
	{
		$max = -9999;
		
		foreach( $this->templateUnits as $key => $model ){
			if( $model->col_number > $max )
				$max = $model->col_number;
		}
		
		return $max;
	}
	
	public function getAssocColumns()
	{
		$columns = [];
		foreach ( $this->templateUnits as $key => $model ) {
			$columns[ $model->alias ] = 'col'.$model->col_number;
		}
		return $columns;
	}
	
	public function getDelimiter()
	{
		switch( $this->extension )
		{
			case self::EXT_XLS:
				return ',';
			break;
			
			case self::EXT_XLSX:
				return ',';
			break;
			
			case self::EXT_SEMICOL:
				return ';';
			break;
			
			case self::EXT_TAB:
				return '\t';
			break;
		}
	}
	
	public static function getByAlias( $alias )
	{
		return self::find()->where(['alias' => $alias ])->one(); 
	}
	
	public static function exists( $alias )
	{
		if( self::getByAlias( $alias ) == null ) {
			return false;
		} else {
			return true;
		}
	}
	
}