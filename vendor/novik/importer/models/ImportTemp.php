<?php

namespace vendor\novik\importer\models;

use Yii;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
/**
 * This is the model class for table "temp_exel".
 *
 * @property integer $id
 */
class ImportTemp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'import_temp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
        ];
    }

    public static function createErrorField()
    {   
    	if( !( new self() )->hasAttribute('error') ){
			\Yii::$app->db->createCommand("
		    ALTER TABLE ".self::tableName()." ADD error INT(2) DEFAULT NULL
		    ")->execute();
    	}
    }

    public static function dropErrorField()
    {
    	if( ( new self() )->hasAttribute('error') )
	        \Yii::$app->db->createCommand("
	            ALTER TABLE ".self::tableName()." DROP COLUMN error
	        ")->execute();
    }

    public static function createFields( $amount )
    {
    	for ( $i = 1; $i <= $amount;  $i++ )
    	{
    		\Yii::$app->db->createCommand()
    				  ->addColumn( self::tableName(), 'col'.$i, 'string')
    				  ->execute();
    	}
    }
    
    public static function getDynamicFields()
    {
    	$columnNames = \Yii::$app->db->schema->getTableSchema( self::tableName() )->columnNames;
    	unset( $columnNames[0] );
    	return $columnNames;
    }
    
    public static function dropFields( $amount = 20 )
    {
    	$columns = self::getDynamicFields();
    	foreach( $columns as $key => $column )
    	{
    		\Yii::$app->db->createCommand()
    				->dropColumn( self::tableName(), $column )
    				->execute();
    	}
    }
    
    public static function resetTable( $amount = 20 )
    {
        self::deleteAll();
        self::dropFields();
        self::dropErrorField( $amount );
    }

    public static function markMissing( $serialCol, $mark )
    {
        self::updateAll( [ 'error' => $mark ],
                         "substring( ${serialCol}, 1, 19 ) NOT IN ( SELECT serial FROM storage )" );
    }

    public static function getUnequal( $cols )
    {
        $query = 
                "SELECT tv.name product, st.sim_number phone_number, st.serial serial, 
                        st.activation_date active, tm.col{$cols['activation-date']} AS tempActive, 
                        st.registration_date reg, tm.col{$cols['registration-date']} AS tempReg
                FROM temp_exel tm
                LEFT JOIN storage st ON st.serial = substring(tm.col{$cols['serial']}, 1, 19)
                LEFT JOIN tovar tv ON st.FK_tovar = tv.id
                WHERE tm.error = 2 OR tm.error = 3";

        $dataProvider = new SqlDataProvider([
        'sql' => $query,
                
            ]);

        return $dataProvider;
    }

    public static function getMissing( $cols )
    {
        $count = \Yii::$app->db->createCommand("
                SELECT COUNT(*) FROM ".self::tableName()."
                WHERE error = 1
            ")->queryScalar();

        $query = "SELECT col${cols['product']} product,
                        1 as f, 
                        col${cols['serial']} serial, 
                        col${cols['abon-number']} abon,
                        col${cols['activation-date']} active,
                        col${cols['registration-date']} reg
                FROM temp_exel 
                WHERE error = 1";

        $dataProvider = new SqlDataProvider([
        'sql' => $query,
        'totalCount' => $count,
        'pagination' => [
            'pageSize' => 20,
            ],
        ]);

        return $dataProvider;
    }
}
