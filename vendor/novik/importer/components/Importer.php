<?php

namespace vendor\novik\importer\components;

use yii\base\Component;
use vendor\novik\importer\models\ImportTemplate;
use vendor\novik\importer\models\ImportTemp;
use yii\db\ActiveRecord;


class Importer extends Component
{	
	
	const IMPORT_PORTION = 300;
	
	/**
	 * Псевдоним шаблона 
	 * @var ImportTemplate
	 */
	public $template;
	
	/**
	 * Класс модели в которую производится импорт
	 * @var string
	 */
	public $destinationClass = "";
	
	//============ События ======================
	/**
	 * Вызывается каждый раз перед запуском цикла импорта 
	 * @param array $columns содержит названия полей модели как ключи, 
	 * и названия полей временной таблицы как их значения
	 */
	public function beforeImport( $columns )
	{
	}
	
	/**
	 * Вызывается один раз, после завершения импорта
	 */
	public function afterImport()
	{
	}
	
	/**
	 * Вызывается перед тем как сохранить изменения в модели назначения
	 * @param ActiveRecord $model
	 * @param ImportTemp $temp
	 * @param array $columns 
	 * @return boolean true если изменения модели следует сохранить, в обратном случае false
	 */
	public function onRecordImport( $model, $temp, $columns )
	{
		return $model->validate();
	}
	
	/**
	 * Вызывается каждый раз после успешного импорта одной записи
	 * @param ActiveRecord $model
	 * @param ImportTemp $temp
	 * @param array $columns
	 */
	public function afterRecordImport( $model, $temp, $columns )
	{
	}
	
	/**
	 * Вызывается если метод onRecordImport вернул false
	 * @param unknown $model
	 * @param unknown $temp
	 */
	public function onRecordSkip( $model, $temp )
	{
	}
	
	/**
	 * Вызывается если не удалось сохранить изменения модели назначения 
	 * @param unknown $model
	 * @return mixed
	 */
	public function onRecordFail( $model )
	{
		return print_r( $model->getErrors(), true );
	}
	
	/**
	 * Возвращает условие импорта 
	 * @param array $columns
	 * @return multitype:
	 */
	public function getImportCondition( $columns )
	{
		return [];
	}
	
	/**
	 * Возвращает экземпляр модели, по усмолчанию создает новый
	 * наследники могуь специфицироать в зависимости от бизнес логики
	 * @param unknown $temp
	 * @param unknown $columns
	 * @return unknown
	 */
	public function getModelInstance( $temp, $columns )
	{
		$class = $this->destinationClass;
		return new $class;
	}
	
	/**
	 * Процесс заполнения модели 
	 * @param unknown $model
	 * @param unknown $temp
	 * @param unknown $columns
	 */
	private function populateModel( $model, $temp, $columns )
	{
		foreach ( $columns as $alias => $col ){
			if( $model->hasAttribute( $alias ) )
				$model->$alias = $temp->$col;
		}
	}
	
	/**
	 * Процесс импорта
	 * @return string
	 */
	public function import()
	{	
		
		$errors = "";
		$columns = $this->template->assocColumns;
		$this->beforeImport( $columns );
		$temp = ImportTemp::find()->where( $this->getImportCondition( $columns ) )->all();
		$count = count( $temp );
		
		$limit = ($count <= self::IMPORT_PORTION)?$count:self::IMPORT_PORTION;
		
		if( $count !== 0 )
		{
			for ( $i = 0; $i < $limit; $i++)
			{
				$record = $temp[ $i ];
				$model = $this->getModelInstance( $record, $columns );
				
				$this->populateModel( $model, $record, $columns );
				
				if ( $this->onRecordImport( $model, $record, $columns ) == false ) {
					$this->onRecordSkip( $model, $record ).PHP_EOL;
					continue;
				}
		
				if( !$model->save() ) {
					$errors .= $this->onRecordFail( $model, $record ).PHP_EOL;
				}
				$model->refresh();
				$this->afterRecordImport($model, $record, $columns);
				
				$record->delete();
				unset( $model );
			}
			return json_encode( [ 'status' => 1, 'process' => $count, 'errors' => $errors ] );
		}
		else
		{
			$this->afterImport();
			return json_encode( [ 'status' => 0, 'errors' => $errors ] );
		}
	}
	
}