<?php 

namespace vendor\novik\importer\classes;

require_once( \Yii::getAlias('@vendor/phpoffice/phpexcel/Classes/PHPExcel.php'));

class ChunkReadFilter implements \PHPExcel_Reader_IReadFilter
{
	private $_startRow = 0;
	private $_endRow = 0;

	private $_columns;

	public function setRows($startRow, $chunkSize) {
	    $this->_startRow    = $startRow;
	    $this->_endRow      = $startRow + $chunkSize;
	}

	public function setCols( $colsArray )
	{
		$this->_columns = $colsArray;
	}

	public function readCell($column, $row, $worksheetName = '') {
	    
	    $colCond = true;
	    $rowCond = true;
	    
	    if( !empty( $this->_columns ) ) {
	    	$colCond = in_array( $column, $this->_columns );
	    }

	    if( $this->_endRow !== 0 )
	    {
	    	$rowCond = ($row >= $this->_startRow && $row < $this->_endRow);
	    }
	    
	    return ($colCond && $rowCond);
	}
}

?>