<?php

namespace vendor\novik\importer\classes;

use vendor\novik\importer\classes\ChunkReadFilter;

require_once( \Yii::getAlias('@vendor/phpoffice/phpexcel/Classes/PHPExcel.php') );

class XlsToCsvConverter
{
	private function __construct(){}

	/**
    *   Конвертирование файла XLS в CSV
	*	@return Путь ка сконвертированному файлу  
    */
    public static function convert( $inputFileName, $startRow = 0, $chunkSize = 0, $columns = [], $savePath = '', $fileType = 'Excel2007' )
    {
        $error = '';
        /**  Определяем тип входного файла  **/
        //$inputFileType = \PHPExcel_IOFactory::identify($inputFileName); could take a lot of time
        /**  Создаем Reader нужного типа  **/
        $reader = \PHPExcel_IOFactory::createReader($fileType);

        // Читаем только данные, без форматирования
        $reader->setReadDataOnly(true);
        
        // Создаем объект фильтра 
        $readFilter = new ChunkReadFilter();

        $readFilter->setRows( $startRow, $chunkSize );
        if( !empty( $columns ) )
        	$readFilter->setCols( $columns );

        $reader->setReadFilter( $readFilter );

        $doc = $reader->load( $inputFileName );

        $worksheet = $doc->getActiveSheet();
		
        /**
         * Check if read data is empty
         */
        $isEmpty = true;
        foreach ( $worksheet->getRowIterator() as $row ) {
        	$cellIterator = $row->getCellIterator();
        	$cellIterator->setIterateOnlyExistingCells(false);
        	foreach ( $cellIterator as $cell ) {
        		if ( ( $cell->getValue() !== null ) && ( $cell->getValue() !== ''  ) ) {
        			$isEmpty = false;
        			break;
        		}
        	}
        	if( !$isEmpty ) {
        		break;
        	}
        }
        
        if( $isEmpty ) {
        	return false;
        } else {
        	$writer = \PHPExcel_IOFactory::createWriter( $doc, 'CSV');
        	
        	$resultPath = $savePath. '/temp.csv';
        	
        	//unlink( $inputFileName );
        	
        	$writer->save( $resultPath );
        	
        	return $resultPath;
        }
           
    }
}

?>