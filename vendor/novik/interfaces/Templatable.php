<?php

namespace vendor\novik\interfaces;

interface Templatable
{
	const TEMPLATE_FORM = 1;
	const TEMPLATE_GRID = 2;
	
	function getDisplayedAttributes();
}