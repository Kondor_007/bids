<?php

use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use backend\assets\AppAsset;
use kartik\sidenav\SideNav;
use mdm\admin\components\MenuHelper;


$js = <<<SCRIPT
    $('#menu-toggle').click(function(e)
    {
        $('#wrapper').toggleClass('toggled');
        return false;

    });
SCRIPT;
$this->registerJs($js);
AppAsset::register($this);
/* @var $this \yii\web\View */
/* @var $content string */

mdm\admin\AdminAsset::register($this);
?>


<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <div id = 'wrapper' class="wrap kv-header toggled">
        <?php
       /* NavBar::begin([
            'brandLabel' => false,
            'options' => ['class' => 'navbar-inverse navbar-fixed-top'],
        ]);*/

        NavBar::begin([
        		'brandLabel' => '<span class="glyphicon glyphicon-menu-hamburger"></span>',
        		'brandUrl' => '#menu-toggle',//Yii::$app->homeUrl,
        		'brandOptions' => [
        				'id' => 'menu-toggle',
        		],
        		'options' => [
        				'class' => 'navbar-inverse navbar-fixed-top',
        		],
        ]);
        
        if (!empty($this->params['top-menu']) && isset($this->params['nav-items'])) {
            echo Nav::widget([
                'options' => ['class' => 'nav navbar-nav'],
                'items' => $this->params['nav-items'],
            ]);
        }
        $menuItems = [
        		[
        				'label' => 'Главная', 'url' => ['/site/index'],
        		],
        ];
        
        if (Yii::$app->user->isGuest) {
        	$menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
        } else {
        	$menuItems[] = [
        			'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
        			'url' => ['/site/logout'],
        			'linkOptions' => ['data-method' => 'post']
        	];
        }
        echo Nav::widget([
        		'options' => ['class' => 'navbar-nav navbar-right'],
        		'items' => $menuItems,
        ]);
       /* echo Nav::widget([
            'options' => ['class' => 'nav navbar-nav navbar-right'],
            'items' => [
            		'label'=>'test1',
            		'url'=>'/',
        ],//$this->context->module->navbar,
         ]);*/
        NavBar::end();
        ?>

		<div id = 'sidebar-wrapper' style = 'margin-top:-20px'>
                <?= SideNav::widget([ //Боковое меню
                    'items' => /*
                    [
                    		'items'=>[],
                ],*/
                MenuHelper::getAssignedMenu(Yii::$app->user->id, null, null, true),
                    'options' => ['class' => 'sidebar-nav']])
                ?>
        </div>
        
        
        <div class="container">
            <?= $content ?>
        </div>
</div>
        <footer class="footer">
        <div class="container">
        <p class="pull-left">&copy; IT-INTEGO <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
