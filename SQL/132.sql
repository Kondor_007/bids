/*
Триггеры к таблице входящих звонков calls
*/


/*
Вставка записи звонка.
Тут будем выставлять дату события dt_start
если она не пришла в запросе вставки
*/
DROP TRIGGER IF EXISTS `insert_call`;
CREATE  TRIGGER  `insert_call` AFTER INSERT ON calls
FOR EACH ROW 
   UPDATE calls SET calls.dt_start=IF(ISNULL(NEW.dt_start),CURRENT_TIMESTAMP(),NEW.dt_start) WHERE calls.id=NEW.id;
/*
Обновление записи звонка.
*/
DROP TRIGGER IF EXISTS `update_test2`;
/*CREATE  TRIGGER  `update_test2` AFTER UPDATE ON calls
FOR EACH ROW 
   INSERT INTO test SET test.id=IF(ISNULL(NEW.dt_talk),CURRENT_TIMESTAMP(),OLD.dt_talk);*/
