<?php
use kartik\form\ActiveForm;
use common\models\UsExt;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use backend\controllers\ServiceController;
use common\models\RequestDraft;
/* @var $this yii\web\View */
/* @var $model common\models\Request */
/**
 * var_access
 * 1 - владелец
 * 3 - исполнитель
 * 5 - наблюдатель
 * 
 * */

?>
<h2>Заказ №<?php echo $model->id ?></h2>
<?php 
$reqcost =0;
foreach (RequestDraft::find()->where('FK_request='.$model->id)->all() as $dmod)
{
	//echo $dmod->cost."|";
	$reqcost+=$dmod->cost;
}
//echo $reqcost;
//exit();
$strNeedSaveShow='<div class="btn btn-warning">
Пожалуйста, не забудьте сохранить данные, нажав кнопку "Сохранить заказ"
</div>';
?>
<a href = 'index.php?r=request' class = 'btn btn-primary'>К списку заказов</a>
<?php //if (is_null($model->FK_client))
	   echo $strNeedSaveShow;

?>

  <br>
  
      <br>


     <!--    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOneNC">
                    Новый заказчик
                </a>
            </h4>
        </div>


            <div class="panel-body">     
                <div class="form-group"> -->
<?php Modal::begin([

    'toggleButton' => ['class' => 'btn btn-success',
        'label'=>'Добавить информацию о заказчике'],
    'id' => 'add-client-modal',
    //'options'=>['class'=>" col-md-10"],
]);

//echo 'Say hello...';
?>
                    <div class="">
     				 <?php echo $this->render('_clientcreate').$this->render('_orgcreate')?>

                    </div>
<?php Modal::end(); ?>
<!--  </div>
   </div>
    </div>  -->




  <!--                 <div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOneNO">
                    Новая организация
                </a>
            </h4>
        </div>
                  <div id="collapseOneNO" class="panel-collapse  ">
            <div class="panel-body">
                <div class="form-group"> -->
                  <!--  <div class=" modal-content col-md-10">
     				 <?php // echo $this->render('_orgcreate')?>

                    </div> -->
               <!-- </div>
                 </div>
                  </div>
                  </div>
                 </div> -->

 


 <?php
 //Флаг разрешения ухода со страницы. Если 1, то все ок, можем уйти. 0 - есть несохраненная инфа
 echo HTML::hiddenInput('','1',['id'=>'can_leave']);
 /*
  * Поля для хранения временных данных о материале
  * Поставить скрытый режим Для релиза. 
  * */
 $needHidden=true;
 if ($needHidden){
 echo HTML::hiddenInput('','1',['id'=>'sell_price_material']);
 echo HTML::hiddenInput('','1',['id'=>'width_material']);
 echo HTML::hiddenInput('','1',['id'=>'length_material']);
 }
 else{
 	echo HTML::textInput('','1',['id'=>'sell_price_material']);
 	echo HTML::textInput('','1',['id'=>'width_material']);
 	echo HTML::textInput('','1',['id'=>'length_material']);
 }
 ?>
<?php
$var_access=0;

if ($model->FK_user_owner == \yii::$app->user->identity->id){
    $var_access = 1;
    $form = '_form';
    } /*elseif ($responsible->FK_user == \yii::$app->user->identity->id){
        $var_access = 3;
        $form = '_ispoln';
    }*/ else {
            $var_access = 1;
            $form = '_form';
            //return 'Изините, у Вас нет доступа к этому заказу!';
        }
    /**
 * if (in_array('manager',UsExt::getItemName())){
 *         $var_access = 1;
 *         $form = '_form';
 *         } else {
 *             return 'Изините, у Вас нет доступа к этому заказу!';
 *             }
 */
 
    echo $this->render(
        $form, [
		'model' => $model,
		'draft' => $draft,
		'process' => $process,
        //'responsible'   => $responsible,
		'refs' => $refs,
        'var_access' => $var_access
    ]);
     
?>

