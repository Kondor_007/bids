


<?php

use kartik\builder\Form;
use kartik\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\models\Material;
use common\models\MaterialUnit;
use common\models\ProcessType;
use common\models\MachineType;
use common\models\RequestProcess;



//if ($access == 1) {
	//echo '<h3>Это vork-view:</h3>';
    echo Form::widget([
        'model' => $draft,
        'form' => $form,
        'columns' => 4,

        'attributes' => [
         

            'name' => [
                'label' => 'Назв. Участка',
                'type' => Form::INPUT_TEXT,
            		'options' => ['id' => 'name',
						'readonly' => true,],
            ],

           'type_dev'=>[
                'label'=>'Устройство',
                'widgetClass' => '\kartik\widgets\Select2',
                 'type' => Form::INPUT_TEXT,
               'options' => [
               		'id' => 'type_dev',
               		'readonly' => true,
               		
                  'value'  => MachineType::findOne(ProcessType::findOne($draft->FK_work_type)->fk_machine)->name,
               ],

            ],

            'FK_material'=> [
                'label' => 'Материал',
                'type' => Form::INPUT_TEXT,

                
                'options' => [
                		'readonly' => true,
                    'value' => Material::findOne($draft->FK_material)->name,'readonly' => true,
                ],


            ],
           'FK_work_type'=> [
                'label' => 'Тип обработки',
                'type' => Form::INPUT_TEXT,

               
                'options' => [
                     'id' => 'type_work',
                		'readonly' => true,
                   'value'  => ProcessType::findOne($draft->FK_work_type)->name,                
                ],


            ],
            'unit'=> [
                'label' => 'Ед. изм. материала',
                'type' => Form::INPUT_TEXT, //WIDGET,
            		'options' => ['id' => 'unit',
            				'readonly' => true,
            		'value' => MaterialUnit::findOne(Material::find($draft->FK_material)->one()->FK_unit)->name,'readonly' => true,],
                /*'widgetClass' =>  '\kartik\depdrop\DepDrop',
                'options' => [
                    'readonly' => true,
                     'id'=>'unit', 
                ],*/
            ],

           
          
            'height' => [
                'label' => 'Длина изделия',
                'type' => Form::INPUT_TEXT,
                'options' => ['id' => 'height', 'value' => $draft->height,'readonly' => true,],
            ],

            'width' => [
                'label' => 'Ширина изделия',
                'type' => Form::INPUT_TEXT,
                'options' => ['id' => 'width', 'value' =>  $draft->width,'readonly' => true,],
            ],

            'work_space' => [
                'label' => 'Площадь изделия',
                'type' => Form::INPUT_TEXT,
                'options' => ['id' => 'area', 'value' =>  $draft->work_space,'readonly' => true,],
            ],

            'blank_space' => [
                'label' => 'Свободное поле',
                'type' => Form::INPUT_TEXT,
                'options' => ['id' => 'free_space', 'value' =>  $draft->blank_space,'readonly' => true,],

            ],

           
            'amount' => [
                'label' => 'Количество',
                'type' => Form::INPUT_TEXT,
                'options' => ['id' => 'amount', 
                		'value' => RequestProcess::find()->where('FK_draft='.$draft->id)->one()->amount,
                		'readonly' => true, ],
            ],

           

           /* 'add_button' => [
                'type' => Form::INPUT_RAW,
                'value' => '<div style="text-align: left; margin-top: 20px">' .
                    Html::button('Добавить в заказ', ['class' => 'btn btn-primary', 'id' => 'submit-draft']) . ' ' .
                    '</div>'
            ],*/

        		/*'tempF'=>[
        				'label' => 'Макет (path to file)',
        				'type' => Form::INPUT_FILE,
        				'options' => [
        						'id' => 'draft-file2',
        				],
        		
        		],*/
            'file_path' => [
                'label' => 'Макет',
                'type' => Form::INPUT_TEXT,//FILE,
                'options' => [
                    'id' => 'draft-file',
                		'readonly'=>true,
                		//'hidden'=>true,
                ],
            		

            ],
			
        		
        ],
    ]);
//}
?>



