<?php

use common\components\modalWidgets\AddressModalWidget;
use common\components\modalWidgets\TaskWidget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use common\components\modalWidgets\AddWidget;
use kartik\widgets\FileInput;
use common\models\Organisation;



/* @var $this yii\web\View */
/* @var $model common\models\Organisation */
/* @var $userModel common\models\UsExt */
/* @var $activityModel common\models\OrgActivityField */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs("
		$('#attach-contract-check').removeAttr('checked');
		$('div#contract-block').slideUp();
		$('#attach-contract-check').change(function(e)
		{
			$('div#contract-block').slideToggle();
		});	");

?>

<div  class="organisation-form">

    <?php $form = ActiveForm::begin([
    		'options' =>[
    				'id'=>'org-create',
    				'enctype' => "multipart/form-data",
    		]
    ]);
     $model=new Organisation();?>
    <!-- =============== -->
    <!-- Блок о компании -->
    <!-- =============== -->
      
        <?= $form->field($model, 'short_name')->textInput(['maxlength' => 1024]) ?>

        
    
        <?php 
       
            $userList = ArrayHelper::map( common\models\UsExt::find()->all(), 'ID', 'fullName' );
            $typeList = ArrayHelper::map( common\models\OrgType::find()->all(), 'id', 'name' );
            $headcountList = ArrayHelper::map( common\models\OrgHeadcount::find()->all(), 'id', 'count' );
            $activityList = ArrayHelper::map( common\models\OrgActivityField::find()->all(), 'id', 'name' );
            
            

           


        
        ?>
    
    <div class="form-group">
        <?= Html::a( 'Добавить',[''], ['id' => 'submit-org','class' =>  'btn btn-success form-control' ]);
         //Html::a('Новый заказчик',[''],['id' => 'submit-client','class' => 'btn btn-success form-control']);?>
        
    </div>

    <?php 
        ActiveForm::end(); 

        $userModel = new common\models\UsExt;
        $userModel->scenario = 'createByManager';

        $addUser = AddWidget::begin([ 
                'actionUrl' => 'usext/create-phantom',
                'sourceFieldId' => 'user-head-list',
                'modalId' => 'addUserModal',
                'buttonLabel' => 'Add user',
                'title' => 'Add user',
            ]);     
        $addUser->beginModalForm();
        
        echo $addUser->modalForm->field( $userModel, 'LastName' )->textInput(['maxlength' => 1024]);
        echo $addUser->modalForm->field( $userModel, 'FirstName' )->textInput(['maxlength' => 1024]);
        echo $addUser->modalForm->field( $userModel, 'PatName' )->textInput(['maxlength' => 1024]);

        $addUser->endModalForm();
        AddWidget::end();

        $activityModel = new common\models\OrgActivityField;
        
        $addActivity = AddWidget::begin([
                'actionUrl' => 'organisation/create-activity',
                'sourceFieldId' => 'activity-field-list',
                'modalId' => 'addActivityModal',
                'buttonLabel' => 'Добавить сферу деятельности',
                'title' => 'Добавить сферу деятельности',
            ]);     
        $addActivity->beginModalForm();
        
        echo $addActivity->modalForm->field( $activityModel, 'name' )->textInput(['maxlength' => 1024]);

        $addUser->endModalForm();
        AddWidget::end();
    ?>
</div>
