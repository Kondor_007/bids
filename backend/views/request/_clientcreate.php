<?php
use common\models\UsExt;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
?>
<div class="us-ext-form">

    <?php
      $Umodel=new UsExt();
      
  
      $form = ActiveForm::begin(['id'=>'client-create']); ?>

  <!--
    <?= $form->field($Umodel, 'newUsername')->textInput() ?>

    <?= $form->field($Umodel, 'newEmail')->textInput() ?>
    <?php if( $Umodel->isNewRecord ): ?>
        <?= $form->field( $Umodel, 'newPassword' )->passwordInput() ?>
    <?php endif; ?>    
    --> 
    <?= $form->field($Umodel, 'LastName')->textInput(['maxlength' => 1024]) ?>

    <?= $form->field($Umodel, 'FirstName')->textInput(['maxlength' => 1024]) ?>

    <?= $form->field($Umodel, 'PatName')->textInput(['maxlength' => 1024]) ?>
    <?= $form->field($Umodel, 'phone')->textInput(['maxlength' => 1024]) ?>
    <?php 
    echo  $form->field($Umodel, 'FK_org')->hiddenInput(['maxlength' => 1024, 'label'=>'', 'id'=>'short_name'])
        /*$orgList = ArrayHelper::map( common\models\Organisation::find()->all(), 'id', 'short_name' );

        echo $form->field( $Umodel, 'FK_org' )
                            ->dropDownList(
                            $orgList,           
                            ['id'=>'short_name',
                            'prompt'=>'- Выберите организацию -']
        );*/                       
    ?>
    


   <!-- <div class="form-group">
        <?=  $a=1;//Html::submitButton($Umodel->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $Umodel->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
        
       // Html::a('Новый заказчик',[''],['id' => 'submit-client','class' => 'btn btn-success form-control']);
        
       /* 'add_button' => [
        		'type' => Form::INPUT_RAW,
        		'value' => '<div style="text-align: left; margin-top: 20px">' .
        		Html::button('Добавить в заказ', ['class' => 'btn btn-primary', 'id' => 'submit-draft']) . ' ' .
        		'</div>'
        ],*/
        ?>
    </div> -->

    <?php ActiveForm::end(); 
   ?>

</div>