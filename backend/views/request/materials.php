


<?php


use common\models\Material;
use kartik\builder\Form;
use kartik\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\file\FileInput;


$mmod = new  common\components\widgets\ModalConfirm\ModalConfirm();
//$mmod->OutModalForm();
if ($access == 1) {

	
    echo Form::widget([
        'model' => $draft,
        'form' => $form,
       // 'onchange'=>$formula_area_calc,
        'columns' => 2,

        'attributes' => [
          /*  [
                'label' => 'Устройство обработки',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => '\kartik\widgets\Select2',
                'options' => [ 'options' =>[
                    //'id' => 'device-type',
                    'placeholder' => 'Тип устройства',
                    'multiple' => false,
                ],
                    'data' => []]// $refs['device_types']],
            ],*/

            'name' => [
               // 'label' => 'Назв. Участка',
                'type' => Form::INPUT_TEXT,
            ],

           'type_dev'=>[
                'label'=>'Тип устройства',
                'widgetClass' => '\kartik\widgets\Select2',
                'type' => Form::INPUT_WIDGET,
               'options' => [
                   'options' =>[ 
                   		'id' => 'type_dev', 'placeholder' => 'Тип устройства' ],
                   'data' =>  \yii\helpers\ArrayHelper::map(\common\models\MachineType::find()->all(),'id','name'),
               ],

            ],

            'FK_material'=> [
                'label' => 'Материал',
                'type' => Form::INPUT_WIDGET,

                'widgetClass' =>  '\kartik\depdrop\DepDrop',
                'options' => [

                    'pluginOptions' => [
                        'depends' => ['type_dev'],
                        'url' => Url::to(['/request/get-material']),
                    ],
                ],


            ],
            'FK_work_type'=> [
                'label' => 'Тип обработки',
                'type' => Form::INPUT_WIDGET,

                'widgetClass' =>  '\kartik\depdrop\DepDrop',
                'options' => [
                     'id' => 'type_work',
                    'pluginOptions' => [
                        'depends' => ['type_dev'],
                        'url' => Url::to(['/request/get-work-type']),
                    ],
                  //  'onchange' => $formula_area_calc,
                ],


            ],
            'unit'=> [
                'label' => 'Ед. изм. материала',
                'type' => Form::INPUT_TEXT, //WIDGET,

                'widgetClass' =>  '\kartik\depdrop\DepDrop',
                'options' => [
                    'readonly' => true,
                     'id'=>'unit',
                    /*'pluginOptions' => [
                        'depends' => ['type_work'],
                        'url' => Url::to(['/request/get-work-unit']),
                    ],*/
                ],


            ],

            'process_unit'=> [
                'label' => 'Ед. изм. работ',
                'type' => Form::INPUT_TEXT, //WIDGET,

                //'widgetClass' =>  '\kartik\depdrop\DepDrop',
                'options' => [
                    'readonly' => true,
                    'id'=>'process_unit',
                    /*'pluginOptions' => [
                        'depends' => ['type_work'],
                        'url' => Url::to(['/request/get-work-unit']),
                    ],*/
                ],


            ],
           /* 'FK_quality' => [
                'label' => 'Качество',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => '\kartik\depdrop\DepDrop',
                'options' => [
                    'pluginOptions' => [
                        'depends' => ['material'],
                        'url' => Url::to(['/quality/get-qualities']),
                    ],
                ],
            ],*/
            'height' => [
              //  'label' => 'Длина изделия',
                'type' => Form::INPUT_TEXT,
                'options' => [//'id' => 'height', 
                		'value' => 1,],
            ],

            'width' => [
                //'label' => 'Ширина изделия',
                'type' => Form::INPUT_TEXT,
                'options' => [//'id' => 'width', 
                		'value' => 1,],
            ],

            'work_space' => [
                //'label' => 'Площадь изделия',
                'type' => Form::INPUT_TEXT,
                'options' => [//'id' => 'area', 
                		'value' => 1,// 'readonly'=>true
                ],
            ],

            'blank_space' => [
               // 'label' => 'Свободное поле',
                'type' => Form::INPUT_TEXT,
                'options' => [ 'value' => 0],

            ],

            'price_blank_space' => [
                // 'label' => 'Свободное поле',
                'type' => Form::INPUT_TEXT,
                'options' => [ 'value' => 0],

            ],
            'price' => [
               // 'label' => 'Цена',
                'type' => Form::INPUT_TEXT,//WIDGET,
                //'widgetClass' => 'kartik\money\MaskMoney',
                'options' => [
                		//'id' => 'price', 
                		'value' => 0,],

            ],

            'amount' => [
                //'label' => 'Количество материала на участок в ед. изм.',
                'type' => Form::INPUT_TEXT,
                'options' => [
                		//'id' => 'amount',
                		'value' => 0, ],
            ],

            'cost' => [
                //'label' => 'Стоимость',
                'type' => Form::INPUT_TEXT,//WIDGET,
                'options' => [
                		//'id' => 'cost', 
                		'value' => 0],
                //'widgetClass' => 'kartik\money\MaskMoney',

            ],
            'handle_work_price'  => [
                 'label' => 'Подготовка файла, руб.',
                'type' => Form::INPUT_TEXT,
                'options' => [ 'value' => 0],

            ],

        		'file_path' => [
        				'label' => 'Макет (расположение)',
        				'type' => Form::INPUT_TEXT,//FILE,
        				'options' => [
        						'id' => 'draft-file',
        						//'readonly'=>true,
        						//'hidden'=>true,
        				],
        		
        		
        		],
            'add_button' => [
                'type' => Form::INPUT_RAW,
                'value' => '<div style="text-align: left; margin-top: 20px">' .
                    Html::button('Добавить в заказ', ['class' => 'btn btn-primary', 'id' => 'submit-draft']) . ' ' .
                    '</div>'
            ],

        		/*'tempF'=>[
        				'label' => 'Макет (path to file)',
        				'type' => Form::INPUT_FILE,
        				'options' => [
        						'id' => 'draft-file2',
        				],
        		
        		],*/
           
			
        		
        ],
    ]);
   /* echo $form->field($draft, 'file_path')->widget(FileInput::classname(), [
    		'options' => ['multiple' => false,
    				'id'=>'draft-file2'],
    ]);*/
    /*echo FileInput::widget([
    		'model' => $draft,
    		'attribute' => 'file_path',
    		'options' => ['multiple' => false,
    				'id'=>'draft-file2']
    ]);*/
}
?>

<?php
Pjax::begin(['id' => 'draft-grid-pjax', 'enablePushState' => false]);
echo GridView::widget([
    'dataProvider' => $refs['draftsProvider'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Участок',
            'attribute' => 'name',
        ],
        [
            'label' => 'Материал',

            //'attribute'
            'content'=> function($data) {
                // print_r($data);
                // exit();
                if (!is_null($data)and ($data->FK_material != ''))
                return Material::findOne( $data->FK_material)->name;
                else
                    return "не задано";
            },// substr( $data->FK_material,0,50);}//'material.name',
        ],
        [
            'label' => 'Файл макета',
            'attribute' => 'fileName',
            'value'=> function($data){return substr( $data->file_path,0,50);}
        ],
        [
            'label' => 'Тип обработки',
            'content' => function ($data) {
            if ((!is_null($data)) and ($data->FK_work_type != ''))
                return \common\models\ProcessType::findOne($data->FK_work_type)->name;
            else
                return "не задано";

        },
        ],
        [
            'label' => 'Длина',
            'attribute' => 'height',
        ],
        [
            'label' => 'Ширина',
            'attribute' => 'width',
        ],
        [
            'label' => 'Площадь',
            'attribute' => 'work_space',
        ],
        [
            'label' => 'Количество',
            'attribute' => 'amount',
        ],
        [
            'label' => 'Цена',
            'attribute' => 'price',
        ],
        [
            'label' => 'Стоимость',
            'attribute' => 'cost',
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => $access == 1 ? '{delete}' : '',
            'buttons' => [
                'delete' => function ($url, $model, $key) {
                    return "<a href = 'index.php?r=request/delete-draft&id=$model->id&codereq=$model->FK_request'><span class = 'glyphicon glyphicon-trash'></span></a>";
                }
            ],
        ],
    ],
]);
Pjax::end();
?>

<!--<script src = 'jquery.js'>
<script src = 'fieldupdate.js'>-->