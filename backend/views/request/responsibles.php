<?php

use kartik\builder\Form;
use kartik\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\assets\RequestAsset;



//\backend\controllers\ServiceController::actionAlert(implode($refs[]));
echo Form::widget([
		'model' => $responsible,
		'form' => $form,
		'columns' => 2,
		'attributes' => [
                'type' => [
						'widgetClass' => '\kartik\widgets\Select2',
						'type' => Form::INPUT_WIDGET,
                        'options' => ['options' =>[ 'id' => 'type_isp', 'placeholder' => 'Тип исполнителя' ],
							'data' => [1=>'Индивидуальная',4=>'Групповая']],
                        'label'=>'Тип ответственности'
				],
                
				'FK_user' => [
						'label' => 'Исполнители',
						'type' => Form::INPUT_WIDGET,
						'widgetClass' => '\kartik\depdrop\DepDrop',
						//'options' => ['data' => ['1'=>'1']],
                        
                        'options' => [
                        		'pluginOptions' => [
										'depends' => ['type_isp'],
										'url' => Url::to(['/request/get-responsibles']),
								],
                        ],
                        

				],
                
				
		],
]);


?>

<?php
/*
Pjax::begin(['id' => 'process-grid-pjax', 'enablePushState' => false ]);
echo GridView::widget([
		'dataProvider' => $refs['responsible_list'],
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'type.name',
		],		
]);
Pjax::end();
*/
?>