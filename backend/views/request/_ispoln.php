<a href = 'index.php?r=request' class = 'btn btn-primary'>К списку заказов</a>
<?php

use common\models\CodesStates;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\money\MaskMoney;
use yii\widgets\MaskedInput;
use yii\helpers\Url;
use common\assets\RequestAsset;
use kartik\datecontrol\Module;
use kartik\datecontrol\DateControl;
use backend\controllers\ServiceController;
use common\models\RequestDraft;
use common\models\RequestProcess;
use common\models\Request;
use backend\models\rbac\AuthAssignment;
use common\models\Material;
use common\models\ReqProcessUnit;


RequestAsset::register($this);

function CheckHaveMaterial($id){
	//echo $id;
	
	$reqproc=RequestProcess::find()->where('FK_draft='.$id)->one();//One($id);
    
    			$material=Material::findOne(RequestDraft::findOne($id)->FK_material);
    			//ServiceController::actionAlert( $material->name);
    			/*ServiceController::actionConsoleLog( $material->name);
    			ServiceController::actionAlert( $material->name);
    			exit();*/
    			$MaterialHaveCount=$material->count;
    			
    			
    			//На весь участок (все повторы) нужно материала
    				$MaterialNeed=$reqproc->amount * 
    				RequestDraft::findOne($id)->amount;
			//1.2) имеющегося Материала > нужного ?
			if ($MaterialHaveCount > $MaterialNeed){ return true;}
			else
				return false;
}
//Можем ли мы отрисовать кнопку принятия в работу
function drawButtonAccept($id_, $btToDraw, $err_message="Не хватает материалов!"){
	$waserror=false;
	if (!CheckHaveMaterial($id_))
	{$err_message='Не хватает материалов для взятия в работу';
	$waserror=true;
	}
   
   $reqproc=RequestProcess::find()->where('FK_draft='.$id_)->one();
   if ($reqproc->amount == 0)
   {
   	$err_message='Нет участков для взятия в обработку';
   	$waserror=true;
   }
		
   if (!$waserror)
   	return $btToDraw;
   else
		$res =
		Html::label($err_message,'',[ 'class' => 'btn btn-danger ',]);
		return $res;
}
$form = ActiveForm::begin([ 'id' => 'request-form', 'method' => 'POST', 'type' => ActiveForm::TYPE_VERTICAL ]);
//print_r($model);

/*
echo $form->field($model, 'deadline')->widget(DateControl::classname(), [
    'type'=>DateControl::FORMAT_DATE,
    'ajaxConversion'=>false,
    'options' => [
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]
]);*/

//echo $refs['clients'][$model->FK_client];
//echo $form->field( $model, 'date' )->staticInput();
/*echo '<pre>';
print_r($refs['activeStatusTask']);
echo '</pre>';
*/
$status = is_numeric($model->getStatusName()) ? $model->getStatusName() : 0.0;
echo '<h3>Процент готовности заказа № '.$model->id.': '.number_format($status,2)
//$refs['activeStatusTask']['taskStatusRef']['name']
.'%</h3><br />';
echo Form::widget([
		'model' => $model,
		'form' => $form,
		'columns' => 2,
		'attributes' => [
				'FK_client' => [
						'label' => 'Заказчик',
						'type' => Form::INPUT_WIDGET,
						'widgetClass' => '\kartik\widgets\Select2',
						'options' => [
                            'data' => $refs['clients'],
                            'disabled' => true,
                        ],
                        
                        
				],
				'deadline' => [
						'label' => 'Готовность',
						'type' => Form::INPUT_WIDGET,
                         //'type' => \kartik\widgets\DatePicker::TYPE_COMPONENT_PREPEND,
						'widgetClass' => '\kartik\widgets\DatePicker',
                        //'language'=>'ru',
                        'options' => [
                            'disabled' => true,
                            'data' => date('d.M.Y',strtotime($model->deadline)),
                            'pluginOptions' => [
                                'format' => 'dd.M.yyyy',
                                'todayHighlight' => true,
                                'autoclose'=>true,
                            ]
                            //
                        ],
				],
              /*  'FK_device' => [
						'label' => 'Устройство обработки',
						'type' => Form::INPUT_WIDGET,
						'widgetClass' => '\kartik\widgets\Select2',
						'options' => [ 'options' =>[ 
                                        //'id' => 'device-type', 
                                        'placeholder' => 'Тип устройства',
                                         'multiple' => false,
                                         'disabled' => true,
                                        ], 
                                'data' => $refs['device_types']],
				],
                'draft_cost' => [
						'label' => 'Стоимость работ',
						'type' => Form::INPUT_WIDGET,
						'widgetClass' => '\kartik\money\MaskMoney',
                        'options' => [
                            'disabled' => true,
                        ]
				],
                'FK_payment_type' => [
						'label' => 'Вид оплаты',
						'type' => Form::INPUT_WIDGET,
						'widgetClass' => '\kartik\widgets\Select2',
						'options' => [
                            'data' => $refs['payment_types'],
                            'disabled' => true,
                        ],
				],
				'payed_part' => [
						'label' => 'Частичная оплата',
						'type' => Form::INPUT_WIDGET,
						'widgetClass' => '\kartik\money\MaskMoney',
                        'options' => [
                            'disabled' => true,
                        ]
				],*/

		],
]);
echo $form->field( $model, 'description' )->textArea(['rows' => 6,'readonly' => true]);
/*echo '<h3>Макеты:</h3>';
echo "<div class = 'well'>".$this->render('materials', [
		'form' => $form,
		'draft' => $draft,
		'refs' => $refs,
        'access' => $var_access
])."</div>";*/



//$filter = "FK_request='".Yii::$app->request->get()['id']."'";
/*
 * Целевой SQL:
 * SELECT * FROM request_draft WHERE (request_draft.id IN (SELECT  request_process.FK_draft 
FROM request_process
 WHERE (request_process.FK_request=62) AND (request_process.FK_user=141)));
 * */
/*foreach (RequestProcess::find()
		->where('FK_user='.'141')->where("FK_request='".Yii::$app->request->get()['id']."'")->all()
		as $draftiter);*/
//if ($draft->ty)
$usfilt=" OR (request_process.FK_role IN (''";
foreach (AuthAssignment::find()->where('user_id='.Yii::$app->user->id)->all() as $iter)
	$usfilt.=','."'".$iter->item_name."'";

	$usfilt.="))";
	//AND (request_process.FK_role IN ('','admin','dashboard','w_frezer'))
	ServiceController::actionConsoleLog($usfilt);
	//ServiceController::actionAlert($usfilt);
	//exit();
/*$mas=RequestDraft::find()->where("request_draft.id IN (
		SELECT  request_process.FK_draft 
FROM request_process
 WHERE ((request_process.FK_request=".$model->id.") OR
		(request_process.FK_user=".Yii::$app->user->id."))
		 ".$usfilt.")")->all();
*/

/*SELECT * FROM request_draft WHERE request_draft.id IN 
 (SELECT request_process.FK_draft FROM request_process WHERE ((request_process.FK_request =58) 
  AND ((request_process.FK_user=131) OR (request_process.FK_role IN ('admin','w_frezer')))) )
*/

$mas=RequestDraft::find()->where("request_draft.id IN (
		SELECT  request_process.FK_draft
FROM request_process
 WHERE ((request_process.FK_request=".$model->id.") AND
		((request_process.FK_user=".Yii::$app->user->id.")
		 ".$usfilt.")))")->all();
//((request_process.FK_request =58) OR (request_process.FK_user=131)) AND (request_process.FK_role='w_frezer'));
/*foreach($mas as $iter)//RequestDraft::find()->all() as $iter)
{
	//print_r($iter);
	echo $iter->id. '         ';
	$material=Material::findOne(RequestDraft::findOne($iter->id)->FK_material);
	echo $material->name;
}

exit();*/
/*print_r($mas);
 exit();*/
foreach($mas as $iter)//RequestDraft::find()->all() as $iter)
{
	$titile=$iter->id." ".$iter->name;
	
echo '<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne'.$iter->id.'">
                    '.'Этап '.$iter->name.'
                </a>
            </h4>
        </div>';

//ServiceController::actionAlert($iter->FK_material);
 echo '<div id="collapseOne'.$iter->id.'" class="panel-collapse collapse ">
            <div class="panel-body">
                '.$this->render('workview', [
		'form' => $form,
		'draft' => $iter,//$draft,
		'refs' => $refs,
		'access' => $var_access
]);
  $curproc=RequestProcess::find()->where('FK_draft='.$iter->id)->one();
  $state = $curproc->state;
  //echo $state;
  /*
   if ($iterator->state == 0) $procent+=0; // назначен, но не взят в работу
        	if ($iterator->state == 1) $procent+=10;// взят в работу
        	if ($iterator->state == 2) $procent+=10; // приостановлен
        	if ($iterator->state == 3) $procent+=90; //сдан наконтроль
        	if ($iterator->state == 4) $procent+=100; // контроль прошел (принят)
        	if ($iterator->state == 5) $procent+=0; // Отказ от выполнения
        	*/

	//Возможные действия
  $btAcceptInWork=Html::a('Принять в работу',['request/in-work','id'=> $curproc->id,'newval'=>CodesStates::$AlreadyInWork],['class' => 'btn btn-success form-control']);
  $btPause=Html::a('Приостановить',['request/in-work','id'=> $curproc->id,'newval'=>CodesStates::$Paused],['class' => 'btn btn-success form-control']);
  $btOnCheck=Html::a('Сдать на выдачу',['request/in-work','id'=> $curproc->id,'newval'=>CodesStates::$Vidacha],['class' => 'btn btn-success form-control']);
  $btCancel= Html::a('Отказаться ',['request/in-work','id'=>$curproc->id,'newval'=>CodesStates::$Cancelled],['class' => 'btn btn-danger form-control']);
  $btResume=Html::a('Возобновить',['request/in-work','id'=> $curproc->id,'newval'=>CodesStates::$AlreadyInWork, 'wasInWork'=>true],['class' => 'btn btn-success form-control']);
  
  switch ($state){
  	case 0: //не брался в работу
  		echo drawButtonAccept($iter->id, $btAcceptInWork);
  		//echo $btPause;
  		
  		//echo Html::a('Выполнено',['request/in-work','id'=> $curproc->id,'newval'=>4],['class' => 'btn btn-success form-control']);
  		break;
  	case 1: //в работе
  		echo $btOnCheck;
  		echo $btPause;
  		echo $btCancel;
  		break;
  		case 2:
			//На паузе
  			//echo $btAcceptInWork;
  			echo $btCancel;
  			echo $btResume;
  			break;
  			case 3:
  				echo drawButtonAccept($iter->id, $btAcceptInWork);
  				echo $btCancel;
  				break;
  				case 4:
  					break;
  					case 5: //отказ от выполнения
  						//if (CheckHaveMaterial($iter->id))
  							//echo $btAcceptInWork;
  							echo drawButtonAccept($iter->id, $btAcceptInWork);
  							//else {}
  						//echo $btAcceptInWork;
  						break;
  		/*default:
  		echo Html::a('Приостановить',['request/in-work','id'=> $curproc->id,'newval'=>3],['class' => 'btn btn-warning form-control']);
  		echo Html::a('Отложить ',['request/in-work','id'=>$curproc->id,'newval'=>4],['class' => 'btn btn-danger form-control']);
  		echo Html::a('Сдать на контроль',['request/in-work','id'=> $curproc->id,'newval'=>5],['class' => 'btn btn-success form-control']);*/
  		/*if ($model->FK_user_owner == Yii::$app->user->id)
  			echo "I KNOWYOU";*/
  }

 
 
echo '
                <div class="form-group">
                    <div class="col-md-4">

                        
                    </div>
                </div>

                
                <br/>
                


            </div>
        </div>';
	echo ' </div>
</div>'
;
	
	/*echo'<br>

 <div  id="knopka"> <button type="button" class="btn btn-danger"
		data-toggle="collapse" data-target="#makets'.$iter->id.'">
		'.'Участок '.$iter->name.'
	 </button> </div>
<br>';
		

//echo $myform;
//ServiceController::actionAlert($draft);
echo "<div id='makets".$iter->id."' class='collapse'>
		<h3>$titile</h3>
		<div class = 'well'>".$this->render('workview', [
		'form' => $form,
		'draft' => $iter,//$draft,
		'refs' => $refs,
		'access' => $var_access
]);
		
		if ($refs['activeStatusTask']['FK_status'] == 1){
	echo Html::a('Принять в работу',['request/in-work','id'=> $model->id,'newval'=>2],['class' => 'btn btn-success form-control']);
	//А 6 точно не занята?
	echo Html::a('Выполнено',['request/in-work','id'=> $model->id,'newval'=>6],['class' => 'btn btn-success form-control']);
		}
	else {
		echo Html::a('Приостановить',['request/in-work','id'=> $model->id,'newval'=>3],['class' => 'btn btn-warning form-control']);
		echo Html::a('Отложить ',['request/in-work','id'=> $model->id,'newval'=>4],['class' => 'btn btn-danger form-control']);
		echo Html::a('Сдать на контроль',['request/in-work','id'=> $model->id,'newval'=>5],['class' => 'btn btn-success form-control']);
	};
	echo "</div></div>";*/

}
//exit();
/*echo '<h3>Работы:</h3>';

echo "<div class = 'well'>".$this->render('processes', [
		'form' => $form,
		'process' => $process,
		'refs' => $refs,
        'access' => $var_access
])."</div>";
*/
?>
<div class = 'col-md-6'>
<?php
 /*if ($refs['activeStatusTask']['FK_status'] == 1){
    echo Html::a('Принять в работу',['request/in-work','id'=> $model->id,'newval'=>2],['class' => 'btn btn-success form-control']);}
        else {
        echo Html::a('Приостановить',['request/in-work','id'=> $model->id,'newval'=>3],['class' => 'btn btn-warning form-control']);
        echo Html::a('Отложить',['request/in-work','id'=> $model->id,'newval'=>4],['class' => 'btn btn-danger form-control']); 
        echo Html::a('Сдать на контроль',['request/in-work','id'=> $model->id,'newval'=>5],['class' => 'btn btn-success form-control']);
        }*/
     ?>
</div>
<?= Html::activeHiddenInput($draft, 'FK_request', ['value' => $model->id]); ?>
<?= Html::activeHiddenInput($process, 'FK_request', ['value' => $model->id]); ?>
<?php ActiveForm::end() ?>




