<?php

use common\models\RequestDraft;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\money\MaskMoney;
use yii\widgets\MaskedInput;
use yii\helpers\Url;
use common\assets\RequestAsset;
use kartik\datecontrol\Module;
use kartik\datecontrol\DateControl;


RequestAsset::register($this);

$form = ActiveForm::begin([ 'id' => 'request-form', 'method' => 'POST', 'type' => ActiveForm::TYPE_VERTICAL ]);
//print_r($model);

/*
echo $form->field($model, 'deadline')->widget(DateControl::classname(), [
    'type'=>DateControl::FORMAT_DATE,
    'ajaxConversion'=>false,
    'options' => [
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]
]);*/
//echo $form->field( $model, 'deadline' )->staticInput();
echo  Html::submitButton('Сохранить заказ', ['class' => 'btn btn-primary', 'id'=>'btn_save_request'] );
echo Form::widget([
		'model' => $model,
		'form' => $form,
		'columns' => 2,
		'attributes' => [
				'FK_client' => [
						'label' => 'Заказчик',
						'type' => Form::INPUT_WIDGET,
						'widgetClass' => '\kartik\widgets\Select2',
						'options' => ['data' => $refs['clients']],
                        
				],
				'deadline' => [
						//'label' => 'Готовность',
						'type' => Form::INPUT_WIDGET,
                         //'type' => \kartik\widgets\DatePicker::TYPE_COMPONENT_PREPEND,
						'widgetClass' => '\kartik\widgets\DatePicker',
                        //'language'=>'ru',
                        'options' => [
                            'data' => date('d.M.Y',strtotime($model->deadline)),
                            'pluginOptions' => [
                                'format' => 'dd.M.yyyy',
                                'todayHighlight' => true,
                                'autoclose'=>true,
                            ]
                            //
                        ],
				],
				
                
               /* 'FK_device' => [
						'label' => 'Устройство обработки',
						'type' => Form::INPUT_WIDGET,
						'widgetClass' => '\kartik\widgets\Select2',
						'options' => [ 'options' =>[ 
                                        //'id' => 'device-type', 
                                        'placeholder' => 'Тип устройства',
                                         'multiple' => false,
                                        ], 
                                'data' => $refs['device_types']],
				],*/
                
                /*
				'FK_device' => [
						'label' => 'Номер устройства',
						'type' => Form::INPUT_WIDGET,
						'widgetClass' => '\kartik\depdrop\DepDrop',
						'options' => [
								'pluginOptions' => [
										'depends' => ['device-type'],
										'url' => Url::to(['/machine/get-machines']),
								],
						],
				],
                */
                //исполнитель
                /**
 * 'FK_user' => [
 * 						'label' => 'Исполнитель',
 * 						'type' => Form::INPUT_WIDGET,
 * 						'widgetClass' => '\kartik\widgets\Select2',
 * 						'options' => ['data' => $refs['clients']],
 * 				],
 */
		],
]);
// исполнители
/*echo "<div class = 'well'>".$this->render('responsibles', [
		'form' => $form,
		'responsible' => $responsible,
		'refs' => $refs,
])."</div>"; */


$items=RequestDraft::find('FK_draft='.$model->id)->all();
$req_cost = 0;
foreach($items as $item)
	$req_cost+=$item->cost;
/*print_r($req_cost);
exit();*/
//echo Html::a('Новый заказчик',['us-ext/create-phantom'],['class' => 'btn btn-success form-control']);
echo Form::widget([
	'model' => $model,
	'form' => $form,
	'columns' => 2,
	'attributes' => [
		'FK_payment_type' => [
			'label' => 'Вид оплаты',
			'type' => Form::INPUT_WIDGET,
			'widgetClass' => '\kartik\widgets\Select2',
			'options' => ['data' => $refs['payment_types']],
		],
		'payed_part' => [
			//'label' => 'Частичная оплата',
			'type' => Form::INPUT_WIDGET,
			'widgetClass' => '\kartik\money\MaskMoney',
		],
			
			'description' => [
					//'label' => 'Описание',
					'type' => Form::INPUT_TEXTAREA,
					 'options'=>['rows'=>7]
			],
			'cost' => [
					//'label' => 'Описание',
					//'value'=> $req_cost,
					'type' => Form::INPUT_WIDGET,
					'widgetClass' => MaskMoney::className(),
					//'options'=>['rows'=>7]
			],
	]
]);
/*echo "<div class = 'row'><div class = 'col-md-12'>";
echo $form->field( $model, 'draft_cost' )->widget( MaskMoney::className() );
echo "</div></div>";*/
//echo "<div class = 'col-md-6'>".$form->field( $model, 'cost' )->staticInput()."</div>";



//echo "<div  >".$form->field( $model, 'description' )->textArea(['rows' => 6])."<br></div>";
echo '<h3>Этапы работ:</h3>';

?>

<br>

 <div  id="knopka"> <button type="button" class="btn btn-danger" data-toggle="collapse" data-target="#makets">
		 Добавить этапы работ
	 </button> </div>
<br>
<div id="makets" class="collapse">
<?php

echo "<div class = 'well'>".$this->render('materials', [
		'form' => $form,
		'draft' => $draft,
		'refs' => $refs,
        'access' => $var_access,
])."</div>";
?>
</div>
<?php


?>
<br>
<button type="button" class="btn btn-danger" data-toggle="collapse" data-target="#processes">
Исполнители по этапам
</button>
<br>
<div id="processes" class="collapse">
<?php

echo "<div class = 'well'>".$this->render('processes', [
		'model'=>$model,
		'form' => $form,
		'process' => $process,
		'refs' => $refs,
        'access' => $var_access,
])."</div>";
?>
</div>
<br>
<div class = 'col-md-4'>
<?php
//Активная задача
/* if ($refs['activeStatusTask']['FK_status'] == 5){
    echo Html::a('Закрыть заказ',['request/in-work','id'=> $model->id,'newval'=>6],['class' => 'btn btn-success form-control']);}
        elseif ($refs['activeStatusTask']['FK_status'] <= 1){
           echo  Html::submitButton('Сохранить', ['class' => 'btn btn-primary form-control'] );
        }*/
        echo  Html::submitButton('Сохранить заказ', ['class' => 'btn btn-primary form-control', 'id'=>'btn_save_request'] );
     ?>




</div>

<?= Html::activeHiddenInput($draft, 'FK_request', ['value' => $model->id]); ?>
<?= Html::activeHiddenInput($process, 'FK_request', ['value' => $model->id]); ?>
<?php ActiveForm::end() ?>


