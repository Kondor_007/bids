<?php

use kartik\builder\Form;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\money\MaskMoney;
use yii\widgets\MaskedInput;
use yii\helpers\Url;
use common\assets\RequestAsset;
use kartik\datecontrol\Module;
use kartik\datecontrol\DateControl;
use common\models\Request;
use common\models\RequestDraft;


RequestAsset::register($this);

$form = ActiveForm::begin([ 'id' => 'request-form', 
		'method' => 'POST', 'type' => ActiveForm::TYPE_VERTICAL ]);
//print_r($model);

/*
echo $form->field($model, 'deadline')->widget(DateControl::classname(), [
    'type'=>DateControl::FORMAT_DATE,
    'ajaxConversion'=>false,
    'options' => [
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]
]);*/
//echo $form->field( $model, 'deadline' )->staticInput();

echo Form::widget([
		'model' => $model,
		'form' => $form,
		'columns' => 2,
		'attributes' => [
				'FK_client' => [
						'label' => 'Заказчик',
						'type' => Form::INPUT_WIDGET,
						'widgetClass' => '\kartik\widgets\Select2',
						'options' => ['data' => $refs['clients'],
						'disabled'=>true,
						],
                        
				],
				'deadline' => [
						//'label' => 'Готовность',
						'type' => Form::INPUT_WIDGET,
                         //'type' => \kartik\widgets\DatePicker::TYPE_COMPONENT_PREPEND,
						'widgetClass' => '\kartik\widgets\DatePicker',
                        //'language'=>'ru',
                        'options' => [
                        		'disabled'=>true,
                            'data' => date('d.M.Y',strtotime($model->deadline)),
                            'pluginOptions' => [
                                'format' => 'dd.M.yyyy',
                                'todayHighlight' => true,
                                'autoclose'=>true,
                            ]
                            //
                        ],
				],

		],
]);

echo Form::widget([
	'model' => $model,
	'form' => $form,
	'columns' => 2,
	'attributes' => [
		
			
			'description' => [
					//'label' => 'Описание',
					'type' => Form::INPUT_TEXTAREA,
					 'options'=>['rows'=>7,
					 		'disabled'=>true,
					 ]
			],

	]
]);
/*echo "<div class = 'row'><div class = 'col-md-12'>";
echo $form->field( $model, 'draft_cost' )->widget( MaskMoney::className() );
echo "</div></div>";*/
//echo "<div class = 'col-md-6'>".$form->field( $model, 'cost' )->staticInput()."</div>";



//echo "<div  >".$form->field( $model, 'description' )->textArea(['rows' => 6])."<br></div>";
echo '<h3>Участки работ:</h3>';

?>

<br>

 <div  id="knopka"> <button type="button" class="btn btn-danger" data-toggle="collapse" data-target="#makets">
		 Добавить участки работ
	 </button> </div>
<br>

<?php

echo "<div id='makets' class='collapse'>
		<div class = 'well'>".$this->render('materials', [
		'form' => $form,
		'draft' => $draft,
		'refs' => $refs,
        'access' => $var_access,
])."</div></div>";
?>




<?php  
$sectors=$process->find();
$formS = ActiveForm::begin([ 'id' => 'show_maker',
		'method' => 'POST', 'type' => ActiveForm::TYPE_VERTICAL,
		'attributes'=>[]
]);
ActiveForm::end();
$filter = "FK_request='".Yii::$app->request->get()['id']."'";
foreach(RequestDraft::find()->where($filter)->all() as $iter){
	$titile=$iter->id." ".$iter->name;
echo'<br>

 <div  id="knopka"> <button type="button" class="btn btn-danger" 
		data-toggle="collapse" data-target="#makets'.$iter->id.'">
		'.$titile.'
	 </button> </div>
<br>';

echo "<div id='makets".$iter->id."' class='collapse'>
		<div class = 'well'>".$this->render('materials', [
				'form' => $form,
				'draft' => $draft,
				'refs' => $refs,
				'access' => $var_access,
		])."</div></div>";
};
?>



<br>
<button type="button" class="btn btn-danger" data-toggle="collapse" data-target="#processes">
Исполнители по участкам
</button>
<br>
<div id="processes" class="collapse">
<?php

echo "<div class = 'well'>".$this->render('processes', [
		'form' => $form,
		'process' => $process,
		'refs' => $refs,
        'access' => $var_access,
])."</div>";
?>
</div>
<br>
<div class = 'col-md-4'>
<?php
//Активная задача
 if ($refs['activeStatusTask']['FK_status'] == 5){
    echo Html::a('Закрыть заказ',['request/in-work','id'=> $model->id,'newval'=>6],['class' => 'btn btn-success form-control']);}
        elseif ($refs['activeStatusTask']['FK_status'] <= 1){
           echo  Html::submitButton('Сохранить', ['class' => 'btn btn-primary form-control'] );
        }
     ?>




</div>

<?= Html::activeHiddenInput($draft, 'FK_request', ['value' => $model->id]); ?>
<?= Html::activeHiddenInput($process, 'FK_request', ['value' => $model->id]); ?>
<?php ActiveForm::end() ?>


