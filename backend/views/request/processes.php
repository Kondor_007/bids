<?php

use kartik\builder\Form;
use kartik\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use backend\controllers\ServiceController;
use common\models\RequestDraft;
use common\models\RequestProcessTypeMy;
use common\models\RequestProcess;
use yii\web\ServerErrorHttpException;
use common\models\User;
use backend\models\rbac\AuthItem;
// echo $model->id;

if ($access == 1) {
	//print_r(Yii::$app->request->post());
	//ServiceController::actionAlert(Yii::$app->request->get()['id']);
    Pjax::begin(['id' => 'form-process-grid-pjax', 'enablePushState' => false]);
    
   // if (isset(Yii::$app->request->get()['id']))
    	//FK_request=userID AND (SELECT )
/*	$filter =  "request_draft.id IN (SELECT  request_process.FK_draft
				FROM request_process 
  WHERE (request_process.FK_request=".Yii::$app->request->get()['id'].") AND
		(request_process.FK_user=".'0'."))"; */
  /*  $filter="FK_request='".Yii::$app->request->get()['id']."'";
	else
		$filter = "id=1";*/
		
	if (isset( $model->id))//['id']))
	$dat=\yii\helpers\ArrayHelper::map(
                    		RequestDraft::find()->leftJoin(RequestProcess::tableName(),
                    				RequestDraft::tableName().'.id='.RequestProcess::tableName().'.FK_draft')
                    		->where(RequestDraft::tableName().'.FK_request='.
                    				//Yii::$app->request->get()['id']
                    			 $model->id
                    				.' AND ISNULL('.
                    				RequestProcess::tableName().'.FK_user)')
                    		->all(),'id','name');
	else
	{ $dat=[1=>$process->FK_request];}/*\yii\helpers\ArrayHelper::map(
				RequestDraft::find()
				->all(),'id','name');*/
	
    echo Form::widget([
        'model' => $process,
        'form' => $form,
        'columns' => 3,
        'attributes' => [
            'FK_draft' => [
                'label' => 'Этап',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => '\kartik\widgets\Select2',
                'options' => [
                		/*
                		 * В список участков формы "Исполнители по участкам" 
                		 * добавляем не все участки,  а только те, 
                		 * для которых еще не назначен исполнитель.
						 TODO: см SQL-request
						 FROM (request_draft LEFT JOIN request_process ON request_draft.id=request_process.FK_draft) 
WHERE (request_draft.FK_request=63) AND ISNULL(request_process.FK_user );
                		 * */
                    'data' => $dat/*function(){
                    	ServiceController::actionAlert();
                    	if (isset( $_GET['id']))
                    	
                    	return \yii\helpers\ArrayHelper::map(
                    		RequestDraft::find()->leftJoin(RequestProcess::tableName(),
                    				RequestDraft::tableName().'.id='.RequestProcess::tableName().'.FK_draft')
                    		->where(RequestDraft::tableName().'.FK_request='.Yii::$app->request->get()['id'].' AND ISNULL('.
                    				RequestProcess::tableName().'.FK_user)')
                    		->all(),'id','name');
                    	else
                    		return \yii\helpers\ArrayHelper::map(
                    				RequestDraft::find()
                    				->all(),'id','name');
                    }*/
                		,
                    //$refs['draft_list'],
                    'id'=>'FK_draft',
                ],
            ],

            'FK_process_type' => [
                'label' => 'Тип обработки',
                'type' => Form::INPUT_TEXT,//WIDGET,
                //'widgetClass' =>  '\kartik\depdrop\DepDrop',
                //'widgetClass' => '\kartik\widgets\Select2',
                'options' => [
                    'readonly' => true,
                    'id'=>'draft_process_type',
                    //'value'=>'!!!',
                    /*'pluginOptions' => [
                        'depends' => ['FK_draft'],
                        'url' => \yii\helpers\Url::to(['/request/get-work-name']),
                    ],*/
                    //'data' => $refs['process_types']
                ],
            ],


            //===============================================================
            /*'type_dev'=>[
                'label'=>'Тип устройства',
                'widgetClass' => '\kartik\widgets\Select2',
                'type' => Form::INPUT_WIDGET,
                'options' => ['options' =>[ 'id' => 'type_dev', 'placeholder' => 'Тип устройства' ],
                    'data' =>  \yii\helpers\ArrayHelper::map(\common\models\MachineType::find()->all(),'id','name'),
                ],

            ],

            'FK_material'=> [
                'label' => 'Материалы',
                'type' => Form::INPUT_WIDGET,

                'widgetClass' =>  '\kartik\depdrop\DepDrop',
                'options' => [

                    'pluginOptions' => [
                        'depends' => ['type_dev'],
                        'url' => Url::to(['/request/get-material']),
                    ],
                ],


            ],*/
            //===============================================================
            'amount' => [
                'label' => 'Количество повторений этапа',
            		'type' => Form::INPUT_TEXT,
            		'options' => [
            				//'readonly' => true,
            				//'id'=>'amount',
            				'value'=>'1',
            				//'value'=>'!!!',
            				/*'pluginOptions' => [
            				 'depends' => ['FK_draft'],
            						'url' => \yii\helpers\Url::to(['/request/get-work-name']),
            				],*/
            				//'data' => $refs['process_types']
            		],
                /*'type' => Form::INPUT_WIDGET,
                'widgetClass' => '\kartik\touchspin\TouchSpin',
                'widgetOptions' => [
                   // 'initval' => 3.00,
                    'step' => 1,
                ],
                'pluginOptions' => [
                    'initval' => 3,
                ],
                'options' =>['readonly' => true,
                		'value' => 10,
                		//'initval' => 3,
                   // 'placeholder'=>'Кол-во',
                ],*/
                // kartik\touchspin\TouchSpin::
            ],
        		
        		'deadline'=>[
        				'label' => 'Дата выполнения',
        				'type' => Form::INPUT_WIDGET,
        				//'type' => \kartik\widgets\DatePicker::TYPE_COMPONENT_PREPEND,
        				'widgetClass' => '\kartik\widgets\DatePicker',
        				//'language'=>'ru',
        				'options' => [
        						'data' => date('d.M.Y',strtotime($process->deadline)),
        						'pluginOptions' => [
        								'format' => 'yyyy-m-dd',
        								'todayHighlight' => true,
        								'autoclose'=>true,
        						]
        						//
        				],
        		],
           /* 'price' => [
                'label' => 'Цена',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => 'kartik\money\MaskMoney',
            ],

            'cost' => [
                'label' => 'Стоимость',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => 'kartik\money\MaskMoney',
				'value' => '8888',
            ],*/

            'type_resp' => [
                'widgetClass' => '\kartik\widgets\Select2',
                'type' => Form::INPUT_WIDGET,
                'options' => [
                    'id' => 'type_resp', 
                    'options' =>[ 'id' => 'type_resp', 'placeholder' => 'Тип исполнителя' ],
                    'data' => [1=>'Индивидуальная',4=>'Групповая']],
                'label'=>'Тип ответственности'
            ],


            'FK_user' => [
                'label' => 'Исполнители',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => '\kartik\depdrop\DepDrop',
                //'options' => ['data' => ['1'=>'1']],

                'options' => [
                    'pluginOptions' => [
                        'depends' => ['type_resp'],
                        'url' => \yii\helpers\Url::to(['/request/get-responsibles']),
                    ],
                ],


            ],
            'add_button' => [
                'type' => Form::INPUT_RAW,
                'value' => "<div class = 'form-group'>" . Html::button('Добавить в заказ', ['class' => 'btn btn-primary', 'id' => 'submit-process']) . "</div>",
            ]
        ],
    ]);
    Pjax::end();
}



?>


<?php
Pjax::begin(['id' => 'process-grid-pjax', 'enablePushState' => false]);
echo GridView::widget([
    'dataProvider' => $refs['processesProvider'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        ['label' => 'Макет', 'attribute' => 'draftname'],
        ['attribute' => 'type.name', 'label' => 'Способ обработки'],
        ['attribute' => 'amount', ],
    		['attribute'=>'deadline'],
    		['label' => 'Исполнитель',
    				//'attribute'=>'FK_user'
    				'content'=>function ($data) {
               
    				if ($data->type_resp==1){
    				if (!is_null($data->FK_user)){
    					if ($data->FK_user>0)
                    return User::findOne($data->FK_user)->getFullName();
    					else
    						return "не задан";}
    				}
    				else
    					if ($data->type_resp==4){
    						$out = AuthItem::find()
    								->where("type = 1 and name like 'w_%'")->one()->description;
    								//->andWhere("us_ext.FK_users <> ".\Yii::$app->user->identity->id)
    								//->all(),'name','description');
    						return $out;// "не задано ".($data->FK_role);
    				}
    						

            }
    				
],
    		//['attribute'=>'type_resp'],
       /* ['attribute' => 'price', 'label' => 'Цена'],
        ['attribute' => 'amount', 'label' => 'Количество'],
        ['attribute' => 'cost', 'label' => 'Стоимость'],*/
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => $access == 1 ? '{delete} ' :'',// '{view}',
            'buttons' => [
                'delete' => function ($url, $model, $key) {
                    return "<a href='index.php?r=request/delete-process&id=$model->id&reqid=$model->FK_request'><span class='glyphicon glyphicon-trash' confirm-data='Вы уверены что хотите удалить?'></span></a>";
                },
               /* 'view' => function ($url, $model, $key) {
                    return "<a href='index.php?r=request/download-maket&id=$model->FK_draft'><span class='glyphicon glyphicon-download' ></span></a>";
                }*/
            ],
        ],
    ],
]);
Pjax::end();
?>