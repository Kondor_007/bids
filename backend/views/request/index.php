<?php
use kartik\grid\GridView;
use common\models\Request;
use common\models\RequestProcess;
use common\models\User;
use backend\models\rbac\AuthAssignment;
use backend\controllers\ServiceController;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\RequestSearch */

    if (isset($otvet)){
        ?>
    <div class="alert alert-danger fade in">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <strong><?php echo $otvet; ?></strong>
    </div>        
        
        <?php
    }
    $this->title = 'Заказы';    
    ?>


<h2><?= $this->title ?></h2>
<div class = 'form-group'>
<a href = 'index.php?r=request/create' class = 'btn btn-primary'>Новый заказ</a>
<a href = 'index.php?r=sprav' class = 'btn btn-primary'>Cправочники</a>

</div>


<?php
/*
echo '<pre>';
print_r($dataProvider);
echo '</pre>';
exit;
*/
$query=Request::find()->all();
//if (User::findOne( Yii::$app->user->id)=="141")
//TODO Фиксануть фильтр по групповой ответственности

if (!is_null(AuthAssignment::find()->where('user_id='.Yii::$app->user->id)))
{
	$usfilt=" OR request_process.FK_role IN (''";
	foreach (AuthAssignment::find()->where('user_id='.Yii::$app->user->id)->all() as $iter)
	$usfilt.=','."'".$iter->item_name."'";
	
	$usfilt.=")";
	ServiceController::actionConsoleLog($usfilt);
}

//Yii::$app->user
else $usfilt="";
$query=Request::find()->distinct(Request::tableName().'.id')->leftJoin(RequestProcess::tableName(),
    		Request::tableName().'.id='.RequestProcess::tableName().'.FK_request')
				->where(RequestProcess::tableName().'.FK_user='.Yii::$app->user->id.
						' OR '.Request::tableName().'.FK_user_owner='.Yii::$app->user->id
						.$usfilt
						)->orderBy('id DESC');

/*print_r ($query);
exit();*/
echo GridView::widget([
		'dataProvider' => new \yii\data\ActiveDataProvider([
/*
 * SELECT * FROM request  WHERE request.id = 
 * (SELECT DISTINCT request.id  FROM 
 * (request LEFT JOIN request_process ON request.id=request_process.FK_request)
 *  WHERE request_process.FK_user=141);
 * */				
	/*Из всех заказов выберем только те, в которых участвует данный пользоветаль
	 * 
	 * */
    'query' => $query
				//Request::find()
				/*Request::find()->leftJoin(RequestProcess::tableName(),
    		RequestProcess::tableName().'.id=FK_request')*/ /*Request::find()
				->where(Request::tableName().'.id='
						//Тут подзапрос, отыскивающий id шники всех нужных заказов
						Request::find()->distinct->...->
						)
				->where(Request::tableName().'.id>=40')/*->leftJoin(RequestProcess::tableName(),
    		RequestProcess::tableName().'.id=FK_request'
    		)->where(Request::tableName().'.id>=40')*/,//->filterWhere(['like','id','4']),
    'pagination' => [
        'pageSize' => 30,
    ],
]),//$dataProvider,
		//'filterModel' => $searchModel,
		'columns' => [
            'id',
            'clientName',
			//'deviceName',
            'userownerName' ,
				'description',
           // 'task.name',
            //'currentStatus',
          //  'statusName',
				[
				'header' => 'Уровень готовности, %',
				'class' => 'yii\grid\DataColumn',
				'content' =>  function ($data) {
				//if (!is_null($data))
					if (is_numeric(Request::findOne($data->id)->getStatusName()))
						return number_format(Request::findOne($data->id)->getStatusName(),2);
					else
						return "0";
				
				}],
                
            //'taskStatus.FK_status',
            //'taskStatus.statusTypeName',
            //'taskStatus.is_top',
            //'taskResponsible.Name',
            'deadline',	
            //'name',

		
        ['class' => 'yii\grid\ActionColumn',
             
            'template' => $var_access == 1 ? '{view} {update} {delete}': '{view} {edit} ',
            'controller'=>'request',
        		'buttons' => [
        				'view' => function ($url, $model, $key) {
							if ($model->getIsOwner( Yii::$app->user->id))
        				return yii\helpers\Html::a("<span class = 'glyphicon glyphicon-plus'></span>",
        						"index.php?r=request/view&id=".$model->id."",["title"=>"Для постановщика"]);
        				},
        		'edit' => function ($url, $model, $key) {
					if ($model->getIsWorker( Yii::$app->user->id))
        		return yii\helpers\Html::a("<span class = 'glyphicon glyphicon-edit'></span>",
        				"index.php?r=request/view&id=".$model->id."&nocreate=true",["title"=>"Для исполнителя"]);
        		},
				],
            ],
        ],
        
]);

?>
