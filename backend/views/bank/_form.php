<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Bank */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bank-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 1024]) ?>

    <?= $form->field($model, 'BIK')->textInput(['maxlength' => 1024]) ?>

    <?= $form->field($model, 'cor_account')->textInput(['maxlength' => 1024]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => 4096]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => 4096]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
