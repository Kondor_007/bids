<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SearchMaterial */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Materials';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]);
    // $dp=\common\models\SearchMaterial()::$dataProvider;
    ?>

    <p>
        <?= Html::a('Create Material', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'FK_unit',
            'issue_limit',
            'buy_price',
            // 'sell_price',
            // 'FK_machine_type',
            // 'is_del',
            // 'count',
            // 'prop_name',
            // 'prop_value',
            // 'color',
            // 'dencity',
            // 'thickness',
            // 'width',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
