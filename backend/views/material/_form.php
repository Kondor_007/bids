<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Material */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="material-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 512]) ?>

    <?= $form->field($model, 'FK_unit')->textInput() ?>

    <?= $form->field($model, 'issue_limit')->textInput() ?>

    <?= $form->field($model, 'buy_price')->textInput() ?>

    <?= $form->field($model, 'sell_price')->textInput() ?>

    <?= $form->field($model, 'FK_machine_type')->textInput() ?>

    <?= $form->field($model, 'is_del')->textInput() ?>

    <?= $form->field($model, 'count')->textInput() ?>

    <?= $form->field($model, 'prop_name')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'prop_value')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'color')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'dencity')->textInput() ?>

    <?= $form->field($model, 'thickness')->textInput() ?>

    <?= $form->field($model, 'width')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
