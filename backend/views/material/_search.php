<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SearchMaterial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="material-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>
    <!--<?= $form->field($model, 'fullname') ?>

    <?= $form->field($model, 'FK_unit') ?>

    <?= $form->field($model, 'issue_limit') ?>

    <?= $form->field($model, 'buy_price') ?> -->

    <?php // echo $form->field($model, 'sell_price') ?>

    <?php // echo $form->field($model, 'FK_machine_type') ?>

    <?php // echo $form->field($model, 'is_del') ?>

    <?php // echo $form->field($model, 'count') ?>

    <?php // echo $form->field($model, 'prop_name') ?>

    <?php // echo $form->field($model, 'prop_value') ?>

    <?php // echo $form->field($model, 'color') ?>

    <?php // echo $form->field($model, 'dencity') ?>

    <?php // echo $form->field($model, 'thickness') ?>

    <?php // echo $form->field($model, 'width') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
