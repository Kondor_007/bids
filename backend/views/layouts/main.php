<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use kartik\widgets\SideNav;
use mdm\admin\components\MenuHelper;

$js = <<<SCRIPT
    $('#menu-toggle').click(function(e)
    {
        $('#wrapper').toggleClass('toggled');
        return false;
		
    });
SCRIPT;
$this->registerJs($js);
AppAsset::register($this);
/* @var $this \yii\web\View */
/* @var $content string */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div id = 'wrapper' class="wrap kv-header toggled">
        <?php
            NavBar::begin([
                'brandLabel' => '<span class="glyphicon glyphicon-menu-hamburger"></span>',
                'brandUrl' => '#menu-toggle',//Yii::$app->homeUrl,
                'brandOptions' => [
                    'id' => 'menu-toggle',
                ],
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);


            $menuItems = [
                [
                    'label' => 'Главная', 'url' => ['/site/index'],
                ],
            ];

            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
            } else {
                $menuItems[] = [
                    'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);

            NavBar::end();
        ?>
        

        <div id = 'sidebar-wrapper' style = 'margin-top:30px'>
                <?= SideNav::widget([
                    'items' => /*
                    [
                    		'items'=>[],
                ],*/
                MenuHelper::getAssignedMenu(Yii::$app->user->id, null, null, true),
                    'options' => ['class' => 'sidebar-nav']])
                ?>
        </div>
  
        <div class="container" id = '#page-content-wrapper'>
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
</div>
    <footer class="footer">
        <div class="container">
        <p class="pull-left">&copy; IT-INTEGO <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
