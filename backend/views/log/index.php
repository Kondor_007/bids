<?php

use common\models\UsExt;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'who',
            [
                'header' => 'Имя',
                //'value' => \common\models\ReqProcessUnit::findOne('1')->name,
                'content' => function ($data) {

                    if (!is_null($data)) // and ($data->!= ''))
                    {
                        $md=UsExt::find()->where(["FK_Users"=>$data->who])->one();//One($data->who);
                       // $data->getUserInfo()->username;
                        if (!is_null($md))
                        return $md->username."(".$md->getShortName().")" ;
                        else
                            return $data->who;
                    }
                    else
                        return "";

                }],
            'place',
            'what:ntext',
            'date_time',
            'new_value',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
