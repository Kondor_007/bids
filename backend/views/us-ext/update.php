<?php

use common\models\UsExt;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UsExt */

$this->title = 'Обновление профиля пользователя: ' . ' ' . UsExt::findOne($model->ID)->getFullName();
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Сохранить';
?>
<div class="us-ext-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
