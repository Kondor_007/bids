<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\UsExt */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="us-ext-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'newUsername')->textInput() ?>

    <?= $form->field($model, 'newEmail')->textInput() ?>
    <?php if( $model->isNewRecord ): ?>
        <?= $form->field( $model, 'newPassword' )->passwordInput() ?>
    <?php endif; ?>    
    <?= $form->field($model, 'LastName')->textInput(['maxlength' => 1024]) ?>

    <?= $form->field($model, 'FirstName')->textInput(['maxlength' => 1024]) ?>

    <?= $form->field($model, 'PatName')->textInput(['maxlength' => 1024]) ?>
    <?= $form->field($model, 'phone')->textInput(['maxlength' => 1024]) ?>

    <?php 
        $orgList = ArrayHelper::map( common\models\Organisation::find()->all(), 'id', 'short_name' );

        echo $form->field( $model, 'FK_org' )
                            ->dropDownList(
                            $orgList,           
                            ['id'=>'short_name',
                            'prompt'=>'- Выберите организацию -']
        );                       
    ?>
    
    <?= $form->field($model, 'Photo')->textInput(['maxlength' => 4096]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
