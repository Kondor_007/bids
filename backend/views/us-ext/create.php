<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UsExt */

$this->title = 'Create Us Ext';
$this->params['breadcrumbs'][] = ['label' => 'Us Exts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="us-ext-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
