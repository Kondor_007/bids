<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use common\models\UsExt;
use yii\helpers\ArrayHelper;
use backend\controllers\ServiceController;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UsExtSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    Новый пользователь
                </a>
            </h4>
        </div>



        <div id="collapseOne" class="panel-collapse <?php echo isset($model->id) ? '' : 'collapse '; ?> ">
            <div class="panel-body">
            <div class="us-ext-form">
                <?php
                $form = ActiveForm::begin(['id' => 'usext-form', 'method' => 'POST',
                    'type' => ActiveForm::TYPE_VERTICAL]);
               

				$flagNeedBlockEdits=false;
								
				$model = new UsExt();
				$model->load($_POST);
				//ServiceController::actionAlert(implode($_POST));
               /* echo Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' => 3,
                    'attributes' => [
                       
                        'newUsername' => [
                            'label' => 'Логин',
                            'type' => Form::INPUT_TEXT,
                            'options' => ['id' => 'login', 
                            		]
                       ],
                    		'newEmail' => [
                    				'label' => 'Email',
                    				'type' => Form::INPUT_TEXT,
                    				'options' => ['id' => 'email',
                    				]
                    		],
                    		'newPassword' => [
                    				'label' => 'Пароль',
                    				'type' => Form::INPUT_TEXT,
                    				'options' => ['id' => 'pass',
                    				]
                    		],
                    		'FirstName' => [
                    				'label' => 'Имя ',
                    				'type' => Form::INPUT_TEXT,
                    				'options' => ['id' => 'name',
                    						]
                    		],
                    		'LastName' => [
                    				'label' => 'Отчество ',
                    				'type' => Form::INPUT_TEXT,
                    				'options' => ['id' => 'lastname',
                    				]
                    		],
                    		'PatName' => [
                    				'label' => 'Фамилия Польз.',
                    				'type' => Form::INPUT_TEXT,
                    				'options' => ['id' => 'patname',
                    				]
                    		],
                    		
                    	'FK_org' =>	[
                    		
                    			'label' => 'Организация',
                    			'type' => Form::INPUT_WIDGET,
                    			'widgetClass' => '\kartik\widgets\Select2',
                    			'options' => [
                    					'data' =>  $orgList = ArrayHelper::map( common\models\Organisation::find()->all(), 'id', 'short_name' ),
                    					'options' => ['id' => 'FK_org',
                    					
                    					],
                    				//	'disabled' => $flagNeedBlockEdits,
                    					
                    			],
                    		],

                       
                    ],
                ]);*/
                ?>
                </div>
                <!--  <div class="form-group">
                    <div class="col-md-4">

                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary form-control']) ?>
                    </div>
                </div> 
 -->
 
 
 <div class="us-ext-form col-md-8">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'newUsername')->textInput() ?>

    <?= $form->field($model, 'newEmail')->textInput() ?>
    <?php if( $model->isNewRecord ): ?>
        <?= $form->field( $model, 'newPassword' )->passwordInput() ?>
    <?php endif; ?>
    
        
    <?= $form->field($model, 'LastName')->textInput(['maxlength' => 1024]) ?>

    <?= $form->field($model, 'FirstName')->textInput(['maxlength' => 1024]) ?>

    <?= $form->field($model, 'PatName')->textInput(['maxlength' => 1024]) ?>

    <?php 
        $orgList = ArrayHelper::map( common\models\Organisation::find()->all(), 'id', 'short_name' );

        echo $form->field( $model, 'FK_org' )
                            ->dropDownList(
                            $orgList,           
                            ['id'=>'short_name',
                            'prompt'=>'- Выберите организацию -']
        );                       
    ?>
    
    <?= $form->field($model, 'Photo')->textInput(['maxlength' => 4096]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

 
  <div class="form-group">
       
    </div>
                <?= Html::activeHiddenInput($model, 'id', ['value' => $model->id]); ?>
                <br/>
                <?php ActiveForm::end() ?>


            </div>
        </div>

        <!--
        
        -->
    </div>
</div>

<div class="us-ext-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <!--
    <p>
        <?= Html::a('Создать новый профиль', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    -->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'username',
            'email',
            'fullName',
            'orgName',
            //'Photo',

            ['class' => 'yii\grid\ActionColumn',
             'buttons'=>[
                        'assign'=> function ( $url, $model, $key )
                            {
                                return Html::a( '<span class="glyphicon glyphicon-cog"></span>', $url,
                                                [ 'title'=>Yii::t('yii', 'Assign role' ),] );
                            }
                        ],
            'template' => '{view} {update} {delete} {assign} ',
            'controller'=>'us-ext',
            ],
        ],
    ]); ?>

</div>
