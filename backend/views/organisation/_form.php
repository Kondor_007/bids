<?php

use common\components\modalWidgets\AddressModalWidget;
use common\components\modalWidgets\TaskWidget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use common\components\modalWidgets\AddWidget;
use kartik\widgets\FileInput;



/* @var $this yii\web\View */
/* @var $model common\models\Organisation */
/* @var $userModel common\models\UsExt */
/* @var $activityModel common\models\OrgActivityField */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs("
		$('#attach-contract-check').removeAttr('checked');
		$('div#contract-block').slideUp();
		$('#attach-contract-check').change(function(e)
		{
			$('div#contract-block').slideToggle();
		});	");

?>

<div class="organisation-form">

    <?php $form = ActiveForm::begin([
    		'options' =>[
    				'enctype' => "multipart/form-data",
    		]
    ]); ?>
    <!-- =============== -->
    <!-- Блок о компании -->
    <!-- =============== -->
        <h3>О компании</h3>
        <?= $form->field($model, 'short_name')->textInput(['maxlength' => 1024]) ?>

        <?= $form->field($model, 'full_name')->textInput(['maxlength' => 1024]) ?>
    
        <?php 
            $userList = ArrayHelper::map( common\models\UsExt::find()->all(), 'ID', 'fullName' );
            $typeList = ArrayHelper::map( common\models\OrgType::find()->all(), 'id', 'name' );
            $headcountList = ArrayHelper::map( common\models\OrgHeadcount::find()->all(), 'id', 'count' );
            $activityList = ArrayHelper::map( common\models\OrgActivityField::find()->all(), 'id', 'name' );
            
            echo $form->field($model, 'FK_us_ext')
                                ->dropDownList(
                                $userList,           
                                ['id'=>'user-head-list',
                                'prompt'=>'-- Выберите пункт списка --']
                                );

            // Button trigger modal
            echo '<div class="form-group">'.Html::button( Yii::t( 'yii', 'Добавить руководителя' ), [ 
                            'class' => 'btn btn-primary',
                            'data-toggle'=>'modal', 
                            'data-target' => '#addUserModal',
                            ]).'</div>';


            echo $form->field($model, 'FK_org_type')
                                ->dropDownList(
                                $typeList,           
                                ['id'=>'name',
                                'prompt'=>'- Выберите тип организации -',
                                'required' => true, ]
                                );


            echo $form->field($model, 'FK_headcount')
                                ->dropDownList(
                                $headcountList,           
                                ['id'=>'count',
                                'prompt'=>'- Выберите количество сотрудников -']
                                );

            echo $form->field($model, 'FK_field_of_activity')
                        ->dropDownList(
                        $activityList,           
                        ['id'=>'activity-field-list',
                        'prompt'=>'- Выберите область деятельности -']
                        );                            
            echo '<div class="form-group">'.Html::button( Yii::t( 'yii', 'Добавить сферу деятельности' ), [ 
                            'class' => 'btn btn-primary',
                            'data-toggle'=>'modal', 
                            'data-target' => '#addActivityModal',
                            ]).'</div>';
        ?>
    
    <!-- =============== -->
    <!-- Блок реквизиты  -->
    <!-- Видят только пользователи с разрешением -->
    <?php if( Yii::$app->user->can('showRequisite') ): ?>
    <div class = 'form-data-block'>
        <h3>Реквизиты</h3>

        <?= $form->field($model, 'INN')->textInput(['maxlength' => 1024]) ?>

        <?= $form->field($model, 'OGRN')->textInput(['maxlength' => 1024]) ?>

        <?= $form->field($model, 'KPP')->textInput(['maxlength' => 1024]) ?>

        <?= $form->field($model, 'OKATO')->textInput(['maxlength' => 1024]) ?>

        <?= $form->field($model, 'act_on_authority')->textInput(['maxlength' => 1024]) ?>
    </div>
    <!-- =============== -->
    <!-- Блок контактная информация -->
    <!-- =============== -->
            
        <?= $form->field($model, 'address')->textInput() ?>

        <?= $form->field($model, 'address_legal')->textInput() ?>        

        <?= $form->field($model, 'address_post')->textInput() ?>
            
    <?php endif; ?>
    <!-- Блок контакты -->
    <!-- Видят только пользователи с разрешением -->
    <h3>Контактная информация</h3>
    <?php if( Yii::$app->user->can('showContacts') ): ?>
        <?= $form->field($model, 'phone')->widget(MaskedInput::classname(), [
                'name' => 'phone-input',
                'mask' => '+7(999) 999-99-99',
        ]);?>
        <?= $form->field($model, 'web_site')->textInput(['maxlength' => 4096]) ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => 1024]) ?>
    <?php endif; ?>
    <?php $contractModel = new common\models\Contract;
    	  $contractModel->scenario = 'optional'; ?>
    
    <div class = 'form-group'>
    	<label for = 'attach-contract-check'><input type = 'checkbox' name = 'attach-contract' id = 'attach-contract-check'> Прикрепить договор</label>
    </div>
    <div class = 'form-group' id = 'contract-block' hidden>
	    <div class = 'col-md-4'>
	    	<?= $form->field( $contractModel, 'name')->textInput() ?>
	    </div>
	    <div class = 'col-md-4'>
	    	<?= $form->field( $contractModel, 'number')->textInput() ?>
	    </div>
	    <div class = 'col-md-4'>
	    	<?= $form->field( $contractModel, 'file_path')->widget( FileInput::className(), [
													    		'pluginOptions' => [
													    			'showUpload' => false,
													    			'browseLabel' => '',
													    			'removeLabel' => '',
													    			]
													    		]) ?>
	    </div>
    </div>
    
    <div class = 'form-group'>
    	<?= $form->field($model, 'description')->textArea(['rows' => 6]) ?>
	</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php 
        ActiveForm::end(); 

        $userModel = new common\models\UsExt;
        $userModel->scenario = 'createByManager';

        $addUser = AddWidget::begin([ 
                'actionUrl' => 'usext/create-phantom',
                'sourceFieldId' => 'user-head-list',
                'modalId' => 'addUserModal',
                'buttonLabel' => 'Add user',
                'title' => 'Add user',
            ]);     
        $addUser->beginModalForm();
        
        echo $addUser->modalForm->field( $userModel, 'LastName' )->textInput(['maxlength' => 1024]);
        echo $addUser->modalForm->field( $userModel, 'FirstName' )->textInput(['maxlength' => 1024]);
        echo $addUser->modalForm->field( $userModel, 'PatName' )->textInput(['maxlength' => 1024]);

        $addUser->endModalForm();
        AddWidget::end();

        $activityModel = new common\models\OrgActivityField;
        
        $addActivity = AddWidget::begin([ 
                'actionUrl' => 'organisation/create-activity',
                'sourceFieldId' => 'activity-field-list',
                'modalId' => 'addActivityModal',
                'buttonLabel' => 'Добавить сферу деятельности',
                'title' => 'Добавить сферу деятельности',
            ]);     
        $addActivity->beginModalForm();
        
        echo $addActivity->modalForm->field( $activityModel, 'name' )->textInput(['maxlength' => 1024]);

        $addUser->endModalForm();
        AddWidget::end();
    ?>
</div>
