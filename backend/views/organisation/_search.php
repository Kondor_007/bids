<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\OrganisationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="organisation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'short_name') ?>

    <?= $form->field($model, 'full_name') ?>

    <?= $form->field($model, 'FK_us_ext') ?>

    <?= $form->field($model, 'FK_org_type') ?>

    <?php // echo $form->field($model, 'INN') ?>

    <?php // echo $form->field($model, 'OGRN') ?>

    <?php // echo $form->field($model, 'KPP') ?>

    <?php // echo $form->field($model, 'OKATO') ?>

    <?php // echo $form->field($model, 'act_on_authority') ?>

    <?php // echo $form->field($model, 'FK_adr_legal') ?>

    <?php // echo $form->field($model, 'FK_adr_local') ?>

    <?php // echo $form->field($model, 'FK_adr_post') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'description') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
