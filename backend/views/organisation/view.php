<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Organisation */

$this->title = $model->short_name;

$this->params['breadcrumbs'][] = ['label' => 'Организации', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organisation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php 
        $fields = ['full_name', 'headName', 'fieldName', 'headcount'];

        if( Yii::$app->user->can( 'showRequisite' ) )
        {
            $fields[] = 'headJobs';
            $fields[] = 'INN';
            $fields[] = 'OGRN';
            $fields[] = 'OKATO';
            $fields[] = 'KPP';
            $fields[] = 'payAccCount';
            $fields[] = 'act_on_authority';
        }

        if( Yii::$app->user->can( 'showContacts' ) )
        {
            $fields[] = 'address';
            $fields[] = 'address_legal';
            $fields[] = 'address_post';
            $fields[] = 'phone';
            $fields[] = 'web_site';
            $fields[] = 'email:email';
        }
        $fields[] = 'description';
    ?>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $fields/*[
            'id',
            'short_name',
            'full_name',
            'headName',
            'orgType',
            'headJobs',
            'INN',
            'OGRN',
            'KPP',
            'OKATO',
            'payAccCount',
            'act_on_authority',
            'legalAdr',
            'localAdr',
            'postAdr',
            'phone',
            'email:email',
            'description',
        ],*/
    ]) ?>

</div>
