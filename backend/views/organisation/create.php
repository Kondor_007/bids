<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Organisation */

$this->title = 'Добавление организации';
$this->params['breadcrumbs'][] = ['label' => 'Организации', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organisation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
