<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrganisationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Организации';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organisation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php 
        $columns = [['class' => 'yii\grid\SerialColumn'],'short_name', 'headName', 'fieldName'];

        if( Yii::$app->user->can('showRequisite') )
        {
            $columns[] = 
            [ 
              'class' => 'yii\grid\ActionColumn',
              'header'=>'Расчетные счета',
              'contentOptions' => [ 'align' => 'center' ],
              'buttons'=>   [
                                'index' => function( $url, $model, $key )
                                {
                                    $accCount = $model->payAccCount;
                                    if( $accCount > 0 )
                                        return Html::a( "<font color = blue size = 4 >${accCount}</font>", $url );
                                    else
                                        return Html::a( 'Добавить расчетный счет', [ 'pay-account/create', 'id' => $model->id ], [ 'class' => 'btn btn-danger' ] );
                                } 
                            ],
                'template' => '{index}',
                'controller' => 'pay-account'
            ];
            $columns[] = 'address';
        }

        if( Yii::$app->user->can('showContacts') )
        {
            $columns[] = 'email:email';
            $columns[] = 'phone';
            $columns[] = 'web_site';

        }
        //$columns[] = 'orgType';
        $columns[] = ['class' => 'yii\grid\ActionColumn'];
        //echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <p>
        <?= Html::a('Добавить организацию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns, /*[
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'short_name',
            'headName',
            ['class' => 'yii\grid\ActionColumn',
              'header'=>'Расчетные счета',
              'contentOptions' => [ 'align' => 'center' ],
              'buttons'=>   [
                                'index' => function( $url, $model, $key )
                                {
                                    $accCount = $model->payAccCount;
                                    if( $accCount > 0 )
                                        return Html::a( "<font color = blue size = 4 >${accCount}</font>", $url );
                                    else
                                        return Html::a( 'Добавить расчетный счет', [ 'pay-account/create', 'id' => $model->id ], [ 'class' => 'btn btn-danger' ] );
                                } 
                            ],
                'template' => '{index}',
                'controller' => 'pay-account'
            ],
            'orgType',
            //'full_name',
            //'FK_us_ext',
            //'FK_org_type',
            // 'INN',
            // 'OGRN',
            // 'KPP',
            // 'OKATO',
            // 'act_on_authority',
            // 'FK_adr_legal',
            // 'FK_adr_local',
            // 'FK_adr_post',
            // 'phone',
            // 'email:email',
            // 'description',

            ['class' => 'yii\grid\ActionColumn'],
        ],*/
    ]); ?>

</div>

