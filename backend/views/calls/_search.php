<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SearchCalls */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="calls-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'cid') ?>

    <?= $form->field($model, 'dest_id') ?>

    <?= $form->field($model, 'dt_start') ?>

    <?= $form->field($model, 'dest_tl') ?>

    <?php // echo $form->field($model, 'dt_talk') ?>

    <?php // echo $form->field($model, 'dt_fin') ?>

    <?php // echo $form->field($model, 'rf') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
