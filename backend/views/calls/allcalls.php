

<?php
/**
 * Created by PhpStorm.
 * User: kondor007
 * Date: 11.03.16
 * Time: 11:12
 */
use yii\grid\GridView;
?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'options'=>['id'=>'callGrid'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'src',
        'dst',
        'dt_start',
        // 'dest_tl',
        'dt_talk',
        'dt_fin',
        'recfile',
        [ 'label'=>'Звонившие телефоны',
            'content'=>function($call){ return $call->noans;}
        ],
        [ 'label'=> 'Состояние','content'=>function($data){return  $data->getStateCall();}],
        // ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>