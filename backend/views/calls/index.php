<?php

use backend\controllers\BehaviorCodes;
use backend\controllers\CallsController;
use backend\controllers\LogController;
use common\assets\CallsAsset;
use common\components\modalWidgets\TaskWidget;
use common\models\Calls;
use common\models\SearchCalls;
use common\models\Task;
use common\models\TaskStatus;
use common\models\UsExt;

use kartik\builder\Form;
use kartik\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\grid\GridView;

use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SearchCalls */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Звонки';
$this->params['breadcrumbs'][] = $this->title;

CallsAsset::register($this);


?>


<div class="form-group">
    <div class="col-md-8">
        <?php Modal::begin(['id' => 'add-task-modal',
           // 'aria-hidden'=>true,
            /*'toggleButton' => ['class' => 'btn btn-success',
                'label'=>'Тест заявок'],*/
           // 'id' => 'add-client-modal',
        ]) ;

        $taskAdd =ActiveForm::begin([
            'id' => 'task-add', 'method' => 'POST',
            //После принятия заявки отправляем ее на создание в контроллер
            'action'=> '/bids/backend/web/index.php?r=calls/accept'
        ]);
        $mtask= new Task();

        echo Form::widget([
            'model' => $mtask,
            'form' => $taskAdd,
            'columns' => 1,
            'attributes'=>[
                'name' => [
                    'name' => Form::INPUT_TEXT,


                ],
                'description' => [ 'description' => Form::INPUT_TEXT,],
               // 'dt_talk' => ['dt_talk' => Form::INPUT_TEXT, 'value'=>"AAAA"], //$dt_fin=null
            ]
        ]);
        ?>

        <?= Html::submitButton('Добавить значение', ['class' => 'btn btn-primary form-control',
            'id'=>'submit_new_type'
        ]) ?>


        <?php
        ActiveForm::end();
        Modal::end();
        ?>

    </div>
</div>

<div class="calls-index">







    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //echo Html::a('Create Calls', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(['id'=>'pjcount_in'])?>
    <?php
    echo HTML::label("Ожидающие на линии клиенты (число всех входящих в общем)");
    echo Html::textInput('count',0,['id'=>'count_in', 'readonly'=>true, 'class'=>'form-control']);
         ?>
    <?php Pjax::end()?>


    <?php Pjax::begin(['id'=>'pjdescr_in'])?>
    <?php
    echo HTML::label("Описание "." ".$description."");
    echo Html::textInput('count',0,['id'=>'descr_in', 'readonly'=>true,
        'class'=>'form-control'
    ]);

    /*echo $description;
    exit();*/
    ?>
    <?php Pjax::end()?>


    <?php
    if ($behavior == BehaviorCodes::$BehCreateTask)
    echo Html::a('Создать задачу текущему звонку)', ['accept'], ['class' => 'btn btn-success']);
   /* echo CallsController::actionSimplePlayerOut('111');
    exit();*/
    ?>


    <?php Pjax::begin(['id'=>'acceptedCalls']);
    $mphone=UsExt::getCurrentUserPhone();

   // echo UsExt::getCurrentUserPhone();//find()->where(['FK_users'=>Yii::$app->user->id])->one()->phone;
    //exit();
   //echo CallsController::actionGetStringViewInCall();
    //LogController::actionFastAdd();
    //$cmpt=new LogController();
   // echo $this->render('allcalls',[]);
    echo Html::a("Все звонки",'index.php?r=calls/all');
    ?>

<!--
    <?= GridView::widget([
        'dataProvider' => (new SearchCalls())->searchByPhone($mphone),
        //'filterModel' => $searchModel,
        'options'=>['id'=>'AcallGrid'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['content'=>function($data){
                return CallsController::actionSimplePlayerOut($data->recfile, $data->id);
            }],
           /* [
                'content'=>function($data){

                    $datetime1 = new DateTime($data->dt_talk);
                  //  return is_null( $data->dt_talk);
                    if (is_null($data->dt_talk))
                        return 'none';


                    $datetime2 = new DateTime($data->dt_start);
                    $interval = $datetime1->diff($datetime2);
                    $formats = 'Y-m-d H:I:s';


                   // print_r($interval);
                   // exit();
                    return $datetime2->format($formats)." | ". $datetime1->format($formats).
                        "   res= ".$interval->s;
                }
            ],*/
            'id',
            'src',
            'dst',
          //  'dt_start',
          //  'dest_tl',
           // 'dt_talk',
           // 'dt_fin',
          //  'recfile',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
    <?php Pjax::end()?>
    -->
    <?php echo "Все звонки"?>
  <?php Pjax::begin(['id'=>'mypjax1'])?>

    <?php
    $query = Task::find()->orderBy(['id' => SORT_DESC]);
    $dataProviderM = new ActiveDataProvider([
        'query' => $query,
    ]);
    echo GridView::widget([
        'options'=>['id' => 'acceptedTasks'],
        'dataProvider' => $dataProviderM,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //['class' => 'yii\grid\CheckBoxColumn'],
            'id',
            [
                'label'=>'Ответственный',
                'content'=>function($data){
                   // print_r($data);
                    $user = is_null(UsExt::findOne($data->FK_responsible)) ? null : UsExt::findOne($data->FK_responsible)->id;
                    if (is_null($user))
                        return "Не задано";
                    else {
                        $model = UsExt::find()->where(['id' => $user])->one();//Calls::find()->where(['FK_linkedid'=>$data->FK_linkedid])->one();
                        if (!is_null($model))
                            return $model->getFullName(); //CallsController::actionSimplePlayerOut($model->recfile, $model->id).$model->recfile;
                    }

            }],
            [
                'label'=>'Заявитель',
                'content'=>function($data){
                    // print_r($data);
                    //$model = UsExt::find()->one();//Calls::find()->where(['FK_linkedid'=>$data->FK_linkedid])->one();
                    // if (!is_null($model))
                    $us=UsExt::find()->where(['phone'=> $data->FK_cid])->one();
                    return  is_null($us) ? 'Не задано':$us->getFullName();
                }
            ],
            [
                'label'=>'Дата',
                'content'=>function($data){
                   // print_r($data);
                //$model = UsExt::find()->one();//Calls::find()->where(['FK_linkedid'=>$data->FK_linkedid])->one();
               // if (!is_null($model))
                return $data->date;
            }],
            
            [
                'class'=>'yii\grid\DataColumn',
                'attribute'=>

                    'name',
                'content' => function( $model )
                {
                    $content = $model->name;
                    return $content;
                    /* if( $model->currentStatus == TaskStatus::TASK_FINISHED )
                     {
                         return '<strike>'.$content.'</strike>';
                     }*/

                    // return str_repeat( CHILD_IDENT, $model->level )."<input type = 'checkbox'>&nbsp".$model->name;
                }
            ],
            //'originatorName',
            // 'responsiblesString',
            'description',
           /* [
                'content'=> function(){ return Html::button("Edit",['class'=>'EditTask']);}
            ],*/
            ['class' => 'yii\grid\ActionColumn',

                'template' => '{view}', // ' {edit} ',
                'controller'=>'calls',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                       // if ($model->getIsOwner( Yii::$app->user->id))
                            return yii\helpers\Html::a("<span class = 'glyphicon glyphicon-plus'></span>",
                                "index.php?r=task/edit&id=".$model->id."",["title"=>"Посмотреть связанную задачу"]);
                    },
                    'edit' => function ($url, $model, $key) {
                       // if ($model->getIsWorker( Yii::$app->user->id))
                            return yii\helpers\Html::a("<span class = 'glyphicon glyphicon-edit'></span>",
                                "index.php?r=calls/view&id=".$model->id."&nocreate=true",["title"=>"Редактировать"]);
                    },
                ],
            ],

        ],
    ]);
    ?>
   <!-- <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options'=>['id'=>'callGrid'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'src',
            'dst',
            'dt_start',
           // 'dest_tl',
            'dt_talk',
             'dt_fin',
             'recfile',

            [ 'label'=> 'Состояние','content'=>function($data){return  $data->getStateCall();}],
           // ['class' => 'yii\grid\ActionColumn'],



    ]]); ?> -->
    <?php Pjax::end()?>
<!--
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        Принятые звонки
                    </a>
                </h4>
            </div>



            <div id="collapseOne" class="panel-collapse <?php echo isset($model->id) ? '' : 'collapse '; ?> ">
                <div class="panel-body">

                </div>
            </div>


        </div>
    </div>


</div>
-->
