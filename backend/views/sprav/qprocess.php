<?php

use kartik\builder\Form;
use kartik\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\form\ActiveForm;

?>
    <div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                Добавить качество обработки
              </a>
            </h4>
    </div>
    <div id="collapseOne" class="panel-collapse <?php echo isset($model->id) ? '' : 'collapse '; ?> ">
      <div class="panel-body">
<?php                
    $form = ActiveForm::begin([ 'id' => 'quality-form', 'method' => 'POST', 'type' => ActiveForm::TYPE_VERTICAL ]);

    echo Form::widget([
		'model' => $model,
		'form' => $form,
		'columns' => 3,
		'attributes' => [
				'name' => [
						'label' => 'Наименование',
						'type' => Form::INPUT_TEXT,
				        ],
/*                'FK_unit' => [
						'label' => 'Единица измерения',
						'type' => Form::INPUT_WIDGET,
						'widgetClass' => '\kartik\widgets\Select2',
						'options' => ['data' => ['1'=>'шт','2'=>'метр'], 'options' => ['id' => 'material'] ],
				        ],
  */                      
		],
    ]);
?>
<div class="form-group">
        <div class="col-md-4">
        
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary form-control' ]) ?>
        </div>
</div>
<?= Html::activeHiddenInput($model, 'id', ['value' => $model->id]); ?>
<br />
<?php ActiveForm::end() ?>                
                
        
      </div>
    </div>
  </div>
  </div>
  
<?php

Pjax::begin([ 'id' => 'material-grid-pjax', 'enablePushState' => false ]);
echo GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'id',
			'name',
			[
				'class' => 'yii\grid\ActionColumn',
                'template' => '{edit} {delete} ',
				'buttons' => [
                    'delete' => function ( $url, $model, $key ) {
						return "<a href = 'index.php?r=sprav/delete-qprocess&id=$model->id'><span class = 'glyphicon glyphicon-trash'></span></a>";
					},
                    'edit' => function ( $url, $model, $key ) {
						return yii\helpers\Html::a("<span class = 'glyphicon glyphicon-edit'></span>",  "index.php?r=sprav/edit-quality&id=$model->id");
					},
                     
				],

			],
		],		
]);
Pjax::end();

?>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Название модали</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary">Сохранить изменения</button>
      </div>
    </div>
  </div>
</div>