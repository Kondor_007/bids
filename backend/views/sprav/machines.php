<?php

use common\assets\MachinesAsset;
use kartik\builder\Form;
use kartik\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use common\models\ProcessType;
use common\models\MachineType;
use yii\base\Widget;
use yii\bootstrap\Modal;

MachinesAsset::register($this);
?>

<!--
<script type="text/javascript" src="../../common/assets/jquery.js"></script>

<script type="text/javascript" src="../../common/assets/crmscripts/machines/machines.js"></script>
 -->
 
    <!-- <script type="text/javascript" src="../../common/assets/crmscripts/list_modal_show.js">
// ListModalWorker('#FK_type', '#add-type-modal', '#submit_new_type');
  </script>
 <script type="text/javascript" >$(document).ready(
		 function(){
			//$.getScript("../../common/assets/crmscripts/list_modal_show.js", 
					//function(){
						 ListModalWorker('#FK_type', '#add-type-modal', '#submit_new_type');
			//}
			 }
			 );</script>
  -->
  
    <div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                Добавить оборудование
              </a>
            </h4>
    </div>
    <div id="collapseOne" class="panel-collapse <?php echo isset($model->id) ? '' : 'collapse '; ?> ">
      <div class="panel-body">
      
 




<div class="form-group">
        <div class="col-md-8">
<?php Modal::begin(['id' => 'add-type-modal',
]) ;

$typeForm = ActiveForm::begin([ 
		'id' => 'machines-add-type', 'method' => 'POST', 'type' => ActiveForm::TYPE_VERTICAL, 
         'action'=> '/dial/backend/web/index.php?r=machine/create-machine-type'
]);
$mtype= new MachineType();
//TODO: JS and MachineTypeController Fix
//$mtype= new MachineType();
 echo Form::widget([
 'model' => $mtype,
 'form' => $typeForm,
 'columns' => 3,
 'attributes'=>[
 'name' => [
 'type' => Form::INPUT_TEXT,

 ],
 ]
 ]);
?>

            <?= Html::submitButton('Добавить значение', ['class' => 'btn btn-primary form-control', 
            		 'id'=>'submit_new_type'
            ]) ?>

  
<?php
   ActiveForm::end();
   Modal::end();
?>

        </div>
</div>
<?php
    $form = ActiveForm::begin([ 'id' => 'machines-form', 'method' => 'POST', 'type' => ActiveForm::TYPE_VERTICAL ]);
    echo Form::widget([
		'model' => $model,
		'form' => $form,
		'columns' => 2,
		'attributes' => [
				

				'FK_type' => [
						'label' => 'Тип',
						'type' => Form::INPUT_WIDGET,
						'widgetClass' => '\kartik\widgets\Select2',
						'options' => ['data' =>
								\yii\helpers\ArrayHelper::map(\common\models\MachineType::find()->all(),'id','name'),
								//FillArray(MachineType::find()->asArray()->all(), 'name'),
								'options' => ['id' => 'FK_type',
										//'readonly'=>$flagNeedBlockEdits
								],
								
						],
				],
				'name' => [
						'label' => 'Наименование',
						'type' => Form::INPUT_TEXT,
				],
		],
    ]);
?>
<div class="form-group">
        <div class="col-md-4">
        
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary form-control' ]) ?>
        </div>
</div>
<?= Html::activeHiddenInput($model, 'id', ['value' => $model->id]); ?>
<br />
<?php ActiveForm::end() ?>                
                
        
      </div>
    </div>
  </div>
  </div>
  
<?php

Pjax::begin([ 'id' => 'material-grid-pjax', 'enablePushState' => false ]);
echo GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			//'id',
			//'name',
			[
			 'label'=>'Название станка',
					'attribute'=>'name'
			],
			[
            'label' => 'Тип станка',
            'content' => function ($data) {
            if ((!is_null($data)) and ($data->FK_type != ''))
            	$mod=MachineType::findOne($data->FK_type);
            if (!is_null($mod))
                return $mod->name;
            
            else
                return "не задано";

        },
        ],
			[
				'class' => 'yii\grid\ActionColumn',
                'template' => '{edit} {delete}',
				'buttons' => [
                    'delete' => function ( $url, $model, $key ) {
						return "<a href = 'index.php?r=sprav/delete-machine&id=$model->id'><span class = 'glyphicon glyphicon-trash'></span></a>";
					},
                    'edit' => function ( $url, $model, $key ) {
						return yii\helpers\Html::a("<span class = 'glyphicon glyphicon-edit'></span>",  "index.php?r=sprav/edit-machine&id=$model->id");
					}
				],

			],
		],		
]);
Pjax::end();

?>
</div>