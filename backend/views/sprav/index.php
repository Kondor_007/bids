<?php
use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\RequestSearch */

$this->title = 'Справочники';

?>
<h2><?= $this->title ?></h2>
<div class = 'form-group'>
<a href = 'index.php?r=sprav/index&razdel=materials' class = 'btn btn-primary'>Материалы</a>
<a href = 'index.php?r=sprav/index&razdel=machines' class = 'btn btn-primary'>Оборудование</a>
<a href = 'index.php?r=sprav/index&razdel=prtype' class = 'btn btn-primary'>Типы работ</a>
<a href = 'index.php?r=sprav/index&razdel=qprocess' class = 'btn btn-primary'>Качество обработки</a>
 <!--  <a href = 'index.php?r=log' class = 'btn btn-primary'>Логирование</a> -->
</div>




<?php
if (isset($razdel)){

echo "<div class = 'well'>".$this->render($razdel, [
		'dataProvider'      => $providers[$razdel],
        'model'             => $models[$razdel],
/*		'process' => $process,
		'refs' => $refs,
        */
]);
}




?>




<?php
/*
echo '<pre>';
print_r($dataProvider);
echo '</pre>';
exit;
*/
/*
echo GridView::widget([
		'dataProvider' => $dataProvider,  
		'filterModel' => $searchModel,
		'columns' => [
            'id',
            'clientName',
			'deviceName',
            'userownerName',
            'task.name',
            //'taskStatus.FK_status',
            'taskStatus.statusTypeName',	
            //'name',
		
        ['class' => 'yii\grid\ActionColumn',
             
            'template' => '{view} {update} {delete} ',
            'controller'=>'request',
            ],
        ],
        
]);
*/
?>
