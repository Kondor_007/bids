<?php

use kartik\builder\Form;
use kartik\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use \common\models\ProcessType;
use \common\models\ProcesstypeMachine;
use \common\models\Machine;

//use \app\controllers\

?>
<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    Добавить виды работ
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse <?php echo isset($model->id) ? '' : 'collapse '; ?> ">
            <div class="panel-body">


                <?php
                //public $mymodel=null;
                $form = ActiveForm::begin(['id' => 'prtype-form', 'method' => 'POST', 'type' => ActiveForm::TYPE_VERTICAL]);

                function FillArray($array, $tag)
                {
                    $target = ['----Выберите----'];
                    foreach ($array as $miter) {
                        $target[] = $miter[$tag];
                    };
                    return $target;
                }

                ;
                //new (\app\controllers\_ServiceController\\

                echo Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' => 3,
                    'attributes' => [
                        'name' => [
                            'label' => 'Тип работ',
                            'type' => Form::INPUT_TEXT,
                        ],
                        'fk_p_type_unit' => [
                            'label' => 'Единица измерения',
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => '\kartik\widgets\Select2',

                            'options' => ['data' => FillArray(
                                \common\models\ReqProcessUnit::find()->asArray()->all(), 'unitname'
                            ),

                            ],


                        ],

                        'price_one_unit' => [
                            'label' => 'Стоимость единицы обработки',
                            'type' => Form::INPUT_TEXT,
                        ],
                        'fk_machine' => [
                            'label' => 'Тип совместимого оборудования',
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => '\kartik\widgets\Select2',
                            'options' => ['data' => FillArray(\common\models\MachineType::find()->asArray()->all(),'name'),
                                'options' => ['id' => 'FK_machine_type']],
                        ],
                    ],
                ]);
                ?>
                <div class="form-group">
                    <div class="col-md-4">

                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary form-control']) ?>
                    </div>
                </div>
                <?= Html::activeHiddenInput($model, 'id', ['value' => $model->id]); ?>
                <br/>
                <?php ActiveForm::end() ?>


            </div>
        </div>
    </div>
</div>

<?php

Pjax::begin(['id' => 'material-grid-pjax', 'enablePushState' => false]);
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        'name',
        /*   ['value'=>function($model){
               $var='';
               $model->getAttributes($var);
           return $var;}],*/
        // 'test',
        //'machine_name',

        [
            'header' => 'Ед. изм.',
            //'value' => \common\models\ReqProcessUnit::findOne('1')->name,
            'content' => function ($data) {

                if (!is_null($data)) // and ($data->!= ''))
                    return \common\models\ReqProcessUnit::findOne($data->fk_p_type_unit)->unitname;

                else
                    return "";

            }], [
            'header' => 'Тип устройства',
            'content' => function ($data) {
                return $data->getMachineType();
            }],
        'price_one_unit',
        /*'fk_p_type_unit',
        'fk_machine',*/

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{edit} {delete} {quality}',
            'buttons' => [
                'delete' => function ($url, $model, $key) {
                    return "<a href = 'index.php?r=sprav/delete-prtype&id=$model->id'><span class = 'glyphicon glyphicon-trash'></span></a>";
                },
                'edit' => function ($url, $model, $key) {
                    return yii\helpers\Html::a("<span class = 'glyphicon glyphicon-edit'></span>", "index.php?r=sprav/edit-prtype&id=$model->id");
                },
                'quality' => function () {
                    return '<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">q</button>';
                }
            ],

        ],

    ],
]);
Pjax::end();

?>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Название модали</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary">Сохранить изменения</button>
            </div>
        </div>
    </div>
</div>