<?php


use common\assets\MaterialsAsset;
use common\models\Manufacturers;
use common\models\MaterialGroups;
use common\models\SearchMaterial;
use kartik\builder\Form;
use kartik\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use common\models\MachineType;
use common\models\MaterialUnit;
MaterialsAsset::register($this);
?>
<script type="text/javascript" src="../../common/assets/jquery.js"></script>


<div class="form-group">
    <div class="col-md-8">
        <?php Modal::begin(['id' => 'add-man-modal',
        ]) ;

        $typeForm = ActiveForm::begin([
            'id' => 'materials-add-man', 'method' => 'POST', 'type' => ActiveForm::TYPE_VERTICAL,
            'action'=> '/dial/backend/web/index.php?r=manufacturers/create-manufacturer'
        ]);
        $mtype= new Manufacturers();
        //TODO: JS and MachineTypeController Fix
        //$mtype= new MachineType();
        echo Form::widget([
            'model' => $mtype,
            'form' => $typeForm,
            'columns' => 3,
            'attributes'=>[
                'name' => [
                    'type' => Form::INPUT_TEXT,
                ],
            ]
        ]);
        ?>

        <?= Html::submitButton('Добавить значение', ['class' => 'btn btn-primary form-control',
            'id'=>'submit_new_man'
        ]) ?>


        <?php
        ActiveForm::end();
        Modal::end();
        ?>

    </div>
</div>

<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    Прием материалов
                </a>
            </h4>
        </div>



        <div id="collapseOne" class="panel-collapse <?php echo isset($model->id) ? '' : 'collapse '; ?> ">
            <div class="panel-body">
                <?php
                $form = ActiveForm::begin(['id' => 'material-form', 'method' => 'POST',
                    'type' => ActiveForm::TYPE_VERTICAL]);
                function FillArray($array, $tag)
                {
                    $target = ['----Выберите----'];
                    foreach ($array as $miter) {
                        $target[] = $miter[$tag];
                    };
                    return $target;
                }

                ;
                function SetVal($var=null, $default=1, $incorrect_format=null){
                    if ($incorrect_format==null)
                    return is_null($var) ? $default : $var;
                    else
                        return $var==$incorrect_format ? $default : $var;
                }
				$flagNeedBlockEdits=true;
				//Флаг режима редактирования.
				//Блокирует поля кроме нужных для приемки если редактирование выключено
				$flagNeedBlockEdits = (!isset($_REQUEST['isedit']) && (isset($_REQUEST['id'])));
                echo Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' => 3,
                    'attributes' => [
                        'name' => [
                            'label' => 'Наименование материала',
                            'type' => Form::INPUT_TEXT,
                            'options' => ['id' => 'name', 
                            		'readonly'=>$flagNeedBlockEdits ,]
                       ],
                        'FK_group' => [
                            'label' => 'Тип материала',
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => '\kartik\widgets\Select2',
                            'options' => ['data' =>
                                ArrayHelper::map(MaterialGroups::find()->all(),'id','name'),
                                //FillArray(MachineType::find()->asArray()->all(), 'name'),
                                'options' => ['id' => 'FK_group',
                                    //'readonly'=>$flagNeedBlockEdits
                                ],
                                'disabled' => $flagNeedBlockEdits,
                            ],
                        ],
                        'FK_manufacturer' => [
                            'label' => 'Производитель',
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => '\kartik\widgets\Select2',
                            'options' => [
                                'data' =>
                                    ArrayHelper::map(Manufacturers::find()->all(),'id','name'),
                                //FillArray(MachineType::find()->asArray()->all(), 'name'),
                                'options' => ['id' => 'FK_manufacturer',
                                    'readonly'=>$flagNeedBlockEdits ,
                                ],
                               ]
                        ],

                        'FK_machine_type' => [
                            'label' => 'Тип совместимого оборудования',
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => '\kartik\widgets\Select2',
                            'options' => ['data' =>
                                \yii\helpers\ArrayHelper::map(\common\models\MachineType::find()->all(),'id','name'),
                                //FillArray(MachineType::find()->asArray()->all(), 'name'),
                                'options' => ['id' => 'FK_machine_type',
                                    //'readonly'=>$flagNeedBlockEdits
                                ],
                                'disabled' => $flagNeedBlockEdits,
                            ],
                        ],

                        'buy_price' => [
                            'label' => 'Цена закупочная',
                            'type' => Form::INPUT_TEXT,
                            'options' => [
                        				'value'=> SetVal($model->buy_price, 0.0) ,]
                        ],

                        'sell_price' => [
                           // 'label' => 'Цена продажи',
                            'type' => Form::INPUT_TEXT,
                        		'options' => ['value'=>SetVal($model->sell_price, 0.0)
                        ]
                        ],

                        'FK_unit' => [
                            'label' => 'Единица измерения',
                            'type' => Form::INPUT_WIDGET,//WIDGET,
                            'widgetClass' => '\kartik\widgets\Select2',
                            'options' => ['data' => FillArray(MaterialUnit::find()->asArray()->all(), 'name'),
                                'options' => ['id' => 'material',
                               
                                ],
                            		'disabled' => $flagNeedBlockEdits,
                            		
                            ],
                        	
                        	
                      
                        ],
                        'count' => [
                            'label' => 'Количество в ед. изм.',//.MaterialUnit::findOne($model->FK_unit)->name,
                            'type' => Form::INPUT_TEXT,
                            //'widgetClass' => '\kartik\widgets\Select2',

                            'options' => [//'id' => 'count', //'prop_value',
                            'value'=>SetVal($model->count, 1),
                            ]
                        ],


                    		'length' => [
                    				'type' => Form::INPUT_TEXT,
                    				'options' => [
                    						'value'=>SetVal($model->length, 1),
                    						'readonly'=>$flagNeedBlockEdits ,]
                    		],
                        'width' => [
                            'type' => Form::INPUT_TEXT,
                        		'options' => [
                        				'value'=>SetVal($model->width, 1.0),
                        				'readonly'=>$flagNeedBlockEdits ,]
                        ],
                        'color' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => [
                               // 'value'=>SetVal($model->color, 'Белый',''),
                                'readonly'=>$flagNeedBlockEdits ,]
                        ],
                        'dencity' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => [
                                'value'=>SetVal($model->dencity, 1),
                                'readonly'=>$flagNeedBlockEdits ,]
                        ],
                        'thickness' => [
                            'type' => Form::INPUT_TEXT,
                        		'options' => [
                        				'value'=>SetVal($model->thickness, 1.0),
                        				'readonly'=>$flagNeedBlockEdits ,]
                        ],

                    ],
                ]);
                ?>
                <div class="form-group">
                    <div class="col-md-4">

                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary form-control']) ?>
                    </div>
                </div>

                <?= Html::activeHiddenInput($model, 'id', ['value' => $model->id]); ?>
                <br/>
                <?php ActiveForm::end() ?>


            </div>
        </div>


    </div>
</div>

<?php

Pjax::begin(['id' => 'material-grid-pjax', 'enablePushState' => false]);
$searchModel=new \common\models\SearchMaterial();
echo $this->render('_search', ['model' => $searchModel]);

$query = \common\models\Material::find();
$filter="";
/*foreach ($_GET as $val) {
    $filter=$filter." ".$val;

}*/

$filter = $filter.(isset($_REQUEST['SearchMaterial']['name'])?$_REQUEST['SearchMaterial']['name']:"default");
    //.(isset($_GET['name'])?$_GET['name']:"");

//\backend\controllers\ServiceController::actionAlert($filter);
//if ( $filter!='default')
//    $query->andFilterWhere(['like','CONCAT(name,manufacturer,width,color,thickness)',$filter]);
echo $filter;
$query->andFilterWhere(['=','is_del','0']); //материал не удален, доступен для работ с ним
print_r( $query->sql);
//\backend\controllers\ServiceController::actionAlert();
//if ( isset($_GET['']))
  //  $query->andFilterWhere(['like','CONCAT(name,id,width)',$filter]);
$dataProvider = new \yii\data\ActiveDataProvider([
    'query' => $query,//\common\models\Material::find()->filterWhere(['like','id','4']),
    'pagination' => [
        'pageSize' => 10,
    ],
]);
//print_r($_REQUEST['SearchMaterial']);
//exit();
//if ( isset($_GET['needgrid']))
$NewSearch=new SearchMaterial();
$dataProvider= isset($_REQUEST['SearchMaterial']) ? $NewSearch->searchByLike($_REQUEST['SearchMaterial'])
    : $dataProvider;
echo GridView::widget([
    //$NewSearch->searchByLike($_REQUEST['SearchMaterial']), //
    'dataProvider' =>  $dataProvider,//$NewSearch->searchByLike($_REQUEST['SearchMaterial']),//$dataProvider,
    //'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
       // 'id',
        'name',
        [
            'label'=>'Описание',
            'content'=> function($data){
            if(!is_null($data))
            {
                //оргстекло 4 мм, красное, 100ммх100мм
               /* print_r($data);
                exit();*/
               return $data->getFullInfo($data->id);
               //getFullNameSelect($data->name);//getFullName();
            }
            else return "data is null";

        }],
       // 'fullName',
        'buy_price',
        'sell_price',

        'count',

        [
            'header' => 'Тип совместимых устройств',
            'class' => 'yii\grid\DataColumn',
            'content' =>  function ($data) {
                if ((!is_null($data)) and ($data->FK_machine_type != ''))
                    return MachineType::findOne($data->FK_machine_type)->name;
                else
                    return "";

            }],
        [
            'header' => 'Ед. изм.',
            'class' => 'yii\grid\DataColumn',
            'content' => function ($data) {
                if ((!is_null($data)) and ($data->FK_unit != '')) {
                    $var = MaterialUnit::findOne($data->FK_unit);
                    if (!is_null($var))
                        return MaterialUnit::findOne($data->FK_unit)->name;
                    else return "";
                } else
                    return "";

            }],
        //'machine_type.name',
        /*   'prop_name',
           'prop_value',*/
       // 'color', 'dencity', 'thickness', 'width',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '  {accept} {edit} {delete}',
            'buttons' => [
                'delete' => function ($url, $model, $key) {
                    return
                    yii\helpers\Html::a("<span class = 'glyphicon glyphicon-trash'></span>",'index.php?r=sprav/delete-material&id='.$model->id,["title"=>"Удалить"]);
                  //  "<a href = 'index.php?r=sprav/delete-material&id=$model->id'><span class = 'glyphicon glyphicon-trash'></span></a>";
                },
                'edit' => function ($url, $model, $key) {
                    return yii\helpers\Html::a("<span class = 'glyphicon glyphicon-edit'></span>",
                        "index.php?r=sprav/edit-material&id=$model->id&name=$model->name&isedit=true",["title"=>"Редактировать"]);
                },
                'accept' => function ($url, $model, $key) {
                    return yii\helpers\Html::a("<span class = 'glyphicon glyphicon-plus'></span>",
                        "index.php?r=sprav/edit-material&id=$model->id&name=$model->name",["title"=>"Принять новую партию\nвыбранного материала"]);
                },
            ],

        ],

        //'FK_machine_type',
        //'FK_unit',

    ],
]);
Pjax::end();

?>




