<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Button;
use yii\bootstrap\Modal;

use common\components\modalWidgets\AddWidget;

/* @var $this yii\web\View */
/* @var $model common\models\PayAccount */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="pay-account-form">

    <?php $form = ActiveForm::begin(['id'=>'pay-account-form']); ?>

    <?php 
        $orgList = ArrayHelper::map( common\models\Organisation::find()->all(), 'id', 'short_name' );

        echo $form->field( $model, 'FK_org' )
                            ->dropDownList(
                            $orgList,           
                            ['id'=>'short_name',
                            'prompt'=>'-- Выбирете организацию --',]
        );

        $bankList = ArrayHelper::map( common\models\Bank::find()->all(), 'id', 'name' );

        echo $form->field($model, 'FK_bank')
                            ->dropDownList(
                            $bankList,           
                            ['id'=>'bank-list',
                            'prompt'=>'-- Выберите банк --']
        );
                       
        // Button trigger modal
        echo Html::button( Yii::t( 'yii', 'Add bank' ), [ 
                        'class' => 'btn btn-primary',
                        'data-toggle'=>'modal', 
                        'data-target' => '#addBankModal',
                        ]);                    
    ?>

    <?= $form->field($model, 'account')->textInput(['maxlength' => 1024]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => 4096]) ?>

    <div class="form-group">
        <?= Html::submitButton( Yii::t('yii', $model->isNewRecord ? 'Create' : 'Update' ), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php 
        ActiveForm::end(); 

        $bankModel = new common\models\Bank;

        $addBankModal = AddWidget::begin([ 
                'actionUrl' => 'pay-account/addbank',
                'sourceFieldId' => 'bank-list',
                'modalId' => 'addBankModal',
                'buttonLabel' => 'Add bank',
                'title' => 'Add bank',
            ]);     
        $addBankModal->beginModalForm();
        echo $addBankModal->modalForm->field( $bankModel, 'name' )->textInput(['maxlength' => 1024]);
        echo $addBankModal->modalForm->field( $bankModel, 'BIK' )->textInput(['maxlength' => 1024]);
        echo $addBankModal->modalForm->field( $bankModel, 'cor_account' )->textInput(['maxlength' => 1024]);
        echo $addBankModal->modalForm->field( $bankModel, 'address' )->textInput(['maxlength' => 1024]);
        echo $addBankModal->modalForm->field( $bankModel, 'description' )->textInput(['maxlength' => 1024]);

        $addBankModal->endModalForm();
        AddWidget::end();
    ?>

</div>



