<?php

use backend\controllers\CallsController;
use common\assets\TaskAsset;
use common\components\modalWidgets\AddressModalWidget;
use common\models\Calls;
use common\models\Machine;
use common\models\User;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use common\models\UsExt;
use backend\models\UsExtSearch;
use kartik\widgets\Select2;
use kartik\widgets\DateTimePicker;

use common\models\Task;
use common\models\Project;


/* @var $this yii\web\View */
/* @var $model common\models\Task */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="organisation-form">

    <?php $form = ActiveForm::begin([ 'action' => 'index.php?r=task/edit&id='.$model->id ]); ?>

        <h3><?= $model->name ?></h3>
        <?= $form->field($model, 'name')->textInput(['maxlength' => 1024]) ?>

        <?php
        TaskAsset::register($this);
        /*print_r($model);
        exit();*/
            $responsibleList = ArrayHelper::map( UsExt::getOwnOrgEmployees(), 'ID', 'shortName' );
            //$projectList = ArrayHelper::map( Project::find()->all(), 'id', 'name' );
        //exit();
           /* $parentList = ArrayHelper::map( Task::find()
                                                ->where( "level < ".$model->level )
                                                ->all(), 'id', 'name' );*/


            if( empty( $parentList ) )
                $parentList = [ 0 => 'Отсутствуют задачи высшего уровня!' ];

            unset( $parentList[ $model->id ] );
        ?>

        <div class = 'form-group'>
        <?php if (\Yii::$app->user->can( 'taskOriginator', [ 'task' => $model ] )): ?>
            <label class="control-label" for='responsible'>Ответственный</label>
            <?= Select2::widget( ['name'=>'responsible',
                                  'data' => $responsibleList,
                                  'id' => 'responsible',
                                  'value' => $model->taskResponsible->fKUser->ID,
                                 ]) ?>
        <?php endif;


        $phoneNum=Calls::find()->where(['FK_linkedid'=>$model->FK_linkedid])->one()->src; //Откуда звонили

        //Кто звонил из юзеров
        $user=UsExt::find()->where(['phone'=>$phoneNum])->one();
        $nameL=(!is_null($user))? $user->LastName : "Не найдено";
        $nameF=(!is_null($user))? $user->FirstName : "Не найдено";
        $nameP=(!is_null($user))? $user->PatName : "Не найдено";

        echo Html::label('От кого');
            echo Html::textInput('phone',$phoneNum,['class'=>'form-control']);
            echo Html::textInput('LName',$nameL,['class'=>'form-control']);
            echo Html::textInput('FName',$nameF,['class'=>'form-control']);
            echo Html::textInput('PName',$nameP,['class'=>'form-control']);
        echo Html::tag('br');
        if (is_null($user))
        echo Html::button('Добавить в базу',['class'=>'btn btn-primary']);
        echo Html::tag('br');
        echo Html::activeTextarea($model,'description',['rows'=>5,'class'=>'form-control',
        ]);
       // echo Html::textInput('description',$model->description,['rows'=>5,'class'=>'form-control']);
        echo '<br>';
        $modelR =Calls::find()->where(['FK_linkedid'=>$model->FK_linkedid])->one();
        echo CallsController::actionSimplePlayerOut($modelR->recfile, $modelR->id);
        ?>
        </div>

        <?= $form->field($model, 'priority')
                 ->radioList(
                 [
                    1 => '<font color = gray >Низкий</font>',
                    2 => '<font color = blue >Средний</font>',
                    3 => '<font color = red >Высокий</font>'
                 ],
                 [
                   'encode' => false,
                 ]);

        echo Html::label('Ответственный ' .$model->getResponsibleName(), 'lsresp',[]);

        $datas=['-1'=> 'Не задано'];
        $datas[]=ArrayHelper::map( UsExt::find()->all(), 'id',
            'LastName'
        );

        echo Select2::widget([
            'id'=>'lsresp',
            'name'=>'resp',
            'attribute'=>'id',
            'data' => $datas,

        ]);
        //  $refs['device_types'] = ArrayHelper::map( Machine::find()->all(), 'id', 'name' );
        ?>

        <?= Html::checkbox( 'is-deadline',
                            false,
                           [
                            'label' => 'Установить сроки',
                            'onchange' =>
                                    'javascript: if( this.checked )
                                                 {
                                                    $("#row-deadline").removeAttr("hidden");
                                                 }
                                                 else
                                                 {
                                                    $("#row-deadline").attr("hidden", true);
                                                 };'
                            ] ) ?>
        <div class = 'row' id = 'row-deadline' hidden>
            <div class = 'col-sm-6 field-deadline' >
                <?= DateTimePicker::widget([
                    'name' => 'deadline',
                    'value' => date( 'd-M-y' ),
                    'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                    'readonly' => true,
                    'removeButton' => false,
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'format' => 'dd/MM/yyyy HH:i P',
                        'autoclose' => true,
                        'todayBtn' => true,
                    ]
                ]);

                ?>
                <div class = 'help-block'></div>
            </div>
        </div>
        <br>
       <!-- <?= Html::checkbox( 'is-project',
                            ( isset( $model->FK_project ) ),
                           [
                            'label' => 'Ассоциировать с проектом',
                            'onchange' =>
                                    'javascript: if( this.checked )
                                                 {
                                                    $("#row-project").removeAttr("hidden");
                                                 }
                                                 else
                                                 {
                                                    $("#task-fk_project > option:selected").removeAttr( "selected" );
                                                    $("#row-project").attr("hidden", true);
                                                 };'
                            ] ) ?>
        <div class = 'row ' id = 'row-project' <?= !( isset( $model->FK_project ) )? 'hidden':'' ?>>
            <div class = 'col-sm-6 field-project' >
                <?php //echo $form->field( $model, 'FK_project' )->dropDownList( $projectList, ['prompt' => '-- Выберите проект --'] ) ?>
                <div class = 'help-block'></div>
            </div>
        </div>

        <br>
        <div class="form-group">
        <?= Html::checkbox( 'is-parent-task',
                            ( $model->parent_id !== 0 ),
                           [
                            'label' => 'Установить базовую задачу',
                            'onchange' =>
                                    'javascript: if( this.checked )
                                                 {
                                                    $("#row-parent-task").removeAttr("hidden");
                                                 }
                                                 else
                                                 {
                                                    $("#task-parent_id > option:selected").removeAttr( "selected" );
                                                    $("#row-parent-task").attr("hidden", true);
                                                 };'
                            ] ) ?>
        <div class = 'row ' id = 'row-parent-task' <?= ( $model->parent_id == 0 )? 'hidden':'' ?>>
            <div class = 'col-sm-6 field-parent' >
                <?= $form->field( $model, 'parent_id' )->dropDownList( $parentList, [ 'prompt' => '-- Выберите базовую задачу --' ] ) ?>
                <div class = 'help-block'></div>
            </div>
        </div>
        </div> -->
        <div class="form-group">
            <?php

            echo Html::submitButton( 'Сохранить', [ 'class' => 'btn btn-primary', 'id'=>'tasksubmit']) ?>
        </div>
    <?php
        ActiveForm::end();
    ?>
</div>
