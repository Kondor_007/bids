<?php

use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\Organisation */

$this->title = 'Редактирование задачи';
//$this->params['breadcrumbs'][] = ['label' => 'Задачи', 'url' => ['view-all']];
$this->params['breadcrumbs'][] =['label' => 'Звонки', 'url' => [Url::to('/calls')]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-edit">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
       // 'file_sound'=>"!!!",
    ]);
    //
    ?>

</div>