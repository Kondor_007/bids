<?php

// $model - yii\common\Task
// $key - Task::id

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Dropdown;
use common\models\TaskStatus;


	if( !function_exists( 'getPriorityIcon' ) )
	{
		function getPriorityIcon( $model )
		{
			$result = '';
			if( $model->currentStatus !== TaskStatus::TASK_FINISHED )
			{
			    switch ( $model->priority ) 
			    {
			        case 1:
			            $result .= '<span title = "Низкий" class="glyphicon glyphicon-time low-priority"></span>';
			        break;
			        
			        case 2:
			            $result .= '<span title = "Средний" class="glyphicon glyphicon-time medium-priority"></span>';
			        break;
			        
			        case 3:
			            $result .= '<span title = "Высокий" class="glyphicon glyphicon-time hight-priority"></span>';
			        break;

			    }
			    $result .= '&nbsp';
			}
			return $result;
		}
	}

	if( !function_exists( 'defaultFinishButton' ) )
	{
		function defaultFinishButton( $model )
		{
	        $result = '';
			$isOrigin = $model->isOriginator();
	        if( ( $model->isResponsible() ) || ( $isOrigin ) )
	        {
	            $result .= Html::a( '<span class="glyphicon glyphicon-flag finish-task-button"></span>', '#',
	                        [ 'title'=>Yii::t('yii', 'Finish task' ),
	                          'class' => ( $isOrigin )?'finishTaskButton':'putToInspect',
	                          'name' => $model->id,
	                          'id' => 'fin-task-'.$model->id,
	                        ]);
	        }
	        return $result;
		}
	}

	if( !function_exists( 'createPauseButton' ) )
	{
		function createPauseButton( $model )
		{
	        return  [
				        'label' => '<span class="glyphicon glyphicon-pause pause-task-button">&nbsp</span>Приостановить', 
				        'url' => '#', 
				        'linkOptions' => [
				                            'class'=>'pauseTaskButton',
				                            'title' => 'Приостановить',
				                            'name' => $key,
				                            'id' => 'pause-task-'.$key,
			                            ],
				    ];
		}
	}
	
	$actionsList = [];
	$url = '#';
	$html = '';
	
	//------ Статус задачи ------------------------
	switch ( $model->currentStatus ) 
	{
	    case TaskStatus::TASK_WAIT:
	        $html .= Html::a( '<span class="glyphicon glyphicon-play-circle start-task-button"></span>', $url,
	                    [ 'title'=>Yii::t('yii', 'Start task' ),
	                      'class' => 'startTaskButton',
	                      'name' => $key,
	                      'id' => 'task-'.$key,
	                    ]);
	        $html .= '&nbsp';
	        $html .= getPriorityIcon( $model );
	        $html .= "<span class='finish-task-button' id = 'fin-task-{$key}' ></span>";
	    break;
	    
	    case TaskStatus::TASK_STARTED:
	        $html .= "<span title = \"Выполняется\" class=\"glyphicon glyphicon-play task-started\" id = task-${key}></span>&nbsp"
	        		 .getPriorityIcon( $model ).defaultFinishButton( $model );

	    break;
	    
	    case TaskStatus::TASK_PAUSED:
	    	$html .= Html::a( '<span class="glyphicon glyphicon-pause task-paused"></span>', $url,
	                    [ 'title'=>Yii::t('yii', 'Продолжить выполнение' ),
	                      'class' => 'startTaskButton',
	                      'name' => $key,
	                      'id' => 'task-'.$key,
	                    ]).'&nbsp'.getPriorityIcon( $model ).defaultFinishButton( $model );
	    break;

	    case TaskStatus::TASK_FROZEN:
	    	$html .= Html::a( '<span class="glyphicon glyphicon-stop task-stoped"></span>', $url,
	                    [ 'title'=>Yii::t('yii', 'Продолжить выполнение' ),
	                      'class' => 'startTaskButton',
	                      'name' => $key,
	                      'id' => 'task-'.$key,
	                    ]).'&nbsp'.getPriorityIcon( $model ).defaultFinishButton( $model );
	    break;

	    case TaskStatus::TASK_WAIT_CHECK:
	        $html .= '<span title = "Ожидает контроль постановщика" class="glyphicon glyphicon-check task-wait-check"></span>';

	        if( \Yii::$app->user->can( 'isOriginator' ) )
	            $html .= Html::a( '<span class="glyphicon glyphicon-flag finish-task-button"></span>', $url,
	                    [ 
	                      'title'=>Yii::t('yii', 'Finish task' ),
	                      'class' => 'finishTaskButton',
	                      'name' => $key,
	                      'id' => 'fin-task-'.$key,
	                    ]).'&nbsp'.getPriorityIcon( $model ).defaultFinishButton( $model );
	    break;

	    case TaskStatus::TASK_FINISHED:
	    	$html .= '<span title = "Завершена" class="glyphicon glyphicon-ok task-finished"></span>';
	    break;

	    default:
	    break;
	}
	//------------------------------------------------------

	//----- Выпадающее меню ------------------------
	$actionsList = [
				        [
				            'label' => '<span class="glyphicon glyphicon-pause pause-task-button">&nbsp</span>Приостановить', 
				            'url' => '#', 
				            'linkOptions' => [
				                                'class'=>'pauseTaskButton',
				                                'title' => 'Приостановить',
				                                'name' => $key,
				                                'id' => 'pause-task-'.$key,
				                             ],
				        ],
				        [
				            'label' => '<span class="glyphicon glyphicon-stop stop-task-button">&nbsp</span>Отложить', 
				            'url' => '#',
				            'linkOptions' => [
				                                'class'=>'stopTaskButton',
				                                'title' => 'Отложить',
				                                'name' => $key,
				                                'id' => 'stop-task-'.$key,
				                             ],
				        ],

				        [
				            'label' => '<span class="glyphicon glyphicon-pencil edit-task-button">&nbsp</span>Редактировать', 
				            'url' => 'index.php?r=task/edit&id='.$key,
				            'linkOptions' => [
				                                'class'=>'editTaskButton',
				                                'title' => 'Редактировать',
				                                'name' => $key,
				                                'id' => 'edit-task-'.$key,
				                                //'data-toggle' => 'modal',
				                                //'data-target' => '#w0',
				                                'widget-id' => 'edit',
				                             ],
				        ],

				    ];

    if( \Yii::$app->user->can( 'editTask', [ 'task' => $model ] ) )
    {
        $actionsList[] = ['label' => '<span class="glyphicon glyphicon-remove remove-task-button">&nbsp</span>Удалить', 'url' => '#'];
    }

	$html .= '<div class="dropdown">
	          <a data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-tasks"></span></a>'.
	          Dropdown::widget([
	                'items' => $actionsList,
	                'encodeLabels' => false,
	            ]).
	          '</div>';

	echo $html;


	

?>