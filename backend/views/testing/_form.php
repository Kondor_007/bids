<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Calls */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="calls-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cid')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'dest_id')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'dt_start')->textInput() ?>

    <?= $form->field($model, 'dest_tl')->textInput() ?>

    <?= $form->field($model, 'dt_talk')->textInput() ?>

    <?= $form->field($model, 'dt_fin')->textInput() ?>

    <?= $form->field($model, 'rf')->textInput(['maxlength' => 256]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
