<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SearchCalls */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Тестовая панель';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="calls-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <p>
        <?= Html::a('Create Calls', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= Html::a("Монитор звонков",Url::to('index.php?r=calls'),['target'=>"_blank", 'class' => 'btn btn-primary']); ?>
    <?= Html::a("Имитировать звонок TODO: realise code add",Url::to('index.php?r=calls&cid=2-12-45'),['target'=>"_blank", 'class' => 'btn ']); ?>
    <?= Html::a("Положить трубку TODO: realise code add",Url::to('index.php?r=calls'),['target'=>"_blank", 'class' => 'btn ']); ?>


</div>
