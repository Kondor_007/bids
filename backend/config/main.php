<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    
    'components' => 
    [   
        'user' => 
        [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],


        // Эта часть может находиться как в бэкенде так и в common 
        // думаю в common будет смотреть ся лучше
        /*'view' => [
            'class' => 'yii\web\View',
            'theme' => [
                'basePath' => '@app/themes/modern',
                'baseUrl' => '@web/themes/modern',
                'pathMap' => [
                    '@app/views' => '@app/themes/modern',
                    //'@app/modules' => '@app/themes/modern/modules'
                 ],
            ],
        ],*/

        'log' => 
        [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => 
            [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManagerBackEnd' => 
        [
            'class' => 'yii\web\urlManager',
            'baseUrl' => '/crm-release/backend/web',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
    ],  
    'params' => $params,
];
