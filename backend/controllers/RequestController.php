<?php


namespace backend\controllers;

use common\models\Request;
use common\models\RequestSearch;
use common\models;
use common\models\RequestDraft;
use yii\base\Exception;
use yii\data\ArrayDataProvider;
use common\models\RequestProcess;
use common\models\UsExt;
use common\models\Machine;
use yii\helpers\ArrayHelper;
use common\models\PaymentType;
use common\models\MachineType;
use yii\helpers\Url;
use common\models\Material;
use common\models\ProcessType;
use common\models\Task;
use common\models\TaskResponsible;
use common\models\TaskStatus;
use common\models\User;
use yii\helpers\Json;
use common\models\Debug;
use common\classes\DepDropFormatter;
use common\models\AuthItem;
use common\models\ReqProcessUnit;
use common\models\MaterialUnit;
use common\models\Organisation;


class RequestController extends \yii\web\Controller
{
    public function actionIndex()
    {
        /*
        echo '<pre>';
        
        print_r(User::find()->where(['id'=>\yii::$app->user->identity->id])->one()->profile->email);
        print_r(User::find()->where(['id'=>\yii::$app->user->identity->id])
        ->leftJoin('auth_assigment',['on' => "user_id=".\yii::$app->user->identity->id])->all();
        //->one()->profile->assignment->attributeLabels->item_name);
        echo '</pre>';
        exit; 
        */
                
    	$searchModel = new RequestSearch();
    	
    	$dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
             'searchModel' => $searchModel,
             'var_access'  => $this->getAccess($searchModel)
             
             ]);
    }
    
    public function actionCreate( $id = null, $is_edit=false )
    {
    	//$model;
    	$model = Request::findOne(\Yii::$app->request->post('RequestProcess')['FK_request']);
        //return "hi there";
        if (\Yii::$app->request->post()) {         
           // return print_r(\Yii::$app->request->post());
/*
              echo '<pre>';
              print_r (\Yii::$app->request->post());
              print_r (\Yii::$app->request->post('RequestProcess'));
              echo '</pre>';
              exit;
  */          
            $Processes=\Yii::$app->request->post('RequestProcess');
        
        
        $process = new RequestProcess();
        $processesProvider = new ArrayDataProvider([
    			'allModels' => RequestProcess::findAll([ 'FK_request' => $Processes['FK_request']]),
    	]);
         
            
            /*if (isset($model->FK_task)){
                $modelT = Task::findOne($model->FK_task);
            } else {
                $modelT= new Task;    
            }
            $task['Task']['FK_originator']=\Yii::$app->user->identity->id;
            $task['Task']['name']='Работы по Заказу № '.\Yii::$app->request->post('RequestProcess')['FK_request'];
            $task['Task']['description']='выполнить работу:';
            $modelT->load($task);
            
            
            $modelT->save();*/
        
        
// создаю  task_responsible
          /*  if (isset($model->FK_task)){
                $modelR = TaskResponsible::findOne(['FK_task'=>$model->FK_task]);
            } else {
                $modelR= new TaskResponsible;    
            }            
            $task['TaskResponsible']['FK_user']=\Yii::$app->request->post('TaskResponsible')['FK_user'];
            $task['TaskResponsible']['type']=\Yii::$app->request->post('TaskResponsible')['type'];
            $task['TaskResponsible']['FK_task']=$modelT->id;
            $modelR->load($task);
            $modelR->save();*/
			// создаю  task_status
         /*  $modelS = new TaskStatus;
            $task['TaskStatus']['FK_user']=\Yii::$app->user->identity->id;
            $task['TaskStatus']['FK_status']='1';
            $task['TaskStatus']['FK_task']=$modelT->id;
            $task['TaskStatus']['description']='Первичная';
            
            $modelS->load($task);
            $modelS->save();*/
            
          /*  
            echo '<pre>';
            print_r($task);
            echo '</pre>';
         exit;
         */
         
/*
        // создаю задачу
        $modelT= new Task;
        $task['Task']['FK_originator']=\Yii::$app->user->identity->id;
        $task['Task']['name']='Работы по заказуЗаказ № '.;

        $modelT->load($task);
        $modelT->save();
        
        echo 'создана задача №: '.$modelT->id;
        exit;
        //$modelZ->FK_originator = \Yii::$app->user->identity->id;
*/
        //$model = Request::findOne(\Yii::$app->request->post('RequestProcess')['FK_request']);
        /*if (is_null($model))
        { echo "!!!";
        exit();
        }
        print_r($model);
        exit();*/
        //TODO saving model
       /*if (!is_null($model)){
        	$modelT = Task::findOne($model->FK_task);
        } else {
        	$modelT= new Task;
        	$Request = \Yii::$app->request->post();
        	$Request['Request']['FK_task']=$modelT->id;
        	echo '<pre>';
        	print_r ($Request);
        	echo '</pre>';
        	exit;
        	
        	if ($model->load($Request)) {
        		echo '<pre>';
        		print_r ($model->errors);
        		echo '</pre>';	
        }*/
        //::model()->findByPK(\Yii::$app->request->post('RequestProcess')['FK_request']);
 //           echo "post";
 //           echo '<pre>';
            //print_r (\Yii::$app->request->post('Request'));
            $Request = \Yii::$app->request->post();
            //$Request['Request']['FK_task']=$modelT->id;
            /*echo '<pre>';
            print_r ($Request);
            echo '</pre>';*/
            //exit;

        if ($model->load($Request)) {
           /* echo '<pre>';
            print_r ($model->errors);
            echo '</pre>';*/
            //exit();
        //$model->isNewRecord = false;
        //$model->description = 'new desc';



        $model->update();
            $items=RequestDraft::find()->where('FK_request='.$model->id)->all();
            /*print_r($items);
            exit();*/
            $req_cost = 0;
            foreach($items as $item)
                $req_cost+=$item->cost;
            $model->cost = $req_cost;
            $model->save();
           /*  echo $req_cost;
            echo $model->cost;
                exit();*/
        }
        //выход после сохранения
        $this->redirect('index.php?r=request/create&id='.$model->id);
       // $this->redirect(array('request/index'));//,['otvet'=>'Заказ №'.$model->id.' успешно сохранен!']));
       // $this->actionIndex();
       /* $this->redirect(array('request/create',['id'=>62]));
        TODO*/
       // return \Yii::$app->getResponse()->redirect(Url::to('?r=request/create&id='.'68'), 302);
        } 
      
        //По GET запросу приходит только создание нового заказа с нуля.
        //сохраним и редиректнем.
    	if(( $id == null )) {
    		echo "id empty";
    		//exit();
    		if (is_null($model)){
    			echo "model empty";
    			
	    	//$model = Request::findEmpty();
	    	//if( !$model ) {
	    		$model = new Request;
	    		//$model->FK_client
                $model->FK_user_owner = \Yii::$app->user->identity->id;
                
	    		echo $model->save();
	    		echo $model->id;
	    		//exit();
	    		$this->redirect('index.php?r=request/create&id='.$model->id);
	    		
	    	}
	    	$this->redirect('index.php?r=request/create&id='.$model->id);
    	} else {
    		$model = Request::findOne( $id );
    	}
    	$draft = new RequestDraft;
    	$process = new RequestProcess(); 
        /*if (isset($model->FK_task)){
            $responsible = TaskResponsible::findOne(['FK_task'=>$model->FK_task]);    
        } else {
            $responsible = new TaskResponsible();
        }*/
        
    	
        
    	$draftsProvider = new ArrayDataProvider([ 
    			'allModels' => RequestDraft::findAll([ 'FK_request' => $model->id ]),
    	]);
    	
    	$processesProvider = new ArrayDataProvider([
    			'allModels' => RequestProcess::findAll([ 'FK_request' => $model->id ]),
    	]);
        
      /*  $responsibleProvider = new ArrayDataProvider([
    			'allModels' => TaskResponsible::findAll([ 'FK_task' => $model->FK_task ]),
    	]);*/

        //TODO: Кандидат на выделение в функцию
    	$refs['clients'] = ArrayHelper::map( User::findOne( \yii::$app->user->identity->id )->getClients(), 'id', 'fullName');
    	//Вытащим список организаций, соответствующих каждому клиенту
    	//И приклеим к полному имени
    	$clients=User::findOne( \yii::$app->user->identity->id )->getClients();

    	//print_r($refs['clients']);
    	$ids =ArrayHelper::map( User::findOne( \yii::$app->user->identity->id )->getClients(), 'id', 'id');
    	//print_r($ids);
    	foreach($ids as $idi){
    		$org=Organisation::findOne(
    				UsExt::find()->where('FK_users='.$idi)->one()->FK_org);
    				if (!is_null($org))
    					$refs['clients'][$idi]=$refs['clients'][$idi]."  орг. ".$org->getName()."";
    		//echo $refs['clients'][$idi];
    	}

    	//print_r($clients);
    	//exit();
    	$refs['device_types'] = ArrayHelper::map( Machine::find()->all(), 'id', 'name' );
        //$refs['device_types'] = ArrayHelper::map( Machine::find()->where(['is_del' => '0'])->all(), 'id', 'name' );
        
    	$refs['devices'] = []; 
    	$refs['materials'] = ArrayHelper::map( Material::find()->all(), 'id', 'name' );; 
    	$refs['quality'] = []; 
    	$refs['process_types'] = ArrayHelper::map( ProcessType::find()->all(), 'id', 'name' );
    	$refs['payment_types'] = ArrayHelper::map( PaymentType::find()->all(), 'id', 'name' );
    	$refs['draftsProvider'] = $draftsProvider;
    	$refs['processesProvider'] = $processesProvider;
       // $refs['responsibleProvider'] = $responsibleProvider;
        
       // $refs['activeStatusTask'] = TaskStatus::find()->joinWith('taskStatusRef')->where(['task_status.FK_task'=>$model->FK_task])->andWhere('task_status.is_top = 1')->one();
       // $refs['Processes']
        //return print_r( TaskStatus::find()->leftJoin('task_status_ref',' task_status.FK_status = task_status_ref.id ')->where(['task_status.FK_task'=>$model->FK_task])->andWhere('task_status.is_top = 1')->one());
//        return print_r( TaskStatus::find()->joinWith('taskStatusRef')->where(['task_status.FK_task'=>$model->FK_task])->andWhere('task_status.is_top = 1')->one());
        //$refs['responsible_list'] = ArrayHelper::map( User::find()->clients()->all(), 'id', 'fullName' );
        
        
        // список макетов в текущем проекте
        $refs['draft_list']=ArrayHelper::map(RequestDraft::findAll(['FK_request' => $model->id]),'id','name');
        
        
        
/**
         *         echo '<pre>';
         *         print_r ($refs['responsible_list']) ;
         *         echo '</pre>';
         *         exit;    	
         */        
    	//$refs['user_responsible'] = ArrayHelper::map( User::findOne( \yii::$app->user->identity->id )->getClients(), 'id', 'fullName'); 
    	
 
    	
        
        if ($is_edit)
    	return $this->render('_ispoln', [ 
    			'model' => $model,
    			'draft' => $draft,
    			'process' => $process,
               // 'responsible' => $responsible,// ответственные  
    			'refs' => $refs,
                'var_access' => $this->getAccess($model),
    	]);
    	else
    		return $this->render('create', [
    				'model' => $model,
    				'draft' => $draft,
    				'process' => $process,
    			//	'responsible' => $responsible,// ответственные
    				'refs' => $refs,
    				'var_access' => $this->getAccess($model),
    		]);
    }
    
    public function actionAttachDraft()
    {
    	/*$draft = new RequestDraft();
        //return "!!!";
        //ServiceController::actionAlert("");
    	if( $draft->load( \Yii::$app->request->post() )
            && $draft->save(false)
        ) {
                      
    	 	$uri = $this->saveDraftFile( $_FILES );
    		
    		$draft->file_path = ( $uri?$uri:null);
            //ServiceController::actionAlert($draft->file_path);
    	 	$draft->save();
    	 	return json_encode(['status' => 1, 'error' => print_r( $draft->getErrors(), true ) ]);
    	} else {
            return json_encode(['status' => 1, 'error' => print_r( $draft->getErrors(), true ) ]);
    		//return json_encode(['status' => 99, 'error' => $draft->getErrors() ]);
    	}*/

    	print_r(\Yii::$app->request->post());
        $model  = new RequestDraft( );
        //echo implode( \Yii::$app->request->post());
        //$request = $model->FK_request;
        /*
         * 
    [RequestDraft] => Array
        (
            [name] => sadsad
            [type_dev] => 1
            [FK_material] => 13
            [FK_work_type] => 7
            [unit] => шт
            [process_unit] => метр
            [height] => 0
            [width] => 0
            [work_space] => 0
            [blank_space] => 0
            [price] => 0
            [amount] => 0
            [cost] => 0
            [tempF] => 
            [file_path] => 
            [FK_request] => 53
        )
        In Model:
        Array
(
    [id] => 
    [FK_material] => 
    [FK_request] => 
    [amount] => 
    [work_space] => 
    [blank_space] => 
    [height] => 
    [width] => 
    [price] => 
    [cost] => 
    [file_path] => 
    [FK_quality] => 
    [name] => 
    [FK_work_type] => 
)
         */
        
        $post=\Yii::$app->request->post();
        $post['RequestDraft']['process_unit']=ReqProcessUnit::find()->where("unitname='". $post['RequestDraft']['process_unit']."'")->one()->id;
        
        $post['RequestDraft']['unit']=MaterialUnit::find()->where("name='". $post['RequestDraft']['unit']."'")->one()->id;
        $post['RequestDraft']['unit']=$post['RequestDraft']['price_blank_space'];

        
        $loaded = $model->load( $post,'RequestDraft');
       // print_r($model->attributes);
       /* $model->unit=MaterialUnit::find()->where("name='". $post['RequestDraft']['unit']."'")->one()->id;
        $model->process_unit=ReqProcessUnit::find()->
        where("unitname='". $post['RequestDraft']['process_unit']."'")->one()->id;
        $res='';*/
       
        
        /**
         * TODO: VALIDATE CHECK!
        */
        print_r($post);
        //exit();
        $saved=$model->save(false);
        return  json_encode(['status' => 1, 'error' => $model->getErrors(),
        'loaded'=>$loaded,
        		'saved'=>$saved,
        	        ]);//$this->actionCreate( $request );
    }



   
    public function actionAttachProcess()
    {
    	$post=\Yii::$app->request->post();
    	$post['RequestProcess']['deadline']= $post['RequestProcess']['deadline']." "."10:10:10";
    	print_r($post['RequestProcess']);
    	$process = new RequestProcess();
    	
    	$loadflag=$process->load( $post['RequestProcess']);
    	/**
    	 * [RequestProcess] => Array
        (
            [FK_draft] => 1
            [FK_process_type] => 
            [amount] => 
            [deadline] => 
            [type_resp] => 
            [FK_request] => 53
        )
    	 * */
    	//$process->load($post['RequestProcess']);//getAttributes();
    	$process->amount=$post['RequestProcess']['amount'];
    	$fkproc = ProcessType::find()->where("name='".$post['RequestProcess']['FK_process_type']."'")->one()->id;
    	$process->FK_process_type=$fkproc;//$post['RequestProcess']['FK_process_type'];
    	$process->FK_request=$post['RequestProcess']['FK_request'];//53; //RequestProcess
    	$process->FK_draft=$post['RequestProcess']['FK_draft'];
    	$process->deadline=$post['RequestProcess']['deadline'];
    	$process->type_resp = $post['RequestProcess']['type_resp'];
    	//$process->FK_role="w_frezer";
    	//print_r($process->FK_user = $post['RequestProcess']);
    	//exit();
    	//ServiceController::actionAlert();
    	if ($post['RequestProcess']['type_resp']==1)
    	$process->FK_user = $post['RequestProcess']['FK_user'];
    	else
    		if ($post['RequestProcess']['type_resp']==4)
    			$process->FK_role = $post['RequestProcess']['FK_user'];
       /* $process->deadline=\Yii::$app->request->post('deadline',"1100:00:00");
    	$process->deadline=$process->deadline." "."10:10:10";*/
    	$saveflag = false;
    	try {
    	 $saveflag = $process->save(false);
    	} catch (Exception $e) {
    		$saveflag = false;
    	}
    	//$mmap = ArrayHelper::map
    	/*if( $process->load( \Yii::$app->request->post() ) && $process->save(false) ) {
    		return json_encode(['status' => 1 ]);
    	} else {
    		return json_encode(['status' => 99, 'error' => $process->getErrors() ]);
    	}*/
    	return json_encode(['status' => 1, 'loaded'=>$loadflag, 
    			'saved'=>false,
    			'vals'=>implode("|",$process->attributes),
              //'reqvals'=>implode("|",\Yii::$app->request/*$process->attributes*/),
           ]);
         //'req'=> implode(',',\Yii::$app->request->getParams())]);
    }
    
    
    public function actionAttachMyProcess()//$amount=1, $price=1, $cost=1, $FKProcType=1,
    //$FKReq=1,
    //  $fkdraft=1,$deadline="2000-01-01", $worker="") //*/
    {
    	$process = new RequestProcess();
    	$post=\Yii::$app->request->post();
    	$loadflag=$process->load( $post);
    
    	//echo "AAAAA";
    	//        $post=\Yii::$app->request->post();
    
    	// $process->amount=$post['RequestProcess']['FK_draft'];
    	// $process->price=$price;
    	//$process->cost=$cost;
    	// $process->FK_process_type=$FKProcType;
    	//$process->FK_request=$FKReq;
    	/* $fkdraftM = RequestDraft::find()->where('name='.$fkdraft)->one()->id;
    	 $process->FK_draft=$fkdraftM;
    	 $process->price = RequestDraft::findOne($fkdraftM)->price;
    	 $process->cost = RequestDraft::findOne($fkdraftM)->cost;*/
    	// $process->deadline=$deadline." 10:10:10";//\Yii::$app->request->post('deadline',"1100:00:00");
    	// $process->deadline=$process->deadline." "."10:10:10";
    	//  return "!!!!";
    	return json_encode(['status' => 1,
    
    			'vals'=>implode("|",$process->attributes),
    			'loaded'=>$loadflag,
    			'posted'=>implode(';',$post),
    			/* 'ename'=>$fkdraftM." ".$fkdraft,
    			 'workuser'=>$worker,
    	'saaa'=>$deadline,*/
    	]);
    
    	$saveflag = false;
    	try {
    		//$saveflag = $process->save(false);
    	} catch (Exception $e) {
    		$saveflag = false;
    	}
    	//$mmap = ArrayHelper::map
    	/*if( $process->load( \Yii::$app->request->post() ) && $process->save(false) ) {
    	 return json_encode(['status' => 1 ]);
    	 } else {
    	 return json_encode(['status' => 99, 'error' => $process->getErrors() ]);
    	 }*/
    	return json_encode(['status' => 1, 'loaded'=>$loadflag,
    			'saved'=>$saveflag,
    			'saaa'=>$deadline,
    			'vals'=>implode("|",$process->attributes),]);
    	//'req'=> implode(',',\Yii::$app->request->getParams())]);
    }
    
    
    public function actionDeleteProcess( $id, $reqid=null )
    {
    	echo $id.$reqid;
    	//exit();
    	$model1 = RequestProcess::findOne($id);
    	//print_r($model1);
    	//exit();
    	if (!is_null($model1)){
    	$request1 = $model1->FK_request;
    	//КОСТЫЛЬ.
    	//Все удаления в релизе заменить на установку в 1 поля is_del соответствующей таблицы.
    	//При необходимости добавить это поле с дефолтным value=0
    	$model1->delete();
    	return $this->actionCreate( $request1 );
    	}
    	else
    		return $this->actionCreate( $reqid );
    	/*catch(\Exception $e)
    	{
    		print_r($model1);
    		echo $id;
    		//exit();
    		LogController::actionLogAdd(\Yii::$app->user->id, $_SERVER['REMOTE_ADDR'], 'method actionDeleteProcess. null model on delete process '.$id.' from request processes', $id);
    		 $this->redirect(array('request/index'));
    	}*/
    }
    
    public function actionDeleteDraft( $id, $codereq=null )
    {
    	//echo $id.$codereq;
    	//exit();
    	$model = RequestDraft::findOne( $id );
    	//print_r($model);
    	//echo $model->FK_request;
    	//exit();
    	if (!is_null($model)){
    	$request = $model->FK_request;
    	$model->delete();
    	return $this->actionCreate( $request );
    	}
    	else
    		return $this->actionCreate( $codereq );
    		
    }
    
    public function saveDraftFile( $params )
    {
    	if( empty( $params ) || !isset( $params['RequestDraft'] ) ) {
           // ServiceController::actionAlert("empty params");
    		return false;
    	}
    	
        
        
    	$path = \Yii::getAlias('@webroot').'/res/files/drafts/'.date('d.m.Y');

    	$uri = \Yii::getAlias('@web').'/res/files/drafts/'.date('d.m.Y');
    	if( !is_dir( $path ) ) {
    		mkdir( $path, 0777, true );
    	}
    	$file_info = pathinfo($params['RequestDraft']['name']['file_path']);
        echo $file_info;
        $newfilename = md5($params['RequestDraft']['name']['file_path'].time()).'.'.$file_info['extension'];
        
        $debug = new Debug();
            $debug->name = $newfilename;
            $debug->save();
        
    	if( !move_uploaded_file( $params['RequestDraft']['tmp_name']['file_path'], 
    							$path."/".$newfilename ) ){
    		return false;
    	}
    	
    	return $uri."/".$newfilename;
    }

    public function actionView()
    {
        //$this->redirect(array("/request/create",['id'=>$_GET['id']]));
        if (!isset($_GET['nocreate']))
        	//Создание заказа, обычный режим
        return \Yii::$app->getResponse()->redirect(Url::to('?r=request/create&id='.$_GET['id']), 302);
        else
        	//Редактироваиние заказа, режим нового оформления (см эскизы Николая и ТЗ)
        	return \Yii::$app->getResponse()->redirect(Url::to('?r=request/create&id='.$_GET['id']).'&is_edit=true', 302);
        	
    }    
    
    
    
    public function actionGetListusers($type=null){
 
 $out =[];
 if ($type==1){
        $org = User::find()->where( [ 'id' => \Yii::$app->user->identity->id ])->one()->profile->FK_org;
        $out = ArrayHelper::map(User::find()->joinWith(['profile'])
                ->where("us_ext.FK_org = $org")
                //->andWhere("us_ext.FK_users <> ".\Yii::$app->user->identity->id)
                ->all(),'id','fullName');

        } else {
          $out = ArrayHelper::map(AuthItem::find()
                ->where("type = 1 and name like 'w_%'")
                //->andWhere("us_ext.FK_users <> ".\Yii::$app->user->identity->id)
                ->all(),'name','description');
        }

    return $out;
        
    }

    //================================================================
    public function actionGetMaterial(){
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $type = $parents[0];
                $out = self::actionGetListMaterials($type);
                //ServiceController::actionConsoleLog("adsad");
                //echo $out;
                echo Json::encode(['output' => DepDropFormatter::format($out), 'selected' => '']);
                //exit();
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    //TODO: Допилить выборку производителя и подклеить к имени материала
    public function actionGetListMaterials($type=null){
            $out = ArrayHelper::map(Material::find()
                ->where("is_del = 0 and FK_machine_type=".$type)
                ->all(),'id','FullInfo');

        //print_r($out);
       // exit();
        $ids =Material::find()->all();
       /* foreach ($ids as $iter) {
            $idd = 44;
            str_replace( $iter->id, " ", $idd);
            $out[44] = $out[$idd] . "!!!!";
        }*/
       // $tmp = ArrayHelper::map( User::findOne( \yii::$app->user->identity->id )->getClients(), 'id', 'fullName');
        //Вытащим список организаций, соответствующих каждому клиенту
        //И приклеим к полному имени

        //print_r($ids);
        /*foreach($ids as $idi){
            $man="!!!";//Material::findOne($idi)->is_del;
            //if (!is_null($org))
                $out[$idi]=$out[$idi]."  орг. ".$man."";
            //echo $refs['clients'][$idi];
        }*/
        return $out;

    }



    public function actionGetWorkType(){
        if (isset($_POST['depdrop_parents'])) {
           // echo "!!!!";
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $type = $parents[0];
                $mmap=[0=>"Aaa"];
                foreach (ProcessType::find()
                             ->where("fk_machine =".$type." AND is_del=0")
                             ->all() as $iter)
                    $mmap[]=[$iter->id => $iter->name];
              //  $out = ArrayHelper::map(mmap);
              $out =ArrayHelper::map(ProcessType::find()
                    ->where("fk_machine =".$type." AND is_del=0")
                    ->all(),'id','name');
                echo Json::encode(['output' => DepDropFormatter::format($out), 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    public function actionGetProcessTypeByDraft($code=null){
           try {
               $out =ProcessType::findOne( RequestDraft::findOne($code)->FK_work_type)->name;

           }
           catch(Exception $err)
           {
               $out="SOS";
           }
            return $out;
      //  return $code;


    }

    public function actionGetWorkName(){
        if (isset($_POST['depdrop_parents'])) {
            //echo "!!!";
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $type = $parents[0];

              //  $out = ArrayHelper::map(mmap);
              $out =ArrayHelper::map(ProcessType::findOne($type),'id','name');
                echo Json::encode(['output' => DepDropFormatter::format($out), 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
       // print_r($_POST);
    }

    public function actionGetWorkUnit(){
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $type = $parents[0];
              //  ServiceController::actionAlert($type);
                $out =ArrayHelper::map(models\ReqProcessUnit::find()
                    ->where("id =".$type)
                    ->all(),'id','unitname'); // self::actionGetListWorkTypes($type);
                echo Json::encode(['output' => DepDropFormatter::format($out), 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    public function actionGetMaterialInfoForDraft($code){
    
    	try {
    		//$name = models\MaterialUnit::findOne(Material::findOne($code)->FK_unit)->name;
    		$material = Material::findOne($code);
    		$sellprice = $material->sell_price;
    		return  json_encode(['status' => 1, 'error'=>[],
    				'sellprice'=>$sellprice,
    				'width'=>$material->width,
    				'length'=>$material->length,
    				
    		]);
    	}
    	catch (Exception $e){
    		return json_encode(['status' => 99, 'error'=>[], 'name'=>$e->getName().$e->getTraceAsString() ]);
    	}
    
    }
    public function actionGetMaterialUnit($code){

        try {
            $name = models\MaterialUnit::findOne(Material::findOne($code)->FK_unit)->name;
            $sellprice = Material::findOne($code)->sell_price;
            return  json_encode(['status' => 1, 'error'=>[], 'name'=>$name,
            		'sellprice'=>$sellprice,
            ]);
        }
        catch (Exception $e){
            return json_encode(['status' => 99, 'error'=>[], 'name'=>$e->getName().$e->getTraceAsString() ]);
        }

    }

    public function actionGetProcessUnit($code){

        try {
            $name = models\ReqProcessUnit::findOne(ProcessType::findOne($code)->fk_p_type_unit)->unitname;
            return  json_encode(['status' => 1, 'error'=>[], 'name'=>$name ]);
        }
        catch (Exception $e){
            return json_encode(['status' => 99, 'error'=>[], 'name'=>"" ]);
        }

    }
   /* public function actionGetListWorkTypes($type=null){
        $out = ArrayHelper::map(ProcessType::find()
            ->where("fk_machine =".$type)
            ->all(),'id','name');
        return $out;

    }*/
    //================================================================

    public function actionGetResponsibles(){
        $out = [];

       // ServiceController::actionConsoleLog("EE");
       // echo "!!!";
        if (isset($_POST['depdrop_parents'])) {
        $parents = $_POST['depdrop_parents'];
        if ($parents != null) {
            $type = $parents[0];
            $out =  self::actionGetListusers($type);
            echo Json::encode(['output'=>DepDropFormatter::format( $out ), 'selected'=>'']);
            return;
        }
    } 
    echo Json::encode(['output'=>'', 'selected'=>'']);
    }
    
    public function actionSprav(){
    $model = Machine::find()->one();
        $this->render('sprav',['model'=>$model]);
    }
    
    public function actionDownloadMaket($id){
        $model = RequestDraft::findOne($id);
        

         
        
        $fp = substr(\Yii::getAlias('@webroot'),0,strpos(\Yii::getAlias('@webroot'),'/backend/web')).$model->file_path;
        
        $file_ext = substr($model->file_path,-3);

         
        header("Content-Length: ".filesize($fp)." "); 
        header("Content-Disposition: attachment; filename=out.".$file_ext.""); 
        header("Content-type: application/force-download");
        header("Content-Transfer-Encoding: binary");
        readfile($fp);
        exit;

/*        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Description: File Transfer");
        
        $x=fread(fopen($fp, "r"), filesize($fp));
        echo $x; exit();
        */
        
        /*
        $handle=fopen($fp, "r");
        $contents = fread($handle, filesize($fp)); 
        echo $contents; 
        fclose($handle);
        */
        //echo file_get_contents($path_name);
        
        //return \Yii::getAlias('@webroot');
        //file_get_contents ($fp); 
  /*
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=out.'. $file_ext);
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($fp));
    // читаем файл и отправляем его пользователю
    readfile($fp);
    exit;
*/        
        
    }
    
    public function actionInWork($id,$newval, $wasInWork=false, $is_continue=false){
        /*$request= Request::findOne($id);
        
        TaskStatus::updateAll(['is_top' => 0], 'is_top = 1 and FK_task='.$request->FK_task);*/
        
        //$task = TaskStatus::findAll(['FK_task'=>$request->FK_task,'is_top'=>1]);
        /*echo '<pre>';
        print_r($task);
        echo '</pre>';
        exit;
        */
        //$task->updateAll(['is_top'=>0],'is_top = 1');
        
        /*
         * if ($iterator->state == 0) $procent+=0; // назначен, но не взят в работу
        	if ($iterator->state == 1) $procent+=10;// взят в работу
        	if ($iterator->state == 2) $procent+=10; // приостановлен
        	if ($iterator->state == 3) $procent+=90; //сдан наконтроль
        	if ($iterator->state == 4) $procent+=100; // контроль прошел (принят)
         * */
    	/*if ($wasInWork)
            exit();*/
    	$reqproc=RequestProcess::findOne($id);
    	//Ставлю состояние участку
    	$logstr="change in request sectors; участок  ".$id." ";
    	if ($newval==1){//новое состояние участка
    		//изменим тип ответственности если была групповой на индивидуальную
    		
    		//КОСТЫЛЬ
    		//if (!is_null($reqproc))
    		
    		/*Проверки:
    		 * 1) Можем ли мы взять участок?
    		 * 1.1) Кол-во >0?
    		 * 1.2) Материала > NeedCount ?
    		 *  2) Нет - алерт нелзя, да - берем
    		 *  3) Меняем ответственного
    		 *  	Меняем ссостояние участка
    		 *    	Списываем материалы*/
    		//1.1) Кол-во >0?
    		if ($reqproc->amount>0)
    		{ 
    			$material=Material::findOne(RequestDraft::findOne(RequestProcess::findOne($id)->FK_draft)->FK_material);
    			$MaterialHaveCount=$material->count;
    			
    			
    			//На весь участок (все повторы) нужно материала
    				$MaterialNeed=RequestProcess::findOne($id)->amount * 
    				RequestDraft::findOne(RequestProcess::findOne($id)->FK_draft)->amount;
    				
    				//1.2) имеющегося Материала > нужного ?
    				if ($MaterialHaveCount > $MaterialNeed){
    					/* Приемка
    					 * Меняем ответственного
    					 *  	Меняем ссостояние участка
    			 		*    	Списываем материалы ТОЛЬКО для первого принятия участка 
    			 		*    (повторы учитываются в полном объеме)
    					 * */
    					$reqproc->FK_user=\Yii::$app->user->id;
    					$reqproc->type_resp=1;
    					if ($reqproc->state != 2) //Он не был на паузе, уменьшать кол-во повторов  надо
    					$reqproc->amount=$reqproc->amount -1; 
    					//$reqproc->save(false);
    					
    					
    					//$proc=RequestProcess::findOne($id);
    					$reqproc->state=$newval;
    					$reqproc->save(false);
    					$logstr.="кол-во оставшихся повторов: ".$reqproc->amount;
    					ServiceController::actionConsoleLog('in work');
    					//$material=Material::findOne(RequestProcess::findOne($id)->FK_material);
    					$reqproc=RequestProcess::findOne($id);
    					$wasInWork=$reqproc->was_in_work==1;
    						/*if ($reqproc->was_in_work==0)
    							$reqproc*/
    					if (!$wasInWork){
    						$logstr.=" материал ".$material->name." нужно (есть): ".$MaterialNeed." (".$MaterialHaveCount.")";
    						$reqproc->was_in_work=1;
    						$reqproc->update(false);
    					$material->count =
    					($MaterialHaveCount- $MaterialNeed);
    			
    					$material->save(false);
    			
    					$logstr.=" материал ".$material->name." списано ".$MaterialNeed."\n";
    					//print_r($material->count);
    					ServiceController::actionAlert('Обновлены материалы.');
    					}
    			
    			
    				}
    				else { 
    					ServiceController::actionAlert('Не хватает материалов на складе для совершения операции.');
    				$logstr.=" не хватает материалов (".$material->name.") для работы";
    				}
    		}
    		else 
    		{
    			
    		}
    		/*if ($reqproc->type_resp==4){
    		
    		//$reqproc->amount--;
    		//$reqproc->FK_role=null;
    		$reqproc->FK_user=\Yii::$app->user->id;
    		$reqproc->type_resp=1;
    		$reqproc->save(false);}*/
    		
    	
    		//$this->redirect(array("/request/index")); 
    		//Списываю со склада только если участок еще не был в работе
    		/*if (!$wasInWork){
    			$logstr.=" впервые на исполнении";
    		//echo Material::findOne(RequestDraft::findOne(RequestProcess::findOne($id)->FK_draft)->FK_material)->count;
    		$MaterialHaveCount=Material::findOne(RequestDraft::findOne(RequestProcess::findOne($id)->FK_draft)->FK_material)->count;
    		if (RequestProcess::findOne($id)->amount >0)
    		{
    		$MaterialNeed=RequestProcess::findOne($id)->amount * RequestDraft::findOne(RequestProcess::findOne($id)->FK_draft)->amount;
    			if ($MaterialHaveCount > $MaterialNeed){
    				ServiceController::actionConsoleLog('in work');
    				$material=Material::findOne(RequestProcess::findOne($id)->FK_material);
    				$material->count =
    				($MaterialHaveCount- $MaterialNeed);
    				
    				$material->save(false);
    				
    				$logstr.=" материал ".$material->name." списано ".$MaterialNeed."\n";
    				//print_r($material->count);
    				ServiceController::actionAlert('Обновлены материалы.');
    				
    				
    			}
    			else { ServiceController::actionAlert('Не хватает материалов на складе для совершения операции.');
    			$logstr.=" не хватает материалов для работы";
    			}
    		}
    		else{ ServiceController::actionAlert('В задании осталось 0 необходимых выполнений участка');
    		$logstr.=" нечего выполнять";}
    		}
    		else
    		{

    		}
    		$proc=RequestProcess::findOne($id);
    		$proc->state=$newval;
    		$proc->save();*/
    	}
    	else
    	{
    		$proc=RequestProcess::findOne($id);
    		$proc->state=$newval;
    		$proc->save();
    	}
       /* switch ($newval){
        	case 0:
        		break;
        		case 1:
        			$proc=RequestProcess::findOne($id);
        			$proc->state=$newval;
        			$proc->save();
        			break;
        }*/
       /* $newtask = new TaskStatus;
        $newtask->FK_user = \Yii::$app->user->identity->id;
        $newtask->FK_status = $newval;
        $newtask->FK_task = $request->FK_task;
        $newtask->save();*/
    	LogController::actionLogAdd(
    			\Yii::$app->user->id,$_SERVER['REMOTE_ADDR'],$logstr,
    			implode(" | ",$_REQUEST)
    			);
    	ServiceController::actionConsoleLog($logstr);
    	
    	//ServiceController::actionAlert($logstr);
    	//sleep(5);
    	//ServiceController::actionAlert();
    	//sleep(5);
        // $this->actionIndex();
        //http://localhost:8081/dial/backend/web/index.php?r=request/create&id=68&is_edit=true
        //Url::to('?r=request/create&id='.$_GET['id']).'&is_edit=true'
      //  $this->redirect(array(Url::to('?r=request/create&id='.$reqproc->FK_request).'&is_edit=true'));
       // $this->redirect(array("/request/index"));
    	return \Yii::$app->getResponse()->redirect(Url::to('?r=request/create&id='.$reqproc->FK_request).'&is_edit=true', 302);
    	}
    	
    
    
    public function getAccess($model){
        $var_access=0;

        if ($model->FK_user_owner == \yii::$app->user->identity->id){
            $var_access = 1;
        } else {
            $responsible = TaskResponsible::findOne(['FK_task'=>$model->FK_task]);
            if ($responsible['FK_user'] == \yii::$app->user->identity->id){
            $var_access = 3;
        }
        }
        return $var_access;
    }     
}
