<?php

namespace backend\controllers;

use Yii;
use common\models\Organisation;
use common\models\PayAccount;
use common\models\Contract;
use common\models\OrgActivityField;
use backend\models\OrganisationSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\models\UsExt;


/**
 * OrganisationController implements the CRUD actions for Organisation model.
 */
class OrganisationController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Lists all Organisation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrganisationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Organisation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Organisation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($us_ext=null)
    {
    	//return print_r($_FILES, true);
    	//return Json::encode(['orgId' => 1000]);
        $model = new Organisation();

        $post = Yii::$app->request->post();

        if( $model->load( $post ) && $model->save() )
        {
        	if ($model->FK_org_type==null){
        	$model->FK_org_type=1;
        	$model->update();
        	return Json::encode(['orgId' => $model->id]);
        	}
            // 
            if (is_null($us_ext)){
            	ServiceController::actionConsoleLog("us empty");
            $usext = UsExt::findOne($model->FK_us_ext);
            $usext->FK_org = $model->id;
            $usext->update();}
            else
            {
            	$usext = UsExt::findOne($us_ext);
            	$usext->FK_org = $model->id;
            	$usext->update();
            }
            
            
            
            
        	$model->refresh();
        	if( isset( $_POST['attach-contract'] ) )
        		$this->loadContracts( $model->id );
        	if( \Yii::$app->request->isAjax )
        		return Json::encode(['orgId' => $model->id]);
        	else 
            	return $this->redirect(['view', 'id' => $model->id]);
        } 
        else 
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    private function loadContracts( $id )
    {
    	if( isset( $_FILES['Contract'] ) )
    	{
    		$allowed_filetypes = [ '.doc','.docx', '.pdf', '.odt' ];
    		$file_name = $_FILES['Contract']['name']['file_path'];
    		$destinationPath = Yii::$app->basePath . '\\files\\documents\\contracts\\';
    		$ext = substr($file_name, strpos($file_name,'.'), strlen($file_name)-1);
    		
    		//---- Валидация и перемещение файла в постоянную директорию
    		if( !in_array( $ext, $allowed_filetypes ) )
    		{
    			return 'Некорректный формат файла';
    		}
    		if ( ! move_uploaded_file($_FILES["Contract"]["tmp_name"]["file_path"],
    				$destinationPath.$file_name ) )
    		{
    			return "Не удалось загрузить файл";
    		}
    		
    		$model = new Contract();
    		$model->load($_POST);
    		
    		$model->name = isset($model->name)?$model->name:$file_name; 
    		$model->number = isset($model->number)?$model->number:date('ymdhis');
    		
    		$model->FK_organisation = $id;
    		$model->file_path = $destinationPath.$file_name;
    		
    		$model->save();
    		
    		return "";
    	}
    	return 'Error';
    }

    /**
     * Updates an existing Organisation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Organisation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete( $id )
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionCreateActivity()
    {
        $model = new OrgActivityField();

        if( $model->load( Yii::$app->request->post() ) && $model->save() )
        {
            $model->refresh();
            $activityList = ArrayHelper::map( OrgActivityField::find()->all(), 'id', 'name' );
            $activityList['selected'] = $model->id;
            return json_encode( $activityList );
        }

        return 'error';
    }

    /**
     * Finds the Organisation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Organisation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Organisation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}
