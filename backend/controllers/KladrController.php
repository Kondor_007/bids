<?php

namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

use common\models\Kladr;
use common\models\Socrbase;
use common\models\Address;

/*
 * Контроллер работает с моделью КЛАДРа и сокращений
*/

class KladrController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }


    /*
     * Метод генерирует html код выпадающего списка из массива $array.
     *
     * $array - массив из которого формируются элементы выпадающего списка
     * $selectedID - ID элемента который должен быть помечен как выбранный.
     * $preSocr - определяет позицию сокращения ( true - перед или false - после имени элемента )
     */
    public function generateHtmlSelect( $array, $selectedID = 0, $preSocr = false )
    {
    	$select = '<option value = \'0\'>-- Выберите из списка --</option>';

    	foreach ($array as $key => $value) 
    	{
            if( $preSocr )
            {
                $select.= "<option value = '".$value->code."'>"
                       .$value->socr.".".$value->name."</option>";    
            }
            else
            {
        		$select.= "<option value = '".$value->code."'>"
        			   .$value->name." ".$value->socr."</option>";
            }
    	}
    	return $select;
    }


    /*
    * Возвращает html выпадающего списка регионов.
    */
    public function actionGetRegions()
    {
    	$array =  Kladr::find()
    			->where("code LIKE '%00000000000'")
    			->orderBy('name')
    			->all();
    	return $this->generateHtmlSelect( $array, 0 );
    }

    /*
    * Возвращает html выпадающего списка районов.
    *
    * $code - код региона внутри которого выбираются районы.
    */
    public function actionGetDistricts( $code )
    {
    	if( $code != 0 )
    	{
	    	$code = substr($code,0,2);
	    	$array =  Kladr::find()
	    			->where("code LIKE '".$code."%00000000' OR code LIKE '"
	    					.$code."000%00000' AND code NOT LIKE '"
	    					.$code."00000000000' ")
	    			->orderBy('name')
	    			->all();
	    	$htmlSelect = $this->generateHtmlSelect( $array, 0 );
    	}
    	else
    	{
    		$htmlSelect = '<option value = \'0\'>-- Выберите из списка --</option>';
    	}
    	return $htmlSelect;
    }
    
    /*
    * Возвращает html выпадающего списка населенных пунктов.
    *
    * $code - код района внутри которого выбираются населенные пункты.
    */
    public function actionGetTowns( $code )
    {
    	if( $code != 0 )
    	{
	    	$code = substr( $code, 0, 8 );
	    	$array =  Kladr::find()
	    			->where("code LIKE '".$code
	    					."%' AND code NOT LIKE '"
	    					.$code."00000'")
	    			->orderBy('name')
	    			->all();
	    	$htmlSelect = $this->generateHtmlSelect( $array, 0, true );
    	}
    	else
    	{
    		$htmlSelect = '<option value = \'0\'>-- Выберите из списка --</option>';
    	}
    	return $htmlSelect;
    }


    /*
    * Возвращает html выпадающего списка сокращений местностей.
    */
    public function actionGetStreetTypes()
    {
    	$array = Socrbase::find()
    				->where(['level' => '5'])
    				->orderBy('name')
    				->all();
    	
    	$select = '<option value = \'0\'>Выберите</option>';

    	foreach ($array as $key => $value) 
    	{
    		$select.= "<option value = '".$value->code."'>"
    			   .$value->scname."</option>";
    	}
    	return $select;
    }

    /*
    * Заполнение полей и сохранение модели адреса.
    */
    public function actionSaveAddress()
    {
        $post = Yii::$app->request->post();
        $addressModel = new Address();
        $kladrModel = Kladr::find()->where( ['code' => $post['code'] ] )->one();
        $socrModel = Socrbase::find()->where( ['code' => $post['street-type'] ] )->one();
        
        if( empty( $post['street-type'] ) == false )
        {
            $addressModel->FK_socr  = $socrModel->id;
        }

        $addressModel->FK_kladr = $kladrModel->id;
        $addressModel->post_index = empty( $post['post-index'] )?$kladrModel->indexAnyway:$post['post-index'];
        $addressModel->street = empty( $post['street'] )?null:$post['street'];
        $addressModel->building = $post['building'];
        $addressModel->room = empty( $post['room'] )?null:$post['room'];
        $addressModel->other_place = $post['other-place'] == 'undefined'?null:$post['other-place'];

        $addressModel->full_address = ( empty( $addressModel->post_index )?'':$addressModel->post_index.', ').$post['full-address'];

        if( $addressModel->save() )
        {
            $addressModel->refresh();
            return '<option value=\''.$addressModel->id.'\' selected>'.$addressModel->full_address.'</option>';
        }
        else
        {
            $errors = '<option value> Ошибка добавления адреса в базу данных. </option>';//Yii::$app->response->statusCode = Yii::BadRequestHttpException;
            foreach ( $addressModel->getErrors() as $key => $err) 
            {
                $errors .= '<option value>'.$err[0].'</option>';
            }
            return $errors;
        }
    }

    public function getAddressSelect( $id )
    {
        $addressList = ArrayHelper::map( Address::find()->all(), 'id', 'fullAddress' );

        $select = '<option value = 0>-- Выбрать адрес --</option>';

        foreach ($addressList as $key => $address) 
        {
            $select .=  '<option value='.$key
                        .( $key == $id?' selected':'').'>'
                        .$address.'</option>';
        }

        return $select;
    }
}
