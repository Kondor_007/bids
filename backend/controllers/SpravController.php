<?php


namespace backend\controllers;

use common\classes\DepDropFormatter;
use common\models\ProcesstypeMachine;
use common\models\Request;
use common\models;
use common\models\RequestDraft;
use yii\base\Exception;
use yii\bootstrap\Alert;
use yii\data\ArrayDataProvider;
use common\models\RequestProcess;
use common\models\Machine;
use yii\helpers\ArrayHelper;
use common\models\MachineType;
use yii\helpers\Url;
use common\models\Material;
use common\models\ProcessType;
use yii\helpers\Json;
use common\models\QualityProcess;


class SpravController extends \yii\web\Controller
{

    public function actionSearchMaterial(){

    }
    public function actionIndex($razdel = 'materials', $id = null, $name = "default", $isedit = "")
    {

       // $this->actionAlert("Creating");
        try {
            print_r($_REQUEST);

          //  $this->actionAlert(implode("", $_REQUEST['SearchMaterial']));
        }
        catch(Exception $err){}
        // добавление/сохранение
        if (\Yii::$app->request->post()) {
           // $this->actionAlert("Creating");
            $post = \Yii::$app->request->post();
            //print_r( "!!!!");
            // материалы
            /*print_r($post);
            exit();*/
            if (isset($post['Material'])) {
              /*  print_r($post['Material']);
                exit();*/
                //$this->actionAlert("From ActionIndex " . $razdel . " " . $id . " " . $name);

                /***
                 * Если переименовывание будет не нужно, то переписать строку
                 * $nameToSearch = $name;
                 * на
                 * $nameToSearch = $post['Material']['name'];
                 * Либо заменить все использования $nameToSearch на $name;
                 * первое предпочтительнее если решим вернуть возможность в работу.
                 */
            
                $nameToSearch = $name;
                $name = $post['Material']['name'];
               
                // $this->actionAlert("From ActionIndex " . $razdel . " " . $id . " " . $name);
                //$this->actionAlert("Need Edit ".$isedit);
                //Флаг режима редактирования, сумматор НЕ НУЖНО включать если включено редактирование!

                $oldSellPrice = 0;
                $newSellPrice = $post['Material']['sell_price'];
                //Количества
                $oldCount = 0;
                $newCount = $post['Material']['count'];
                //$this->actionAlert($newCount);
                /***
                 * Если поиск по имени зафейлится, то запись надо добавить. Колво можно и просуммировать - по дефолту оно 0.
                 * Если нет (model != null), то в найденную модель надо досуммировать количество и сменить цену.
                 * В параметре $name - старое имя, при редактировании по нему и стоит делать поиск, т.к. иначе
                 * (если делать по новому имени, пришедшему в post-запросе) мы не сможем переименовывать материалы.
                 * А нам же надо и кол-ва добавлять.
                 * Если поиск по старому имени найдет запись, то мы обновим ее параметры.
                 * Иначе создастся новая с новыми значениями.
                 * SUMMARY:
                 * SearchModel(name);
                 * if (model == null):
                 *  { new model;}
                 *   PriceUpdate(model, post_data);
                 *   CountUpdate(model, post_data);
                 *
                 ***/

                if ($post['Material']['name']) {
                    $model = Material::find()->where(['name' => $nameToSearch, 'is_del' => 0])->one();// Material::findOne($name);
                    if (is_null($model)) {
                        //$this->actionAlert("Creating");
                        //Не нашли, создаем
                        $model = new Material;
                        $oldSellPrice = $model->sell_price;
                    } else {
                        //Нашли, обновляем
                       // $this->actionAlert("Updating");
                        $oldCount = $model->count;
                    }


                } else {
                    $model = new Material;
                }
                $oldSellPrice = $model->sell_price;


                if ($model->load(\Yii::$app->request->post())) {
                    $model->sell_price = max([$newSellPrice, $oldSellPrice]);
                    if ($isedit == "")
                        $model->count = $oldCount + $newCount;
                    // $this->actionAlert("W/O Saving on test!");
                   /* ServiceController::actionAlert(
                    		\Yii::$app->user->id .
                    		$_SERVER['REMOTE_ADDR'].
                    		"change info in materials".
                    		implode(" | ",$_REQUEST['Material'])
                    		);*/
                    LogController::actionLogAdd(
                        \Yii::$app->user->id,$_SERVER['REMOTE_ADDR'],"change info in Materials", 
                    		implode(" | ",$_REQUEST['Material'])
                    );

                    //Write to database
                    //if (!
                    $model->FK_group = $post['Material']['FK_group'];
                   /* print_r($model);
                    exit();*/
                    $savflag = $model->save(false);
                    return $this->redirect(['index', 'razdel' => 'materials',]);
                   // ServiceController::actionConsoleLog("!!!!!");
                   //if (!$savflag )
                   //	ServiceController::actionAlert("!!!");
                   //else
                  
                  /*  )
                        $this->actionAlert("some error on save material");*/



                }
            }

            
            // оборудование
            if (isset($post['Machine'])) {
            	//ServiceController::actionAlert(implode(" ", $post['Machine']));
                if ($post['Machine']['id']) {
                    $model = Machine::findOne($post['Machine']['id']);
                } else {
                    $model = new Machine;
                }
                if ($model->load(\Yii::$app->request->post())) {
                   $model->save();
                    LogController::actionLogAdd(
                    		\Yii::$app->user->id,$_SERVER['REMOTE_ADDR'],"change info in Machine",
                    		implode(" | ",$_REQUEST['Machine'])
                    		);
                }

            }

            // Типы работы
            if (isset($post['ProcessType'])) {
                if ($post['ProcessType']['id']) {
                    $model = ProcessType::findOne($post['ProcessType']['id']);

                } else {
                    $model = new ProcessType;
                }
                if ($model->load(\Yii::$app->request->post())) {
                    $model->save();
                    LogController::actionLogAdd(
                    		\Yii::$app->user->id,$_SERVER['REMOTE_ADDR'],"change info in Machines", implode(" | ", $post['ProcessType'])
                    		);
                }

            }

             if (isset($post['ProcessType'])){

                 if ($post['ProcessType']['id']){
                     $model = ProcessType::findOne($post['ProcessType']['id']);
                     //$modelPTM = ProcesstypeMachine::findOne($post['ProcessTypeMachine']['id']);
                 } else {
                     $model = new ProcesstypeMachine();
                 }
                // $this->actionAlert("from RPController2...");
                 if ($model->load(\Yii::$app->request->post())){
                   //  $this->actionAlert("saving");
                     $model->save();
                     LogController::actionLogAdd(
                         \Yii::$app->user->id,$_SERVER['REMOTE_ADDR'],"change info in ProcessType", implode(" | ", $post['ProcessType'])
                     );
                 }

             }
            // качество обработки
            if (isset($post['QualityProcess'])) {
                if ($post['QualityProcess']['id']) {
                    $model = QualityProcess::findOne($post['QualityProcess']['id']);
                } else {
                    $model = new QualityProcess;
                }
                if ($model->load(\Yii::$app->request->post())) {
                    $model->save();
                    LogController::actionLogAdd(
                    		\Yii::$app->user->id,$_SERVER['REMOTE_ADDR'],"change info in QualityProcess",
                    		implode(" | ",$_REQUEST['QualityProcess'])
                    		);

                }

            }


        }

        switch ($razdel) {
            case 'materials':
                $Providers['materials'] = new ArrayDataProvider(['allModels' => Material::find()->where(["is_del" => '0'])->orderBy('id desc')->all(),]);
                if ($id == null) {
                    $Models['materials'] = new  Material;
                } else {
                    $Models['materials'] = Material::findOne($id);
                }
                break;
            case
            'machines':
                $Providers['machines'] = new ArrayDataProvider([
                    'allModels' => Machine::find()->where(["is_del" => '0'])->orderBy('id desc')->all(),
                ]);
                if ($id == null) {
                    $Models['machines'] = new  Machine;
                } else {
                    $Models['machines'] = Machine::findOne($id);
                }
                break;
            case 'prtype':
               // $this->actionAlert("from RPController...");

                $Providers['prtype'] = new ArrayDataProvider([
                    'allModels' => ProcessType::find()->where(["is_del" => '0'])->orderBy('id desc')->all(),
                ]);
                if ($id == null) {
                    $Models['prtype'] = new  ProcessType;

                    $Models['prtypemachine'] = new ProcesstypeMachine();
                } else {
                    $Models['prtype'] = ProcessType::findOne($id);
                }
                break;
            case 'qprocess':
                $Providers['qprocess'] = new ArrayDataProvider([
                    'allModels' => QualityProcess::find()->where(["is_del" => '0'])->orderBy('id desc')->all(),
                ]);
                if ($id == null) {
                    $Models['qprocess'] = new  QualityProcess;
                } else {
                    $Models['qprocess'] = QualityProcess::findOne($id);
                }
                break;


        }


        return $this->render('index',
            [
                'razdel' => $razdel,
                'providers' => $Providers,
                'models' => $Models
            ]
        );
    }


    public function actionTest($id)
    {
        return $this->actionIndex('materials', $id);
    }

    public function actionDeleteMaterial($id)
    {
        $model = Material::findOne($id);
        $model->is_del = 1;
        $model->save(false);
        return $this->actionIndex('materials');
    }

    //Будем передавать дополнительно имя, чтоб было можно обновить цену. id пока оставим в параметрах.
    public function actionEditMaterial($id, $name = "", $isedit = "")
    {
       /*  if ($isedit)
             $this->actionAlert($name);*/

        if ($isedit !== "")
            return $this->redirect(['index', 'razdel' => 'materials', 'id' => $id, 'name' => $name,
                'isedit' => $isedit]);
        else
            //Обычное добавление товара
            return $this->redirect(['index', 'razdel' => 'materials', 'id' => $id, 'name' => $name]);
    }


    //Отладочное сообщение содержимое переменной
    public static function  actionAlert($content = "")
    {
        echo '<script> alert("' . $content . '");</script>';
    }


    public function actionDeleteMachine($id)
    {
        $model = Machine::findOne($id);
        $model->is_del = 1;
        $model->save();
        return $this->actionIndex('machines');
    }

    public function actionEditMachine($id)
    {
        return $this->redirect(['index', 'razdel' => 'machines', 'id' => $id]);
    }



    public function actionGetMaterialName(){
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $name = $parents[0];
                //"CONCAT(name, manufacturer) LIKE '%".$name."%'"
                $out =  ArrayHelper::map(Material::find()->andFilterWhere(
                    //['like','name','%ан%'],
                    ['like','manufacturer','%e%']
                )
               // where("(CONCAT(name, manufacturer) LIKE '%"."ан"."%')")
                    ->all(),'id','name'); // self::actionGetListMaterials($type);
               // $out[] = ['hhh',$type];
                echo Json::encode(['output' => DepDropFormatter::format($out), 'selected' => '']);
                //return $this->redirect;
                $id = Material::find()->where("name='".$name."'")->one()->id;
                return $this->redirect(['index', 'razdel' => 'materials', 'id' => $id, 'name' => $name,
                    'needgrid'=>''
                ]);
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
    public function actionDeletePrtype($id)
    {
        $model = ProcessType::findOne($id);
        $model->is_del = 1;
        $model->save();
        return $this->actionIndex('prtype');
    }

    public function actionEditPrtype($id)
    {

        return $this->redirect(['index', 'razdel' => 'prtype', 'id' => $id]);
    }

    public function actionDeleteQprocess($id)
    {
        $model = QualityProcess::findOne($id);
        $model->is_del = 1;
        $model->save();
        return $this->actionIndex('qprocess');
    }

    public function actionEditQprocess($id)
    {
        return $this->redirect(['index', 'razdel' => 'qprocess', 'id' => $id]);
    }

}
