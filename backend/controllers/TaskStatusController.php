<?php 

namespace backend\controllers;

use Yii;
use common\models\TaskStatus;
use common\models\Task;
use common\models\UsExt;

use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;


class TaskStatusController extends \yii\web\Controller
{
	/*
    * Устанавливает статус задачи в "Выполняется"
    */
    public function actionStart()
    {
        $post = Yii::$app->request->post();
        return $this->setStatus( $post, TaskStatus::TASK_STARTED, 'Выполняется' );
    }

    /*
    * Устанавливает статус задачи в "Завершена" 
    * если текущий пользователь является постановщиком
    */
    public function actionFinish()
    { 
        $post = Yii::$app->request->post();
        $taskModel = Task::findOne( $post['id'] );

        if( $taskModel->isOriginator() )
            return $this->setStatus( $post, TaskStatus::TASK_FINISHED, 'Завершена' );
        else
            return 'Вы не обладаете правом завершить данную задачу.';
    }

    /*
    * Устанавливает статус задачи 
    * "Ожидает контроля постановщика"
    */
    public function actionInspect()
    {   
        $post = Yii::$app->request->post();
        return  $this->setStatus( $post, TaskStatus::TASK_WAIT_CHECK, 'Ожидает контроль постановщика' );
    }

    public function actionPause()
    {
        $post = Yii::$app->request->post();
        return  $this->setStatus( $post, TaskStatus::TASK_PAUSED, 'Приостановлена' );
    }

    public function actionStop()
    {
        $post = \Yii::$app->request->post();
        return $this->setStatus( $post, TaskStatus::TASK_FROZEN, 'Отложена' );
    }

    /*
    * Устанавливает статус задачи.
    */
    private function setStatus( $post, $statusType, $taskDesc )
    {   
        //--- Проверяем передан ли параметр id записи
        if( isset( $post['id'] ) == false )
        {
            return 'В запросе отстутствует ID';
        }

        $taskModel = Task::findOne( $post['id'] );

        if( ( !$taskModel->isOriginator() ) && ( !$taskModel->isResponsible() ) )
        {
            return 'Вы не являетесь ни постановщиком, ни исполнителем задачи,'
                  .' поэтому не имеете права менять ее состояние';
        }
        // Сбрасываем предыдущее состояние задачи
        if( $this->discardPreviousStatus( $post['id'] ) == false )
        {
            throw new NotFoundHttpException('Не удалось обновить предыдущий статус.');
            return 'Не удалось обновить предыдущий статус';
        }   

        // Создаем новую модель статуса
        $statusModel = new TaskStatus();
        // и заполняем ее значениями
        $statusModel->FK_task = $post['id'];
        $statusModel->FK_user = ( new UsExt() )->getCurrentUserId();
        $statusModel->FK_status = $statusType;
        $statusModel->description = $taskDesc;
        // Сохраняем в таблицу
        if( $statusModel->save() == false )
        {
            return 'Не удалось сохранить новый статус';
        }

        return $this->renderPartial( 'action-field', ['model' => $taskModel, 'key' => $post['id'] ] );
    }

    /*
    * Сбрасывает поле is_top в предыдущих статусах задачи
    * id которой передается в качестве параметра.
    */
    public function discardPreviousStatus( $id )
    {
        return TaskStatus::updateAll(['is_top' => 0 ], [ 'FK_task' => $id, 'is_top' => 1 ]);
    }

    public function getViewPath()
    {
        return Yii::getAlias('@app').'/views/task';
    }

}

?>