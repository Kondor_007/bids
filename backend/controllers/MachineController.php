<?php

namespace backend\controllers;

use common\models\Request;
use common\models\RequestSearch;
use common\models\RequestDraft;
use yii\data\ArrayDataProvider;
use common\models\Machine;
use yii\helpers\ArrayHelper;
use common\models\MachineType;
use yii\helpers\Json;
use common\classes\DepDropFormatter;
use yii\helpers\Url;

class MachineController extends \yii\web\Controller
{
	public function actionCreateMachineType(){
		$post=\Yii::$app->request->post();
	     /*print_r( $post);
	     exit();*/
	    
	    if (isset($post['MachineType']['name'])){
	    //	print_r( $post);
	    	
	    $model = new MachineType();
	    $model->name = $post['MachineType']['name'];
	    if (isset($post['MachineType']['alias']))
	    	$model->alias = $post['MachineType']['alias'];
	    else
	    	$model->alias = '';
	     $saved="not saved;";
	     
	     $success=$model->save();
	     if ($success)
	     	$saved="saved";
	     else
	     	$saved="not saved";
	     LogController::actionLogAdd(\Yii::$app->user->id, $_SERVER['REMOTE_ADDR'], 'add in machine_type, data is '.$saved, 'name='.$model->name.'; alias='.$model->alias);

	    //print_r( $model);
	   // exit();
	  // TODO:write
	  // $SC = new SpravController();
	    // $SC->actionIndex('machines');
	    
	     return \Yii::$app->getResponse()->redirect('index.php?r=sprav/index&razdel=machines',302);
	  //return  json_encode(['statusM' => 1, 'error' => $model->getErrors(),404]);
	    }
	  // return  json_encode(['statusM' => 0, 'error' => $model->getErrors(),302  ]);
	    return \Yii::$app->getResponse()->redirect('index.php?r=sprav/index&razdel=machines',302);
		//$name = Yii::$app->request[]
	}
    public function actionGetMachines()
    {
    	$machines = [];
    	if( !empty( $_POST['depdrop_parents'] ) ) {
    		$machine_type = $_POST['depdrop_parents'][0];
    	
    		$out = ArrayHelper::map( MachineType::findOne( $machine_type )->machines, 'id', 'name' );
    		echo Json::encode(['output' => DepDropFormatter::format( $out ), 'selected' => '' ]);
    		return;
    	}
    	 
    	if( isset($_POST['id']) ) {
    		$model = MachineType::findOne( $_POST['id'] );
    	
    		if( isset( $model ) )
    			$machines = ArrayHelper::map( $model->machines, 'id', 'name' );
    	}
    	
    	return json_encode([ 'status' => 1, 'stores' => $machines ]);
    }
    

}
