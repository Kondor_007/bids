<?php

namespace backend\controllers;

use common\models\Request;
use common\models\RequestSearch;
use common\models\RequestDraft;
use yii\data\ArrayDataProvider;
use common\models\Machine;
use yii\helpers\ArrayHelper;
use common\models\MachineType;
use yii\helpers\Json;
use common\classes\DepDropFormatter;
use common\models\Material;

class QualityController extends \yii\web\Controller
{
    public function actionGetQualities()
    {
    	$material = [];
    	if( !empty( $_POST['depdrop_parents'] ) ) {
    		$material = $_POST['depdrop_parents'][0];
    	
    		$out = ArrayHelper::map( Material::findOne( $material )->qualities, 'id', 'name' );
    		echo Json::encode(['output' => DepDropFormatter::format( $out ), 'selected' => '' ]);
    		return;
    	}
    	 
    	if( isset($_POST['id']) ) {
    		$model = Material::findOne( $_POST['id'] ); 
    	
    		if( isset( $model ) )
    			$material = ArrayHelper::map( $model->qualities, 'id', 'name' );
    	}
    	
    	return json_encode([ 'status' => 1, 'qualities' => $machines ]);
    }

    public function actionGetDrafts($id=null)
    {
    	if( isset($_POST['id']) ) {
    	   $model = new RequestDraft();
    	   if( isset( $model ) )
    			$dratfList = ArrayHelper::map( $model->find()->where('FK_request = '.$id), 'id', 'name' );
    	}
    	
    	return json_encode([ 'status' => 1, 'dratfList' => $dratfList ]);
    }



	public function actionGetResponsibles()
	{
		//$out = [];
		//ServiceController::actionAlert("DRAFT");
		if (isset($_POST['depdrop_parents'])) {
			$parents = $_POST['depdrop_parents'];
			if ($parents != null) {
				$type = $parents[0];
				$out = self::actionGetListMaterials($type);
				echo Json::encode(['output' => DepDropFormatter::format($out), 'selected' => '']);
				return;
			}
		}
		echo Json::encode(['output'=>'', 'selected'=>'']);
	}

	public function actionGetMaterial(){
		if (isset($_POST['depdrop_parents'])) {
			$parents = $_POST['depdrop_parents'];
			if ($parents != null) {
				$type = $parents[0];
				$out = self::actionGetListMaterials($type);
				echo Json::encode(['output' => DepDropFormatter::format($out), 'selected' => '']);
				return;
			}
		}
		echo Json::encode(['output'=>'', 'selected'=>'']);
	}
	public function actionGetListMaterials($type=null){

		$out =[];
		/*if ($type==1){
			$org = User::find()->where( [ 'id' => \Yii::$app->user->identity->id ])->one()->profile->FK_org;
			$out = ArrayHelper::map(User::find()->joinWith(['profile'])
				->where("us_ext.FK_org = $org")
				//->andWhere("us_ext.FK_users <> ".\Yii::$app->user->identity->id)
				->all(),'id','fullName');

		} else {
			$out = ArrayHelper::map(AuthItem::find()
				->where("type = 1 and name like 'w_%'")
				//->andWhere("us_ext.FK_users <> ".\Yii::$app->user->identity->id)
				->all(),'name','description');
		}
*/
		$out = ArrayHelper::map(AuthItem::findAll(),'id','name');
		return $out;

	}

}
