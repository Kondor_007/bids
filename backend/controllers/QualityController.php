<?php

namespace backend\controllers;

use common\models\Request;
use common\models\RequestSearch;
use common\models\RequestDraft;
use yii\data\ArrayDataProvider;
use common\models\Machine;
use yii\helpers\ArrayHelper;
use common\models\MachineType;
use yii\helpers\Json;
use common\classes\DepDropFormatter;
use common\models\Material;

class QualityController extends \yii\web\Controller
{
    public function actionGetQualities()
    {
    	$material = [];
    	if( !empty( $_POST['depdrop_parents'] ) ) {
    		$material = $_POST['depdrop_parents'][0];
    	
    		$out = ArrayHelper::map( Material::findOne( $material )->qualities, 'id', 'name' );
    		echo Json::encode(['output' => DepDropFormatter::format( $out ), 'selected' => '' ]);
    		return;
    	}
    	 
    	if( isset($_POST['id']) ) {
    		$model = Material::findOne( $_POST['id'] ); 
    	
    		if( isset( $model ) )
    			$material = ArrayHelper::map( $model->qualities, 'id', 'name' );
    	}
    	
    	return json_encode([ 'status' => 1, 'qualities' => $machines ]);
    }
    

}
