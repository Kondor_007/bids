<?php

namespace backend\controllers;

use common\models\Manufacturers;

class ManufacturersController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreateManufacturer(){
        $post=\Yii::$app->request->post();

        if (isset($post['Manufacturers']['name'])){
            //	print_r( $post);

            $model = new Manufacturers();
            $model->name = $post['Manufacturers']['name'];

            $saved="not saved;";

            $success=$model->save();
            if ($success)
                $saved="saved";
            else
                $saved="not saved";
            LogController::actionLogAdd(\Yii::$app->user->id, $_SERVER['REMOTE_ADDR'], 'add in manufacturers, data is '.$saved,
                'name='.$model->name);

            return \Yii::$app->getResponse()->redirect('index.php?r=sprav/index&razdel=materials',302);
            //return  json_encode(['statusM' => 1, 'error' => $model->getErrors(),404]);
        }
        return \Yii::$app->getResponse()->redirect('index.php?r=sprav/index&razdel=materials',302);

    }
    public function actionDelete($id){
        $model=Manufacturers::findOne($id);
        if (!is_null($model))
        $model->delete();
       // exit();
        //$this->actionIndex();
        return \Yii::$app->getResponse()->redirect('index.php?r=manufacturers');
    }
    public function actionCreate(){
        $post=\Yii::$app->request->post();

        if (isset($post['Manufacturers']['name'])){
            //	print_r( $post);

            $model = new Manufacturers();
            $model->name = $post['Manufacturers']['name'];

            $saved="not saved;";

            $success=$model->save();
            if ($success)
                $saved="saved";
            else
                $saved="not saved";
            LogController::actionLogAdd(\Yii::$app->user->id, $_SERVER['REMOTE_ADDR'], 'add in manufacturers, data is '.$saved,
                'name='.$model->name);


        }
        $this->actionIndex();

    }
}
