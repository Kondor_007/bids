<?php

namespace backend\controllers;

use common\models\Task;
use common\models\User;
use common\models\UsExt;
use Faker\Provider\DateTime;
use kartik\helpers\Html;
use Yii;
use common\models\Calls;
use common\models\SearchCalls;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\BaseUrl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


 class BehaviorCodes {

    static $BehDefault='default',
        $BehCreateTask='create_task';
}
/**
 * CallsController implements the CRUD actions for Calls model.
 */
class CallsController extends Controller
{
    public $behavior_code;
    public $call_description="";
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }




    /**
     * Lists all Calls models.
     * @return mixed
     */
    public function actionIndex($userid=null)
    {

       /* print_r(Yii::$app->request->queryParams);
        exit();*/

        $this->actionSetBehavior();
//        $this->call_description="";
        $searchModel = new SearchCalls();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        /*switch ($behavior_code){
            case BehaviorCodes::$BehDefault:


                break;
            case BehaviorCodes::$BehCreateTask:

                break;
            default:
                echo " Unknown Behavior code ".$behavior_code;
                exit();
                break;
        }*/


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'behavior' => $this->behavior_code,
            'description'=>$this->call_description,
        ]);

    }


    public function actionSetBehavior($id_call=null)
    {

        $userid=UsExt::getCurrentUserId();
       // echo $userid;
        /*
         * Проверим что делать пользователю.
         * default = покажем табилцу звонков и ожидающие на линии.
         * Если у него нет текущего разговора, то  default
         *      Иначе Если есть сохраненная задача по текущему звонку то default
         *              Иначе экшн создания задачи-пустышки (позднее - показ формы создания задачи), сохранение задачи.
         * */
        //Флаги
        $have_in_call=false; //Болтает в текущий момент
        $have_task_for_curren_call = true; //Для текущей беседы уже поставлена задача

        $have_in_call = (Calls::find()->where("ISNULL(dt_talk) AND  ISNULL(dt_fin)")->count()) != 0;
        $callmodel=$this->actionGetFirstIncall();
        $id_call = (is_null($callmodel)) ? null :$callmodel->id;
        $this->call_description="";
        if (!is_null($callmodel)) {
            $calluserp = UsExt::find()->where(['phone' =>$callmodel->src])->one();

        $this->call_description =
            (is_null($calluserp)) ? ""  : $calluserp->fullName;
           //(!is_null($this->actionGetFirstIncall()->src)) ? $this->actionGetFirstIncall()->src  : "---";
            }
        if (is_null($id_call))
            $have_task_for_curren_call = BehaviorCodes::$BehDefault;
        else
            $have_task_for_curren_call = (Task::find()->where('FK_cid='.$id_call)->count()) == 1;
        $status = BehaviorCodes::$BehDefault;
        //Трубка НЕ поднята (звонит) или Разговаривает И есть задача
        if ((!$have_in_call  ) or
            ($have_in_call and $have_task_for_curren_call))
            $status = BehaviorCodes::$BehDefault;
        //Трубка поднята и нет задачи
        if (($have_in_call and !$have_task_for_curren_call))
            $status = BehaviorCodes::$BehCreateTask;

        $this->behavior_code = $status;
       // return $status;
        //exit();
    }
    //-----------------------------Обработка звонков.--------------------------------------------------------
    //Количество входящих звонков, сидящих на линии без учета целевого номера,
    //т.к. канал только что создан и целевого номера в общем случае нет
    public function actionGetCountIn(){
        //$this->actionSetBehavior();
       /* print_r(count( Calls::find()->where('dt_talk=0000-00-00 00:00:00')->asArray));
        exit();*/
        /*
         * Звонок нужно засчитать как входящий, если дата/время начала И конца беседы установлены в NULL
         * Т.е. звонок гудит, и трубку еще не бросили
         * */
        $count = 0;
        $count= Calls::find()->where(
            " ( ISNULL(dt_talk)) AND ISNULL(dt_fin)
        #AND        (dst=:phone)
        AND (info.id NOT IN (SELECT task.FK_cid FROM task)) ",
            [
                ':phone' =>UsExt::getCurrentUserPhone(),
                //':rgroup' =>UsExt::getCurrentUserRgroup(),
            ])->count();

        return json_encode(['count' => $count, 'status'=>1]);
    }

    /**
     * @return string
     * Получение первого непринятого и еще активного звонка
     * Чтобы дать задачу
     * Потом бум его как-то обрабатывать
     */
    public function actionGetFirstIncall(){
        //SELECT * FROM calls WHERE calls.dt_talk='0000-00-00 00:00:00' LIMIT 1
        //Добавим проверку принятия звонка с текущим id в задачи
       /* echo UsExt::getCurrentUserPhone();
        exit();*/
        /*echo Calls::find()->where(" (NOT ISNULL(dt_talk)) AND ISNULL(dt_fin)
        AND
        (dst=:phone)
        AND (info.id NOT IN (SELECT task.FK_cid FROM task)) ",
            [
                ':phone' =>UsExt::getCurrentUserPhone(),
                //':rgroup' =>UsExt::getCurrentUserRgroup(),
            ])->count();
        exit();*/
        return  Calls::find()->where(" ( ISNULL(dt_talk)) AND ISNULL(dt_fin)
        AND (info.id NOT IN (SELECT task.FK_cid FROM task)) ")->one();
        // NOT IN (SELECT task.FK_cid FROM task)
    }

    public function actionGetStringViewInCall(){
        $descr_first_in = "Звонит неизвестный ";
        //Получили звонок
        $first_in = $this->actionGetFirstIncall();
       // return json_encode(['rtext' => "", 'status'=>1]);
        $descr_first_in=$first_in->src;
        if (!is_null($first_in)) {
             $usinf = UsExt::find()->where("phone=".$first_in->src)->one();
            $descr_first_in = (!is_null($usinf)) ? $usinf->getFullName().$first_in->FK_linkedid :$descr_first_in ;
        }
        else
            $descr_first_in = "Никто не звонит";
        return json_encode(['rtext' => $descr_first_in, 'status'=>1]);
    }

    public function actionAll(){

        $mphone=UsExt::getCurrentUserPhone();
        $searchModel = new SearchCalls();
        return $this->render('allcalls',[
            'dataProvider' => (new SearchCalls())->searchByPhone($mphone),
        'searchModel'=>$searchModel]);
    }
    /*
     * Когда проходит событие ANSWER появляется dst в таблице info.
     * Отберем такую запись (видимо как последнюю с dst==Номер_Текущего_Польз. И NOT NULL( dt_talk)
    */
    //Отсюда по идее можно и создание заявки вызывать
    public function actionAccept($dt_talk=null, $dt_fin=null,$notRedir=false){
        //Заявку можно создавать, если ее еще не создавали.
        //Иначе пока поговорят, тикетов накопится куча вместо 1.
       /* print_r(BaseUrl::home());
        exit();*/
        $model = $this->actionGetFirstIncall();

        //Если есть разговор без задачи
        if (!is_null($model)) {
           // LogController::actionFastAdd("Есть входящий  для номера ". UsExt::getCurrentUserPhone());
            $model->dst = UsExt::getCurrentUserPhone();
            /* if (!is_null($dt_talk))
             $model->dt_talk = $dt_talk;

             if (!is_null($dt_fin))
                 $model->dt_fin = $dt_fin;*/
            // "0000-00-00 00:00:00";// DateTime::dateTime();

           // print_r($model);
           //exit();
            /* echo DateTime::dateTime()->format('Y-m-d H:i:s');
             echo date('Y-m-d H:i:s');
              DateTime::timezone( 'Europe/Moscow');
             echo DateTime::timezone();*/
            // exit();

            //Проверка существования задачи
            /*
             * Проверка ведется путем поиска задач, у которых нет кода входящего звонка.
             * Т.к. код уникален (id записи таблицы звонков 2 уровня), то и задача идентифицируется однозначно.
             *
             * */
            $task = new Task();
            $task->load(Yii::$app->request->post());

            $task->description = "Заявка поступила на номер " . $model->dst;
            $task->name = "Принята заявка с номера " . $model->src . " код записи в таблице info " . $model->id;
            $task->FK_cid = $model->id;
            $task->FK_originator = UsExt::getCurrentUserId();
            $task->FK_responsible = UsExt::getCurrentUserId();
           // $task->fk_info = $model->id;
          /*  print_r($task);
            exit();*/
           // print_r($task);

            if ($model->save(false))
                echo $task->save(false);

        }
        else {
            echo "запись уже есть";
            //sleep(3);
            //exit();
           // LogController::actionFastAdd("Нет входящих звонков для номера " . UsExt::getCurrentUserPhone());
        }
        /*exit();
        echo "____________________________________________\n";
        print_r($model);*/
        /*$searchModel = new SearchCalls();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);*/
        if (!$notRedir)
        return $this->redirect('index.php?r=calls');
        //$this->actionIndex();
       /* exit();
        $model->save();
        $task->save();*/

    }

    //----------------------------------------------------------------------------------------------------------

    public function actionGetGrid()
    {
       // return "AAA";
    }
    /**
     * Displays a single Calls model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Calls model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Calls();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Calls model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Calls model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Calls model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Calls the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Calls::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //source - адрес источника, id - код идентификации плеера
    public static function actionSimplePlayerOut($source, $id){
        //$source="/var/spool/asterisk/monitor/sample.wav";
        $outHtml = '
        <audio controls id="SimplePlayer'.$id.'" class="myAudio" title="'.$source.'">

        <source src=/../../'.$source.' type="audio/wav">
            Тег audio не поддерживается вашим браузером.
        <a href=""'.$source.'">Скачайте файл</a>.
         </audio>
        ';
        return $outHtml;
    }
}
