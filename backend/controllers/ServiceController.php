<?php
namespace backend\controllers;

class ServiceController extends \yii\web\Controller
{
    public static function FillArray($array, $tag)
    {
        $target = ['----Выберите----'];
        foreach ($array as $miter) {
            $target[] = $miter[$tag];
        };
        return $target;
    }

    public static function  actionAlert($content = "")
    {
        echo '<script> alert("' . $content . '");</script>';
    }

    public static function  actionConsoleLog($content = "")
    {
        echo '<script> console.log("' . $content . '");</script>';
    }

}

?>