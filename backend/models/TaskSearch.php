<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\components\DataProviders\TreeDataProvider;
use common\models\Task;

/**
 * BankSearch represents the model behind the search form about `common\models\Bank`.
 */
class TaskSearch extends Task
{
    /**
     * @inheritdoc
     */
    public $FK_responsible;
    
    public function rules()
    {
        return [
            [['id', 'FK_originator', 'FK_responsible'], 'integer'],
            [['name', 'date'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
    	return [
    		'originatorName' => 'Постановщик',
    	];
    }
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search( $params = '', $or = false )
    {
        $query = Task::find();
/*
        echo '<pre>';
        print_r($query);
        echo '</pre>';
        exit;
*/
        $dataProvider = new TreeDataProvider([
            'query' => $query,
        ]);

        if( $params !== '' )
        {
            $this->load($params);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if( $or && ( isset( $this->FK_responsible ) && isset( $this->FK_originator ) ) )
        {
            $query->joinWith( 'taskResponsible' );//->where(['tr.FK_user'=> 4]);
            $query->where([ 'FK_user' => $this->FK_responsible ]);               
            $query->orWhere(['FK_originator' => $this->FK_originator ]);
        }
        else
        {
            if( isset($this->FK_responsible) )
            {            
                $query->joinWith( 'taskResponsible' );//->where(['tr.FK_user'=> 4]);
                $query->andFilterWhere([ 'FK_user' => $this->FK_responsible ]);
            }

            $query->andFilterWhere([
                'id' => $this->id,
                'FK_originator' => $this->FK_originator,
                'active' => $this->active,
                //'parent_id' => '0',
            ]);

            $query->andFilterWhere(['like', 'name', $this->name]);
                //->andFilterWhere(['like', 'BIK', $this->BIK])
                //->andFilterWhere(['like', 'cor_account', $this->cor_account])
                //->andFilterWhere(['like', 'address', $this->address])
                //->andFilterWhere(['like', 'description', $this->description]);
        }
        $query->andFilterWhere(['active' => 1 ]);

        return $dataProvider;
    }
}
?>