<?php

namespace backend\models\rbac;

use Yii;
use yii\helpers\ArrayHelper;
use backend\models\rbac\AuthItemChild;

/**
 * This is the model class for table "auth_item".
 *
 * @property string $name
 * @property integer $type
 * @property string $description
 * @property string $rule_name
 * @property string $data
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AuthAssignment[] $authAssignments
 * @property AuthRule $ruleName
 * @property AuthItemChild[] $authItemChildren
 */
class AuthItem extends \yii\db\ActiveRecord
{
    const TYPE_ROLE = 1;
    const TYPE_PERMISSION = 2;

    /**
     * @inheritdoc
     */
    public $childrenList;

    public static function tableName()
    {
        return 'auth_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['description', 'data'], 'string'],
            [['name', 'rule_name'], 'string', 'max' => 64],
            [['childrenList'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Идентификатр роли',
            'type' => 'Type',
            'childrenString' => 'Наследуемые разрешения', 
            'childrenList' => 'Наследуемые разрешения', 
            'description' => 'Описание роли',
            'rule_name' => 'Rule Name',
            'data' => 'Data',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['item_name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuleName()
    {
        return $this->hasOne( AuthRule::className(), ['name' => 'rule_name'] );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItemChildren()
    {
        return $this->hasMany( AuthItemChild::className(), ['parent' => 'name'] );
    }

    //-- Возвращает строку со списком наследуемых 
    //   разрешений и ролей через запятую -------
    public function getChildrenString()
    {
        $string = '';

        foreach( $this->authItemChildren as $childName )
        {
            $string .= ' '.$childName->child.',';
        }
        return $string = trim( $string, ',' ) ;
    }

    //-- Возвращает массив пар ключ -> значение 
    //   наследуемых разрешений и ролей -------
    public function getChildrenArray()
    {
        $items = $this->authItemChildren;
        $array = array();

        foreach ( $items as $value ) 
        {
            $array[] = $value->child;
        }

        return $array;
    }

    //-- Возвращает массив с полным списком разрешений и ролей
    public function getItemsArray()
    {
        $items = AuthItem::find()->all();
        
        foreach ( $items as $value ) 
        {
            if ( $this->name != $value->name )
            {
                $array[] = $value->name;
            }
            
        }
        return $array;
    }

    //-- Возвращает массив с полным списком разрешений и ролей
    public function getItemNamesArray()
    {
        $items = AuthItem::find()->all();
        
        foreach ( $items as $value ) 
        {
            if ( $this->name != $value->name )
            {
                $array[] = $value->description;
            }
            
        }
        return $array;
    }

    // Возвращает ключи полного массива разрешений
    // которые пересекаются с массивом наследуемых 
    // для отметки в чекбоксах
    public function getCheckedChildren()
    {
        $array = array_intersect( $this->itemsArray , $this->childrenArray );

        return array_keys( $array ); 
    }

}
